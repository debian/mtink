mtink (1.0.16-16) unstable; urgency=medium

  * Team upload
  * Stop installing gimp plugin (Closes: #1088321)

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 05 Dec 2024 18:03:37 -0500

mtink (1.0.16-15) unstable; urgency=medium

  * new maintainer (Closes: #557878)
    (move package to debian-printing)
  * debian/control: bump standard to 4.6.2 (no changes)
  * debian/po: Romanian debconf templates translation (Closes: #1032805)

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 15 Nov 2023 23:38:53 +0100

mtink (1.0.16-14) unstable; urgency=medium

  * QA upload.
  * debian/rules: Pass relative prefix to Configure avoid embedding the
    full build path in the binary. (Closes: #1010462)
  * Add Portuguese manpage translations. (Closes: #965957)
  * debian/control: Update to Standards-Version 4.6.0.

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 10 May 2022 13:38:53 -0700

mtink (1.0.16-13) unstable; urgency=medium

  * QA upload.
  * Source-only upload.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sat, 25 Sep 2021 16:23:13 -0300

mtink (1.0.16-12) unstable; urgency=low

  * QA upload.

  [ Debian Janitor ]
  * Fix day-of-week for changelog entries 0.9.11-1, 0.9-1.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 20 Aug 2021 13:46:10 +0100

mtink (1.0.16-11) unstable; urgency=medium

  * QA upload
  * Update Vcs-* URIs for move to salsa.debian.org
  * Switch to debhelper 13
  * Install documentation in /usr/share/doc/mtink (policy §12.3)
  * Patch Makefile to not require root for installing binaries
  * Set Rules-Requires-Root: no
  * Drop dh-exec, not needed with debhelper 13
  * Drop unused dh-buildinfo
  * Fix FTBFS with GCC 10 (Closes: #957569)
  * Do not link with -Wl,--as-needed, it is now the default
  * Mark mtink-doc Multi-Arch: foreign
  * Drop mtink.menu, see #741573
  * Use web.archive.org for upstream Homepage and Source,
    drop debian/watch
  * Drop trailing whitespace in debian/changelog and debian/rules
  * Fix some more spelling errors reported by Lintian
  * Bump Standards-Version to 4.5.0, no changes

 -- Graham Inggs <ginggs@debian.org>  Sun, 05 Jul 2020 17:05:48 +0000

mtink (1.0.16-10) unstable; urgency=medium

  * QA upload.
  * Add translators copyright to debian/copyright.
  * Fix various (small) issues in debian/po/de.po
  * Add German translation of the manual page. (Closes: #871411)
  * Comment out link to previous SCM (alioth).
  * Remove versioned build depends, even o-o-stable fulfills them.
  * Switch priority from extra to optional.
  * Added agreement from Morten Brix Pedersen <morten@wtf.dk> regarding the
    license of his debconf work.
  * Added agreement from Sylvain Le Gall <sylvain.le-gall@polytechnique.org>
    regarding the license of his translation work.
  * Added agreement from Daniel Nylander <daniel@danielnylander.se>
    regarding the license of his translation work and correct header in po
    file.
  * Update French translation of the manual page, thanks to Jean-Pierre
    Giraud. (Closes: #918043)

 -- Helge Kreutzmann <debian@helgefjell.de>  Sun, 06 Jan 2019 10:45:56 +0100

mtink (1.0.16-9) unstable; urgency=medium

  * QA upload.
  * Non-void functions should return a value (Closes: #742063).
    (Fixes FTBFS with clang instead of gcc - thanks Nicolas Sévelin-Radiguet)
  * Fix conflicting and implicit function declarations (Closes: #749433).
    (Reported by goto-cc from the cbmc package - thanks Michael Tautschnig)
  * Do not include build date and time in version strings.
  * Machine-readable debian/copyright file.
  * Switch to dh(1), debhelper 9 and dh-exec, drop d/control.in.
  * Expose compiler flags in the build logs, enable PIE.
  * Bump Standards-Version to 3.9.6 (no further changes).

 -- Graham Inggs <graham@nerve.org.za>  Sun, 12 Jul 2015 13:45:45 -0400

mtink (1.0.16-8) unstable; urgency=medium

  * QA upload.
  * Really build with hardening flags.
  * Override Lintian spelling error warnings in non-English text.
  * Add keywords entries to d/*.desktop.
  * Update d/control.in and regenerate d/control:
    - Add VCS fields for new collab-maint repository
    - Rewrite package descriptions
    - Remove trailing spaces
  * Update d/watch - thanks Bart Martens.

 -- Graham Inggs <graham@nerve.org.za>  Sat, 30 Nov 2013 11:22:01 +0200

mtink (1.0.16-7) unstable; urgency=low

  * QA upload.
  * Build-depend on libmotif-dev instead of lesstif2-dev (Closes: #714664).
  * Remove duplicate build-depend on debhelper.
  * Fix mtinkc crash when run with an empty environment (Closes: #716125).
  * Fix mtink crash when run with an empty environment (Closes: #716543).
  * Compile detect/askPrinter.c with hardening flags.
  * Fix various spelling errors reported by Lintian.
  * Use 'set -e' in mtink.postinst.
  * Bump Standards-Version to 3.9.4 (no changes).

 -- Graham Inggs <graham@nerve.org.za>  Wed, 31 Jul 2013 11:12:34 +0200

mtink (1.0.16-6) unstable; urgency=low

  * QA upload.
  * Regenerate debian/control properly from control.in.
  * patches/lesstif-multiarch: Remove a bit more Configure cleverness
    trying too hard to detect Motif, finding it in the wrong m-a paths.
  * Fix FTBFS from recent dpkg-dev: Ensure that docs are built from
    the build-arch and build-indep targets, not just the build target.
  * Remove README.source, which just talked about dpatch.

 -- Peter Samuelson <peter@p12n.org>  Thu, 26 Jul 2012 08:57:46 -0500

mtink (1.0.16-5) unstable; urgency=low

  * QA upload.
  * patches/lesstif-multiarch: Fix FTBFS due to lesstif2 multiarch.
    - Also explicitly remove Xp detection (bug #623650).  This was needed
      because, now that we can detect libraries in multiarch paths, the
      libxp dependency would have returned from the dead.
  * Add debconf translation:
    - pt_BR from J.S. Júnior (Closes: #660706)
  * Policy 3.9.3 (no changes).

 -- Peter Samuelson <peter@p12n.org>  Wed, 25 Jul 2012 18:17:04 -0500

mtink (1.0.16-4) unstable; urgency=low

  * QA upload.
  * Convert from dpatch to format 3.0 (quilt). Drop dpatch from build-depends.
  * Drop old unapplied patches (cflags, keep-pristine-source).
  * Bump debhelper compat version to 8. Update build-depends accordingly.
  * Bump Standards-Version to 3.9.2.
  * Get rid of duplicated Section in debian/control.
  * Remove obsolete Enconding field from .desktop files.
  * Rebuild seems to remove libxp dependency. Closes: #623650.

 -- Regis Boudin <regis@debian.org>  Tue, 07 Feb 2012 00:45:48 +0100

mtink (1.0.16-3) unstable; urgency=low

  * QA upload.
  * Fix pending l10n issues. Debconf translations:
    - Spanish (Camaleón).  Closes: #583541

 -- Christian Perrier <bubulle@debian.org>  Wed, 28 Jul 2010 09:27:40 +0200

mtink (1.0.16-2) unstable; urgency=low

  * Orphaning the package
  * Acknowledge NMU, thanks Stefano ! (Closes: #554185, #534040)
  * Add japanese po-debconf (Closes: #512975)
  * Add russion po-debconf (Closes: #543188)
  * Apply font change proposed by upstream (Closes: #504317)
  * Fix spell errors in the french translation (Closes: #508519)
  * Upgrade Standards-Version to 3.8.3:
    * Add README.source
    * Remove useless dh_desktop invocation
    * Change doc-base section to System/Monitoring
    * Don't create /var/run/mtink, it is created when mtinkd start
  * Add ${misc:Depends} for mtink-doc package
  * Update debian/copyright

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 24 Nov 2009 23:16:02 +0100

mtink (1.0.16-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control: add missing build-dep on po-debconf (Closes: #554185)
  * debian/mk/docbook-manpage.mk: fix shell test error ('==' operator)
  * debian/rules: remove po threshold to avoid lacking manpages that are
    assummed to be available, fix FTBFS. (Closes: #534040)

 -- Stefano Zacchiroli <zack@debian.org>  Wed, 04 Nov 2009 10:02:20 +0100

mtink (1.0.16-1) unstable; urgency=low

  * New upstream release
  * Upgrade debian/watch version to 3,
  * Add debian/mtink.desktop debian/mtinkc.desktop,
  * Use mtink.install.in to manage gimp plugin installation,
  * Rewrite and relicense manpage debian/xml-man/,
  * Use po4a to translate manpage, reorganize manpage source
    following the po4a convention,
  * Remove po-debconf Build-Depends, since it is a dependency of
    debhelper 5
  * Remove old, unused and partial italian and deutsch translation,
    maybe using .po format will help translation team,
  * Add docbook-manpage.mk and po4a.mk in debian/mk for translation
    and generation of manpage,
  * Change email address to gildor@debian.org everywhere
  * Use DTD from docbook-xml package and not from docbook package
  * Add portuguese translation for debconf messages, thanks to Rui
    Branco (Closes: #414912)
  * Add dutch po-debconf translation, thanks to cobaco (Closes: #420934)
  * Use the new upstream configure option --no-strip to stop stripping
    binary (Closes: #437592)

 -- Sylvain Le Gall <gildor@debian.org>  Tue, 28 Aug 2007 23:20:47 +0200

mtink (1.0.14-2) unstable; urgency=high

  * Forget to set urgency=high.

 -- Sylvain Le Gall <gildor@debian.org>  Sat,  7 Oct 2006 19:50:48 +0200

mtink (1.0.14-1) unstable; urgency=low

  * New upstream release
  * Remove checkenv patch, included in upstream,
  * Minor improvement:
    * Update debian/rules FSF,
    * Better layout in control.in,
    * Upgrade debhelper to standard 5,

 -- Sylvain Le Gall <gildor@debian.org>  Fri,  6 Oct 2006 02:38:09 +0200

mtink (1.0.13c-1) unstable; urgency=high

  * New upstream release,
  * Patch cflags has been applied upstream,
  * Patch keep-pristine-source has been partially applied:
    * call distclean to clean the package,
  * Uncapitalize the variable to prevent adding the shlib dependency of
    gimp-mtink to the mtink package.
  * Move target variable definition after the inclusion of Makefile part,
  * Urgency set to high, because it fix an undicover bug in removing
    XAUTHORITY variable.

 -- Sylvain Le Gall <gildor@debian.org>  Tue,  3 Oct 2006 01:13:00 +0200

mtink (1.0.13a-1) unstable; urgency=low

  * New upstream version
  * Change my email address to gildor@debian.org,
  * Upgrade Standards-Version to 3.7.2 (no change),
  * Remove useless file in debian/,
  * Add db_stop where debconf is used,
  * Add po-debconf as build dependency,
  * Remove Eduard Bloch as uploaders (i am now the main
    uploader), thanks to Eduard for his contribution,
  * Use CDBS.

 -- Sylvain Le Gall <gildor@debian.org>  Sun,  1 Oct 2006 18:14:38 +0200

mtink (1.0.12-3) unstable; urgency=low

  * Release 1.0.12 solved CVE-2005-4604. At the time of first upload the CVE
    number was not assigned (it was assigned on 2006/01/01).

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Wed, 11 Jan 2006 22:56:46 +0100

mtink (1.0.12-2) unstable; urgency=low

  * Stop depending on xlib-dev, depends on libx11-dev, libxpm-dev,
    libxt-dev. (Closes: #345538)

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Tue,  3 Jan 2006 08:40:58 +0100

mtink (1.0.12-1) unstable; urgency=low

  * New upstream release

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Sat, 31 Dec 2005 15:46:55 +0100

mtink (1.0.11-2) unstable; urgency=low

  * Add swedish translation (thanks to Daniel Nylander)

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Sat,  3 Dec 2005 00:03:16 +0100

mtink (1.0.11-1) unstable; urgency=low

  * New upstream release
  * Replace debconf by ${misc:Dependency} from the dependency field of the
    control file, it is automatically filled by dh_installdebconf
  * Update italian debconf translation, thanks to Luca Bruno (Closes: #325623)
  * Remove patches gcc-4, typo_french, mtink-cups since they are applied
    upstream
  * Update FSF address in copyright
  * Correct dates in changelog (Jui = Jul)

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Tue, 27 Sep 2005 23:44:37 +0200

mtink (1.0.9-1) unstable; urgency=low

  * New upstream release
  * Create a patch (gcc4.dpatch) to allow building with gcc-4.0
    (Closes: #287679)
  * Talk with upstream author:
    * Bug #269127: "This may appear very occasionnally. Never see this bug with
      the latest version. This can mostly be ignored." (Closes:#269127)
    * Bug #278080: "The user has attached the printer to the // port, the 2.6
      kernel has a wrong implementation for the read() call which can't be
      interrupted.  This is fixed for the 1.0.9 release." (Closes: #278080)
    * Bug #300254 (udev support): "This is not done yet. I have to integrate
      this, but I need some time in order to do this."
  * Generate mtink-doc in the target binary-indep (Closes: #299558)
  * Include translation for debconf:
    * Vietnamese, thanks to Clytie Siddall (Closes: #312976)
    * German correction, thanks to Jens Seidel (Closes: #313945)
    * Czech, thanks to Miroslav Kure (Closes: #302878)
    * Correct some errors in the debconf, thanks to Clytie Siddall
      (Closes: #312975)
  * Call debconf-updatepo in the clean target
  * Redirect stderr to /dev/null in the cups backend (Closes: #293186)
  * Correct the priority of the package (set to extra)
  * Upgrade policy to 3.6.2.0 (no change)

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Sun, 14 Aug 2005 20:23:47 +0200

mtink (1.0.5-1) unstable; urgency=high

  * New upstream version
  * Fix insecure file permissions (CAN-2004-1110) as describe here
    http://bugs.gentoo.org/show_bug.cgi?id=70310 (CAN-2004-1110). Set
    urgency high, because it is a security bug ( Closes: #284121 )
  * Change the GFDL tp GPL of the different manual pages ( since the different
    translator have let my name as the upstream authors of the documentation,
    i just have permit myself to change the licence from GFDL to GPL )
  * Correct a spelling in the README.Debian, add my email address in it
  * Update the mtink.init copyright section

 -- Sylvain Le Gall <sylvain.le-gall@polytechnique.org>  Sat,  7 Aug 2004 20:12:59 +0200

mtink (1.0.1-2) unstable; urgency=low

  * Correct the plugin location to comply with gimp2.0 ( Closes: #247419 )
  * Move from contrib to main ( hope this will pass ! )

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Wed,  5 May 2004 23:48:34 +0200

mtink (1.0.1-1) unstable; urgency=low

  * New upstream version
  * Fix dependency on libgimp1.3 to libgimp2.0 ( Closes: #242645 )
  * Added da.po for Danish translation ( Closes:  #241043 )
  * Add an explanation in README.Debian, to explain that CONFIG_PRINTER_READBACK
    is CONFIG_PARPORT_1284 on 2.4 kernels ( closes: #229631, #210472 )
  * Correct some typo and send the patch to upstream ( closes: #230266 )
  * Remove any depends on motif... I will try to depend only on main-packages,
    those permitting to migrate mtink from contrib to main, the day when
    contrib and non-free will disappear ( i should do it right now, but i am
    lazy... i will wait for the END of my NM process )
  * Build against docbook-xsl version >> 1.6.4. Correct the fact that the new
    layout for xsl files is lying in /usr/share/xml and no more in
    /usr/share/sgml
  * Fix the change of gimp1.3 pkg-config script name ( gimptool-1.3 ->
    gimptool-2.0 )
  * Don't use patch use_lesstif, it has been applied upstream

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Thu,  8 Apr 2004 23:14:56 +0200

mtink (1.0.0-1) unstable; urgency=low

  * New upstream version
  * Update policy compliance to 3.6.1
  * Fix po/POTFILES.in and de.po to correct debconf-updatepo problems
  * Fixed in previous release: removed dependency on libgimp* generated
    by dh_shlibdeps (closes: #201910)

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Mon,  5 Jan 2004 12:58:24 +0100

mtink (0.9.57-1) unstable; urgency=low

  * New upstream version
  * Use debian/compat
  * Addition of more documentation, especially one that state the need
    of kernel compiled with CONFIG_PRINTER_READBACK ( closes: #210472 )
  * Add dependency on mtink-doc for mtink ( in order to have the preceding
    documentation at hand ).

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Mon, 27 Oct 2003 21:52:28 +0100

mtink (0.9.56-1) unstable; urgency=low

  * New upstream version

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Thu, 18 Sep 2003 19:27:43 +0200

mtink (0.9.55-4) unstable; urgency=low

  * Correct some bugs with location of doc/mtink in mtink-doc package
  * Remove dependency on libgimp* generated by dh_shlibdeps (closes: #201910)
  * Go to gimp1.3, but there is a problem about gimptool ( submit a bug )
  * Use dpatch for correcting different bugs of mtink configure script
  * Add a directory debian/patches to store different patches
  * Write faster than i can can't go to gimp1.3, it doesn't build, warn the
    upstream and send him some patches...

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Fri, 18 Jul 2003 19:27:23 +0200

mtink (0.9.55-3) unstable; urgency=low

  * Split documentation of mtink to mtink-doc, (closes: #193959)
  * Move to 3.5.10 policy

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Thu, 12 Jun 2003 21:59:36 +0200

mtink (0.9.55-2) unstable; urgency=low

  * Added dependency over docbook-xml which was missing

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Tue, 22 Apr 2003 11:56:47 +0200

mtink (0.9.55-1) unstable; urgency=low

  * Eduard Bloch:
    - New upstream version
    - Back to .orig.tar.gz
    - debian/control: Added lesstif-dev as (primary) alternative for
      libmotiv-dev. Removed the paranoid Motif checks from Configure.
      And the section -is* already contrib/misc (closes: #155647).
    - debian/control: Moved to the new debconf l10n scheme, added Build-Dep
    - debian/control: Added myself to the Uploaders list
    - debian/templates: Fixed spelling errors in debian/templates, but
       most seem not to be used yet. I hope DDTP will pick up them soon.
    - debian/rules: removed ttink.de.1 and ttink.it.1 manpages from the
      installation list since they looked like bad babelfish output
    - debian/control: improvied synopsis of the package description
    - debian/copyright: set a pointer to the systemwide GPL file
  * Sylvain LE GALL:
    - debian/manpages : added to install the manpages
    - debian/*.sgml -> debian/xml-man/*.xml : move the sgml to xml and new
      directory in order not to mix debian files and manpages
    - debian/xml-man/Makefile : use xsltproc which is more stable than
      docbook-to-man ( sorry but it is far more simplier )
    - debian/control : move priority optional to extra, because we rely on
      extra packages
    - debian/control : move docbook-to-man dependency to xsltproc / docbook-xsl
    - debian/templates.* : remove since we use po-debconf
    - debian/po/french.po : translation completed


 -- Eduard Bloch <blade@debian.org>  Fri,  4 Apr 2003 21:04:07 +0200

mtink (0.9.52-1) unstable; urgency=low

  * New upstream version
  * Move to section contrib, Closes: #155813
  * Change the Configure script in order to need just libgimp1.2-dev,
    Closes: #157070, #158053
  * Apply patch to correct the typo error, Closes: #155813

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Tue,  1 Oct 2002 12:31:38 +0200

mtink (0.9.11-1) unstable; urgency=low

  * New upstream version

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Tue, 30 Jul 2002 22:27:33 +0200

mtink (0.9-1) unstable; urgency=low

  * Fix the doc-base problem
  * Add source dependency

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Tue, 23 Jul 2002 00:15:33 +0200

mtink (0.81-1) unstable; urgency=low

  * First release of the upstream version
  * First package of mine, be indulgent

 -- Sylvain LE GALL <sylvain.le-gall@polytechnique.org>  Mon, 17 Jun 2002 23:20:33 +0200
