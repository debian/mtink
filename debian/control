Source: mtink
Section: misc
Priority: optional
Maintainer: Debian Printing Team <debian-printing@lists.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13),
               docbook-xml,
               docbook-xsl,
               libmotif-dev,
               libx11-dev,
               libxext-dev,
               libxml2-utils,
               libxpm-dev,
               libxt-dev,
               po-debconf,
               po4a,
               xsltproc
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://web.archive.org/web/20180324135050/http://xwtools.automatix.de/
Vcs-Browser: https://salsa.debian.org/debian/mtink
Vcs-Git: https://salsa.debian.org/debian/mtink.git

Package: mtink
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Suggests: mtink-doc
Description: Status monitor tool for Epson inkjet printers
 This is a printer status monitor which enables to get the remaining ink
 quantity, to print test patterns, to reset printer and to clean nozzles.
 .
 Although it mainly targets Epson printers, certain HP and Canon models
 are supported.

Package: mtink-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Suggests: mtink
Depends: ${misc:Depends}
Description: Status monitor tool for Epson inkjet printers - documentation
 This is a printer status monitor which enables to get the remaining ink
 quantity, to print test patterns, to reset printer and to clean nozzles.
 .
 Although it mainly targets Epson printers, certain HP and Canon models
 are supported.
 .
 This package contains the documentation.
