Name: mtink
Summary: View status of and tune Epson printer ink cartridges
#Icon: 
Version: 0.9.54b
Release: 1
Packager: Keith Amidon <camalot@picnicpark.org>
Copyright: GPL
Group: Applications/Publishing
URL: http://xwtools.automatix.de/english/startE.htm
Source0: %{name}-%{version}.tar.gz
#Patch0: mtinkc.patch
BuildRoot: %{_tmppath}/%{name}-root
#BuildRequires: gimp >= 1.0.0

%package gimp-plugin
Summary: Plugin for gimp to access Epson print cartridge maintaince functions
Group: Applications/Publishing
Requires: mtink = %{version}
%description gimp-plugin
Plugin for gimp to access Epson print cartridge maintaince functions

See documentation in mtink package.

%package mtinkd
Summary: Server allowing ink status read while printing on Epson printers
Group: Applications/Publishing
%description mtinkd
Server allowing ink status read while printing on Epson printers

See documentation in the mtink package.

%description 
The Epson utilities allow to display with the Linux kernel 2.2.x the
remaining ink quantity, to perform a nozzle check, or to clean the
head.

# -------------------------    prep     -----------------------------------
%prep
%setup -n mtink-%{version}

# -------------------------    build    -----------------------------------
%build

./Configure -p %{_prefix}
make

# ------------------------    install    -----------------------------------
%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT%{_prefix}/bin
mkdir -p $RPM_BUILD_ROOT%{_prefix}/sbin
cp mtink $RPM_BUILD_ROOT%{_prefix}/bin/
cp mtinkc $RPM_BUILD_ROOT%{_prefix}/bin/
cp ttink $RPM_BUILD_ROOT%{_prefix}/bin/
cp mtinkd $RPM_BUILD_ROOT%{_prefix}/sbin/
mkdir -p $RPM_BUILD_ROOT/etc/rc.d/init.d/
cp etc/mtink $RPM_BUILD_ROOT/etc/rc.d/init.d/
mkdir -p $RPM_BUILD_ROOT/usr/lib/cups/backend/
cp etc/mtink-cups $RPM_BUILD_ROOT/usr/lib/cups/backend/mtink
cp etc/installInitScript.sh $RPM_BUILD_ROOT%{_prefix}/sbin/mtink-installInitScript
cp detect/askPrinter $RPM_BUILD_ROOT%{_prefix}/sbin/askPrinter
if [ -f gimp-mtink ]
then
   GIMPPLUGINDIR=/usr/lib/gimp/1.2
   mkdir -p $RPM_BUILD_ROOT$GIMPPLUGINDIR/plug-ins
   cp gimp-mtink $RPM_BUILD_ROOT$GIMPPLUGINDIR/plug-ins/
   echo $GIMPPLUGINDIR/plug-ins/gimp-mtink > gimp-plugin-files.lst
else
   echo> gimp-plugin-files.lst
fi
mkdir -p $RPM_BUILD_ROOT%{_prefix}/lib/mtink
cp utils/printer.desc.bldin $RPM_BUILD_ROOT%{_prefix}/lib/mtink

# ------------------------     clean     -----------------------------------
%clean
rm -rf $RPM_BUILD_ROOT

# -------------------------    files    ------------------------------------
# remember: %config, %config(missingok), %config(noreplace), %doc, %ghost,
#           %dir, %docdir, %attr(mode,user,group)
%files
%defattr(-, root, root)
%doc html/* CHANGE.LOG 
%attr(2555, root,lp) %{_prefix}/bin/mtink
%attr(2555, root,lp) %{_prefix}/bin/ttink
%{_prefix}/bin/mtinkc
%{_prefix}/lib/mtink/printer.desc.bldin

%files mtinkd
/etc/rc.d/init.d/mtink
%{_prefix}/sbin/mtinkd
%{_prefix}/sbin/askPrinter
%{_prefix}/sbin/mtink-installInitScript
%{_prefix}/lib/cups/backend/mtink

%files gimp-plugin -f gimp-plugin-files.lst



# -------------------------    changelog    --------------------------------
%changelog
* Sun Dec 28 2003  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [1.0.0-1]
  - Version 1.0.0
* Sun Oct 19 2003  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [0.9.57-1]
  - Problems while ink low fixed.
* Sun Jan 12 2003  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [0.9.54-1]
  - Translation of resource into danish (Mogens Kaeger).
* Sat Dec 07 2002  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [0.9.52-1]
  - New version of mtink.
* Sat Oct 19 2002  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [0.9.51-1]
  - Change for rmp 4.1, new mtink version.
* Thu Oct 03 2002  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [0.9.50-1]
  - Add for rpm packages
* Sat Sep 29 2002  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [0.9.48-1]
  - New version of mtink
* Sat Sep 28 2002  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [0.9.47-1]
  - Rules for gimp version modified as proposed by keith, but
    setting of the directory via a script.
* Fri Sep 27 2002  Jean-Jacques Sarton <jj.sarton@t-online.de>
  [0.9.46-1]
  - A few modifications. Call Configure with the option -p /usr
* Thu Sep 26 2002  Keith Amidon  <camalot@picnicpark.org>
  [0.9.44-1]
  - New version ? (maybe old version was really this one, this is
    correct version number now at least, obtained from CHANGE.LOG instead 
    of website.)
  - Install askPrinter executable as part of mtinkd package, as the only
    place it seems to be used is in the mtink init.d script
  - Determined location of gimp-plugin dir using gimp-config
  - Updated build/install dependencies
* Wed Sep 25 2002  Keith Amidon  <camalot@picnicpark.org>
  [0.9.39-1]
  - Original package
