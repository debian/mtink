# This will work for Linux and Solaris

prefix=/usr/local
setUid=1
gimpVersion=

DBG = -s -O

# comment this out for Solaris

LIBXP = -lXp

# xpm header an library, if no entry, xpm will not be used
XPM_INC = -DXPM_XPM
XPM_LIB = -lXpm

# for me, test with Lesstif
#PI=-I/usr/X11R6/LessTif/Motif1.2/include
#PL=-L/usr/X11R6/LessTif/Motif1.2/lib

# You may specify the Motif library version you want to use here
# path for library will be included in the binary, as well as the library version
#MOLIB = -Xlinker -rpath -Xlinker /usr/X11R6/LessTif/Motif1.2/lib/ /usr/X11R6/LessTif/Motif1.2/lib/libXm.so.1

# the libXm.so.1 is under the standard path, we need only to tell wich lib is to be loaded
#MOLIB = /usr/X11R6/lib/libXm.so.1

# this is the standard and will normally work.
# normal value
MOLIB = -lXm
MOINC =

#MACINCL = -I/usr/OpenMotif/include -I/sw/include
#MACLB   = -L/usr/OpenMotif/lib -L/sw/lib

MACOSLDFLG=-framework IOKit -framework CoreFoundation -L/sw/lib -lusb  -lpthread $(MACLB)
MACOSCFLG=-DMACOS=1

MTINK_CFLAGS= $(DBG) -I. -I./$(SRCDIR) $(PI) $(MOINC) -I/usr/X11R6/include -I/usr/dt/include $(MACINCL) -DWAIT_FOR_PRINTER=0 $(XPM_INC) -DPREFIX=\"$(prefix)\" $(MACOSCFLG)
LFLAGS= $(DBG) $(PL) -L/usr/X11R6/lib64  -L/usr/X11R6/lib -L/usr/dt/lib $(MACLIB) $(MOLIB) $(LIBXP) $(XPM_LIB) -lXext -lXt -lX11 $(MACOSLDFLG)

# Modify CC to cc if needed
CC=gcc

# Set rm
RM=/bin/rm

# I must be able to build a static linked version of mtink
# this is not for you
#LFLAGS= $(DBG) -L/usr/X11R6/lib -L/usr/dt/lib /home/jj/MOTIF/motif/lib/Xm/libXm.a -lXp -lXt -lSM -LICE -lXext -lX11

# Adapt this if necesssary
GIMP_INC = -I/usr/include  -I/usr/local/include -I/usr/include/glib-1.2 -I/usr/local/include/glib-1.2 -I/usr/include/glib -I/usr/local/include/glib
GIMP_LIB = -lgimp -rdynamic

############################################################
# The programs we generate

PRG = mtink mtinkc ttink mtinkd $(DETDIR)/askPrinter

SRCDIR = mainSrc
DOCDIR = html
UTILS  = utils
SVRDIR = server
DETDIR = detect
CHODIR = chooser

############################################################
# If you want to get debug informations

DEBUG = -DDEBUG=0 -DPTIME=0
#DEBUG = -DDEBUG=0 -DPTIME0 -DSIMULATION=1
#DEBUG = -DDEBUG=1 -DPTIME=0

############################################################
# time out for reading from // port, value in  msec.

RDTIMEOUT=-DRDTIMEOUT=20000

############################################################
# exit after at least 10 seconds (time in ms)

WRTIMEOUT=-DWRTIMEOUT=2000

############################################################
# Our objekt files

OBJ =    $(SRCDIR)/mtink.o \
         $(SRCDIR)/rw.o \
         $(SRCDIR)/d4lib.o \
         $(SRCDIR)/cfg1.o \
         $(SRCDIR)/rdPrtDesc.o \
         $(SRCDIR)/fsb.o \
         $(SRCDIR)/cfg2.o \
         $(SRCDIR)/alignbt.o \
         $(SRCDIR)/scrtxt.o \
         $(SRCDIR)/resource.o \
         $(SRCDIR)/rcfile.o \
         $(SRCDIR)/cmd.o \
         $(SRCDIR)/tooltip.o \
         $(SRCDIR)/wheelmouse.o \
         $(SRCDIR)/usbHlp.o \
         $(SRCDIR)/micon.o \
         $(SRCDIR)/checkenv.o \
         $(SRCDIR)/access.o

TTYOBJ = $(SRCDIR)/ttink.o \
         $(SRCDIR)/rw.o \
         $(SRCDIR)/d4lib.o \
         $(SRCDIR)/rdPrtDesc.o \
         $(SRCDIR)/nXcmd.o \
         $(SRCDIR)/usbHlp.o \
         $(SRCDIR)/rdRes.o \
         $(SRCDIR)/access.o

SVR_OBJ = $(SVRDIR)/mtinkd.o \
         $(SRCDIR)/rw.o \
         $(SVRDIR)/cmd.o \
         $(SVRDIR)/d4lib.o \
         $(SRCDIR)/usbHlp.o


DOC = *.gif *.html

all: $(PRG) $(UTILS)/bldRes $(UTILS)/rdPrtDesc $(UTILS)/chckRes

clean:
	@if [ "`echo $(SRCDIR)/*.o`" != "$(SRCDIR)"/'*.o'  ]; then $(RM) $(SRCDIR)/*.o; fi; >/dev/null 2>/dev/null
	@if [ -f mtink                       ]; then $(RM) mtink               ; fi
	@if [ -f mtinkc                      ]; then $(RM) mtinkc              ; fi
	@if [ -f ttink                       ]; then $(RM) ttink               ; fi
	@if [ -f gimp-mtink                  ]; then $(RM) gimp-mtink          ; fi
	@if [ -f $(UTILS)/bldRes             ]; then $(RM) $(UTILS)/bldRes     ; fi
	@if [ -f $(UTILS)/rdPrtDesc          ]; then $(RM) $(UTILS)/rdPrtDesc  ; fi
	@if [ -f $(UTILS)/chckRes            ]; then rm $(UTILS)/chckRes    ; fi
	@if [ "`echo $(SVRDIR)/*.o `" != "$(SVRDIR)"/'*.o' ]; then $(RM) $(SVRDIR)/*.o; fi; >/dev/null 2>/dev/null
	@if [ -f mtinkd                      ]; then $(RM) mtinkd              ; fi
	@if [ -f  $(DETDIR)/askPrinter       ]; then $(RM) $(DETDIR)/askPrinter; fi

distclean:
	@make clean 2>/dev/null 1>/dev/null
	@if [ -f Makefile       ]; then $(RM) Makefile;       fi;
	@if [ -f etc/mtink      ]; then $(RM) etc/mtink;      fi;
	@if [ -f mtink.spec     ]; then $(RM) mtink.spec;     fi;
	@if [ -f mtink-all.spec ]; then $(RM) mtink-all.spec; fi;

############## COMMON AND frintends ##################
$(SRCDIR)/d4lib.o: $(SRCDIR)/d4lib.c $(SRCDIR)/d4lib.h Makefile
	@echo Compile d4lib.c
	@$(CC) -c -o $(SRCDIR)/d4lib.o $(SRCDIR)/d4lib.c $(RDTIMEOUT) $(WRTIMEOUT) $(DEBUG) $(KERNEL_USB_BUG) $(MACOSCFLG)

$(SRCDIR)/access.o: $(SRCDIR)/access.c  Makefile
	@echo Compile access.c
	@$(CC) -c -o $(SRCDIR)/access.o $(SRCDIR)/access.c $(DEBUG) $(MACOSCFLG)

$(SRCDIR)/rw.o: $(SRCDIR)/rw.c  Makefile
	@echo Compile rw.c
	@$(CC) -c -o $(SRCDIR)/rw.o $(SRCDIR)/rw.c $(DEBUG) $(MACOSCFLG)

$(SRCDIR)/rcfile.o: $(SRCDIR)/rcfile.c $(SRCDIR)/rcfile.h $(SRCDIR)/mtink.h  Makefile
	@echo Compile rcfile.c
	@$(CC) -c -o $(SRCDIR)/rcfile.o $(SRCDIR)/rcfile.c $(CFLAGS) $(MTINK_CFLAGS)   -DWITH_X=1

$(SRCDIR)/fsb.o: $(SRCDIR)/fsb.c $(SRCDIR)/fsb.h $(SRCDIR)/mtink.h $(SRCDIR)/wheelmouse.h Makefile
	@echo Compile fsb.c
	@$(CC) -c -o $(SRCDIR)/fsb.o $(SRCDIR)/fsb.c $(CFLAGS) $(MTINK_CFLAGS)   -DWITH_X=1

$(SRCDIR)/scrtxt.o: $(SRCDIR)/scrtxt.c $(SRCDIR)/scrtxt.h $(SRCDIR)/mtink.h $(SRCDIR)/wheelmouse.h Makefile
	@echo Compile scrtxt.c
	@$(CC) -c -o $(SRCDIR)/scrtxt.o $(SRCDIR)/scrtxt.c $(CFLAGS) $(MTINK_CFLAGS)   -DWITH_X=1

$(SRCDIR)/cfg1.o: $(SRCDIR)/cfg1.c $(SRCDIR)/cfg1.h $(SRCDIR)/mtink.h $(SRCDIR)/cmd.h $(SRCDIR)/wheelmouse.h $(SRCDIR)/model.h Makefile
	@echo Compile cfg1.c
	@$(CC) -c -o $(SRCDIR)/cfg1.o $(SRCDIR)/cfg1.c $(CFLAGS) $(MTINK_CFLAGS)  -DWITH_X=1

$(SRCDIR)/micon.o: $(SRCDIR)/micon.c $(SRCDIR)/micon.h  Makefile
	@echo Compile micon.c
	@$(CC) -c -o $(SRCDIR)/micon.o $(SRCDIR)/micon.c $(CFLAGS) $(MTINK_CFLAGS)  -DWITH_X=1

$(SRCDIR)/rdPrtDesc.o: $(SRCDIR)/rdPrtDesc.c $(SRCDIR)/model.c $(SRCDIR)/rdPrtDesc.h $(SRCDIR)/model.h Makefile
	@echo Compile rdPrtDesc.c
	@$(CC) -c -o $(SRCDIR)/rdPrtDesc.o $(SRCDIR)/rdPrtDesc.c $(CFLAGS) $(MTINK_CFLAGS) -DBLD_UTIL=0

$(SRCDIR)/cfg2.o: $(SRCDIR)/cfg2.c $(SRCDIR)/cfg2.h $(SRCDIR)/mtink.h $(SRCDIR)/cfg1.h $(SRCDIR)/cmd.h  Makefile
	@echo Compile cfg2.c
	@$(CC) -c -o $(SRCDIR)/cfg2.o $(SRCDIR)/cfg2.c $(CFLAGS) $(MTINK_CFLAGS)  -DWITH_X=1

$(SRCDIR)/alignbt.o: $(SRCDIR)/alignbt.c $(SRCDIR)/alignbt.h $(SRCDIR)/mtink.h $(SRCDIR)/cfg1.h $(SRCDIR)/cmd.h  Makefile
	@echo Compile alignbt.c
	@$(CC) -c -o $(SRCDIR)/alignbt.o $(SRCDIR)/alignbt.c $(CFLAGS) $(MTINK_CFLAGS)  -DWITH_X=1

$(SRCDIR)/checkenv.o: $(SRCDIR)/checkenv.c $(SRCDIR)/checkenv.h $(SRCDIR)/mtink.h $(SRCDIR)/cfg1.h $(SRCDIR)/cmd.h  Makefile
	@echo Compile checkenv.c
	@$(CC) -c -o $(SRCDIR)/checkenv.o $(SRCDIR)/checkenv.c $(CFLAGS) $(MTINK_CFLAGS)

$(SRCDIR)/cmd.o: $(SRCDIR)/cmd.c $(SRCDIR)/d4lib.h $(SRCDIR)/cmd.h $(SRCDIR)/mtink.h $(SRCDIR)/model.h $(SRCDIR)/al820.c  $(SRCDIR)/align.c $(SRCDIR)/alC82.c $(SRCDIR)/bldpcode.c Makefile
	@echo Compile cmd.c
	@$(CC) -c -o $(SRCDIR)/cmd.o $(SRCDIR)/cmd.c $(CFLAGS) $(MTINK_CFLAGS)  $(DEBUG) $(RDTIMEOUT) $(WRTIMEOUT) -DWITH_X=1 -DSERVER=0

$(SRCDIR)/nXcmd.o: $(SRCDIR)/cmd.c $(SRCDIR)/d4lib.h $(SRCDIR)/cmd.h $(SRCDIR)/mtink.h $(SRCDIR)/model.h Makefile $(SRCDIR)/al820.c  $(SRCDIR)/align.c $(SRCDIR)/alC82.c $(SRCDIR)/bldpcode.c
	@echo Compile cmd.c as nXcmd.o
	@$(CC) -c -o $(SRCDIR)/nXcmd.o $(SRCDIR)/cmd.c $(CFLAGS) $(MTINK_CFLAGS)  $(DEBUG) $(RDTIMEOUT) $(WRTIMEOUT) -DWITH_X=0 -DSERVER=0

$(SRCDIR)/resource.o: $(SRCDIR)/resource.c $(SRCDIR)/version.h Makefile
	@echo Compile resource.c
	@$(CC) -c -o $(SRCDIR)/resource.o $(SRCDIR)/resource.c $(CFLAGS) $(MTINK_CFLAGS)  $(DEBUG)  -DWITH_X=1

$(SRCDIR)/mtink.o: $(SRCDIR)/mtink.c $(SRCDIR)/d4lib.h $(SRCDIR)/cmd.h $(SRCDIR)/mtink.h $(SRCDIR)/cfg1.h $(SRCDIR)/model.h $(SRCDIR)/checkenv.h Makefile
	@echo Compile mtink.c
	@$(CC) -c -o $(SRCDIR)/mtink.o $(SRCDIR)/mtink.c $(CFLAGS) $(MTINK_CFLAGS)  $(DEBUG)  -DWITH_X=1

$(SRCDIR)/ttink.o: $(SRCDIR)/ttink.c $(SRCDIR)/d4lib.h $(SRCDIR)/cmd.h $(SRCDIR)/mtink.h  $(SRCDIR)/model.h Makefile
	@echo Compile ttink.c
	@$(CC) -c -o $(SRCDIR)/ttink.o $(SRCDIR)/ttink.c $(CFLAGS) $(MTINK_CFLAGS)  $(DEBUG)  -DWITH_X=0

$(SRCDIR)/tooltip.o: $(SRCDIR)/tooltip.c $(SRCDIR)/tooltip.h Makefile
	@echo Compile tooltip.c
	@$(CC) -c -o $(SRCDIR)/tooltip.o $(SRCDIR)/tooltip.c $(CFLAGS) $(MTINK_CFLAGS)  $(DEBUG)  -DWITH_X=1

$(SRCDIR)/wheelmouse.o: $(SRCDIR)/wheelmouse.c Makefile
	@echo Compile wheelmouse.c
	@$(CC) -c -o $(SRCDIR)/wheelmouse.o $(SRCDIR)/wheelmouse.c $(CFLAGS) $(MTINK_CFLAGS)  $(DEBUG)  -DWITH_X=1

$(SRCDIR)/rdRes.o: $(SRCDIR)/rdRes.c $(SRCDIR)/tres.c Makefile
	@echo Compile rdRes.c
	@-$(CC) -c -o $(SRCDIR)/rdRes.o $(SRCDIR)/rdRes.c

$(SRCDIR)/usbHlp.o: $(SRCDIR)/usbHlp.c Makefile
	@echo Compile usbHlp.c
	@-$(CC) -c -o $(SRCDIR)/usbHlp.o $(SRCDIR)/usbHlp.c $(CFLAGS) $(MTINK_CFLAGS)  $(DEBUG)

mtink:  $(OBJ) $(SRCDIR)/d4lib.h $(SRCDIR)/alignbt.h $(SRCDIR)/scrtxt.h $(SRCDIR)/cfg1.h $(SRCDIR)/cfg2.h $(SRCDIR)/cmd.h $(SRCDIR)/mtink.h $(SRCDIR)/cfg1.h $(SRCDIR)/version.h
	@echo Link mtink
	@$(CC) -o mtink $(OBJ)  $(CFLAGS) $(MTINK_CFLAGS) $(LDFLAGS) $(LFLAGS) $(RDTIMEOUT) $(DEVICE) $(WRTIMEOUT) $(DEBUG) $(KERNEL_USB_BUG) -DWITH_X=1

ttink: $(TTYOBJ) $(SRCDIR)/d4lib.h $(SRCDIR)/cmd.h $(SRCDIR)/mtink.h $(SRCDIR)/model.h $(SRCDIR)/version.h Makefile
	@echo Link ttink
	@$(CC) -o ttink $(TTYOBJ) $(CFLAGS) $(MTINK_CFLAGS) $(RDTIMEOUT) $(DEVICE) $(WRTIMEOUT) $(DEBUG) $(KERNEL_USB_BUG) $(MACOSLDFLG) -DWITH_X=0

############# PLUG-IN #######################
gimp-mtink: $(SRCDIR)/gimp-mtink.c Makefile
	@echo Compile and link gimp-mtink.c
	@-$(CC) -o gimp-mtink $(SRCDIR)/gimp-mtink.c $(GIMP_INC) $(GIMP_LIB)

############# SERVER ########################
$(SVRDIR)/mtinkd.o: $(SVRDIR)/mtinkd.c Makefile
	@echo Compile mtinkd.c
	@$(CC) -c -o $(SVRDIR)/mtinkd.o $(SVRDIR)/mtinkd.c -DSERVER=1 $(CFLAGS) $(MTINK_CFLAGS)

$(SVRDIR)/d4lib.o: $(SRCDIR)/d4lib.c  Makefile
	@echo Compile d4lib.c
	@$(CC) -c -o $(SVRDIR)/d4lib.o $(SRCDIR)/d4lib.c $(RDTIMEOUT) $(WRTIMEOUT) $(DEBUG) $(KERNEL_USB_BUG) $(CFLAGS) $(MTINK_CFLAGS)

$(SVRDIR)/cmd.o: $(SRCDIR)/cmd.c Makefile
	@echo Compile cmd.c
	@$(CC) -c -o $(SVRDIR)/cmd.o $(SRCDIR)/cmd.c -DSERVER=1 $(CFLAGS) $(MTINK_CFLAGS)

mtinkd: $(SVR_OBJ) Makefile
	@echo Link mtinkd
	@$(CC) -o mtinkd $(SVR_OBJ) $(CFLAGS) $(MTINK_CFLAGS) $(MACOSLDFLG) 

########### Chooser ##############

mtinkc: $(CHODIR)/mtinkc.c Makefile
	@echo Compile and link mtinkc.c
	@$(CC) -o mtinkc $(CHODIR)/mtinkc.c $(CFLAGS) $(MTINK_CFLAGS) $(LDFLAGS) $(LFLAGS)

########### printer detection ##############

$(DETDIR)/askPrinter: $(DETDIR)/askPrinter.c
	@echo Compile and link askPrinter.c
	@$(CC) -o $(DETDIR)/askPrinter $(DETDIR)/askPrinter.c -I. -I$(DETDIR)

########### utilities ######################

$(UTILS)/bldRes: $(UTILS)/bldRes.c
	@echo Compile bldRes.c
	@$(CC) -o $(UTILS)/bldRes $(UTILS)/bldRes.c

$(UTILS)/rdPrtDesc: $(SRCDIR)/rdPrtDesc.c
	@echo Compile rdPrtDesc.c
	@$(CC) -o $(UTILS)/rdPrtDesc $(SRCDIR)/rdPrtDesc.c -I$(SRCDIR)

$(UTILS)/chckRes: $(SRCDIR)/rdRes.c
	@echo Compile chckRes
	@$(CC) -o $(UTILS)/chckRes $(SRCDIR)/rdRes.c -I$(SRCDIR) -DRESCHECK=1

install: install-mtink install-ttink install-desc install-plugins install-askPrinter install-mtinkd install-start install-cups install-pyink

install-mtink:
	@#echo install mtink
	@if id | grep '=0(root)' >/dev/null; \
	then \
	   mkdir -p $(prefix)/bin >/dev/null 2>&1; \
	   if [ -d $(prefix)/doc ]; \
	   then  \
	      mkdir -p $(prefix)/doc/mtink >/dev/null 2>&1; \
	      (cd $(DOCDIR); cp $(DOC) $(prefix)/doc/mtink); \
	   else \
	      mkdir -p $(prefix)/doc/mtink >/dev/null 2>&1; \
	      (cd $(DOCDIR); cp $(DOC) $(prefix)/doc/mtink); \
	   fi; \
	   cp mtink $(prefix)/bin; \
	   cp mtinkc $(prefix)/bin; \
	   echo "Mtink installed / installe, installiert."; echo ; \
	   if [ $(setUid) -eq 0 ]; then \
	      echo "Make sure that $(prefix)/bin/mtink has read/write access to the device files."; \
	   else \
	      chmod  4755 $(prefix)/bin/mtink; \
	   fi; \
	   if [ "$(gimpVersion)" != "" ]; then \
	      echo "Do installation of plug-in as normal user."; \
	      echo "L'installation du plug-ins doit etre effectuee avec le compte normal."; \
	      echo "Um das Plug-in zu installieren, muessen Sie als normalen Anwender eingelogt sein."; \
	      echo ; \
	   fi; \
	fi

install-ttink:
	@#echo install ttink
	@if id | grep '=0(root)' >/dev/null; \
	then \
	   mkdir -p $(prefix)/bin >/dev/null 2>&1; \
	   cp ttink $(prefix)/bin; \
	   echo "Ttink installed / installe, installiert."; echo ; \
	   if [ $(setUid) -eq 0 ]; then \
	      echo "Make sure that $(prefix)/bin/ttink has read/write access to the device files."; \
	   else \
	      chmod  4755 $(prefix)/bin/ttink; \
	   fi; \
	fi

install-pyink:
	#@#echo install pyink server
	#@if id | grep '=0(root)' >/dev/null; \
	#then \
	#   cd pyink;\
	#   ./configure -p $(prefix);\
	#   make install;\
	#fi

install-askPrinter:
	@#echo install askPrinter utility
	@if [ `uname` = Linux ];\
	then \
	   if id | grep '=0(root)' >/dev/null; \
	   then \
	      mkdir -p $(prefix)/sbin >/dev/null 2>&1; \
	      cp $(DETDIR)/askPrinter $(prefix)/sbin; \
	      echo "askPrinter installed / installe, installiert."; echo ; \
	      chmod 755 $(prefix)/sbin/askPrinter; \
	   fi;\
	fi

install-mtinkd:
	@#echo install mtink daemon
	@if [ `uname` = Linux ];\
	then \
	   if id | grep '=0(root)' >/dev/null; \
	   then \
	      mkdir -p $(prefix)/sbin >/dev/null 2>&1; \
	      cp mtinkd $(prefix)/sbin; \
	      echo "mtinkd installed / installe, installiert."; echo ; \
	      chmod 755 $(prefix)/sbin/mtinkd; \
	   fi;\
	fi

install-desc:
	@#echo install printer description file
	@if [ `uname` = Linux ];\
	then \
	   if id | grep '=0(root)' >/dev/null; \
	   then \
	      mkdir -p $(prefix)/lib/mtink >/dev/null 2>&1; \
	      cp utils/printer.desc.bldin $(prefix)/lib/mtink/printer.desc; \
	      cp utils/printer.desc.bldin $(prefix)/lib/mtink/printer.desc; \
	      cp utils/*.align $(prefix)/lib/mtink/; \
	   fi;\
	fi

install-plugins: 
	@#echo install plugins
	@if id | grep '=0(root)' >/dev/null; \
	then \
	   PLUGDIR=`./getGimpPluginDir.sh`; \
	   if [ "$$PLUGDIR" != "" ]; \
	   then \
	      if [ -f gimp-mtink ]; \
	      then \
	         cp gimp-mtink $$PLUGDIR/; \
	         echo "Plugins installed, installe, installiert."; \
	      fi; \
	   fi; \
	else \
	   if [ -f gimp-mtink ]; then \
	     if [ -d $(HOME)/.gimp/plug-ins ]; then \
	        cp gimp-mtink $(HOME)/.gimp/plug-ins; \
	        echo "Plugins installed, installe, installiert."; \
	     else \
	        if [ -d $(HOME)/.gimp-$(gimpVersion)/plug-ins ]; then \
	           cp gimp-mtink $(HOME)/.gimp-$(gimpVersion)/plug-ins; \
	           echo "Plugins installed, installe, installiert."; \
	        fi; \
	     fi; \
	     echo; \
	     echo "Installation of mtink only as root."; \
	     echo "L'installation de mtink necessite le compte root."; \
	     echo "Um mtink zu installieren muessen Sie als root angemeldet sein."; \
	     echo Devi essere root per fare questo.; \
	  fi; \
	fi

# On slackware install start script to /etc/rc.d instead of /etc/init.d
install-start:
	@#echo install start part
	@if id | grep '=0(root)' >/dev/null; then \
	  if [ -d /etc/init.d ]; \
	  then \
	     cp etc/installInitScript.sh $(prefix)/sbin/mtink-installInitScript; \
	     cp etc/mtink /etc/init.d/mtink; \
	     chmod +x $(prefix)/sbin/mtink-installInitScript; \
	     chmod +x /etc/init.d/mtink; \
	     echo "$(prefix)/sbin/mtink-installInitScript installed / installe, installiert.";  \
	     echo "/etc/init.d/mtink installed / installe, installiert."; echo ; \
	  else \
	     cp etc/installInitScript.sh $(prefix)/sbin/mtink-installInitScript; \
	     cp etc/mtink /etc/rc.d/mtink; \
	     chmod +x $(prefix)/sbin/mtink-installInitScript; \
	     chmod +x /etc/rc.d/mtink; \
	     echo "$(prefix)/sbin/mtink-installInitScript installed / installe, installiert.";  \
	     echo "/etc/rc.d/mtink installed / installe, installiert."; echo ; \
	  fi; fi


install-cups:
	@#echo install cups part
	@if id | grep '=0(root)' >/dev/null; then \
	 if [ -d /usr/lib/cups/backend/ ]; then \
	   cp etc/mtink-cups /usr/lib/cups/backend/mtink; \
	   chmod +x /usr/lib/cups/backend/mtink; \
	   echo "/usr/lib/cups/backend/mtink installed / installe, installiert."; echo ; \
	fi; fi

uninstall:
	@if id | grep '=0(root)' >/dev/null; \
	then \
	   :; \
	else \
	   echo You must be root for this.; \
	   echo Vous devez etre root pour ceci.; \
	   echo Sie muessen root sein.; \
	   echo Devi essere root per fare questo.; \
	   exit 2; \
	fi
	@-if [ -f /etc/rc.d/mtink ]; then rm /etc/rc.d/mtink; fi
	@-if [ -f /etc/init.d/mtink ]; then rm /etc/init.d/mtink; fi
	@-if [ -f /etc/rc.d/S*mtink ]; then rm /etc/rc.d/*mtink; fi
	@-if [ -f $(prefix)/lib/mtink ]; then rm -fr $(prefix)/lib/mtink; fi
	@-if [ -f $(prefix)/bin/mtink ]; then rm $(prefix)/bin/mtink; fi
	@-if [ -f $(prefix)/bin/mtinkc ]; then rm $(prefix)/bin/mtinkc; fi
	@-if [ -d $(prefix)/mtink ]; then rm -fr $(prefix)/mtink; fi
	@-if [ -d $(prefix)/doc/mtink ]; then rm -fr $(prefix)/doc/mtink; fi
	@-if [ -f $(prefix)/sbin/askPrinter ]; then rm -fr $(prefix)/sbin/askPrinter; fi
	@-if [ -f $(prefix)/sbin/mtinkd ]; then rm -fr $(prefix)/sbin/mtinkd; fi
	@-if [ -f $(prefix)/sbin/mtink-installInitScript ]; then rm -fr $(prefix)/sbin/mtink-installInitScript; fi
	@-if [ -f etc/mtinkd.conf ]; then rm -fr /etc/mtinkd.conf; fi
	@-for userDir in `cut -d':' -f6 /etc/passwd`; do \
	   if [ -f $$userDir/.gimp-$(gimpVersion)/plug-ins/gimp-mtink ]; \
	   then rm $$userDir/.gimp-$(gimpVersion)/plug-ins/gimp-mtink; fi; \
	      if [ -f $$userDir/.gimp/plug-ins/gimp-mtink ]; \
	      then rm $$userDir/.gimp/plug-ins/gimp-mtink; fi; \
	done
	@-PLUGDIR=`./getGimpPluginDir.sh`; \
	if [ "$$PLUGDIR" != "" ]; \
	then \
	   if [ -f $$PLUGDIR/gimp-mtink ]; \
	   then \
	      rm $$PLUGDIR/gimp-mtink; \
	   fi;\
	fi
	@-if [ -f /usr/lib/cups/backend/mtink ]; then rm /usr/lib/cups/backend/mtink; fi
	@-if [ -f $prefix/sbin/mtink-installInitScript ]; then rm $prefix/sbin/mtink-installInitScript; fi
	@-if [ -f $prefix/lib/pyink ]; then rm -fr  $prefix/lib/pyink; fi

dist:
	@make distclean 2>/dev/null 1>/dev/null
	@( NM=`pwd`; NM=`basename $$NM`; cd ..; rm $$NM.tar.gz 2>/dev/null 1>/dev/null; tar cf $$NM.tar $$NM; gzip -9 $$NM.tar )

vdist:
	@make distclean 2>/dev/null 1>/dev/null
	@( \
	   VER=`sed -n 's/.*V \(.*\)"/-\1/p' mainSrc/version.h`; \
	   NM=`pwd`; NMS=$$NM; NM=`basename $$NM | sed 's/-.*//'`; \
	   cd ..; \
	   rm $$NM$$VER.tar.gz 2>/dev/null 1>/dev/null; \
	   if [ -d $$NM$$VER ] ; \
	   then \
	      : ;\
	   else \
	      mkdir $$NM$$VER; \
	      (cd $$NMS; tar cf - .)|(cd $$NM$$VER;tar xf -); \
	   fi; \
	   tar cf $$NM$$VER.tar $$NM$$VER; \
	   gzip -9 $$NM$$VER.tar; \
	)

spec: mainSrc/version.h mtink.spec.ORG mtink-all.spec.ORG
	@VER=`sed -n 's/.*V \(.*\)"/\1/p' mainSrc/version.h`; \
	PLUGDIR=`./getGimpPluginDir.sh`; \
	if [ "$$PLUGDIR" != "" ]; \
	then \
	   sed -e "s@^Version:.*@Version: $$VER@" -e "s@^ *GIMPPLUGINDIR=.*@GIMPPLUGINDIR=$$PLUGDIR@" mtink.spec.ORG > mtink.spec;\
	   sed -e "s/^Version:.*/Version: $$VER/" -e "s@^ *GIMPPLUGINDIR=.*@GIMPPLUGINDIR=$$PLUGDIR@"  mtink-all.spec.ORG > mtink-all.spec;\
	else \
	   sed -e "s@^Version:.*@Version: $$VER@" mtink.spec.ORG > mtink.spec;\
	   sed -e "s/^Version:.*/Version: $$VER/" mtink-all.spec.ORG > mtink-all.spec;\
	fi

rpm-all:
	@( \
	if [ -f mtink-all.spec ]; then rm mtink-all.spec; fi; \
	make spec; \
	make clean; \
	RPMD=`rpm -q --eval=%{_topdir} --specfile mtink.spec | grep -v mtink`; \
	if [ -d "$$RPMD" -a -w "$$RPMD" ];\
	then \
	   VER=`sed -n 's/.*V \(.*\)"/-\1/p' mainSrc/version.h`; \
	   NM=`pwd`; NMS=$$NM; NM=`basename $$NM | sed 's/-.*//'`; \
	   NMS=`basename $$NMS`; \
	   cd ..; \
	   rm $$NM-all$$VER.tar.gz 2>/dev/null 1>/dev/null; \
	   tar cf $$NM-all$$VER.tar $$NMS; gzip -9 $$NM-all$$VER.tar; \
	   cp $$NM-all$$VER.tar.gz $$RPMD/SOURCES/; \
	   if [ -x /usr/bin/rpmbuild ]; \
	   then \
	      rpmbuild -ba $$NMS/mtink-all.spec; \
	   else \
	      rpm -ba $$NMS/mtink-all.spec; \
	   fi;\
	else \
	   echo You must be root for this.; \
	   echo Vous devez etre root pour ceci.; \
	   echo Sie muessen root sein.; \
	   echo Devi essere root per fare questo.; \
	   exit 2; \
	fi; \
	)

rpm:
	@( \
	if [ -f mtink.spec ]; then rm mtink.spec; fi; \
	make spec; \
	make clean; \
	RPMD=`rpm -q --eval=%{_topdir} --specfile mtink.spec | grep -v mtink`; \
	if [ -d "$$RPMD" -a -w "$$RPMD" ];\
	then \
	   VER=`sed -n 's/.*V \(.*\)"/-\1/p' mainSrc/version.h`; \
	   NM=`pwd`; NMS=$$NM; NM=`basename $$NM | sed 's/-.*//'`; \
	   NMS=`basename $$NMS`; \
	   cd ..; \
	   rm $$NM$$VER.tar.gz 2>/dev/null 1>/dev/null; \
	   tar cf $$NM$$VER.tar $$NMS; gzip -9 $$NM$$VER.tar; \
	   cp $$NM$$VER.tar.gz $$RPMD/SOURCES/; \
	   if [ -x /usr/bin/rpmbuild ]; \
	   then \
	      rpmbuild -ba $$NMS/mtink.spec; \
	   else \
	      rpm -ba $$NMS/mtink.spec; \
	   fi; \
	else \
	   echo You must be root for this.; \
	   echo Vous devez etre root pour ceci.; \
	   echo Sie muessen root sein.; \
	   echo Devi essere root per fare questo.; \
	   exit 2; \
	fi;\
	)
