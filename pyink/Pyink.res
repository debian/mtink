By.check: Düsn\nprüfn
By.clean: Düsn\nsaubamacha
By.align: Kepf\nausrichtn
By.reset: Druckr\nzrucksetzn
By.cartridge: Patrona\nwechsln
By.printerState: Schtatus: 
By.pref: Wos eigschtäit is
By.cfg2Device: Port zum Auswäin:
Da.check:          Check\nDyser
Da.clean:          Rens\nDyser
Da.align:          Indret\nHoved
Da.reset:          Reset\nPrinter
Da.cartridge:      Skift\nPatron
Da.printerState:   Tilstand:
Da.pref:           Indstillinger
Da.cfg2Device:      Port valg:
De.check:Düsen\nprüfen
De.clean:Düsen\nreinigen
De.align:Köpfe\nausrichten
De.reset:Drucker\nzurücksetzen
De.cartridge:Patrone\nwechseln
De.printerState:Status: 
De.pref:Einstellungen
De.cfg2Device:Port Auswahl:
En.check:          Check\nNozzle
En.clean:          Clean\nNozzle
En.align:          Align\nHead
En.reset:          Reset\nPrinter
En.cartridge:      Change\nCartridge
En.printerState:   State: 
En.pref:           Preference
En.cfg2Device:      Port Choice:
Fr.check:Test\nbuses
Fr.clean:Nettoyage\nbuses
Fr.align:Alignement\n
Fr.reset:RAZ\nimprimante
Fr.cartridge:Changement\ncartouche
Fr.printerState:Status: 
Fr.cfg2Device:Choix du port:
Hu.check:          Fúvóka\nellenõrzés
Hu.clean:          Fúvóka\ntisztítás
Hu.align:          Fej\npozicionálás
Hu.reset:          Nyomtató\nreset
Hu.cartridge:      Tintapatron\ncsere
Hu.printerState:   Állapot:
Hu.pref:           Beállítások
Hu.cfg2Device:      Port választás:
It.check: Controllo\nUgelli
It.clean: Pulizia\nugelli
It.align: Allineamento\nTestina
It.reset: Ripristina\nStampante
It.cartridge: Cambio\ncartuccia
It.printerState: Stato: 
It.pref: Preferenze
It.cfg2Device: Scelta port:
Tr.check: Üfleç\nkontrolü
Tr.clean: Üfleç\ntemizliği
Tr.align: Başları\nAyarlamak
Tr.reset: Yeniden\nyerleştir
Tr.cartridge: Boya\ndeğişecek
Tr.printerState: Status: 
Tr.pref: Ayarlamak
Tr.cfg2Device: Port seçimi:
