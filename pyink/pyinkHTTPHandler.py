

__version__ = "0.1"

__all__ = ["pyinkHTTPRequestHandler","gl","res", "rdRes"]

import os
import posixpath
import BaseHTTPServer
import urllib
import cgi
import shutil
import mimetypes
from StringIO import StringIO
import string
import time

""" Class res for storage of internationalized textes
"""
class res:
   check='Check<br>Nozzle'
   clean='Clean<br>Nozzle'
   align='Align<br>Head'
   reset='Reset<br>Printer'
   cartridge='Change<br>Cartridge'
   printerState='State:'
   pref='Preference'
   ok='OK'
   cfgDevice='Port Choice:'

""" Function rdRes, read the external resource files and
    store the values into the res class structur
"""
def rdRes():
   lang=os.getenv('LANG')
   if len(lang) >= 2:
      lang = lang[:2]
   else:
      lang='en'

   # capitalize the first char
   lang = lang[0:1].upper()+lang[1:].lower()

   #print lang

   #open the resource file
   f = open('Pyink.res','r')
   while True:
      m=f.readline()
      if m == '':
         break
      m=m[:-1]
      if lang == m[0:2]:
         print m
         colon = m.find(':')
         id=m[3:colon]
         re=m[colon+1:]
         # substitute \n with <br>
         while True:
            br = re.find('\\n')
            if br == -1:
               break
            re=re[0:br]+'<br>'+re[br+2:]

         if id == 'check':
            res.check=re
         elif id == 'clean':
            res.clean=re
         elif id == 'align':
            res.align=re
         elif id == 'reset':
            res.reset=re
         elif id == 'cartridge':
            res.cartridge=re
         elif id == 'printerState':
            res.printerState=re
         elif id == 'pref':
            res.pref=re
         elif id == 'ok':
            res.ok=re
         elif id == 'cfg2Device':
            res.cfgDevice=re
   f.close()

""" Function gl, some global variable
"""

class gl:
   devfile=''
   model=''
   modelTyp=''
   enCheck=True
   enClean=True
   enAlign=False
   enReset=True
   enExchange=False
   init=False
   confDir=''
   workDir=''
   tmpDir=''

""" Function readConf, read our configuration files and
    store the values into the gl class structur
"""
def readConf():
   if os.path.exists(gl.tmpDir+'/htink.conf'):
      f=open(gl.tmpDir+'/htink.conf','r')
      while True:
         l=f.readline()
         if l=='':
            break;
         l=l.strip()
         w=l.split('=')
         if w.count > 1:
            if w[0]=='deviceFile':
               gl.devfile=w[1]
            elif w[0]=='modelName':
               gl.model=w[1]
            elif w[0]=='modelTyp':
               gl.modelTyp=w[1]
            else:
               if w[1] == 'True':
                  st=True
               else:
                  st=False

               if w[0]=='check':
                  gl.enCheck=st
               elif w[0]=='clean':
                  gl.enClean=st
               elif w[0]=='align':
                  gl.enAlign=st
               elif w[0]=='reset':
                  gl.enReset=st
               elif w[0]=='exchange':
                  gl.enSxchange=st
      f.close()


""" Help Function setOpts used bay StoreConfig, 
    after the printer was found, we have only to
    read a few description lines and to write this
    to our configuration file 
"""
def setOpts(p):
    # read up to end of the printer description
    while True:
       l = p.readline()
       l = l[:-1]
       l = l.strip()
       if l == '.END':
          break
       if  l == '':
          break
       w = l.split(':')
       w[0] = w[0].strip()
       w[1] = w[1].strip()
       # At this time exchange and align not supported !
       #if w[0] == '.exchangeFlg':
       #   if w[1] == 'True':
       #      gl.enExchange=True
       #elif w[0] == '.passesNb':
       #   if long(w[1]) > 0:
       #      gl.enAlign=True
    # all is parsed, write configuration file
    gl.enCheck=True
    gl.enClean=True
    gl.enReset=True
    cf=open(gl.tmpDir+'/htink.conf','w')
    cf.write('deviceFile='+gl.devfile+'\n')
    cf.write('modelTyp='+gl.modelTyp+'\n')
    if gl.enAlign == True:
       cf.write('align=True\n')
    else:
       cf.write('align=False\n')
    if gl.enExchange == True:
       cf.write('exchange=True\n')
    else:
       cf.write('exchange=False\n')
    cf.close()
    gl.init=True

""" Function StoreConfig, write out our configuration
   file
"""
def StoreConfig(f):
    # check anwer
    l = f.readline()
    l = l[:-2]
    l = l.strip()
    l.strip()
    ws = l.split(' ')
    c = len(ws)
    i=1
    n=''
    while i<c:
       w = ws[i]
       if n == '':
           n = ws[i]
       else:
           n = n+'_'+ws[i]
       i = i + 1
    
    if ws[0] == 'Stylus':
       gl.modelTyp=n
       p=open(gl.confDir+'/printer.desc','r')
       #search for printer
       while True:
          pl = p.readline()
          if pl=='':
             break
          pl=pl.strip(' ')
          pl=pl[:-1]
          wp = pl.split(':')
          if wp[0] == '.name':
             pn = wp[1].strip()
             if pn == l:
                setOpts(p)
                break
          
       p.close()


""" Function pyinkHTTPRequestHandler, this is the overloaded
    function for HTTP 
"""
class pyinkHTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    server_version = "pyinkHTTP/" + __version__

    def do_GET(self):
        """Serve a GET request."""
        if gl.modelTyp != '':
           modCmd=' -m '+gl.modelTyp+' '
        else:
           modCmd=''

        # check for action
        if  self.path == '/':
           nocall=''
        if  self.path == '/spacer.gif':
           nocall=''
           #print 'Send gif file'
           f = open('spacer.gif', 'r')
           self.send_response(200)
           self.send_header("Content-type", 'image/gif')
           self.send_header("Content-Length", str(os.fstat(f.fileno())[6]))
           self.end_headers()
           self.copyfile(f, self.wfile)
           f.close()
           return
        elif  self.path == '/Config':
           gl.init = False
           gl.devfile = ''
           if os.path.exists('htink.conf'):
               os.unlink('htink.conf')
        elif  self.path == '/Reset':
           os.system('ttink -u -d '+gl.devfile+modCmd+' -r >'+gl.tmpDir+'/ttink.response 2>&1')
        elif  self.path == '/Clean':
           os.system('ttink -u -d '+gl.devfile+modCmd+' -c >'+gl.tmpDir+'/ttinkx.response 2>&1')
           time.sleep(5)
        elif  self.path == '/Align':
           nocall=''
           #print 'Call Align'
        elif  self.path == '/exchange':
           nocall=''
        elif  self.path == '/Check':
           print 'call: '+'ttink -u -d '+gl.devfile+modCmd+' -n >'+gl.tmpDir+'/ttinkx.response 2>&1'
           os.system('ttink -u -d '+gl.devfile+modCmd+' -n >'+gl.tmpDir+'/ttinkx.response 2>&1')
           time.sleep(2)
        else:
           cmds = self.path.split('?')
           if cmds.count > 1:
              if cmds[0] == '/DevSel':
                 gl.devfile=cmds[1]

        #os.unlink('ttink.response')
        #os.system('echo RE:;cat ttink.response')
        #if  self.path == '/Reset':
        #   print 'modelTyp <'+gl.modelTyp+'>'
        #print 'call: '+'ttink -d '+gl.devfile+' -m '+gl.modelTyp+' >ttink.response 2>&1'
        os.system('ttink -u -d '+gl.devfile+modCmd+' >'+gl.tmpDir+'/ttink.response 2>&1')
        #os.system('echo RE:;cat ttink.response')

        # if  model typ not set, collect all informations and
        # put this into our configuration file
        if gl.init==False:
            #print 'Not initialized'
            if os.path.exists(gl.tmpDir+'/ttink.response'):
               f = open(gl.tmpDir+'/ttink.response', 'r')
               StoreConfig(f)
               f.close()
            else:
               f = open(gl.tmpDir+'/ttink.response','w')
               f.write('No answer from ttink!')
               f.close()

        f = open(gl.tmpDir+'/ttink.response', 'r')
        self.buildFile(f)
        f.close()


        f = open(gl.tmpDir+'/ttink.html', 'r')
        self.send_response(200)
        self.send_header("Content-type", 'text/html')
        self.send_header("Content-Length", str(os.fstat(f.fileno())[6]))
        self.end_headers()
        self.copyfile(f, self.wfile)
        f.close()


    def copyfile(self, source, outputfile):
      shutil.copyfileobj(source, outputfile)
       
    def buildFile(self,f):
        st =  open(gl.tmpDir+'/ttink.html', 'w')
        #print 'buildFile: devfile='+gl.devfile
        if gl.devfile == '' :
           self.askDev(st)
           return

        t1     = f.readline()
        t1     = t1[:-1]
        status = f.readline()
        status = status[:-1]
        if t1 == status:
            status = f.readline()
            status = status[:-1]
        n = ''
        if status.find(':') > 0:
            ws = string.split(status, ':')
            status = string.strip(ws[1])
        f.readline()
        # color name and % value
        i = 0
        while 1:
            s = f.readline()
            s = s[:-3]
            if s == '':
                break
            if s.find(':') > 0:
                ws = string.split(s, ':')
            n = ws[0].strip()
            s  = ws[1].strip()
            if i == 0:
                black  = s
                blackn = n
            elif i == 1:
                cyan  = s
                cyann = n
            elif i == 2:
                magenta  = s
                magentan = n
            elif i == 3:
                yellow  = s
                yellown = n
            elif i == 4:
                lcyan  = s
                lcyann = n
            elif i == 5:
                lmagenta  = s
                lmagentan = n
            elif i == 6:
                grey  = s
                greyn = n
            i = i + 1

        #printButton(self,st, text, id, command, actif):

        if status == '':
            st.write( '<html>\n'+\
                      '<HEAD><meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n'+\
                      '</HEAD>\n'+\
                      '<body bgColor="#dddddd" onLoad="hide()">\n'+\
                      ' <script type="text/javascript">\n'+\
                      '  <!--\n'+\
                      '    function hide()\n'+\
                      '    {\n'+\
                      '       host=location.hostname;\n'+\
                      '       port=location.port;\n'+\
                      '       file=location.pathname;\n'+\
                      '       if ( file != "/" )\n'+\
                      '       {\n'+\
                      '           //location.href="http://"+host+":"+port;\n'+\
                      '           location.replace("http://"+host+":"+port+"/");\n'+\
                      '       }\n'+\
                      '    }\n'+\
                      '    function bg(elem,color) {\n'+\
                      '      document.getElementById(elem).bgColor=color;\n'+\
                      '    }\n'+\
                      '    function send(cmd) {\n'+\
                      '       host=location.hostname;\n'+\
                      '       port=location.port;\n'+\
                      '       //location.href="http://"+host+":"+port+"/"+cmd;\n'+\
                      '       location.replace("http://"+host+":"+port+"/"+cmd);\n'+\
                      '    }\n'+\
                      '  //-->\n'+\
                      ' </script>\n'+\
                      ' <table align="center" width="100%" border="0">'+\
                      '  <tr>\n'+\
                      '   <td align="left" colspan="3">'+t1+'<br><br></td>\n'+\
                      '  </tr>\n'+\
                      '   <td align="center" width="30%">\n'+\
                      '    <table border=0 cellpadding=0 cellspacing=0 witdh="10%">\n'+\
                      '     <tr>\n'+\
                      '      <td colspan=3 BGCOLOR="White"><img src="spacer.gif" border=0 height=1 width="100%"></td>\n'+\
                      '     </tr>\n'+\
                      '     <tr>\n'+\
                      '      <td BGCOLOR="White" width=1 ><img src="spacer.gif" border=0 width=1></td>\n'+\
                      '      <TD width="100%">\n'+\
                      '       <Table width="100%" border=0 cellpadding=0 cellspacing=0>\n'+\
                      '        <tr>\n'+\
                      '         <td width="100%" BGCOLOR="#cccccc" align="center" id="ok"\n'+\
                      '            onMouseOver="bg(\'ok\',\'#eeeeee\')" onMouseOut="bg(\'ok\',\'#cccccc\')"\n'+\
                      '            onClick="send(\'\')">\n'+\
                      '          <font style="color:Black">OK</font>\n'+\
                      '         </td>\n'+\
                      '        </tr>\n'+\
                      '       </table>\n'+\
                      '      </TD>\n'+\
                      '      <td BGCOLOR="Black" width=1><img src="spacer.gif" border=0 width=1></td>\n'+\
                      '     </tr>\n'+\
                      '     <tr>\n'+\
                      '      <td colspan=3 BGCOLOR="Black"><img src="spacer.gif" border=0 height=1></td>\n'+\
                      '     </TR>\n'+\
                      '    </TABLE>\n'+\
                      '   </td>  \n'+\
                      '   <td>  </td>  \n'+\
                      '   <td align="center" width="30%">'+\
                      '    <table border=0 cellpadding=0 cellspacing=0 witdh="10%">\n'+\
                      '     <tr>\n'+\
                      '      <td colspan=3 BGCOLOR="White"><img src="spacer.gif" border=0 height=1 width="100%"></td>\n'+\
                      '     </tr>\n'+\
                      '     <tr>\n'+\
                      '      <td BGCOLOR="White" width=1 ><img src="spacer.gif" border=0 width=1></td>\n'+\
                      '      <TD width="100%">\n'+\
                      '       <Table width="100%" border=0 cellpadding=0 cellspacing=0>\n'+\
                      '        <tr>\n'+\
                      '         <td width="100%" BGCOLOR="#cccccc" align="center" id="cfg"\n'+\
                      '            onMouseOver="bg(\'cfg\',\'#eeeeee\')" onMouseOut="bg(\'cfg\',\'#cccccc\')"\n'+\
                      '            onClick="send(\'Config\')">\n'+\
                      '          <font style="color:Black">'+res.pref+'</font>\n'+\
                      '         </td>\n'+\
                      '        </tr>\n'+\
                      '       </table>\n'+\
                      '      </TD>\n'+\
                      '      <td BGCOLOR="Black" width=1><img src="spacer.gif" border=0 width=1></td>\n'+\
                      '     </tr>\n'+\
                      '     <tr>\n'+\
                      '      <td colspan=3 BGCOLOR="Black"><img src="spacer.gif" border=0 height=1></td>\n'+\
                      '     </TR>\n'+\
                      '    </TABLE>\n'+\
                      '  </tr>\n'+\
                      ' </table>\n'+\
                      '</body></html>')
        elif status != '':
            gl.model=t1[:-1]
            st.write('\
<html>\n\
<HEAD><meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n\
</HEAD>\n\
<body bgcolor="#dddddd"  onLoad="hide()">\n')
            st.write('\
<table width="100%" border="0" bgColor="#dddddd">\n\
  <tbody ><p>\n\
    <TR><TD colspan="2" align="center" bgColor="White"><font size="+2" color="Blue"><b>'+gl.model+'</b></font></TD></TR>\n\
    <TR><TD colspan="2" height="10px"> </TD></TR>\n')
            if i > 0:
                self.printColor(st,blackn,black,'#000000',True)
            if i > 1:
                self.printColor(st,cyann,cyan,'#00ffff',False)
                self.printColor(st,magentan,magenta,'#ff00ff',False)
                self.printColor(st,yellown,yellow,'#ffff00',False)
            if i > 4:
                self.printColor(st,lcyann,lcyan,'#a0ffff',False)
                self.printColor(st,lmagentan,lmagenta,'#ffa0ff',False)
            if i > 6:
                self.printColor(st,greyn,grey)

            st.write('\
    <TR><TD colspan="2"><BR></TD></TR>\n\
    <TR><TD width="1%">'+res.printerState+'</TD><TD width="99%">'+status+'</TD></TR>\n\
    <TR><TD colspan="2"><BR></TD></TR>\n\
  </tbody>\n\
</table>\n\
<!-- Push buttons -->\n\
\n\
<script type="text/javascript">\n\
<!--\n\
  function hide()\n\
  {\n\
     host=location.hostname;\n\
     port=location.port;\n\
     file=location.pathname;\n\
     if ( file != "/" )\n\
     {\n\
         //location.href="http://"+host+":"+port;\n\
         location.replace("http://"+host+":"+port+"/");\n\
     }\n\
  }\n\
  function bg(elem,color) {\n\
    document.getElementById(elem).bgColor=color;\n\
  }\n\
  function send(cmd) {\n\
     host=location.hostname;\n\
     port=location.port;\n\
     //location.href="http://"+host+":"+port+"/"+cmd;\n\
     location.replace("http://"+host+":"+port+"/"+cmd);\n\
  }\n\
//-->\n\
</script>\n\
      <TABLE width="100%"  border="0" bgcolor="#dddddd" align="left" cellspacing="0" cellpadding="5">\n\
        <TR>\n')

            self.printButton(st, '18', res.check,     'check', 'Check',    gl.enCheck)
            self.printButton(st, '18', res.clean,     'clean', 'Clean',    gl.enClean)
            self.printButton(st, '18', res.align,     'align', 'Align',    gl.enAlign)
            self.printButton(st, '18', res.reset,     'reset', 'Reset',    gl.enReset)
            self.printButton(st, '18', res.cartridge, 'ex',    'Exchange', gl.enExchange)
            st.write('\
        </TR\n>\
      </TABLE>\n\
</body>\n\
</html>')
            st.close()

    def printColor(self,st, name,value,cname,invert):
         val1=str(long(value))
         val2=str(100-long(value))
         txtColor='Black'
         if invert:
            txtColor='White'
         st.write('\
    <TR>\n\
     <TD width="1%" nowrap>'+name+'</TD>\n\
     <TD width="99%">\n\
      <table border=0 cellpadding=0 cellspacing=0 width="100%">\n\
       <tr>\n\
        <td colspan=3 BGCOLOR="Black"><img src="spacer.gif" border=0 height=1 width="100%"></td>\n\
       </tr>\n\
       <tr>\n\
        <td BGCOLOR="Black" width=1 ><img src="spacer.gif" border=0 width=1></td>\n\
        <TD width="100%" >\n\
         <Table width="100%" border=0 cellpadding=0 cellspacing=0>\n\
          <tr>\n\
           <td width="'+val1+'%" BGCOLOR="'+cname+'" align="center"><font color="'+txtColor+'">'+val1+'%</font></td>\n\
           <td width="'+val2+'%"></td>\n\
          </tr>\n\
         </table>\n\
        </TD>\n\
        <td BGCOLOR="White" width=1><img src="spacer.gif" border=0 width=1></td>\n\
       </tr>\n\
       <tr>\n\
        <td colspan=3 BGCOLOR="White"><img src="spacer.gif" border=0 height=1></td>\n\
       </TR>\n\
      </TABLE>\n\
     </TD>\n\
    </TR>')

    def printButton(self, st, sz, text, id, command, actif):
        txtColor='Black'
        if actif == False:
           txtColor = 'Grey'
        st.write('<TD width="'+sz+'%" align="center">\n'+\
                 ' <TABLE border=0 cellpadding=0 cellspacing=0 width="100%">\n'+\
                 '  <TR>\n'+\
                 '   <TD colspan=3 BGCOLOR="White"><img src="spacer.gif" border=0 height=1 width="100%"></TD>\n'+\
                 '  </TR>\n'+\
                 '  <TR>\n'+\
                 '   <TD BGCOLOR="White" width=1 ><img src="spacer.gif" border=0 width=1></TD>\n'+\
                 '   <TD width="100%">\n'+\
                 '    <Table width="100%" border=0 cellpadding=0 cellspacing=0>\n'+\
                 '     <TR>\n'+\
                 '      <TD width="100%" BGCOLOR="#cccccc" align="center" id="'+id+'"')
        if actif:
           st.write('\n'+\
                 '        onMouseOver="bg(\''+id+'\',\'#eeeeee\')" onMouseOut="bg(\''+id+'\',\'#cccccc\')"\n'+\
                 '        onClick="send(\''+command+'\')">\n')
        else:
           st.write('>\n')
        st.write('       <font style="color:'+txtColor+'">'+text+'</font>\n'+\
                 '      </TD>\n'+\
                 '     </TR>\n'+\
                 '    </TABLE>\n'+\
                 '   </TD>\n'+\
                 '   <TD BGCOLOR="Black" width=1><img src="spacer.gif" border=0 width=1></TD>\n'+\
                 '  </TR>\n'+\
                 '  <TR>\n'+\
                 '   <TD colspan=3 BGCOLOR="Black"><img src="spacer.gif" border=0 height=1></TD>\n'+\
                 '  </TR>\n'+\
                 ' </TABLE>\n'+\
                 '</TD>\n')

    def askDev(self,st):
        t1=res.cfgDevice
        #print'ASK for device file'
        # build a list of file device
        os.system('(ls -c /var/mtink/*;ls -cr /dev/lp*;ls -c /dev/usb/lp*) >deviceFiles 2>/dev/null')
        st.write( '<html>\n'+\
                  '<HEAD><meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n'+\
                  '</HEAD>\n'+\
                  '<body bgColor="#dddddd" onLoad="hide()">\n'+\
                  ' <script type="text/javascript">\n'+\
                  '  <!--\n'+\
                  '    function hide()\n'+\
                  '    {\n'+\
                  '       host=location.hostname;\n'+\
                  '       port=location.port;\n'+\
                  '       file=location.pathname;\n'+\
                  '       if ( file != "/" )\n'+\
                  '       {\n'+\
                  '           location.replace("http://"+host+":"+port+"/");\n'+\
                  '       }\n'+\
                  '    }\n'+\
                  '    function bg(elem,color) {\n'+\
                  '      document.getElementById(elem).bgColor=color;\n'+\
                  '    }\n'+\
                  '    function send(cmd) {\n'+\
                  '       // check for selction\n'+\
                  '       sd=document.getElementById("dev").value;\n'+\
                  '       if ( sd != "")\n'+\
                  '       {\n'+\
                  '          host=location.hostname;\n'+\
                  '          port=location.port;\n'+\
                  '          location.replace("http://"+host+":"+port+"/"+cmd+"?"+sd);\n'+\
                  '       }\n'+\
                  '    }\n'+\
                  '  //-->\n'+\
                  ' </script>\n'+\
                  ' <table align="center" border="0">'+\
                  '  <tr>\n'+\
                  '   <td align="left" valign="top"  nowrap>'+t1+'</td>\n'+\
                  '   <td align="left">\n'+\
                  '    <FORM action="Select">\n'\
                  '     <select id="dev" size="10">\n'+\
                  '\n')
        # insert all our device file into the selection list
        d=open('deviceFiles','r')
        while True:
            df = d.readline()
            df.strip()
            if df == '':
                break
            st.write(\
                  '       <option>'+df+'</option>\n')        
        # close the file
        d.close()
        st.write('      </select>\n'\
                  '    </FORM>\n'\
                  '   </td>\n'+\
                  '  </tr>\n'+\
                  '  <tr>\n'+\
                  '   <td colspan="2">\n'+\
                  '    <table width="100%"align="center" border="0">\n'+\
                  '     <tr>\n'+\
                  '      <td></td>\n'+\
                  '      <td align="center" width="20%">\n'+\
                  '       <TABLE border=0 cellpadding=0 cellspacing=0 width="100%">\n'+\
                  '        <tr>\n'+\
                  '         <td colspan=3 BGCOLOR="White"><img src="spacer.gif" border=0 width=1></td>\n'+\
                  '        </tr>\n'+\
                  '        <tr>\n'+\
                  '         <td>\n'+\
                  '          <Table width="100%" border=0 cellpadding=0 cellspacing=0 border="0">\n'+\
                  '           <tr>\n'+\
                  '            <td BGCOLOR="White" width=1 ><img src="spacer.gif" border=0 width=1></td>\n'+\
                  '            <td width="100%" BGCOLOR="#cccccc" align="center" id="ok"\n'+\
                  '               onMouseOver="bg(\'ok\',\'#eeeeee\')" onMouseOut="bg(\'ok\',\'#cccccc\')"\n'+\
                  '               onClick="send(\'DevSel\')">\n'+\
                  '             '+res.ok+'\n'+\
                  '            </td>\n'+\
                  '            <td BGCOLOR="Black" width=1 ><img src="spacer.gif" border=0 width=1></td>\n'+\
                  '           </tr>\n'+\
                  '          </table>\n'+\
                  '         </td>\n'+\
                  '        </tr>\n'+\
                  '        <tr>\n'+\
                  '         <td colspan=3 BGCOLOR="Black"><img src="spacer.gif" border=0 width=1></td>\n'+\
                  '        </tr>\n'+\
                  '       </TABLE>\n'+\
                  '      </td>\n'+
                  '      <td></td>\n'+
                  '     </tr>\n'+\
                  '    </TABLE>\n'+\
                  '   </td>\n'+\
                  '  </tr>\n'+\
                  ' </table>\n'+\
                  '</body></html>')

