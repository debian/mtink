#!/usr/bin/python 
import os
import sys
import socket
import SocketServer
import BaseHTTPServer
import pyinkHTTPHandler

from pyinkHTTPHandler import rdRes
from pyinkHTTPHandler import readConf

# set these two variable according to your needs
prefix='/usr/local'
pyinkHTTPHandler.gl.confDir=prefix+'/lib/mtink'
pyinkHTTPHandler.gl.tmpDir=prefix+'/lib/pyink/tmp'
workDir=prefix+'/lib/pyink'

pyinkHTTPHandler.gl.devfile=''
pyinkHTTPHandler.gl.model=''
defPort = 8000


os.chdir(workDir)
# a few definitions for the objects we use
protocol     = 'HTTP/1.0'
HandlerClass = pyinkHTTPHandler.pyinkHTTPRequestHandler
ServerClass  = BaseHTTPServer.HTTPServer

hostname     = socket.gethostname()
myIp = socket.gethostbyname(hostname)

print hostname, ' with IP ',myIp

if sys.argv[1:]:
    port = int(sys.argv[1])
else:
    port = defPort


rdRes()

server_address = (myIp, port)

HandlerClass.protocol_version = protocol

httpd = ServerClass(server_address, HandlerClass)

sa = httpd.socket.getsockname()
readConf()


print "Serving HTTP on", sa[0], "port", sa[1], "..."
httpd.serve_forever()

