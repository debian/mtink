V 1.0.16 29-AUG-2007
- Corrected name of no strop option for the Configure file
  (--no-strip Debian)

V 1.0.16.RC1
- Added test for access to device files
- Replaced call of access() with own function
- Some cleanup
- Added flag -nostrip for the Configure file

V 1.0.15a 26-AUG-2007
- Cleanup
- An error message modified.

V 1.0.15 26-AUG-2007
- Minor corrections insuring that the ink color is set
  correctly for some printer with strange color model.

V 1.0.15.RC1 24-AUG-2006
- Added Stylus Photo R2400
  Some tests are necessary !

V 1.0.14 04-OCT-2006
- Variable XAUTORITY was deleted by the environment checker
  corrected.

v 1.0.13c 02-OCT-2006
- Applied a patch from Sylvain Legall concerning
  the CDBS packaging system he use.

v 1.0.13b 10-SEP-2006
- Stylus Photo 950, mtink show only 6 inks, fixed

v 1.0.13a 05-MAI-2006
- Security checks for the SHELL variable modified
  (the may be under /usr/local/bin on some systems).

V 1.0.13
- Changes for FreeBSD which is now supported
- New russian translation donated by anonymous

V 1.0.12 - 1.0.2d
- Some little works and bug fixes.
 
V 1.0.11 DEC-2005
- Workaround for wrong xpm handling under SuSE 10.0
- More Workaround for buggy 2.6.X kernel
- Check of environment added into mtink
- Code for reading of alignment files

V 1.0.11 17-AUG-2005
- Corrections for Fedora Core 4
- Corrections for Gcc 4 (a lot o warnings and a (wrong)error)
- Correction for decoding of inklevel (4 color models as C84)

V 1.0.10 15-AUG-2005
- Corrections for mtink-cups (Sylvain Le Gall)
- Corrections for french textes (Sylvain Le Gall)

V 1.0.9 18-JUL-2005
- Work around for buggy printer intreface (2.6 kernels).
- Correction for Stylus R800.
- some little corrections.

V 1.0.8 06-FEB-2005
- Mac OS detection of printer improved
- Workaround for Stylus Photo 890/120 firmware bug (send a reset printer)

V 1.0.7.7 30-JAN-2005
- Mac OS Port Configuration detect openMotif and Lesstif

V 1.0.7.6 23-JAN-2005
- For Mac OS X use /usr/bin/open for viewing of help
- Security fix for call of a browser (Linux, Solaris)
- remove hungarian textes from makefile (Mac Os don't like this)

V 1.0.7.5 23-JAN-2005
- Reworked some Mac Os specific parts (ttink / mtink).
- new utilty for checking of resources
- adapted pyink for Mac OS X
- corerction of some resources files

V 1.0.7.4 22-JAN-2005
- Removed hungarian textes from Configure tool
- added option -f (find devices) to ttink for use of the pyink interface
- modified rules for configuration of printers on Mac OS

V 1.0.7.3 18-JAN-2005
- Added swedish translation from Daniel Tamm

V 1.0.7.2 15-JAN-2005
- Changed name of some files
- Modifications for mtink (Mac OS X). 
- Modifications of configure script

V 1.0.7.1 10-JAN-2005
- first port works for Mac OS X
- Bug fixing for the decoding of ink quantity. 

V 1.0.7a 06-JAN-2005
- Color recognition new printers corrected, work on Photo R200.

V 1.0.7 12-DEC-2004
- Color recognition and model for new printers.

V 1.0.6 11-DEC-2004
- Added palatin language (Pf)

V 1.0.5 07-NOV-2004
- Handle disconnected printer for mtink
- Dead lock condition while reset removed, modified entries for
  reset protocol.
- Security hole for server (right for /var/run/mtink/*) fixed
  
V 1.0.4 17-AUG-2004
- Added a few printers
- added UTF-8 support for ttink.
- added a little python application which allow to see the ink level
  via a browser
- little problem with spec file fixed

V 1.0.3 30-JUN-2004
- Modification of Makefile.ORG and Configure for X86_84 according to
  Nicola Fabiano ivololeribar@yahoo.it proposal.
- Stylus Photo 2100 show not all 7 inks.
- Stylus C42xx allow catridge exchange from mtink
- Documentation now searched under $prefix/doc/mtink. Past version had
  only a fews build in pathes.
- Added installation of file printer.desc
 
V 1.0.2 16-MAI-2004
- Adaptation for gimp 2.0

v 1.0.1 25-JAN-2004
- Check nscript for motif modified according to proposal from
  Raulk Morales (Problem on Debian where the motif lib is
  located in /usr/lib).

V 1.0.0 02-NOV-2003
- Modified Makefile (ttink don't need X- libraries)
- Support for the binary version of the status message returned by
  newer printers as the C43 and C84
- debug facility for the D4 protocol via command line argument -L
  instead of special compilation flag

V 0.9.59 25-OCT-2003 internal
- Test on CX3200: -> disable alignment pushbutton, set reset to D4 only

V 0.9.58 17-OCT-2003 
- D4 printers will block if the ink level is low. Disable test of
  printer type for exchange ink (unconditionnaly) and for status
  request (GET_IQ) if the autodetection of printer is disabled.
- Modifications of Makefile

V 0.9.57 07-SEP-2003 internal
- Little problem within makefile fixed.

V 0.9.56 05-SEP-2003
- Install startscript added /usr/local/sbin to path
- Hotplug behaviour for mtinkd / mtink improved
- changes for Mac OS-X
- Added doc path for Mandrake 9.0

V 0.9.55b 25-MAI-2003
- Install startscript location is /etc/rc.d if /etc/init.d don't
  exist, this is for Slackware.
  The file /etc/rc will also be corrected.

V 0.9.54b 12-JAN-2003
- Added danish resource translated by Mogens Jaeger.

V 0.9.54 26-DEC-2002 internal
- Added all known printers, modified sorting of printer list
  (alpha numerical).

V 0.9.53 26-DEC-2002
- Added Stylus C82
- Modification regarding Exploit from David Endler build in.

V 0.9.53 21-DEC-2002 internal
- Problem wih C42xx corrected. If the printer is allready
  into the D4 mode, sendinf a get state request don't work, the
  printer must be set tom the non D4 mode and the into the D4 mode
  again.

V 0.9.53 11-DEC-2002 internal
- Error in file getGimpPluginDir.sh corrected.

V 0.9.52 06-DEC-2002
- Added C42SX and C42UX
- Few correction to the start script mtink
- Correction and adds for the documentation

V 0.9.52c 01-DEC-2002 internal
- Modified parameter for the Color 777

V 0.9.52b 26-NOV-2002 internal
- File etc/mtinkd corrected according to report from Yasunari Tosa

V 0.9.52a 19-NOV-2002 internal
- The browser prozess terminate as zombie, corrected.
- Spec-file renamed (mtink.spec.ORG and mtink-all.spec.ORG), Makefile.ORG
  corrected.

V 0.9.51 19-OCT-2002
- Server mtinkd: Create the directory /var/run if necessary (Solaris)
- Makefile / rpm Red Hat 8.0 don't know the -ba flag for rpm, use
  rpmbuild if existing.
- Mtink, preference menu: cyclic scan modified according to old
  state and device (/dev/... /var/mtink/...)
- Mtink: dead lock removed which may appear while the server is
  running and the user try to choose a normal device file.
- Mtink: destroy the "icon" window, if the device file changed
  and no more the fifo.
- Mtink: corrected determining position of the "icon", this was not
  always OK.

V 0.9.51a 06-OCT-2002 internal
- Better checks for Motif / Lesstif (Linux). The library file will
  also be set if the libXm.so link point to the wrong version.
- Code cleaning for mtinkc
- Checked for compiling with gcc 3.2
- New html file (for add of a new printer)
- Correction of wrong link doc

V 0.9.50 03-OCT-2002
- File location rearanged, most text files moved to the directory html
- Created link doc -> html
- modification of *.spec files according to new layout.
- Added index.html into the html directory

V 0.9.49 01-OCT-2002
- Resource corrected according to message from Sylvain Legall
- Configure modified (detection of gimp devel package)
- Add dlag -g / --no-gimp to the configure script

V 0.9.48 29-SEP-2002 internal
- Adds to the Makefile concerning problems with mixed openmotif/lesstif
  environment, not handled automatically
- Add installation / uninstalltion of some file in the Makefile.
- Documentation: corrected name of gimp-installInitScript.sh to gimp-installInitScript.sh

V 0.9.47 28-SEP-2002
- Modified Makefile, the gimp plugin will be installed
  under the global directory.
- RPM Spec file modified for read of global gimp directory
  via the new file getGimpPluginDir.sh

V 0.9.46 27-SEP-2002
- Added check for egid (scanning for device files).
- Corrected Makefile.org regarding rpm-all tag.

V 0.9.45 26-SEP-2002
- Added mtink.spec (rpm spec for Red Hat) donnated by
  Keith Amidon <camalot@picnicpark.org>
- Added a drivative spec file (all packages in one rpm)
- corrected mtinkc.c (one header file missed according to Keith).

V 0.9.44 21-SEP-2002
- Added Stylus C62
- Code cleaning
- Add code to some header file for compatibility to C++

V 0.9.43 20-SEP-2002
- Auto detection don't work with some printers, corrected.

V 0.9.42 16-SEP-2002
- Disabled the Motif drag and drop feature from chooser.
- Modified the gimp pluging in order to call the chooser if
  more as 1 configuration file found.
- Modified Makefile, erroneous error message.

V 0.9.41 16-SEP-2002
- Disabled the Motif drag and drop feature.
- Little code changes in order to have lesstif workink correctly with xpm.
- the "icon" window will now be moved with either the button 2 and 3.

V 0.9.40 15-SEP-2002
- Lesstif 1.2 don't habe Xpm build in as Motif 1.2 under Solaris.
  Check for Xm/Xpm.h and X11/xpm.h in order to create a running
  makefile.
 
V 0.9.39 14-SEP-2002
- More error in Configure script removed
- Several little errors corrected,
- change of device (/var/mtink/... <-> /dev/...) corrected.

V 0.9.38 13-SEP-2002
- Error in Configure script removed

V 0.9.37 12-SEP-2002
- Reviewed the code for the "icon" window, No more wm decorations,
  button 2 -> move function. Now positionning OK for all tested
  window managers.
- Added a little utility (Motif) for choosing of the printer, if
  the user own more as one.
- Behaviour in case of server restart corrected. The server will
  be asked every second for life, this insure that the server will
  terminate if started to soon (time must be greater as 1 sec).
- Lock for user program / server reviewed.
- Added a few resources for printer status.
- Added html files

V 0.9.36 11-SEP-2002 internal
- Some corrections.
- Added save / restore for position of the "icon window"
  a lot of window manager are wrong ! fvwm2 don't work as
  expected.
  The position is OK for Xfce, Mwm, Kde, Gnome Windowmaker.
- Bug within mtinkd removed.

V 0.9.35 10-SEP-2002 internal
- added a second toplevel window looking as an icon.

V 0.9.34 09-SEP-2002 internal
- Reorganisation of code an splitting of cmd.c into more files

V 0.9.33 02-SEP-2002
- Added lock mimick, this avoid problems if cups try to send 2
  job on the same time.
- correction of installation script, adds to the Makefile
- Test on Red Hat, Mandrake, Debian, SuSE, all OK

V 0.9.32 01-SEP-2002 internal
- Installation of cups backend and installInitScript added.
- Minor corrections.

V 0.9.32 31-AUG-2002
- Bug fix for the Stylus Color 580 provided by Glen Steward.
- install script for the mtink start/stop script modified
- Errors within the html files fixed.
 
V 0.9.31 30-AUG-2002
- mtinkd terminate at a early stage, printer just poweed on, fixed
- mtink-cups donnated by Till Kamppeter include into the etc directory,
  installation job must be performed.
- included a version of the start-stop script donnated by Till Kammpeter
  and modified for use of a configuration script, see also below.
- created the directory detect and put askPrinter.c inside, modified
  askPrinter.c with code borrowed from Tim Waugh (Red Hat).
  askPrinter return now the ID string for // and USB printers.
- various improvment
 
V 0.9.30 29-AUG-2002
- Error in the etc/mtink file corrected, missed file etc/mtink.ORG
  added.
  
V 0.9.29 29-AUG-2002
- Italian translations and corrections from Alberto Zanoni added
- Installation script for the server start/stop scrip�ts provided
  (Linux)

V 0.9.28 28-AUG-2002
- The -type option will now be always taken for building of the pipe
  name (of course only if the option was passed).
- If the option -type name is given mtinkd check the printer against
  the proper type and eventually terminate. 

V 0.9.27 27-AUG-2002 external tests
- Added generation of file /var/run/mtink/xxx for stop/status purpose

V 0.9.26 27-AUG-2002 tests
- modified the server and mtink. The fifo name may be the name of
  the printer instead of the device name. Mtinkd detect the correct
  device file according to code given by Till Kamppeter.

V 0.9.25 25-AUG-2002 internal
- Server don't set the printer to the non D4 mode if killed,
  corrected.

V 0.9.24 24-AUG-2002
- More documentation, little change for the official release.

V 0.9.23 24-AUG-2002
- detection and handling of error while printer is powered on/off
  improved.

V 0.9.22 24-AUG-2002
- Added option -i / --identity to ttink. This will allow scanning
  via [mtinkd] ttink.
- cyclic query of ik quantity build into the frontends.

V 0.9.21 22-AUG-2002
- Added a sys V like start/stop script and modified the server
  in order to make the life easier.

V 0.9.20 22-AUG-2002
- Tests and corrections of bugs and behaviour in case of
  power on/off of printer.

V 0.9.19 22-AUG-2002 
- first alpha release for external testing.
- new directory etc for startup scripts, to be filled wit life.

V 0.9.18a 21-AUG-2002
- corrected german textes from Karlheinz Guenster included.

V 0.9.17a 12_AUG-2002 ...  20-AUG-2002
- various modifications
  - new directory server and code for a D4 server allowing
    printing of jobs and query of ink quantity
  - modification of mai sources according to the new
    requirement
  - modification of Makefile

V 0.9.17 11-AUG-2002
- added "-config # "option to mtink for people with printers
  on different ports.
  
V 0.9.16 10-AUG-2002
- added light black, corrected little bugs.

V 0.9.15 06-AUG-2002 internal
- Layout for file location modified. Makefile adapted

V 0.9.14 05-AUG-2002
- more documentation (english, french, german).
- debian directory moved to Debian.Old, this will allow Sylvain
  to maintain the debian package more easelly.

V 0.9.13 03-AUG-2002 / 04-AUG-2002 
- resources for mtink in language specific files instead of
  one big file.
- The configuration script allow to choose only to generate
  the command line version.
- Added the file mtink.sh as example for people which want
  to monitor different printer attached on differnt ports.

V 0.9.12 29-JUL-2002 internal
- little correction of resources acc. to Steven J. Mackenzie proposal
- Code cleanup (not used variables,...)

V 0.9.11 27-JUL-2002
- Bug in description for the Stylus Photo 790 fixed (Steven 
- Code cleaning, removed unused variable,...

V 0.9.10 27-JUL-2002
- The Photo 820 alignment work.
- little correction concerning the printer description.
- Resources for Mtink rearanged, dource are now the Mtink.res file
  and no more the c file resource.c. generating of the resource.c
  file done by the new utility bldRes.

V 0.9.03 24-JUL-2002 internal
- Added code for external printer description file
- tried to fix the Alignment bug for the Photo 820
  The modifications seem to be others as those expected
  and are also due to a buggy firmware. The exit D4
  command must be issued after all channels was closed.

V 0.9.02 21-JUL-2002
- Added the file Ttink.en for translators.
- Added utility bldRes.c for converting of resource files
  to there *.c counterpart.
- Fixed bug for Photo 820 alignment function.

V 0.9.01 21-JUL-2002
- Test on Solaris and corrections for line discipline

V 0.9  20-JUL-2002
- Ttink (console based version of ttink) has now all functions
  available for mtink.
- Code change for internalisation developed.
- first resource file (german) for ttink written.

V 0.8p 15-JUL-2002
- Bug while saving configuration (introduced with V 0.8o) fixed.
- Added head alignment (ttink).
- internationalization beginned.

V 0.8o 14-JUL-2002
- Added code for extern aligment pattern (Photo 820).

V 0.8n 10-JUL-2002
- Security problems fixed as stated by Till Kampeter.
  1) The browser will allways be vcalled with the user id and
     not with the effective uid.
  2) Mtink may work with he normal uid if the user is a member of
     the lp group and the device file have lp as group
     Mtink may be a set uid program with owner root or the owner
     of the device files.
- Added the italian translation provided by Alberto Zanoni.

V 0.8m 01-JUL-2002
- Bug regarding exchange ink for the Stylus Color 480 fixed.

V 0.8n 28-JUN-2002 / 29-JUIN-2002 internal
- Add file for debian package which was provided by Sylvain Le-Gall.
- Makefile.ORG modified as given by Sylvain.
- little modification of resources
- minor modification of the file cfg1.c
- Added the file LICENCE
- included of the first work from Sylvain LE GAL concerning debian package

V 0.8m  08-JUN-2002 09-JUN-2002
- First modifications for writing of an TTY based version,
  model description moved to the new file model.c.
  All functions are not supported (exchange head, align head).
- Added Path in configure file for Debian Linux

V 0.8l  31-MAI-2002
- Added path for the documentation and corrected the Configure
  and Mekefile.ORG files.

V 0.8k  26-MAI-2002
- Modified detection of available device file. The acess call
  check the access rights according to the uid and not the euid.
  If the devfs is used on Linux, mtink must be a root setuid programm,
  the device file are created dynamically amd the rights will not be
  OK for normal users.
- Added device name according to Linux devfs (used by mandrake 8.2).
  
V 0.8j  20-MAI-2002
- Makefile and configure script improved.
  plug-in will also be installed via make install.
  deinstallation of plugins also integrated.

V 0.8i  18-MAI-2002
- Reviewed configurations script and the Makefile.
  Textes will be printed into english, french and german.
  Installlations stuff added.
  Test performed under Linux, Solaris (sparc) and freeBSD.

V 0.8h  04-MAI-2002 / 11-Mai-2002
- Added Stylus C50, Stylus Photo 950, Stylus Photo 2100
  will be marketed soon.
- Correction for first read call at initialization.
- Modifications ofr Solaris 8, first tests witg Stylus Color 980
  on Solaris are OK

V 0.8g  25-APR-2002
- Correction for the C80 / C70 not alignment for color head.
- Some little bugs removed.

V 0.8f  20-APR-2002
- Correction for german resources
- Modification, for better handling of head alignment on C80

V 0.8e2 19-APR-2002 internal
- Created Configure script, test on Solaris, FreeBSD and Linux

V 0.8e1 15-APR-2002 internal
- Added Stylus Color 685
- Modified the Makefile a little bit

V 0.8e  09-APR-2002
- Bayrische �bersetzung von Robert Wachinger integriert
 
V 0.8d  07-APR-2002
- Bug for the Color 580 (exchange cartridge) fixed.
  Better handling of abnormal termination of the GUI.
  Fix call for netscape help browser problems, included
  $(HOME)/mtink in the search path for documentation.
  
V 0.8c  01-APR-2002
- Italian documentation updated

V 0.8b  30-MAR-2002
- Diplay of unkmow printers modified.
- German documentation from Marc Riese substituted by the
  translation from Karlheinz G�nster. 
- Call of netscape modified (version 6.2x)

V 0.8a  24-MAR-2002
- Whell mouse handling reviewed and generalized. The file
  wheelmouse.c and whellmouse.h may be used for other programs.
- Whell mouse handling for all scrolled windows, scrollwide
  modified according to shift and scontrol keys.

V 0.8   23-MAR-2002
- Handling for Button 4 and 5 (scroll mouse) added for printer and
  device lists.
- French textes for the tooltips.
- Provision for unknown (newer printer) integrated. Unknown printer
  are handled as photo printer and the cartridge alignm,ent is disabled.
- Datas are no more stored into 2 files. The configurations file
  is .mtinkrc.
- Restructuration (not yet finished).

V 0.7a  13-MAR-2002
- english textes for tooltips added (if you know english you can
  send better textes).
- Added file foe device FreeBSD.

V 0.7  09-MAR-2002
- Tooltips implemented, not finished know (only german and 
  as resource File).
- Copied a few xpm file from the xfce pacakge to the directory XPM

V 0.6b 23-FEB-2002
- German documentation integrated (translation from Marc Riese)

V 0.6a  internal
- little modifications

V 0.6   15-FEB-2002
- Added plugin for gimp. Tested compiling under SuSE 6.4 and SuSE 7.3

V 0.5d  14-FEB-2002
- Minor change regarding themes, added the file Themes.

V 0.5c  11-FEB-2002
- put the display string to environment (may be usefull if a
  browser is started from a remote instance of mtink).
- If no printer or device is "preset" show the list correctly.
- If no dev found return with proper env and inform the user
  about this.
- Little correction for the resources.

V 0.5b  09-FEB-2002
- correction for german resources, thank to Marc Riese
- Bug in file cfg2.c, ink quantity not processed correctly.
- Ink quantity bars not set correctly if ink level = 0,
  corrected.
- If the printer is powered down or if the printer cable is
  removed, the subprocsee for printer access may remain in
  a loop, fixed.

V 0.5a  05-FEB-2002
- wrong prototype in header file eliminated,

V 0.5  02-FEB-2002
- added configuration of printer and device file into
  configure window.
- T�rkish resource corrected.

V 0.4  20-JAN-2002
- modified the protocol for some older printers

V 0.3f 17-JAN-2002
- Turkish resources completed (intermal)

V 0.3e 15-JAN-2002
- Correction for a few resources.
- Little changes concerning automatic printer recognition
- Resources into a separated file resources.c

V 0.3d 31-DEC-2001
- Error textes completed (emglish, german and french).

V 0.3c 30-DEC-2001
- correction of code for launching of netscape or mozilla
- more documentation.

V 0.3b 29-DEC-2001 Internal
- Added configuration for autodetection (disable this)
- Added command line option -noAutoDetect
- modified autodetection for the first time mtink is called

V 0.3b 29-DEC-2001
- Resource correction (german)
- wrong code fixed

V 0.3a 28-DEC-2001
- Resource correction.
- documentation update
- bug for some printer (get ink quantity ) fixed.
- Script for extraction of resources added.
- Makefile correction and a little script for static
  binding of mtink added.
 
V 0.3 27-DEC-2001
- Added preference menu for selection of the help browser.

V 0.2g 23-DEC-2001
- correction of resources.

V 0.2h 19-DEC-2001 Internal
- correction of documentation.
- Makefile corrected, linking fail on Red Hat 6.2, option -lXp
  amd -letx needed, thanks to Stefan Bohlein.
  
V 0.2g 18-DEC-2001 internal
- Italian html text from Alberto Zanoni integrated, change for
  internationalization regarding help.
  
V 02.f 17-DEC-2001
- Test with Stylus Color 980 done. This device need only 2 pass for
  the head alignment.

V 0.2e 16-DEC-2001
- internatilization was not OK some textes was english instead
  of italian,.... Bug corrected

V 0.2d 16-DEC-2001
- added help via Netscape (not perfect).

V 0.2c 16-DEC-2001
- added % output right of scale

V 0.2b 14-DEC-2001
- added a few turkish textes
- bug for get credit (D4 mode) corrected.

V 0.2a 29-DEC-2001


- german resource corrected.

- Stylus Photo 750 was declared as "OLD" printer, changed to D4
- improvement of look (new resource setting) in order to have the
  same look under openMotif and Motif 1.2 / Lesstif 
