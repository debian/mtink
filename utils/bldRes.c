/* file bldRes.c
 * build c-resource file
 *
 * read printer description file and fill the
 * printer knowledge data base
 *
 *  Copyrights: Jean-Jacques Sarton  j.sarton@t-online.de
 */
/*
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

void printHead()
{
   printf("#include \"version.h\"\nchar *fallbackResources[] = {\n");
}

void printTrailer()
{
   printf("\n   (char *)0\n};\n");
}

void searchVersionPart(char *s, char **b, char **e)
{
   *b = *e = NULL;
   while ( *s )
   {
      if ( s[0] == 'V' && s[1] == ' ' && isdigit(s[2]) )
      {
         *b = s;
         s += 2;
         while ( isdigit(*s) || *s == '.' )
            s++;
         *e = s;
         return;
      }
      s++;
   }
}

void translate(FILE *fp, int utf)
{
   int   inComment = 0;
   int   inText   = 0;
   char  buf[2048];
   char *s;
   char *b, *e;

   /* read the resource file line by line and build the c file */
   while ( fgets(buf, sizeof(buf), fp) )
   {
      s = buf;
      searchVersionPart(s, &b, &e);
      s = buf;
      if ( ! inText )
         while ( *s && (*s == ' ' || *s == '\t') ) s++;
      if ( !inComment && ! inText && *s == '!' )
      {
         inComment = 1;
         printf("#if 0\n/*\n   %s", s);
      }
      else if ( inComment && *s == '\n' )
      {
         printf("\n");
      }
      else if ( inComment && *s == '!' )
      {
         printf("   %s", s);
      }
      else if ( inComment )
      {
         printf("*/\n#endif\n");
         inComment = 0;
      }
      
      if ( ! inComment )
      {
         if ( *s == '\n' && ! inText )
         {
            printf("\n");
            continue;
         }

         inText = 1;
         printf("   \"");
         while ( *s )
         {
            if ( s == b )
            {
               printf("\"VERSION");
               s += e-b;
               if ( *s == '\n' )
               {
                  printf(",\n");
                  break;
               }
               else
               {
                  printf("\"");
               }
            }
            if ( s == buf && s[0] == '.' && s[3] == '.' && utf )
            {
               printf("%c",*s); // .
               s++;
               printf("%c",*s); // D
               s++;
               printf("%c",*s); // e
               s++;
               printf("8");     // add 8
               printf("%c",*s); // .
               s++;
            }
            
            switch (*s)
            {
               case '"':
                  printf("\\\"");
               break;
               case '\\':
                  if ( s[1] == '\n' )
                  {
                     s++; s++;
                     printf("\"\n");
                     continue;
                  }
                  else if ( s[1] == '"' )
                  {
                     printf("\\\"");
                     s++;
                  }
                  else
                  {
                     printf("\\\\");
                  } 
               break;
               case '\n':
                  printf("\",\n");
                  inText = 0;
               break;
               default:
                  printf("%c",*s);
            }
            s++;
         }
      }
   }
}

int main(int argc, char **argv)
{
   FILE *fp;
   int   utf;
   
   argc--;
   argv++;
   
   if ( argc )
   {
      printHead();
      while ( argc > 0 )
      {
         if ( (fp = fopen(argv[0], "r")) != NULL )
         {
            if ( strstr(argv[0],"utf8") )
               utf = 1;
            else
               utf = 0;
            translate(fp, utf);
            fclose(fp);
         }
         argc--;
         argv++;
      }
      printTrailer();
   }
   else
   {
      printHead();
      translate(stdin,0);
      printTrailer();
   }
   exit(0);
   return 0;
}
