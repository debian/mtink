rdPrtDesc:
----------

Ez a seg�dprogram teszi lehet�v� az mtink/ttink-hez sz�ks�ges
nyomtat�le�r� adatok b�v�t�s�t �s �rja azt egy olvashat�
form�tumba.

Ha saj�t le�r�st szolg�ltat, a le�r� f�jl olvas�dik
�s k�sz�t egy �j d f�jlt minden adattal, amelyik
a t�rgyhoz tartozik.

Syntax: rdPrtDesc -p|-c

-p rdPrtDes alkotja az olvashat� f�jlt, amelyik tartalmazni fog
   minden ismert nyomtat�t.

-c rdPrtDes olvassa a printer.desc f�jlt, aminek az aktu�lis
   k�nyvt�rban kell tart�zkodnia, �s gener�l egy �j c f�jlt amit
   a ttink/mtinkhez tud haszn�lni.
   Az �j �s az eddigi le�r� f�jl adatai
   �ssze vannak olvasztva.


FILE printer.desc:
------------------

Ez a f�jl blokkokat tartalmazm amelyek le�rj�k a nyomtat� adotts�gait.
�gy n�z ki egy blokk:

.PRINTER
   .name:                Stylus C20SX
   .colorsNb:            4
   .mainProt:            D4
   .stateFlg:            True
   .exchangeFlg:         False
   .exchangeSeparateFlg: False
   .cleanSeparateFlg:    True
   .resetProt:           D4
   .alignProt:           OLD D4
   .idFlg:               True
   .passesNb:            3
   .choicesNb:           15
   .colorPassesNb:       2
   .colorChoicesNb:      9
   .alignFunctionName:   -
.END


A .PRINTER sz� jelenti, hogy egy �j le�r�s k�vetkezik. A k�vetkez�
szavakkal eg�szen az .END sz�ig, de nem tartalmazva azt, vannak le�rva
a nyomtat� tulajdons�gai.

K�rem ne kezdjen sort ponttal '.', ez a karakter jelz�s a
ttink/mtink valamint a rdPrtDesc seg�ts�g programnak, hogy egy
le�r� sor k�vetkezik.

A .name: sz� tartalmazza a nyomtat� nev�t, ahogy visszat�r a
nyomtat� n�v k�r�sn�l.

A .colorsNb: jelzi az ttink/mtink-nek, hogy h�nyf�le tinta van haszn�latban.

A v�g�n Prot-tal szerkesztve: jelzi, hogy a programok melyik protokollt
haszn�lj�k.

Az �jabb nyomtat�k ismernek egy �j protokollt, amit D4-nek neveznek.
Ez az �j protokoll engedi kommunik�lni a nyomtat�t f�ggetlen
csatorn�kon. Ez engedi p�ld�ul a maradv�ny tinta mennyis�g�nek,
vagy a nyomtat� �llapot�nak lek�rdez�s�t nyomtat�s k�zben.

A D4 protokoll szint�n tartalmaz egy parancsk�szletet, �gy hogy a
legt�bb r�gi parancs ki lett cser�lve. Sajnos nem minden nyomtat�
k�n�lja a D4 parancsok teljes k�szlet�t.
Ennek k��sz�nhet�en �s a kompatibilit�snak a meghajt�khoz, amelyek nem
ismreik a D4 protokollt, a legjobb jelezni a nyomtat�nak, hog a D4 protokoll
nincs enged�lyezve.

Ennek megfelel�en a k�vetkez� kombin�ci�s �rt�kek lehets�gesek:

- OLD       A nyomtat� nem tud semmit a D4-r�l.
- OLD D4    Nem D4 parancsot haszn�l, de ezt a D4 protokollon �t k�ldi.
- OLD EXD4  Haszn�ljon szab�lyos parancsokat �s �ll�tsa a nyomtat�t a kompatibilis m�dba.
- D4        A nyomtat� ismeri a speci�lis D4 parancsot, haszn�lja ezt.

A .mainProt sz�:  meghat�rozza az alap�rtelemezett m�dot.

A .resetProt sz�:	A protokoll t�pus megat�roz�sa a reset parancs legjobb haszn�lat�hoz.
			D4 nyomtat�k mind ismerhetik ezt a parancsot, de nem hajtj�k v�gre
			a m�veletet megfelel�en (p�ld�ul: Stylus Color 980 vagy a Stylus Scan ...).
A .alignProt sz�:	Ez a parancs l�tszik, hogy nem egy D4 m�solat �s mindig a
			klasszikus parancs lesz haszn�latban.
			Itt csak D4-et (OLD bele�rtve) lehet meghat�roznia.

A sz� a v�g�n Flg-vel: jelzi, ha a nyomtat� egy�ni tulajdons�g�. A kijel�lt �rt�k lehet True vagy False.

A .stateFlg sz�:         Jelzi, ha a nyomtat� vissza tud t�rni,
                            ha az foglalt, m�k�dik...

A .cleanSeparateFlg sz�: n�h�ny nyomtat� k�l�n-k�l�n engedi a f�v�k�kat
                            tiszt�tani. Ezen nyomtat�k meghat�roz�sa
                            True.

A .idFlg sz�:            A Stylus Scan 2500 �s tal�n a
                            Stylus Scan 2000 nem fog azonos�t�st visszaadni.
                            Ilyen modellhez ezt False-ra kell �ll�tani.


A .exchangeFlg sz�:         N�h�ny nyomtat�n (Stylus Color 480/580) nem
                               alkalmaztak kezel�gombokat.
                               Ezek a m�veleteket szoftveres �ton v�gzik. Ilyen
                               nyomtat�khoz az �rt�k True.

A .exchangeSeparateFlg sz�: az el�bb eml�tett k�t nyomtat�nak sz�ks�ge van egy
                               nyomtat�patron kiv�laszt�sra, ami a cser�hez kell.
                               Az �rt�ket True-ra kell be�ll�tani.

T�bb nyomtat�, mint a Stylus Photo 890 szoftveres �ton is engedi a
tintapatron cser�j�t. Ha ezt a lehet�s�get akarod haszn�lni, �ll�tsd be a
flag .exchangeFlg-et True-ra �s a .exchangeSeparateFlg-et False-ra.

A marad�k szavak, v�g�k�n Nb-vel:
- .passesNb:
- .ChoicesNb:
- .colorPassesNb:
- .colorChoicesNb:
a fej�ll�t�s folyamat�hoz vannak. Az els� k�t bejegyz�s jelzi, mennyi
enged�lyre van sz�ks�ged, �s mennyi v�laszt�s van enged�lyezve. Ezeket az �rt�kek
az EPSON �ltal szolg�ltatott Windows vagy MacOs meghajt�kban l�that�k.
Ha a nyomtat� nem enged egy be�ll�t�st a sz�nes fejhez, �ll�tsd az
.colorChoicesNb �rt�k�t 0-ra.

Az utols� kulcssz�, az .alignFunctionName speci�lisan a Stylus Photo 820 miatt
ker�lt bevezet�sre. Ez a nyomtat� nem kapott egy szerkezeti k�dot, amelyik nyomtat
egy mint�t. A minta sz�ks�ges a ttink/mtink sz�m�ra. A Stylus Photo 820-hoz �rja
be Pattern820, a t�bbi �rt�k be�ll�t�sa "- ".

Egy printer.desc f�jl telep�t�se:
------------------------------

Ttink/Mtink a printer.desc f�jl megtal�l�s�ra egyik k�vetkez� k�nyvt�rban sz�m�t:

- /usr/lib/mtink
- /usr/local/lib/mtink
- /opt/mtink

Ha a printer.desc f�jlt megtal�lta, a marad�k k�nyvt�rak nincsenek tov�bb kutatva.


Ha m�dos�tani akarja egy adott nyomtat� le�r�s�t ,vegyeki az adatot rdPrtDesc-vel
�s m�dos�tsa az �rt�ket a keresett f�jlhoz, azut�n m�solja az eredm�ny printer.desc
f�jlt egy fent eml�tett k�nyvt�rhoz.

A le�r�s felismer�s a printer.desc f�jlban �rv�nyes�l.

Egy �j nyomtat� beszerkeszt�se a ttink/mtink-be:
-----------------------------------------

Hozza l�tre a saj�t printer.desc f�jlt a .../mtink/utils k�nyvt�r alatt
�s h�vja a

rdPrtDesc -c > ../model.c -t

A f�jl a jelenlegi �s az �j nyomtat�hoz ekkor automatikusan gener�l�dik.

Ezut�n t�rjen vissza az mtink k�nyvt�rhoz �s h�vja a make-t �s a make install-t.
