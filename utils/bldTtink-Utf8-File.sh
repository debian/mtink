#!/bin/sh

for f in Ttink.??
do
   lang=`echo $f|sed 's/.*\\.//'`
   case $lang in
      tr) sc=iso8859-9;;
      *)  sc=iso8859-1;;
   esac
   iconv -f $sc -t UTF-8 $f > $f.utf8
done

