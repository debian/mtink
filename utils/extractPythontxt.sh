#!/bin/sh

# extract a few resources from the Mtink files and
# build a language file for pyink:
#
# The produced file will be named Pyink.res and will
# include the textes for all kwnon languages, the
# textes are to be encoded with UTF-8
#


# Needed resources:

Resources='check_PB.labelString
clean_PB.labelString
align_PB.labelString
reset_PB.labelString
cartridge_PB.labelString
printerState_LB.labelString
pref_PB.labelString
cfg2Device_PB.labelString'

#which files
fileList="`ls Mtink.??`"

getRes()
{
   python<<!
res='$res'
lang='$lang'
f=open('$f','r')
while True:
   s = f.readline()
   if s == '':
      break
   s = s[:-1]
   w = s.split('*')
   if len(w) > 1:
      # found a matching line
      m = w[1]
      while m[-1:] == '\\\\':
         m = m[:-1]
         # add next line
         s = f.readline()
         s = s[:-1]
         m = m + s
      l = m.split(':')
      if len(l) > 1:
         if l[0] == res:
            # build now our resource string
            r=m.split('_PB.labelString')
            if len(r) == 1:
               r=m.split('_LB.labelString')
            print lang+'.'+r[0]+r[1]
f.close()
!
}

for f in $fileList
do
   for res in $Resources
   do
      lang=`echo $f|sed 's/.*\\.//'`
      case $lang in
         Tr) sc=iso8859-9;;
         *)  sc=iso8859-1;;
      esac
      #echo $f: $res $lang $sc

      getRes | iconv -f $sc -t UTF-8
   done
done > Pyink.res

