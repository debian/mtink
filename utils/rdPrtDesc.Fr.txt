rdPrtDesc:
----------

Cet utilitaire permet l'extraction de la descrition des param�tres
pour les diff�rents imprimantes et la pr�sentatin de ceux ci
sous forme lisible. De plus il est possible de g�n�rer le fichier
descriptif en language c a l'aide de rdPrtDesc.

Syntaxe: rdPrtDesc -p|-c

-p rdPrtDes cr�e un fichier lisible comprenant les donn�es de
   toutes les imprimantes connues.
   
-c rdPrtDes cr�e un fichier en language c a partir du fichier
   printer.desc situ� dans le r�pertoire courant. Le fichier 
   g�n�r� peut tre utilis� par ttink/mtink.
   Les donn�es des imprimantes d�ja connues sont bien sur prises
   en compte.


Fichier printer.desc:
---------------------

Ce fichier contient des blocs d�crivant les diff�rentes imprimantes.

Chaque bloc se presente de la fa�on suivante:

.PRINTER
   .name:                Stylus C20SX
   .colorsNb:            4
   .mainProt:            D4
   .stateFlg:            True
   .exchangeFlg:         False
   .exchangeSeparateFlg: False
   .cleanSeparateFlg:    True
   .resetProt:           D4
   .alignProt:           OLD D4
   .idFlg:               True
   .passesNb:            3
   .choicesNb:           15
   .colorPassesNb:       2
   .colorChoicesNb:      9
   .alignFunctionName:   -
.END


Le mot clef ".PRINTER" d�clare le d�but d'un bloc descriptif.
Chaque bloc est t�rmin� pas le mot clef ".END".
Les mots clef entre ".PRINTER" et ".END" correspondent � des
charact�ristiques particuli�res et sont suivis d'un argument.

Pr�re de noter que le charact�re '`' a une signification particuli�re
lors ce qu'il se trouve en d�but de ligne ou s�il n'est pr�c�d� que de
charact�res blancs ou des tabulateurs.

Le nom de l'imprimante, tel qu'il est retournu lors d'une interrogation
de l'imprimante doit suivre le mot clef ".name:". 

".colorsNb:" informe ttink/mtink du nombre de couleurs utilis�es.

Les mots clef se terminant avec "Prot:" d�signe quel protocol est
utilis� pour la fonction donn�e.

Les nouvelles imprimantes peuvent, grace au protocol D4, informer
l'utilisateur quand � la quantit� d'encre r�siduelle et a l'etat de
fonctionemen de l'imprimante (impression en cour, ...). Le protocol
D4 utilise pour ceci des canaux ind�pendants.

Le protocol D4 d�finit �galement un certain nombre de commandes
faisant double emploi avec des commandes existantes et permettant
parfois une meilleure fontionalit�. Malheureusement, l'impl�mentation
varie selon les modelles.

Pour cette raison et de plus pou assur� le fonctionnement avec les
imprimantes ne connaissant pas ce proptocol, il est possible de
param�trer diff�rent mode:

- OLD       L'imprimante n'utilise pas le mode D4.
- OLD D4    Utilisation d'une commande ancienne imbriqu�e dnas le
            protocol D4.
- OLD EXD4  L'imprimante connait le mode D4 mais l'imprimante doit 
            �tre mise en mode de compatibilit�.
- D4        L'imprimante connait une commande speciale D4, celle ci
            est utilis�e.

".mainProt:"  declaration du mode par defaut.

".resetProt:" declarearion du protocol pour la commande de remise a
              z�ro. Certaine imprimante D4 connaisent cette commande
              mais les r�sultats ne sont pas cey escompt�s ( Stylus
              Color 980 et Stylus Scan ...)

".alignProt:" Cette commande (alignement des t�tes) n'est pas accessible
              directement par le protocol D4. Il est necessaire de declarer
              aux moins OLD. Suivamt la cat�gorie de l'imprimante il
              faut �ventuellement ajouter EXD$ ou D4.

Les mots clef se terminant par "Flg:" (pour drapeau) sont a suivre par
les mots False (faut) ou True (vrai) en fonction des possinilit�s de
l'imprimante.

".stateFlg:"  indique si l'imprimante peu retourner des informations
              concernamt sont �tat (imprime, erreur, ...).

".cleanSeparateFlg:" un faible nombre d'imprimante permettent le
              nettyage des buses de fa�on selective. Dans ce cas
              d�clarer True.

".idFlg:"     La Stylus Scan 2500 and probablement laStylus Scan 2000
              ne  retournent pas la chaine d'identification.
              Mettre le drapeau sur false pour ce mat�riel.
                             

".exchangeFlg:" Un certain nombre d'imprimantes (Stylus Color 480/580)
              ne poss�de pas de possoir permettant l'enclenchement du
              processus d'�change des cartouches d'encre. Une solution
              logicielle est neccesaire pour celle ci (valeur True).

".exchangeSeparateFlg:" Les 2 imprimantes suscit�e n�cessite l'envoi
              de commandes pour les 2 types de cartouche d'encre.
              La valeur dot �tre True.

Certaines imprimantes sont pourvuent d'un poussoir pour le changement
des carouches et accepte cependant une commande � partir du logiciel. 
Si vous d�sirez avoir cette possibilit�, placer la valeur True pour
le mot clef  ".exchangeFlg:".

Les mot clef se terminants par "Nb:" concernent l'alignement des
t�tes. 
".passesNb:"
".ChoicesNb:"
".colorPassesNb:"
".colorChoicesNb:"
Les 2 premiers mots clef pr�cide le nombre de passes et de choix
pour l'alignement standard.
Les 2 mots clef suivant permettent de notr les param�tres pour
l'alignement des t�tes couleur. Si cette possibilit� n'existe pas
il est neccessaire d'affecter 0 � ".colorPassesNb:"

Le dernier mot clef a �t� introduit en raison du manque de la
fonction d'impression des lignes d'ajustement pour la Stylus
Photo 820. La valeur est normalement -, pour la Stylus Photo
820 il faut inscrire Pattern820.

Installation du fichier printer.desc:
-------------------------------------

Ttink/Mtink recherchent le fichier printer.desc dans les r�pertoires
suivants:

- /usr/lib/mtink
- /usr/local/lib/mtink
- /opt/mtink

Si le fichier est trouv�, celui ci est lut et la recherche
est abondonn�e.

Si vous direz modifier la description de v�tre imprimante,
vous pouvez extraire les information a l'aide de l'utilitaire
rdPrtDesc et effectuer les changements souhait�s.
Apr�s cette operation il suffit de copier le fichier sous un des
r�pertoires cit�s plus haut.

Installer une nouvelle imprimante dans ttink/mtink:
---------------------------------------------------

g�n�rez le fichier printer.desc dans le r�pertoires .../mtink/utils
et executez la commande 

rdPrtDesc -c > ../model.c

Le fichier model.c contient les donn�es de la novelle imprimante
ainsi que les modelles d�ja connus
 
Avec "make; make install" vous pouvez compiler et installer les
programmes.
