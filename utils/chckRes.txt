chckRes
-------

This little utility compare 2 resource files.

Example:

------------------------------------
$ ./chckRes Ttink.en Ttink.it
Missed resources
 <.llblack:>
 <.Lblack:>
Resources not expected
 <.Hcontinue:>
 <.Wyesorno:>
 <.WsaveCancel:>
 <.sampleNo:>
------------------------------------

Missed resources tell usr that .llblack: and .Lblack:  what found
within the refrence file Ttink.en but not within the checked file
Ttink.it

Resources not expected enumerate the obsolete resources found into the
checked file.

