bldRes:

Ez a program olvassa a megadott forr�s f�jlt �s alkot egy c-f�jlt,
amelyik belefoglaltatik a f� programba.

Syntax: bldRes resource_file_1 .....

Az output a konzolon k�sz�l.

Ha forr�sf�jlt k�v�n alkotni, c�mezZE �jra az outputot
a k�v�nt f�jlhoz.


P�ld�ul:

bldRes Ttink.en Ttink.de > ../mainSrc/tres.c
