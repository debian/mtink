#!/bin/sh

# Add people to the about list for the files Mtink.??

# Set SEARCH to the line which will precede the new entry
# \ must be duplicated


SEARCH='   http://xwgui.automatix.de\\n\\'

# Set APPEND to the line which will is to be added
# \ must be duplicated

APPEND='Rainer Krienke\\n\\\n   krienke@uni-koblenz.de\\n\\'

for f in Mtink.??
do
   awk '{
      if ( $0 == "'"$SEARCH"'" )
      {
         print $0
         print "'"$APPEND"'"
      }
      else
      {
         print $0
      }
   }' $f > $f.new
   mv $f.new $f
done
