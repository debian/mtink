#!/bin/sh

# Changes: 07-MAR-2003 added /usr/local/... path for openMotif
#                      on Mac OS X
#

prefix=/usr/local
setUid=1
OS=`uname`

# set LANG if necessary
case $LANG in
   en*|de*|fr*|it*);; # known languages
   *) LANG=en;;       # default english
esac

prt()
{
   la=$1
   shift
   case $LANG in
     $la*) echo $*;;
   esac
}

pSyntax()
{
   echo Syntax `basename $0` '[-h|--help]'
   echo "      " `basename $0` '[-p|--prefix installPath] [-d devpkgprefix] [-n|--no-suid] [-x|--no-x] [-g]|--no-gimp] [-s|--no-strip]'
   echo
   prt en "Default install path: $prefix"
   prt fr "Chemin d'installation par default: $prefix"
   prt de "Default Installationspfad: $prefix"
   prt it "Percorso d'installazione di default: $prefix"
   echo
   prt en "The documentation files will be installed under:"
   prt fr "Les fichiers de documentation seront installes sous:"
   prt de "Die Dokumentaionsdateien werden installiert unter:"
   prt it "La Documentazione verr� installata in:"
   echo $prefix/mtink
   echo
   prt en "If you want only the tty based version use the -x option."
   prt en
   prt en "This program require read write access to the device"
   prt en "files. You may install mtink with the set uid bit set"
   prt en "or change yourself the permissions to the device files."
   prt en "In this case you may enter the -n option."

   prt fr "Pour ne creer que la version de commande en ligne"
   prt fr "utilise l'option -x."
   prt fr
   prt fr "Le programme necessite les droit d'ecriture et de lecture"
   prt fr "pour les fichiers /dev/... . Vous pouvez installer mtink"
   prt fr "avec le bit set-uid ou modifier vous meme les droits"
   prt fr "d'acces aux fichiers concernes. Dans ce cas vous pouvez"
   prt fr "lancer `basename $0` avec l'option -n"

   prt de "Nur Konsole basierte Version erstellen: Option -x angeben."
   prt de
   prt de "Das Program benoetigt uneingeschraenkte Rechte auf die"
   prt de "Geraetedateien. Sie koennen mtink mit das 'set-uid' bit"
   prt de "installieren. Falls Sie es nicht wuenschen muessen Sie"
   prt de "die Rechte auf der Geraetedateien anpassen und `basename $0`"
   prt de "mit der Option -n aufrufen."

   prt it "Se si vuole solo la versione a linea di comando usare -x."
   prt it
   prt it "Questo programma richiede l'accesso in lettura e scrittura al"
   prt it "file dispositivo. Si pu� installare mtink con il bit sed uid"
   prt it "attivato o cambiare i permessi al file dispositivo."
   prt it "In questo caso si pu� usare l'opzione -n."
   echo
}

noX=0
noGimp=0
useUsbLib=0
Strip=1

# Check for args
while [ $# -ne 0 ]
do
   case $1 in
   -m)
      # simulation with libusb under Linux
      useUsbLib=1;;
   -h|--h*)
      pSyntax
      exit 0;;
   -n|--no-su*)
      setUid=0
      ;;
   -x|--no-x)
      noX=1
      ;;
   -g|--no-g*)
      noGimp=1
      ;;
   -p|--p*)
      shift
      if [ $# -ne 1 ]
      then
        pSyntax
        exit 1
      else
         prefix=$1
      fi;;
   -s|-nostrip|--no-st*)
      Strip=0
      ;;
   *)
      pSyntax
      exit 1;;
   esac
   shift
done

# checks for Motif library and include files.
# check first for 64 bit library (Nicola Fabiano on Fedora Core 2)
LIBDIRS='/usr/X11R6/lib64
/usr/OpenMotif/lib
/sw/lib
/usr/X11R6/lib
/usr/OpenMotif/lib
/usr/lib
/usr/X11/lib
/usr/openwin/lib
/usr/local/lib
/usr/dt/lib'

# Add gimp-2.0 for fedora core 2
INCLDIRS='/usr/include/gimp-2.0/libgimp
/usr/OpenMotif/include
/sw/include
/usr/X11R6/include
/usr/OpenMotif/include
/usr/X11/include
/usr/include
/usr/openwin/include
/usr/local/include
/usr/dt/include'

mlibFound=0
mxpmheader=
mxpmlib=
for dir in $LIBDIRS
do
   if [ "`ls -c $dir/libXm.* 2>/dev/null`" != "" ]
   then
      mlibFound=1
      break
   fi
done

minclFound=0
for dir in $INCLDIRS
do
   if [ -f $dir/Xm/Xm.h ]
   then
      minclFound=1
      if [ -f $dir/Xm/XpmP.h ]
      then
         mxpmheader="-DMOTIF_XPM"
      fi
      break
   fi
done

xinclFound=0
for dir in $INCLDIRS
do
   if [ -f $dir/X11/X.h ]
   then
      xinclFound=1
      if [ "$mxpmheader" = "" ]
      then
         if [ -f $dir/X11/xpm.h ]
         then
            mxpmheader="-DXPM_XPM"
            mxpmlib="-lXpm"
         fi
      fi
      break
   fi
done

if [ $noX -eq 0 ]
then
   if [ $xinclFound -eq 0 -o $minclFound -eq 0 -o $mlibFound -eq 0 ]
   then
      if [ $mlibFound -eq 0 ]
      then
         prt en "The Motif libraries are not installed."
         prt en "Install Motif or Lesstif if your system"
         prt en "don't provide Motif."
         prt en "Please note that the motif package may be"
         prt en "called openmotif, motif or lesstif (Linux)."

         prt fr "La bibliotheque Motif n'est pas installee."
         prt fr "Installez Motif ou, si votre Systeme ne contient"
         prt fr "pas Motif / Openmotif installez Lesstif (Linux)."

         prt de "Die Motif Umgebung wurde nicht gefunden."
         prt de "Installieren Sie bitte Motif / Openmotif oder,"
         prt de "falls nicht vorhanden Lesstif (Linux)."

         prt it "Le librerie Motif non sono installate."
         prt it "Installare Motif o Lesstif se il sistema non"
         prt it "ha Motif."
         prt it "Notare che il paccehtto Motif pu� aver"
         prt it "nome openmotif, motif o lesstif (Linux)."
         echo
      fi

      if [ $minclFound -eq 0 ]
      then
         prt en "Motif/Lesstif header file not found !"
         prt en "mtink require this, please install them."

         prt fr "Les fichiers includes Motif/Lesstif ne sont pas"
         prt fr "present! Installe Motif/Lesstif correctement."

         prt de "Die Motif/Lesstif Include-Dateien wurden nicht"
         prt de "gefunden! Installieren Sie bitte Motif/Lesstif"
         prt de "vollstaendig."

         prt it "Il file header di Motif/Lesstif non sono stati trovati !"
         prt it "mtink ne ha bisogno, si prega di installarlo."
         echo
         if [ "`uname`" = Linux ]
         then
            prt en "The Motif package is mostly called motif-devel"
            prt en "If you have installed Lesstif the package name"
            prt en "will probably be lesstif-devel"

            prt fr "Le paquetage est souvent nomme motif-devel"
            prt fr "Si vous avez installe Lesstif le nom du paquet"
            prt fr "manquant est certainement lesstif-devel"

            prt de "Sie muessen das Entwicklungspacket motif-devel Bzw."
            prt de "openmotif-devel installieren. Falls Lesstif zum"
            prt de "Einsatzkommen soll is das Packetname meistens"
            prt de "lesstif-devel"

            prt it "Il pacchetto Motif � spesso chiamato motif-devel"
            prt it "Se avessi installato Lesstif il nome del pacchetto"
            prt it "dovrebbe probabilmente essere lesstif-devel"
            echo
         fi
         echo
      fi

      if [ $xinclFound -eq 0 ]
      then
         prt en "X is not installed or the include pathes are stranges"

         prt fr "X n'est pas apparement pas installe ou les fichier 'include'"
         prt fr "se trouvent sous un repertoire non standard."

         prt de "X scheint nicht installiert zu sein. Moeglicherweise"
         prt de "befinden sich die Dateien unter einen nicht Standardpfad"

         prt it "X non e installato o i percorsi di inclusione non sono standard."

         if [ "`uname`" = Linux ]
         then
            prt en "The X development package is mostly called X-devel"

            prt fr "Les paquets de developement X ne sont pas installes."
            prt fr "Installez les, le non est generalement X-devel"

            prt de "Die X.Enticklungsumgebung muss installiert werden."
            prt de "Das Packetname ist meistens X-devel"

            prt it "Il pacchetto di sviluppo X � spesso chiamato X-devel."
         fi
         echo
      fi
      exit 1
   fi

   case $OS in
      Linux) . ./checkMotifVersion.sh;;
      Darwin) . ./checkMotifVersion.sh;;
   esac

   if [ "$MotifLib" != "" ]
   then
      mlCmd="s@^MOLIB.*@MOLIB = $MotifLib@"
   else
     mlCmd=
   fi
   if [ "$XmHPath" != "" ]
   then
      miCmd="s@^MOINC.*@MOINC = -I$XmHPath@"
   else
      miCmd=
   fi

   # Check for the Xp libary, some Motif libraries need this

   xplibFound=0
   for dir in $LIBDIRS
   do
      if [ "`ls -c $dir/libXp.* 2>/dev/null`" != "" ]
      then
         xplibFound=1
         break
      fi
   done

   # but not for Solaris
   if [ `uname` = SunOS ]
   then
      xplibFound=0
   fi

   # Gimp part ################################################
   # check for gimp
   includeFileFound=0
   gimptoolFound=0
   gimpVersion=0

   if [ $noGimp -eq 0 ]
   then
     for dir in `echo $PATH | tr ':' '\012'`
     do
        if [ -x $dir/gimptool-2.0 ]
        then
           # and ask for the version
           gimpVersion=`$dir/gimptool-2.0 --version | sed 's/\(1\..\).*/\1/'`
           gimptoolFound=2
           break
        fi
        if [ -x $dir/gimptool ]
        then
           # and ask for the version
           gimpVersion=`$dir/gimptool --version | sed 's/\(1\..\).*/\1/'`
           gimptoolFound=1
           break
        fi
     done
   fi

   if [ $gimptoolFound -eq 0 ]
   then
      if [ $noGimp -eq 0 ]
      then
         prt en "Gimp development package not installed."
         prt en "Plugins will not be compiled"
         prt en "Please install the gimp package"
         prt en "if you want the gimp plugins."

         prt fr "Le paquet de developement gimp n'est pas installe,"
         prt fr "Le plugins ne sera pas genere."
         prt fr "Installez gimp et gimp-devel si vous souhaitez"
         prt fr "le plugins gimp."

         prt de "Das Gimp-Entwiclungs packet ist nicht installieert,"
         prt de "Das plugins wird nicht erzeugt."
         prt de "Installieren Sie, bitte, gimp-devel (Linux) Wenn Sie"
         prt de "das Gimp-Plugins erzeugen moechten."

         prt it "Il pacchetto di sviluppo Gimp non e installato."
         prt it "I plugin non verranno compilati."
         prt it "Installare il pacchetto gimp"
         prt it "se si vogliono i plugin gimp."
         echo
      fi
   else
      # Build MTINK_CFLAGS and LFLAGS
		if [ $gimptoolFound -eq 2 ]
		then
         MTINK_CFLAGS=`gimptool-2.0 --cflags`
         LFLAGS=`gimptool-2.0 --libs`
		else
         MTINK_CFLAGS=`gimptool --cflags`
         LFLAGS=`gimptool --libs`
      fi

      # Check for Header files libgimp/gimp.h
      includeFileFound=0
      for dir in `echo $MTINK_CFLAGS | tr ' ' '\012' | grep -- -I | sed 's/-I//'`\
                 /usr/include /usr/local/include
      do
         if [ -f $dir/libgimp/gimp.h ]
         then
            includeFileFound=1
            break
         fi
      done

      if [ $includeFileFound -eq 0 ]
      then
         prt en "Header file for gimp not found."
         prt en "Please install the gimp development package."

         prt fr "Les fichiers necessaire pour gimp ne sont pas installes."
         prt fr "Installez le paquet gimp-devel."

         prt de "Die notwendige Entwicklungsumgebung fuer gimp ist."
         prt de "nicht vorhanden. Packet gimp-devel bitte installieren."

         prt it "File header per gimp non trovato."
         prt it "Installare il pacchetto di sviluppo gimp."

      fi
   fi

   # Set a few variable according to OS and components found
   guiPrg=
   instGuiPrg=
   if [ $noX -eq 0 ]
   then
      guiPrg='mtink mtinkc'
      instGuiPrg=install-mtink
   fi

   if [ `uname` = "Linux" ]
   then
      otherPrg='mtinkd $(DETDIR)/askPrinter'
      installOther='install-askPrinter install-mtinkd install-start install-cups'
   fi

   if [ "$OS" = "Darwin" -o $useUsbLib -eq 1 ]
   then
      otherPrg=
      installOther=install-cups
   fi

   if [ $gimptoolFound -ge 1 ]
   then
      installOther="$installOther  install-plugins"
      otherPrg="$otherPrg gimp-mtink"
   fi
   prgs="$guiPrg ttink $otherPrg"

   instPrgs="$instGuiPrg install-ttink install-desc install-pyink $installOther"

   # build Makefile.

   cat Makefile.ORG |\
   if [ "$OS" != "Darwin" -a $useUsbLib -eq 0  ]
   then
      sed -e 's/^MACOS.*//'
   else
      if [ $useUsbLib -eq 1 ]
      then
         sed -e 's/^MACOSLDFLG=.*/MACOSLDFLG=-lusb -lpthread $(MACLB)/'
      else
         cat -
      fi
   fi |\
   if [ "$xplibFound" = 1 ]
   then
      sed  -e 's/LIBXP .*/LIBXP = -lXp/'
   else
      sed  -e 's/LIBXP .*/LIBXP =/'
   fi |\
   if [ "$mxpmheader" != "" ]
   then
      sed -e 's@^XPM_INC.*@XPM_INC = '$mxpmheader'@' \
          -e 's@^XPM_LIB.*@XPM_LIB = '$mxpmlib'@'
   else
      sed -e 's@^XPM_INC.*@XPM_INC =@' \
          -e 's@^XPM_INC.*@XPM_LIB =@'
   fi |\
   if [ "$gimptoolFound" -ge 1 ]
   then
      sed  -e "s@GIMP_INC .*@GIMP_INC = $MTINK_CFLAGS@" \
           -e "s@GIMP_LIB .*@GIMP_LIB = $LFLAGS@" \
           -e "s@^gimpVersion=.*@gimpVersion=$gimpVersion@"
   else
      cat -
   fi |\
   sed -e "s@PRG .*@PRG = $prgs@" \
       -e "s@^install:.*@install: $instPrgs@" |\
   if [ "$gimpVersion" = "1.0" ]
   then
      sed  -e 's@\(GIMP_INC .*\)@\1 -DGIMP_VERSION_1@'
   else
      if [ "$gimptoolFound" -gt 1 ]
      then
         sed  -e 's@\(GIMP_INC .*\)@\1 -DGIMP_VERSION_2@'
       else
         cat -
      fi
   fi  |\
   sed -e "s@PRG .*@PRG = $prgs@" \
       -e "s@^install:.*@install: $instPrgs@" |\
   if [ "$gimpVersion" = "1.0" ]
   then
      sed  -e 's@\(GIMP_INC .*\)@\1 -DGIMP_VERSION_1@'
   else
      if [ "$gimptoolFound" -gt 1 ]
      then
         sed -e 's@\(GIMP_INC .*\)@\1 -DGIMP_VERSION_2@'
      else
         cat -
      fi
   fi |\
   if [ "$Strip" = "0" ]
   then
      sed -e 's/^DBG.*/DBG = -g/'
   else
      cat -
   fi |\
   if [ $OS = Darwin ]
   then
       sed 's/^#\(MACIN.*\)/\1/'
   else
      cat -
   fi | sed -e "s@^prefix.*@prefix=$prefix@" \
            -e "s@^setUid.*@setUid=$setUid@" \
            -e "$mlCmd" -e "$miCmd"   > Makefile

   # and server install script
   sed -e 's@^PROG.*@PROG='$prefix'/sbin/mtinkd@' etc/mtink.ORG > etc/mtink

   if [ -d $prefix/doc ]
   then
      docDir=$prefix/doc
   else
      docDir=$prefix
   fi
   prt en "Binary path:"
   prt fr "Chemin d'installation de l'executable:"
   prt de "Installationspfad fuer das ausfuehrbares Programm:"
   prt it "Percorso di installazione dell'eseguibile:"
   echo $prefix/bin
   echo
   prt en "The documentation files will be installed under:"
   prt fr "Les fichiers de documentation seront installes sous:"
   prt de "Die Dokumentationsdateien werden installiert unter:"
   prt it "La Documentazione verr� installata in:"
   echo $docDir/mtink
   echo

   if [ "$gimpVersion" != "" ]
   then
      echo Gimp version  $gimpVersion found / trouve / gefunden / trovato / tal�lva. | $CONV
      echo
   fi
else # no x file to build
   # build Makefile.
   # Set a few variable according to OS and components found
   guiPrg=
   instGuiPrg=

   if [ `uname` = "Linux" ]
   then
      otherPrg='mtinkd $(DETDIR)/askPrinter'
      installOther='install-askPrinter install-mtinkd install-start install-cups'
   fi

   if [ "$OS" = "Darwin" -o $useUsbLib -eq 1 ]
   then
      otherPrg=
      installOther=install-cups
   fi

   prgs="$guiPrg ttink $otherPrg"

   instPrgs="$instGuiPrg install-ttink install-desc install-pyink $installOther"

   cat Makefile.ORG |\
   if [ "$Strip" = "0" ]
   then
      sed -e 's/^DBG.*/DBG = -g/'
   else
      cat -
   fi |\
   if [ "$OS" != "Darwin" ]
   then
      sed -e 's/^MACOS.*//'
   else
      cat -
   fi |\
   sed -e "s@PRG .*@PRG = ttink $detectPrg $svrPrg@" |\
   sed -e "s@^prefix.*@prefix=$prefix@" \
       -e "s@^setUid.*@setUid=$setUid@" \
       -e "s@^install:.*@install: $instPrgs@" > Makefile
   # and server install script
   sed -e 's@^PROG.*@PROG='$prefix'/sbin@' etc/mtink.ORG > etc/mtink

   if [ -d $prefix/doc ]
   then
      docDir=$prefix/doc
   else
      docDir=$prefix
   fi
   prt en "Binary path:"
   prt fr "Chemin d'installation de l'executable:"
   prt de "Installationspfad fuer das ausfuehrbares Programm:"
   prt it "Percorso di installazione dell'eseguibile:"
   echo $prefix/bin
   echo
   prt en "The documentation files will be installed under:"
   prt fr "Les fichiers de documentation seront installes sous:"
   prt de "Die Dokumentationsdateien werden installiert unter:"
   prt it "La Documentazione verr� installata in:"
   echo $docDir/mtink
   echo
fi   
