#!/bin/sh

#set -x
dirs='/usr/X11R6/lib /usr/openwin/lib /usr/dt/lib'

libs='Xm ICE SM Xp Xext Xt X11'


for d in $dirs
do
   for f in $libs
   do
      if [ -f $d/lib${f}.a ]
      then
         ln -s $d/lib${f}.a lib${f}.a
      fi
   done
done

#LFLAGS=' -L/usr/X11R6/lib -L/usr/openwin/lib -L/usr/dt/lib  -s -L. -lXm -lXp -lXext -lSM -lICE -lXt -lX11'
#OBJ='mtink.o d4lib.o d4libh.o cfg1.o fsb.o cfg2.o alignbt.o scrtxt.o cmd.o tooltip.c' 
#rm mtink >/dev/null 2>&1
#gcc -s -o mtink $OBJ $LFLAGS $LFLAGS

#rm *.a
