#include <stdio.h>
#include <string.h>

void fillBuf(unsigned char *t, unsigned char *s, int len)
{
   while ( len-- > 0 )
   {
      *t++ = *s++;
   }
}

int bldAlignPattern(FILE *fp, int bufpos, unsigned char *printer_cmd, int bufSize)
{
   unsigned char  obuf[1024];
   char           ibuf[1024];
   int            len;
   char          *s;
   int            repeat;

   /* read a line */
   while( fgets(ibuf, sizeof(ibuf), fp) )
   {
      /* eliminate CR */
      s = strchr(ibuf, '\n');
      if ( s )
        *s = '\0';
        
      /* initialize */
      len = 0;
      s   = ibuf;
      if ( *s == '0' && s[1] == 'c' )
      {
         /* Form Feed, end of code ! */
         obuf[0] = 12;
         len = 1;
         if ( bufpos + len > bufSize )
         {
            return 0;
         }
         fillBuf(printer_cmd+bufpos, obuf, len);
         bufpos += len;
         break;
      }

      /* process each character */
      while(*s )
      {
         if ( *s == '#' )
         {
             /* comments the line was processed */
             break;
         }
         else if ( *s == '[' )
         {
             /* repeat pattern */
             repeat = 1;
             while ( repeat )
             {
                /* write out the stored values */
                while ( repeat )
                {
                   if ( bufpos + len > bufSize )
                      return 0;
                   fillBuf(printer_cmd+bufpos,obuf, len);
                   bufpos += len;
                   repeat--;
                }
                if ( *s == '[' )
                   s++;
                if ( *s == '#' )
                   break;
                /* read the number of repetition */
                len = 0;
                repeat = strtol(s,NULL, 10);
                while ( *s && *s != ']' )
                   s++;
                s++;
                while ( *s == ' ' )
                   s++;
                while(*s && (*s != '#' || *s != '['))
                {
                   obuf[len] = strtol(s, NULL, 16);
                   len++;
                   s += 2;
                   while ( *s == ' ' )
                      s++;
                }
             }
         }
         else
         {
                 /* normal characters */
            obuf[len] = strtol(s, NULL, 16);
            len++;
                 s += 2;
         }

         /* skip spaces */
         while ( *s == ' ')
         {
            s++;
         }
      }

      /* printout the buffer */
      if ( bufpos + len > bufSize )
         return 0;
      fillBuf(printer_cmd+bufpos,obuf, len);
      bufpos += len;
   }
   return bufpos;
}

int interpretFile(char *name, int bufpos, unsigned char *printer_cmd, int bufSize)
{
   FILE *fp;
   char dir_name[1024];
   int  pos;
   /* load file name located under /usr/local/lib/mtink or
    * /usr/mtink/lib/mtink
    */

   snprintf(dir_name, sizeof(dir_name),"/usr/local/lib/mtink%s", name);
   if ( (fp = fopen(dir_name, "r")) == NULL)
   {
      snprintf(dir_name, sizeof(dir_name),"/usr/lib/mtink%s", name);
      fp = fopen(dir_name, "r");
   }
   if ( fp == NULL )
   {
      return 0;
   }
   
   pos = bldAlignPattern(fp, bufpos, printer_cmd, bufSize );
   fclose(fp);
   return pos;
}
