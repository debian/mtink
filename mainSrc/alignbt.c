/* file alignbt.c
 *
 *  Windows for head alignment
 *
 */

/*
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <strings.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xlib.h>
#include <Xm/Xm.h>
#include <Xm/MwmUtil.h>
#include <X11/Core.h>
#include <X11/Shell.h>
#include <Xm/PanedW.h>
#include <Xm/MainW.h>
#include <Xm/Form.h>
#include <Xm/ScrollBar.h>
#include <Xm/ScrolledW.h>
#include <Xm/List.h>
#include <Xm/Separator.h>
#include <Xm/RowColumn.h>
#include <Xm/Text.h>
#include <Xm/RowColumn.h>
#include <Xm/ToggleB.h>
#include <Xm/PushB.h>
#include <Xm/DrawnB.h>
#include <Xm/Label.h>
#include <Xm/MessageB.h>
#include <Xm/MwmUtil.h>
#include <Xm/Scale.h>

#include "mtink.h"

static Widget      alignWindow;
static Widget      alignNext_PB;
static Widget      align_TF;
static Widget      title_LB;
static Widget      separator;
static Widget      button_FRM;
static Widget      align_PB[15];

static int alignPattern = 0;
static int loop         = 0;

/* create the coonfiguration window */
static void alignNext_CB(Widget w, XtPointer clientData, XtPointer callData);
static void align_CB(Widget w, XtPointer clientData, XtPointer callData);


/*******************************************************************/
/* Function createAlignButtons()                                   */
/*                                                                 */
/* Create the window for aligment                                  */
/*                                                                 */  
/* Input: Widget parent not used                                   */
/*        int   *next   not used                                   */
/*                                                                 */
/* return: -                                                       */
/*                                                                 */
/*******************************************************************/

Widget createAlignButtons(Widget parent, int *next)
{
   XmString    xms = NULL;
   Arg         args[20];
   int         n = 0;
   char        buf[10];
   char        nm[20];
   int         i,j;
   Dimension   h;

   alignWindow = XtVaCreateWidget("alignWindow",
                                 xmFormWidgetClass,
                                 mainForm,
                                 XmNmarginHeight,     0,
                                 XmNmarginWidth,      0,
                                 XmNresizePolicy,     XmRESIZE_GROW,
                                 XmNresizable,        True,
                                 XmNleftAttachment,   XmATTACH_FORM,
                                 XmNrightAttachment,  XmATTACH_FORM,
                                 XmNtopAttachment,    XmATTACH_FORM,
                                 XmNbottomAttachment, XmATTACH_FORM,
                                 NULL);

   if (alignWindow == NULL)
   {
      return False;
   }
   XtManageChild(alignWindow);

   title_LB = XtVaCreateWidget("title_LB",
                             xmLabelWidgetClass,
                             alignWindow,
                             XmNleftAttachment,   XmATTACH_FORM,
                             XmNleftOffset,       5,
                             XmNrightAttachment,  XmATTACH_FORM,
                             XmNrightOffset,      5,
                             XmNtopAttachment,    XmATTACH_FORM,
                             XmNtopOffset,        10,
                             XmNalignment,        XmALIGNMENT_CENTER,
                             NULL);

   XtManageChild(title_LB);

   /* create a Form  */

   n = 0;
   XtSetArg(args[n], XmNleftAttachment, XmATTACH_FORM); n++;
   XtSetArg(args[n], XmNleftOffset, 5); n++;
   XtSetArg(args[n], XmNrightAttachment, XmATTACH_FORM); n++;
   XtSetArg(args[n], XmNrightOffset, 5); n++;
   XtSetArg(args[n], XmNshadowThickness, 0); n++;

   XtSetArg(args[n], XmNnumColumns, 1); n++;
   button_FRM = XmCreateForm(alignWindow, "button_FRM",args,n);
   XtManageChild(button_FRM);

   for ( i = 0; i < 3; i++ )
   {
      for ( j = 0; j < 5; j++ )
      {
         sprintf(buf,"%d",(i*5)+j+1);

         sprintf(nm,"align_%d",(i*5)+j+1);

         xms = XmStringCreateSimple(buf);
         align_PB[(i*5)+j] = XtVaCreateWidget(nm,
                              xmDrawnButtonWidgetClass,
                              button_FRM,
                              XmNlabelString,      xms,
                              XmNalignment,        XmALIGNMENT_CENTER,
                              XmNleftAttachment,   XmATTACH_POSITION,
                              XmNleftOffset,       5,
                              XmNleftPosition,     20*j,
                              XmNrightAttachment,  XmATTACH_POSITION,
                              XmNrightOffset,      5,
                              XmNrightPosition,    20*j+20,
                              XmNtopAttachment,    XmATTACH_POSITION,
                              XmNtopOffset,        5,
                              XmNtopPosition,      33*i,
                              XmNshadowType,       XmSHADOW_OUT,
                              NULL);
         XtManageChild(align_PB[(i*5)+j]);
         XmStringFree(xms);
         XtAddCallback(align_PB[(i*5)+j], XmNactivateCallback, align_CB, (XtPointer) (i*5)+j);
      }

   }

   XtVaGetValues(align_PB[0], XmNheight,&h,NULL );
   XtVaSetValues(button_FRM,
                 XmNheight,(h+4)*3,
                 XmNresizePolicy, XmRESIZE_NONE,
                 NULL);

   alignNext_PB = XtVaCreateWidget("next_PB",
                            xmPushButtonWidgetClass,
                            alignWindow,
                            XmNleftAttachment,   XmATTACH_POSITION,
                            XmNleftOffset,       5,
                            XmNleftPosition,     65,
                            XmNrightAttachment,  XmATTACH_POSITION,
                            XmNrightOffset,      5,
                            XmNrightPosition,    95,
                            XmNbottomAttachment, XmATTACH_FORM,
                            XmNbottomOffset,     5,
                            NULL);
   XtManageChild(alignNext_PB);


   separator = XtVaCreateWidget("separator",
                               xmSeparatorWidgetClass,
                               alignWindow,
                               XmNbottomAttachment,   XmATTACH_WIDGET,
                               XmNbottomOffset,       5,
                               XmNbottomWidget,       alignNext_PB,
                               XmNleftAttachment,     XmATTACH_FORM,
                               XmNleftOffset,         0,
                               XmNrightAttachment,    XmATTACH_FORM,
                               XmNrightOffset,        0,
                               NULL);
   XtManageChild(separator);

   XtVaSetValues(button_FRM,
                 XmNbottomAttachment,   XmATTACH_WIDGET,
                 XmNbottomOffset,       5,
                 XmNbottomWidget,       separator,
                 NULL);
   n = 0;
   XtSetArg(args[n], XmNleftAttachment,        XmATTACH_FORM); n++;
   XtSetArg(args[n], XmNleftOffset,            5); n++;
   XtSetArg(args[n], XmNrightAttachment,       XmATTACH_FORM); n++;
   XtSetArg(args[n], XmNrightOffset,           5); n++;
   XtSetArg(args[n], XmNtopAttachment,         XmATTACH_WIDGET); n++;
   XtSetArg(args[n], XmNtopOffset,             10); n++;
   XtSetArg(args[n], XmNtopWidget,             title_LB); n++;
   XtSetArg(args[n], XmNbottomAttachment,      XmATTACH_WIDGET); n++;
   XtSetArg(args[n], XmNbottomOffset,          5); n++;
   XtSetArg(args[n], XmNbottomWidget,          button_FRM); n++;
   XtSetArg(args[n], XmNalignment,             XmALIGNMENT_BEGINNING); n++;
   XtSetArg(args[n], XmNalignment,             XmALIGNMENT_BEGINNING); n++;
   XtSetArg(args[n], XmNcursorPositionVisible, False); n++;
   XtSetArg(args[n], XmNeditable,              False); n++;
   XtSetArg(args[n], XmNeditMode,              XmMULTI_LINE_EDIT); n++;
   XtSetArg(args[n], XmNwordWrap,              True); n++;
   XtSetArg(args[n], XmNscrollHorizontal,      False);n++;
   XtSetArg(args[n], XmNvalue,                 ""); n++;
   align_TF = XmCreateScrolledText(alignWindow, "align_TF", args, n);

   XtManageChild(align_TF);

   XtVaSetValues(align_TF,
                 XmNbottomAttachment,   XmATTACH_WIDGET,
                 XmNbottomOffset,       5,
                 XmNbottomWidget,       button_FRM,
                 NULL);

   XtAddCallback(alignNext_PB, XmNactivateCallback, alignNext_CB, &loop);
   align_CB(align_PB[7], (XtPointer)7, NULL);
   return alignWindow;
}

/*******************************************************************/
/* Function align_CB()                                             */
/*                                                                 */
/* Set the pushbutton to the correct state (armed/disarmed)        */
/*                                                                 */  
/* Input: Widget     w          not used                           */
/*        XtPointer  clientData pushbutton number                  */
/*        XtPointer  callData   not used                           */
/*                                                                 */
/* return: -                                                       */
/*                                                                 */
/*******************************************************************/

static void align_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   int where = (int)clientData;
   Pixel bg;
   Pixel abg;

   if ( where != alignPattern )
   {
      XtVaGetValues(alignNext_PB,
                    XmNbackground, &bg,
                    XmNarmColor,   &abg,
                    NULL);
      XtVaSetValues(align_PB[alignPattern],
                    XmNshadowType, XmSHADOW_OUT,
                    XmNbackground, bg,
                    NULL);
      alignPattern = where;
      XtVaSetValues(align_PB[where],
                    XmNshadowType, XmSHADOW_IN,
                    XmNbackground, abg,
                    NULL);
   }
}

/*******************************************************************/
/* Function alignNext_CB()                                         */
/*                                                                 */
/* set loop flag to 1                                              */
/*                                                                 */  
/* Input: Widget     w          not used                           */
/*        XtPointer  clientData not used                           */
/*        XtPointer  callData   not used                           */
/*                                                                 */
/* return: -                                                       */
/*                                                                 */
/*******************************************************************/

static void alignNext_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   *((int*)clientData) = 1;
}

/*******************************************************************/
/* Function popupAlignNextWindow()                                 */
/*                                                                 */
/* popup the mask and wait for user interaction                    */
/*                                                                 */  
/* Input: Widget          old   caller mask                        */
/*        wConfig_data_t *data  not used                           */
/*                                                                 */
/* return: -                                                       */
/*                                                                 */
/*******************************************************************/

void popupAlignNextWindow(Widget old, wConfig_data_t* data)
{
   XmString xms;
   int i;

   /* only 1 button */
   if ( data->bt3 )
   {
      xms = XmStringCreateSimple(data->bt3);
      XtVaSetValues(alignNext_PB, XmNlabelString, xms, NULL);
      XtManageChild(alignNext_PB);
      XmStringFree(xms);
   }

   if ( data->message )
   {
      XtVaSetValues(align_TF, XmNvalue,data->message, NULL);
   }

   if ( data->printerName && data->printerName[0] != '?' )
   {
      xms = XmStringCreateSimple(data->printerName);
      XtVaSetValues(title_LB, XmNlabelString, xms, NULL);
      XmStringFree(xms);
   }

   /* number of choices */
   for ( i = 0; i < 15; i++ )
   {
      if ( i > data->choices ) XtUnmanageChild(align_PB[i]);
      else XtManageChild(align_PB[i]);
   }

   /* manage the window */
   XtUnmanageChild(old);
   XtManageChild(alignWindow);
   
   /* preset middle button */
   align_CB(NULL,(XtPointer)(data->choices/2), NULL);
   XmProcessTraversal(align_PB[data->choices/2], XmTRAVERSE_CURRENT);

  /* enter the private main loop */
   *data->wait = 0;
   loop        = 0;
   while(!loop)
      XtAppProcessEvent(theApp, XtIMAll);
   data->intVal = alignPattern;
   data->actWindow = alignWindow;
   *data->wait = loop;
}
