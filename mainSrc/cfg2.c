/*
 * File cfg2.c
 *
 * MAsk for configuration of mtink
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <strings.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xlib.h>
#include <Xm/Xm.h>
#include <Xm/MwmUtil.h>
#include <X11/Core.h>
#include <X11/Shell.h>
#include <Xm/PanedW.h>
#include <Xm/MainW.h>
#include <Xm/Form.h>
#include <Xm/ScrollBar.h>
#include <Xm/ScrolledW.h>
#include <Xm/List.h>
#include <Xm/Separator.h>
#include <Xm/RowColumn.h>
#include <Xm/TextF.h>
#include <Xm/RowColumn.h>
#include <Xm/ToggleB.h>
#include <Xm/PushB.h>
#include <Xm/Label.h>
#include <Xm/MessageB.h>
#include <Xm/MwmUtil.h>
#include <Xm/Scale.h>

#include "mtink.h"
#include "cfg2.h"
#include "cfg1.h"
#include "fsb.h"
#include "tooltip.h"
#include "rcfile.h"
#include "cmd.h"
#include "micon.h"

Widget autodetect_TB;

static Widget cfg2Next_PB;
static Widget cfg2Previous_PB;
static Widget browser_PB;
static Widget browser_TF;
       Widget tooltip_TB;
static Widget title_LB;
static Widget ConfigureForm;
       Widget cfg2Device_PB;
static Widget cfg2Device_LB;
       Widget cfg2Printer_PB;
static Widget cfg2Printer_LB;

static int loop; 
static configData_t tmpConf;

static void cfg2Next_CB(Widget w, XtPointer clientData, XtPointer callData);
static void cfg2Previous_CB(Widget w, XtPointer clientData, XtPointer callData);
static void selectFile_CB(Widget w, XtPointer clientData, XtPointer callData);
void cfg2Device_CB(Widget w, XtPointer clientData, XtPointer callData);
void cfg2Printer_CB(Widget w, XtPointer clientData, XtPointer callData);

/*******************************************************************/
/* Function createConfigureForm()                                  */
/*                                                                 */
/* Build the configuration mask                                    */
/*                                                                 */
/*******************************************************************/

Widget createConfigureForm(Widget parent, int *next)
{
   Widget      browser_LB;
   Widget      separator;
   XmString    xms = NULL;
   Dimension   h1,h2;
   
   ConfigureForm = XtVaCreateWidget("ConfigureForm",
                                 xmFormWidgetClass,
                                 mainForm,
                                 XmNmarginHeight, 0,
                                 XmNmarginWidth, 0,
                                 XmNleftAttachment,   XmATTACH_FORM,
                                 XmNrightAttachment,  XmATTACH_FORM,
                                 XmNtopAttachment,    XmATTACH_FORM,
                                 XmNbottomAttachment, XmATTACH_FORM,
                                 XmNresizePolicy,     XmRESIZE_GROW,
                                 XmNresizable,        True,
                                 NULL);

   if (ConfigureForm == NULL)
   {
      return NULL;
   }
   XtManageChild(ConfigureForm);

   title_LB = XtVaCreateWidget("title_LB",
                             xmLabelWidgetClass,
                             ConfigureForm,
                             XmNleftAttachment,   XmATTACH_FORM,
                             XmNleftOffset,       5,
                             XmNrightAttachment,  XmATTACH_FORM,
                             XmNrightOffset,      5,
                             XmNtopAttachment,    XmATTACH_FORM,
                             XmNtopOffset,        10,
                             XmNalignment,        XmALIGNMENT_CENTER,
                             NULL);
    
   XtManageChild(title_LB);

   browser_LB = XtVaCreateWidget("browser_LB",
                             xmLabelWidgetClass,
                             ConfigureForm,
                             XmNleftAttachment,   XmATTACH_FORM,
                             XmNleftOffset,       5,
                             XmNtopAttachment,    XmATTACH_WIDGET,
                             XmNtopOffset,        10,
                             XmNtopWidget,        title_LB,
                             XmNalignment,        XmALIGNMENT_BEGINNING,
                             NULL);
    
   XtManageChild(browser_LB);

   browser_PB = XtVaCreateWidget("browser_PB",
                            xmPushButtonWidgetClass,
                            ConfigureForm,
                            XmNrightAttachment,   XmATTACH_FORM,
                            XmNrightOffset,       5,
                            XmNtopAttachment,     XmATTACH_WIDGET,
                            XmNtopWidget,         browser_LB,
                            XmNtopOffset,         10,
                            NULL); 
   XtManageChild(browser_PB);


   browser_TF = XtVaCreateWidget("browser_TF",
                            xmTextFieldWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,    XmATTACH_FORM,
                            XmNleftOffset,        5,
                            XmNrightAttachment,   XmATTACH_WIDGET,
                            XmNrightOffset,       5,
                            XmNrightWidget,       browser_PB,
                            XmNtopAttachment,     XmATTACH_WIDGET,
                            XmNtopWidget,         browser_LB,
                            XmNtopOffset,         10,
                            NULL); 
   XtManageChild(browser_TF);

   XtVaGetValues(browser_TF, XmNheight, &h1, NULL);   
   XtVaGetValues(browser_PB, XmNheight, &h2, NULL);   

   XtVaSetValues(browser_PB, XmNtopOffset, 10 +((h1-h2)/2),NULL);

   /* toggle button for mini help */
   tooltip_TB = XtVaCreateWidget("tooltip_TB",
                            xmToggleButtonWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,    XmATTACH_FORM,
                            XmNleftOffset,        5,
                            XmNtopAttachment,     XmATTACH_WIDGET,
                            XmNtopWidget,         browser_TF,
                            XmNtopOffset,         10,
                            NULL); 
   XtManageChild(tooltip_TB);

   /* toggle button for autodetection allowed or not */
   autodetect_TB = XtVaCreateWidget("autodetect_TB",
                            xmToggleButtonWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,    XmATTACH_FORM,
                            XmNleftOffset,        5,
                            XmNtopAttachment,     XmATTACH_WIDGET,
                            XmNtopWidget,         tooltip_TB,
                            XmNtopOffset,         10,
                            NULL); 
   XtManageChild(autodetect_TB);

   cfg2Device_PB = XtVaCreateWidget("cfg2Device_PB",
                            xmPushButtonWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,    XmATTACH_FORM,
                            XmNleftOffset,        5,
                            XmNtopAttachment,     XmATTACH_WIDGET,
                            XmNtopWidget,         autodetect_TB,
                            XmNtopOffset,         10,
                            NULL); 
   XtManageChild(cfg2Device_PB);


   xms = XmStringCreateSimple("");
   cfg2Device_LB = XtVaCreateWidget("cfg2Device_LB",
                            xmLabelWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,    XmATTACH_WIDGET,
                            XmNleftOffset,        10,
                            XmNleftWidget,        cfg2Device_PB,
                            XmNtopAttachment,     XmATTACH_OPPOSITE_WIDGET,
                            XmNtopWidget,         cfg2Device_PB,
                            XmNtopOffset,         0,
                            XmNbottomAttachment,  XmATTACH_OPPOSITE_WIDGET,
                            XmNbottomWidget,      cfg2Device_PB,
                            XmNbottomOffset,      0,
                            XmNlabelString,       xms,
                            NULL); 
   XmStringFree(xms);
   XtManageChild(cfg2Device_LB);

   cfg2Printer_PB = XtVaCreateWidget("cfg2Printer_PB",
                            xmPushButtonWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,    XmATTACH_FORM,
                            XmNleftOffset,        5,
                            XmNtopAttachment,     XmATTACH_WIDGET,
                            XmNtopWidget,         cfg2Device_PB,
                            XmNtopOffset,         10,
                            NULL); 
   XtManageChild(cfg2Printer_PB);

   xms = XmStringCreateSimple("");
   cfg2Printer_LB = XtVaCreateWidget("cfg2Printer_LB",
                            xmLabelWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,    XmATTACH_WIDGET,
                            XmNleftOffset,        10,
                            XmNleftWidget,        cfg2Printer_PB,
                            XmNtopAttachment,     XmATTACH_OPPOSITE_WIDGET,
                            XmNtopWidget,         cfg2Printer_PB,
                            XmNtopOffset,         0,
                            XmNbottomAttachment,  XmATTACH_OPPOSITE_WIDGET,
                            XmNbottomWidget,      cfg2Printer_PB,
                            XmNbottomOffset,      0,
                            XmNlabelString,       xms,
                            NULL); 
   XmStringFree(xms);
   XtManageChild(cfg2Printer_LB);

   cfg2Previous_PB = XtVaCreateWidget("cancel_PB",
                            xmPushButtonWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,   XmATTACH_POSITION,
                            XmNleftOffset,       5,
                            XmNleftPosition,     65,
                            XmNrightAttachment,  XmATTACH_POSITION,
                            XmNrightOffset,      5,
                            XmNrightPosition,    95,
                            XmNbottomAttachment, XmATTACH_FORM,
                            XmNbottomOffset,     5,
                            NULL); 
   XtManageChild(cfg2Previous_PB);

   cfg2Next_PB = XtVaCreateWidget("save_PB",
                            xmPushButtonWidgetClass,
                            ConfigureForm,
                            XmNleftAttachment,   XmATTACH_POSITION,
                            XmNleftOffset,       5,
                            XmNleftPosition,     5,
                            XmNrightAttachment,  XmATTACH_POSITION,
                            XmNrightOffset,      5,
                            XmNrightPosition,    35,
                            XmNbottomAttachment, XmATTACH_FORM,
                            XmNbottomOffset,     5,
                            NULL); 
   XtManageChild(cfg2Next_PB);


   separator = XtVaCreateWidget("separator",
                               xmSeparatorWidgetClass,
                               ConfigureForm,
                               XmNbottomAttachment,  XmATTACH_WIDGET,
                               XmNbottomOffset,      5,
                               XmNbottomWidget,      cfg2Next_PB,
                               XmNleftAttachment,    XmATTACH_FORM,
                               XmNleftOffset,        0,
                               XmNrightAttachment,   XmATTACH_FORM,
                               XmNrightOffset,       0,
                               NULL); 
   XtManageChild(separator);

   XtAddCallback(cfg2Next_PB,     XmNactivateCallback, cfg2Next_CB,     &loop);
   XtAddCallback(cfg2Previous_PB, XmNactivateCallback, cfg2Previous_CB, &loop);
   XtAddCallback(browser_PB,      XmNactivateCallback, selectFile_CB, browser_TF);
 
   XtAddCallback(cfg2Device_PB,    XmNactivateCallback, cfg2Device_CB,  &loop);
   XtAddCallback(cfg2Printer_PB,   XmNactivateCallback, cfg2Printer_CB, &loop);

   /* set wide for cfg2Printer_PB and     cfg2Device_PB */
   XtVaGetValues(cfg2Printer_PB, XmNwidth, &h1, NULL);   
   XtVaGetValues(cfg2Device_PB,  XmNwidth, &h2, NULL);
   if ( h1 > h2 ) XtVaSetValues(cfg2Device_PB,  XmNwidth, h1, NULL);
   else           XtVaSetValues(cfg2Printer_PB, XmNwidth, h2, NULL);

   return ConfigureForm;
}

/*******************************************************************/
/* Function cfg2Device_CB()                                        */
/*                                                                 */
/* Callback for selection of the port                              */
/*                                                                 */
/*******************************************************************/

void cfg2Device_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   wConfig_data_t data;
   int            loop   = 0;
   char           retBuf[1000];
   XmString       xms;

   Widget wid;
   data.ptrVal      = NULL;
   data.printerName = tmpConf.name;
   data.wait        = &loop;
   data.wType       = QUERY_DEVICE;
   data.bt1         = data.bt2 = NULL;
   data.bt3         = appResourceRec.ok;
   data.message     = NULL;

   doCyclicScan = 0; /* disable cyclic scanning for IQ */

   popupCfg1(ConfigureForm, &data, &tmpConf);

   /* wait for sub window */
   while(!loop)
       XtAppProcessEvent(theApp, XtIMAll);

   /* test for results */
   if ( loop == -1 )
   {
      wid = createNoPrinterBox("noAccess");
      doCyclicScan = 0;
      XtManageChild(wid);
   }

   if ( tmpConf.dev && data.ptrVal && strcmp(tmpConf.dev,data.ptrVal) )
   {
      callPrg(TERMINATE,NULL,0, 0, 0, NULL, NULL );
      if ( callPrg(TEST_DEV,data.ptrVal, NO_PROT, 0, 0, (char**)&retBuf, NULL ) > -1 )
      {
         /* device is OK */
         tmpConf.dev    = data.ptrVal; /* allocated value */
         xms = XmStringCreateSimple(tmpConf.dev);
         XtVaSetValues(cfg2Device_LB, XmNlabelString, xms, NULL);
         XmStringFree(xms);
         if ( retBuf && *retBuf )
            doCyclicScan = 0;
      }
      else
      {
         /* open failed */
         wid = createNoPrinterBox("noPrinter");
         doCyclicScan = 0;
         XtManageChild(wid);

         /* reopen the previous opened device */
         if ( tmpConf.dev )
         {
            callPrg(TEST_DEV,tmpConf.dev , NO_PROT, 0, 0, (char**)&retBuf, NULL );
         }
      }
   }

   XtUnmanageChild(data.actWindow);
   XtManageChild(ConfigureForm);
}

/*******************************************************************/
/* Function cfg2Printer_CB()                                       */
/*                                                                 */
/* Callback for selection of the printer                           */
/*                                                                 */
/*******************************************************************/
             
void cfg2Printer_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   wConfig_data_t data;
   int loop = 0;
   char *dev;
   XmString xms;

   data.ptrVal      = NULL;
   data.printerName = tmpConf.name;
   data.wait        = &loop;
   data.wType       = QUERY_PRINTER;
   data.bt1         = data.bt2 = NULL;
   data.bt3         = appResourceRec.ok;
   data.message     = NULL;

   dev = tmpConf.dev; /* old value */
   if ( tmpConf.name && tmpConf.name[0] != '?' )
   {
      xms = XmStringCreateSimple(tmpConf.name);
      XtVaSetValues(cfg2Printer_LB,XmNlabelString,xms,NULL);
      XmStringFree(xms);
   }
   popupCfg1(ConfigureForm, &data, &tmpConf);

   /* wait for sub window */
   while(!loop)
       XtAppProcessEvent(theApp, XtIMAll);

   memcpy(&tmpConf,(configData_t*)(data.ptrVal), sizeof(configData_t));
   tmpConf.dev = dev; /* restore old value */

   if (tmpConf.name && tmpConf.name != data.printerName)
   {
      if ( tmpConf.name[0] != '?' )
      {
         xms = XmStringCreateSimple(tmpConf.name);
         XtVaSetValues(title_LB,XmNlabelString,xms,NULL);
         XtVaSetValues(cfg2Printer_LB,XmNlabelString,xms,NULL);
         XmStringFree(xms);
      }
      else
      {
         xms = XmStringCreateSimple(xmGetResource(title_LB,"labelString"));
         XtVaSetValues(title_LB,XmNlabelString,xms,NULL);
         XmStringFree(xms);
         xms = XmStringCreateSimple("");
         XtVaSetValues(cfg2Printer_LB,XmNlabelString,xms,NULL);
         XmStringFree(xms);
      }
   }
   XtUnmanageChild(data.actWindow);
   XtManageChild(ConfigureForm);
}

/*******************************************************************/
/* Function selectFile_CB()                                        */
/*                                                                 */
/* Callback for selection of browser                               */
/*                                                                 */
/*******************************************************************/             

void selectFile_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   Widget fsb = NULL;
   /* open a file selection box */
   fsb = createFileSelection(mainForm, (Widget)clientData);
}

/*******************************************************************/
/* Function cfg2Next_CB()                                          */
/*                                                                 */
/* Callback for saving of data                                     */
/*                                                                 */
/*******************************************************************/             

void cfg2Next_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   char  rcPath[1024];
   
   *((int*)clientData) = 1;

   /* save the browser name */
   sprintf(rcPath,"%s/.mtinkrc",getenv("HOME"));

   if ( browser )
   {
      free(browser);
      browser = NULL;
   }
   browser = XmTextFieldGetString(browser_TF);

   /* and the other datas */
   if ( autodetect )
   {
      free(autodetect);
      autodetect = NULL;
   }
 
   if ( XmToggleButtonGetState(autodetect_TB) )
   {
      autodetect = strdup("yes");
   }
   else
   {
      autodetect = strdup("no");
   }

   if ( XmToggleButtonGetState(tooltip_TB) )
   {
      miniHelp = strdup("yes");
      xmEnableTooltip(True);
   }
   else
   {
      miniHelp = strdup("no");
      xmEnableTooltip(False);
   }
}

/*******************************************************************/
/* Function cfg2Previous_CB()                                      */
/*                                                                 */
/* Callback for saving csncle                                      */
/*                                                                 */
/*******************************************************************/             

void cfg2Previous_CB(Widget w, XtPointer clientData, XtPointer callData)
{
   *((int*)clientData) = -1;
}

/*******************************************************************/
/* Function popupCfg2()                                            */
/*                                                                 */
/* main entry for this module. modify                              */
/*                                                                 */
/* Input;               Widget old     which mask to popup after   */
/*                                     the job is done             */
/* Input / Output:     wConfig_data_t *data                        */
/*                                                                 */
/*******************************************************************/             

void popupCfg2(Widget old, wConfig_data_t *data)
{
   XmString  xms;
   char     *retBuf;
   int       status;
   int       oldCyclicScan = doCyclicScan;
   memcpy((void*)&tmpConf,(void*)&actConfig,sizeof(configData_t));
   XtUnmanageChild(old);
   XtManageChild(ConfigureForm);

   if ( data->printerName && data->printerName[0] != '?' )
   {
      xms = XmStringCreateSimple(data->printerName);
      XtVaSetValues(title_LB, XmNlabelString, xms, NULL);
      XmStringFree(xms);
   }

   if ( tmpConf.dev )
   {
      xms = XmStringCreateSimple(tmpConf.dev);
      XtVaSetValues(cfg2Device_LB, XmNlabelString, xms, NULL);
      XmStringFree(xms);
   }

   if ( browser )
   {
      XmTextFieldSetString(browser_TF, browser);
   }

   /* privat loop */
   data->ptrVal = NULL;
   *data->wait  = 0;
   loop = 0;
   while(!loop)
       XtAppProcessEvent(theApp, XtIMAll);
   *data->wait  = loop;

   if ( loop != -1 &&
       ((actConfig.dev && tmpConf.dev && strcmp(actConfig.dev,tmpConf.dev )) ||
        (actConfig.name && tmpConf.name && strcmp(actConfig.dev,tmpConf.name ))) ) 
   {
      if ( actConfig.dev != tmpConf.dev )
         free(actConfig.dev);
      memcpy((void*)&actConfig,(void*)&tmpConf,sizeof(configData_t));
      retBuf = NULL;
      status = callPrg(GET_IQ, actConfig.dev, actConfig.prot, 0, 0, &retBuf, NULL);

      if ( status != -1 )
      {
         SetSensitive(True);
      }
      if ( retBuf )
         decodeStatus((unsigned char*)retBuf, strlen(retBuf));
      else
         decodeStatus((unsigned char*)"", 0);
      if ( retBuf && *retBuf ) doCyclicScan = 1;
      else doCyclicScan = 0;
      if ( oldCyclicScan && ! doCyclicScan &&
           strncmp(actConfig.dev,"/var/",5)==0)
	 doCyclicScan = oldCyclicScan;
      saveConfig(configFileIdx);
      iconAddTooltip(actConfig.name);
      refreshMainWindow();

      /* handle the "icon" window */
      if ( strncmp(actConfig.dev,"/var/",5) )
      {
          unmapIcon();
	  usePopup = 0;
	  miconOK = NULL;
      }
      else
      {
          miconOK = createIconLayout(topLevel, theApp, mainResource, guiLanguage, usePopup);
	  usePopup = 1;
      }
   }
   data->actWindow = ConfigureForm;
}
