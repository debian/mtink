/* file rcfile.c
 *
 * Read and save configuration data
 *
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <strings.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <X11/Intrinsic.h>
#include <X11/Xlib.h>

#include "mtink.h"

/*******************************************************************/
/* Function readRc                                                 */
/*                                                                 */
/* Read the rc file                                                */
/*                                                                 */
/*******************************************************************/

void readRc(int idx)
{
   FILE *fp;
   char  rcPath[1024];
   char  line[2048];
   char *s, *e;
   if ( snprintf(rcPath,sizeof(rcPath)-100, "%s/.mtinkrc",getenv("HOME")) == -1 )
   {
      /* try to get a buffer overflow */
      /* don't allow this             */
      fprintf(stderr,"Sorry the HOME directory path is too big\n");
      exit(2);
   }
   if ( idx )
   {
      strcat(rcPath,".");
      sprintf(rcPath+strlen(rcPath),"%d",idx);
   }

   if ( (fp = fopen(rcPath,"r")) )
   {
      while( fgets(line, sizeof(line), fp) )
      {
         if ( strncmp(line, "BROWSER:", 8) == 0 )
         {
            s = line+8;
            while(*s && isspace(*s)) s++;
            e = s;
            while(*e && !isspace(*e) ) e++;
            *e = '\0';
            if ( browser )
            {
               free(browser);
               browser = NULL;
            }
            browser = strdup(s);
         }
         else if ( strncmp(line, "AUTODETECT:", 11) == 0 )
         {
            s = line+11;
            while(*s && isspace(*s)) s++;
            e = s;
            while(*e && !isspace(*e) ) e++;
            *e = '\0';
            if ( autodetect )
            {
               free(autodetect);
               autodetect = NULL;
            }
            autodetect = strdup(s);
         }
         else if ( strncmp(line, "PORT:", 5) == 0 )
         {
            s = line+5;
            while(*s && isspace(*s)) s++;
            e = s;
#ifndef MACOS
            while(*e && !isspace(*e) ) e++;
#else
            while(*e && (*e != '\r' || *e != '\n')) e++;
#endif
            *e = '\0';
            if ( actConfig.dev )
            {
               free(actConfig.dev);
               actConfig.dev = NULL;
            }
            actConfig.dev = strdup(s);
         }
         else if ( strncmp(line, "PRINTER:", 8) == 0 )
         {
            s = line+8;
            while(*s && isspace(*s)) s++;
            e = s + strlen(s) - 1;
            while(e > s && isspace(*e) )
               *e-- = '\0';
            if ( actConfig.name )
            {
               free(actConfig.name);
               actConfig.name = NULL;
            }
            actConfig.name = strdup(s);
         }
         else if ( strncmp(line, "MINIHELP:", 9) == 0 )
         {
            s = line+9;
            while(*s && isspace(*s)) s++;
            e = s;
            while(*e && !isspace(*e) ) e++;
            *e = '\0';
            if ( miniHelp )
            {
               free(miniHelp);
               miniHelp = NULL;
            }
            miniHelp = strdup(s);
         }
      }
      fclose(fp);
   }
}

/*******************************************************************/
/* Function saveConfig                                             */
/*                                                                 */
/* Save the configurytion data (printer model and port) into       */
/* the file $(HOME)/.mtink                                         */
/*                                                                 */
/*******************************************************************/

int saveConfig(int idx)
{
   int stat;
   int pid;
   char *homeDir = getenv("HOME");
   char *fileName;
   FILE *fp;

   if ( actConfig.name )
   {
      if ( homeDir )
      {
         fileName= calloc(strlen(ConfigFile)+ strlen(homeDir) + 50,1);
         if ( fileName )
         {
            if ( ! idx )
            {
               sprintf(fileName, "%s/%s", homeDir, ConfigFile);
            }
            else
            {
               sprintf(fileName, "%s/%s.%d", homeDir, ConfigFile, idx);
            }
            /* the datas are collected, write into the conf file */
            /* If we was started as uid program we have to set   */
            /* the effective id to the our own uid               */
            if ( (pid = fork()) == 0 )
            {
               seteuid(getuid());
               if ( (fp = fopen(fileName,"w")) )
               {
                  if ( browser )
                     fprintf(fp,"BROWSER: %s\n",browser);
                  if ( autodetect )
                     fprintf(fp,"AUTODETECT: %s\n",autodetect);
                  if ( miniHelp )
                     fprintf(fp,"MINIHELP: %s\n",miniHelp);
                  if ( actConfig.name )
                     fprintf(fp,"PRINTER: %s\n",actConfig.name);
                  if ( actConfig.dev )
                     fprintf(fp,"PORT: %s\n",actConfig.dev);
                  fclose(fp);
                  exit(0);
               }
               exit(0);
            }
            else if ( pid >  0 )
            {
               /* wait fror process termination */
               wait(&stat);
            }
         }
         free(fileName);
      }
   }
   return True;
}

