#include "version.h"
char *fallbackResources[] = {
#if 0
/*
   ! file Mtink.gen, general resources
   ! these resources must not be changed

*/
#endif
   "EpsonUtil*background: #cccccc",
   "EpsonUtil*foreground: #000000",
   "EpsonUtil*shadowThickness: 1",
   "EpsonUtil*XmToggleButton.shadowThickness: 0",
   "EpsonUtil*highlightThickness: 1",
   "EpsonUtil*XmSeparator.shadowThickness: 2",
   "EpsonUtil*XmSeparatorGadget.shadowThickness: 2",
   "EpsonUtil*XmSeparatorGadget.foreground: black",

   "EpsonUtil*scaleB.background:   black",
   "EpsonUtil*scaleLb.background:  #808080",
   "EpsonUtil*scaleC.background:   cyan",
   "EpsonUtil*scaleM.background:   magenta",
   "EpsonUtil*scaleY.background:   yellow",
   "EpsonUtil*scaleLc.background:  #80ffff",
   "EpsonUtil*scaleLm.background:  #ff80ff",
   "EpsonUtil*scalePh.background:  #ffffff",

   "EpsonUtil*title_LB.foreground: blue",
   "EpsonUtil*title_LB.background: white",
   "EpsonUtil*TopShadowColor:      #eeeeee",
   "EpsonUtil*BottomShadowColor:   #111111",

   "*tooltipBackground:                       #efefd7",
   "*tooltipForeground:                       blue",
   "*tooltip_label.borderColor:               red",
   "*tooltip_label.borderWidth:               1",
   "*tooltip_label.alignment:                 ALIGNMENT_BEGINNING",
   "*tooltipPost:                             1000",
   "*tooltipDuration:                         5000",
   "*tooltipX:                                10",
   "*tooltipY:                                2",

#if 0
/*
   ! for not european language you must include font resources
   ! into the langauge specific resource file

*/
#endif
   "EpsonUtil*title_LB.fontList: *-helvetica-*-r-normal--14-*-*-*-*-*-iso8859-1",
   "EpsonUtil*fontList:          *-helvetica-medium-r-normal-*-12-*-*-*-*-*-iso8859-1",
   "*tooltip_label.fontList:     *-helvetica-bold-r-normal--14-*",
#if 0
/*
   ! File Mtink.By, bavarian resources
*/
#endif
   "EpsonUtil.By*unknown: kennd koana",
   "EpsonUtil.By*title_LB.labelString: EPSON GRAFFE "VERSION,
   "EpsonUtil.By*legend_LB.labelString: Dindnmeng",
   "EpsonUtil.By*ok_PB.labelString: Aufhean",
   "EpsonUtil.By.mainWindow.ok_PB.tooltip:  Status-Monitor\\nvalassn.",
   "EpsonUtil.By*pref_PB.labelString: Wos eigscht�it is",
   "EpsonUtil.By*pref_PB.tooltip: - Browser,\\n- Minihuif,\\n- Schnittschtein\\n- Druckaauswahl\\n- Betriebsart",
   "EpsonUtil.By*about_PB.labelString: �ba",
   "EpsonUtil.By*about_PB.tooltip: Lizenz und\\nde wo mitgmacht ham",
   "EpsonUtil.By*help_PB.labelString: Huif",
   "EpsonUtil.By*help_PB.tooltip: Huif in am Browser ozoagn.",
   "EpsonUtil.By*check_PB.labelString: D�sn\\npr�fn",
   "EpsonUtil.By*check_PB.tooltip: Wenn da Ausdruck Streifn hod,\\nna konnst kontrollian, ob\\nde D�sn dreggad san.",
   "EpsonUtil.By*clean_PB.labelString: D�sn\\nsaubamacha",
   "EpsonUtil.By*clean_PB.tooltip: Wenn da Ausdruck Streifn hod\\nund de D�sn zua san,\\nna konnst do a\\nReinigung oschd�ssn.",
   "EpsonUtil.By*align_PB.labelString: Kepf\\nausrichtn",
   "EpsonUtil.By*align_PB.tooltip: Bass auf !\\nDo kannt da da Drucka varecka.",
   "EpsonUtil.By*reset_PB.labelString: Druckr\\nzrucksetzn",
   "EpsonUtil.By*reset_PB.tooltip: s'Zrucksetzn vom Drucka\\ng�ht ned bei olle.",
   "EpsonUtil.By*cartridge_PB.labelString: Patrona\\nwechsln",
   "EpsonUtil.By*cartridge_PB.tooltip: Drucka ohne Schoida\\nbraucha des.",
   "EpsonUtil.By*addPrinterTxt: Andere Drucka",
   "EpsonUtil.By*printerState_LB.labelString: Schtatus: ",
   "EpsonUtil.By*state_LB.labelString: - ",
   "EpsonUtil.By*noPrinter*messageString: Dea mog ned!\\nSchaug amoi, obs Fehla gibt:\\n\"Papiafehla\", \"No Dindn da?\",\\n\"Isa iwahabts eigschoid?\".\\n\\nDeng dro, da� manche Drucka\\na boor Sekundn nachm Eischoidn\\nno ned `da` san.",
   "EpsonUtil.By*noPrinter*dialogTitle: F�hla",
   "EpsonUtil.By*cfg1_LB.labelString: Drucka zum Ausw�in:",
   "EpsonUtil.By*cfg2Printer_PB.labelString: Drucka zum Ausw�in:",
   "EpsonUtil.By*cfg2Printer_PB.tooltip: Suach da do an Drucka aus.",
   "EpsonUtil.By*cfg2Device_PB.labelString: Port zum Ausw�in:",
   "EpsonUtil.By*cfg2Device_PB.tooltip: Wenn Mtink des bloss\\nseiba aussuacha\\nkannd !",
   "EpsonUtil.By*next_PB.labelString: Weida",
   "EpsonUtil.By*next: Weida",
   "EpsonUtil.By*previous_PB.labelString: Zruck",
   "EpsonUtil.By*previous: Zruck",
   "EpsonUtil.By*save: Sichan",
   "EpsonUtil.By*cancel: Aufhean",
   "EpsonUtil.By*ok: Ois kloa",

   "EpsonUtil.By*about:"
   "EPSON Graffe\\n"
   "\\n"
   "Version "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001\\n"
   "\\n"
   "Email: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "A Teil vom Code kimmt vom Gimp-Print Projekt\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Lizenz: GPL\\n"
   "\\n"
   "A Dangsche an:\\n"
   "Keith Amidon,\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "  gene_heskett@iolinc.net\\n"
   "Till Kamppeter\\n"
   "  (leader of the Foomatic/www.linuxprinting.org)\\n"
   "  http://linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "  sjk@weserbergland.de\\n"
   "  http://xwgui.automatix.de\\n"
   "Sylvain Le-Gall\\n"
   "  sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat,\\n"
   "Marc Riese Marc-Riese@gmx.de,\\n"
   "Hikmet Salar\\n"
   "  Salar@gmx.de,\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "  nospam@robert-wachinger.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.By*ctTxt0: Kopfausrichtn:\\n\\nBittsche, leg a laars Blattl Papia inn Drucka ei und mach [Weida].",
   "EpsonUtil.By*ctTxtC0: Kopfausrichtn:\\n\\nBittsche, leg a laars Blattl Papia inn Drucka ei,w�j da den Schwarz oda Farb-Kopf\\nund mach [Weiter] oda brich mit [Abbrechen] ob",
   "EpsonUtil.By*ctTxt1: Schaug da amoi des grad druckte Musta o, und suach da de bestn Linienpaare aus.\\nLeg des Blattl nommoi in den Drucka, und mach [Weida].",
   "EpsonUtil.By*ctTxt1: Schaug da des grod druckte Blattl genau o. Suach des Musta des wo an gradastn, senkrechtn Strich gibt. An an jedn Strich is a Zoj vo 1 bis 8 oda 15 higschrim. De Zoin stengan wegam Blootz untaranand. De Zoj klickst o.\\n\\nLeg des Blattl nommoi in den Drucka, und mach [Weida].",
   "EpsonUtil.By*ctTxtP: Wart bis des Blattl ausgwoafa wead und mach [Weida].",
   "EpsonUtil.By*ctTxt5: Etz schaug da de Ergebniss wega da Ausrichtung nommoi ganz guad o, \\n\\nEtz konnst no:\\n\\n  de Einstellung im Drucka [Sichan] oda\\n  [Abbrecha] ohne dasd was g`�ndad host.",
   "EpsonUtil.By*exTxt0: Patrona wechsln:\\n\\nDua amoi :\\n\\n  [Weiter] um weidazmacha oda\\n  [Abbruch]",
   "EpsonUtil.By*exTxt00: Patrona wechsln:\\n\\nSuach da Farb oda Schwarz aus unddua :\\n\\n  [Weida] zum Weidamacha oda\\n  [Abbruch]",
   "EpsonUtil.By*exTxt1: Patrona wechsln:\\n\\nDa Druckkopf wead etz in de Stellung zum Wechsln gfahrn.",
   "EpsonUtil.By*exTxt2: Patrona wechsln:\\n\\nDua a neiche Patrona eine und\\n\\n  druck [Weida]  ",
   "EpsonUtil.By*exTxt3: Patrona wechsln:\\n\\n  De Dindn wead etz nachgfuid.",
   "EpsonUtil.By*capabilities_LB.labelString: Drucka konfigurian:",
   "EpsonUtil.By*capabilities_RC*id_TB.labelString: Da Drucka gibt sei Identifikation zruck",
   "EpsonUtil.By*capabilities_RC*reset_TB.labelString: Es g�ht koa Software Reset",
   "EpsonUtil.By*capabilities_RC*state_TB.labelString: Da Drucka schickt Status Informationen",
   "EpsonUtil.By*capabilities_RC*ex_TB.labelString: Dindnwechsl muasst per Software macha",
   "EpsonUtil.By*colors_RC*four_TB.labelString: 4 Farbn",
   "EpsonUtil.By*colors_RC*six_TB.labelString: 6 Farbn",
   "EpsonUtil.By*head_RC*col_TB.labelString: Farb",
   "EpsonUtil.By*head_RC*bw_TB.labelString: Schwarz",
   "EpsonUtil.By*noAccess*dialogTitle: Fehla",
   "EpsonUtil.By.scrTxt_MW.head_RC.bw_TB.tooltip:  Mach des mid da Patrona\\nmit da schwarzn Dindn.",
   "EpsonUtil.By.scrTxt_MW.head_RC.col_TB.tooltip: Mach des mid da Patrona\\nmit da farbign Dindn.",
   "EpsonUtil.By*noAccess*messageString: sZuagreifa auf de Device-Dateien g�ht ned\\n\\nSchaug, da� mtink gnua Zuagriffsrechte\\ngriagt.\\nWennstas ned woasst, schau in de Biachen nach.",
   "EpsonUtil.By*fsb*dialogTitle: Mtink",
   "EpsonUtil.By*fsb*CancelLabelString: Abbruch",
   "EpsonUtil.By*fsb*okLabelString: Ois kloa",
   "EpsonUtil.By*fsb*applyLabelString: Fuita",
   "EpsonUtil.By*fsb*filterLabelString: Fuita",
   "EpsonUtil.By*fsb*fileListLabelString: Feils",
   "EpsonUtil.By*fsb*dirListLabelString:  Oadna",
   "EpsonUtil.By*browser_LB.labelString:  Browser aussuacha",
   "EpsonUtil.By*browser_PB.labelString:  \\ ...\\ ",
   "EpsonUtil.By*browser_PB.tooltip:  Browser aussuacha.",
   "EpsonUtil.By*tooltip_TB.labelString:  Minihuif",
   "EpsonUtil.By*tooltip_TB.tooltip:  De l�stige Minihuif\\nkonnst do damit\\nei und ausschoitn.",
   "EpsonUtil.By*autodetect_TB.labelString: Lass a automatische Erkennung zua",
   "EpsonUtil.By*autodetect_TB.tooltip: Mtink probiat beim Startn, da�\\nseiba an Drucka bestimmt.\\nDes g�ht aba ned bei an jeden Drucka.",
   "EpsonUtil.By*save_PB.labelString:  Sichan",
   "EpsonUtil.By*save_PB.tooltip:  Ois wosd g�ndat host �banehma.",
   "EpsonUtil.By.ConfigureForm.cancel_PB.tooltip: De �ndarungen wegschmeissn,\\nd`Maskrne valassn.",
   "EpsonUtil.By*cancel_PB.labelString:  Abbruch",

#if 0
/*
   ! Drucker Status

*/
#endif
   "EpsonUtil.By*error:"
   "Fehler",

   "EpsonUtil.By*printing:"
   "Druckt",

   "EpsonUtil.By*selfTest:"
   "Selbst Test",

   "EpsonUtil.By*busy:"
   "Besch�ftigt",

   "EpsonUtil.By*ok:"
   "Ois kloa",

   "EpsonUtil.By*cleaning:"
   "saubamacha",

   "EpsonUtil.By*unknown:"
   "Unbekannt",

#if 0
/*
   ! File Mtink.Da, also default resource
   !
   ! Translated by
   ! Mogens J�ger, Danmark
   ! mogensjaeger@get2net.dk

*/
#endif
   "EpsonUtil.Da*unknown:                       ukendt",

#if 0
/*
   ! The version number will be set into the c-source file !

*/
#endif
   "EpsonUtil.Da*title_LB.labelString:          EPSON UTILITIES "VERSION,
   "",
   "EpsonUtil.Da*legend_LB.labelString:         Bl�k m�ngde",

   "EpsonUtil.Da*ok_PB.labelString:             Afbryd",

   "EpsonUtil.Da*mainWindow.ok_PB.tooltip:      Afslut\\nStatus Monitor.",

   "EpsonUtil.Da*pref_PB.labelString:           Indstillinger",

   "EpsonUtil.Da*pref_PB.tooltip:"
   "- Browser,\\n"
   "- Mini hj�lp,\\n"
   "- Port valg,\\n"
   "- Printer valg,\\n"
   "- arbejds m�de.",

   "EpsonUtil.Da*about_PB.labelString:          Om",

   "EpsonUtil.Da*about_PB.tooltip:"
   "Licens og\\n"
   "medvirkende.",

   "EpsonUtil.Da*help_PB.labelString:           Hj�lp",

   "EpsonUtil.Da*help_PB.tooltip:               Vis hj�lp i en browser.",

   "EpsonUtil.Da*check_PB.labelString:          Check\\nDyser",

   "EpsonUtil.Da*check_PB.tooltip:"
   "Print et test m�nster for at\\n"
   "checke for blokerede dyser.\\n"
   "Blokerede dyser giver blanke\\n"
   "striber p� udskrifter.",

   "EpsonUtil.Da*clean_PB.labelString:          Rens\\nDyser",

   "EpsonUtil.Da*clean_PB.tooltip:"
   "Hvis dyserne ikke er OK,\\n"
   "kan du rense dyserne her.",

   "EpsonUtil.Da*align_PB.labelString:          Indret\\nHoved",

   "EpsonUtil.Da*align_PB.tooltip:"
   "Advarsel !\\n"
   "Dette kan beskadige printeren.",

   "EpsonUtil.Da*reset_PB.labelString:          Reset\\nPrinter",

   "EpsonUtil.Da*reset_PB.tooltip:              Dette virker ikke for alle modeller.",

   "EpsonUtil.Da*cartridge_PB.labelString:      Skift\\nPatron",

   "EpsonUtil.Da*cartridge_PB.tooltip:"
   "Hvis din printer ikke\\n"
   "tilbyder nogen switch\\n"
   "kan du bruge dette.",

   "EpsonUtil.Da*addPrinterTxt:                 Anden Printer",

   "EpsonUtil.Da*printerState_LB.labelString:   Tilstand:",

   "EpsonUtil.Da*state_LB.labelString:   - ",

   "EpsonUtil.Da*noPrinter*messageString:"
   "Problemer med kommunikationen med printeren,\\n"
   "check printeren for fejl:\\n"
   "\"Intet Papir\", \"Ingen bl�k\", \"printer ikke t�ndt\"\\n"
   "\\n"
   "Bem�rk at nogle printere blokerer i nogle f�\\n"
   "sekunder efter de er blevet t�ndt.",

   "EpsonUtil.Da*noPrinter*dialogTitle:          Fejl",

   "EpsonUtil.Da*cfg1_LB.labelString:            Printer valg:",

   "EpsonUtil.Da*cfg2Printer_PB.labelString:     Printer valg:",

   "EpsonUtil.Da*cfg2Printer_PB.tooltip:"
   "En liste af printere vil blive vist.\\n"
   "Du kan v�lge din model der.",

   "EpsonUtil.Da*cfg2Device_PB.labelString:      Port valg:",

   "EpsonUtil.Da*cfg2Device_PB.tooltip:"
   "Kunne mtink bare\\n"
   "selv klare det !",

   "EpsonUtil.Da*next_PB.labelString:            N�ste",

   "EpsonUtil.Da*next:                           N�ste",

   "EpsonUtil.Da*previous_PB.labelString:        Forrige",

   "EpsonUtil.Da*previous:                       Forrige",

   "EpsonUtil.Da*save:                           Gem",

   "EpsonUtil.Da*cancel:                         Afbryd",

   "EpsonUtil.Da*ok:                             OK",

   "EpsonUtil.Da*about:"
   "EPSON Utilities\\n"
   "\\n"
   "Version "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001\\n"
   "\\nEmail: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "Dele af koden er taget fra gimp-print project\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Licens: GPL\\n"
   "\\n"
   "Tak til:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens J�ger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter\\n"
   "  (leader Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n\\n",
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.Da*ctTxt0:"
   "Placer et ark papir i printeren for at begynde indretning af hoved processen.\\n"
   "Tryk [N�ste] for at begynde\\n"
   "eller [Afbryd] for at stoppe.",

   "EpsonUtil.Da*ctTxtC0:"
   "Placer et ark papir i printeren for at begynde indretning af hoved processen.\\n"
   "V�lg Sort eller farve, og\\n"
   "Tryk [N�ste] for at begynde, eller [Afbryd] for at stoppe.",

   "EpsonUtil.Da*ctTxt1:"
   "Unders�g printet, og v�lg det bedste liniepar i det sidste m�nster,\\n",
   "geninds�t arket i printeren og tryk [N�ste] for at forts�tte",

   "EpsonUtil.Da*ctTxt1C:"
   "Unders�g arket, og afg�r hvilket m�nster der er glattest.\\n"
   "Dette m�nster er det der har mindst 'korn'.\\n"
   "Hvis du ikke kan finde et glat m�nster, skal du v�lge nummeret p� det\\n"
   "bedste m�nster, og gentage processen\\n"
   "Inds�t arket i printeren, og tryk [N�ste] for at forts�tte.",

   "EpsonUtil.Da*ctTxtP:"
   "Vent til arket er kommet ud af printeren, og tryk [N�ste] for at forts�tte.",

   "EpsonUtil.Da*ctTxt5:"
   "Unders�g det sidste udskrift meget omhyggeligt, for at v�re sikker p� at din printer er rettet helt ind.\\n"
   "\\n"
   "Du kan nu:\\n"
   "\\n"
   "  [Gem] resultatet i printeren, eller\\n"
   "  [Afbryd] uden at gemme resultatet",

   "EpsonUtil.Da*exTxt0:"
   "Udskiftning patron:\\n"
   "\\n"
   "Tryk :\\n"
   "\\n"
   "  [N�ste] \\for at forts�tte, eller\\n"
   "  [Afbryd]",

   "EpsonUtil.Da*exTxt00:"
   "Udskiftning patron:\\n"
   "\\n"
   "V�lg sort eller farve\\n"
   "Tryk:\\n"
   "\\n"
   "  [N�ste] for at forts�tte, eller\\n"
   "  [Afbryd]",

   "EpsonUtil.Da*exTxt1:"
   "Udskiftning patron:\\n"
   "\\n"
   "Patron vognen bliver f�rt til positionen for udskiftning.",

   "EpsonUtil.Da*exTxt2:"
   "Udskiftning patron:\\n"
   "\\n"
   "Ins�t en ny patron, og:\\n"
   "\\n"
   "  Tryk [N�ste]",

   "EpsonUtil.Da*exTxt3:"
   "Udskiftning patron:\\n"
   "\\n"
   "  proces \"p�fyldning bl�k\".",

   "EpsonUtil.Da*colors_RC*four_TB.labelString:    4 Farver",

   "EpsonUtil.Da*colors_RC*six_TB.labelString:     6 Farver",

   "EpsonUtil.Da*head_RC*col_TB.labelString:       Farver",

   "EpsonUtil.Da*head_RC*bw_TB.labelString:        Sort",

   "EpsonUtil.Da*noAccess*dialogTitle:             Fejl",

   "EpsonUtil.Da*scrTxt_MW.head_RC.bw_TB.tooltip:"
   "Udf�rer p�fyldning af\\n"
   "sort bl�k i patronen.",

   "EpsonUtil.Da*scrTxt_MW.head_RC.col_TB.tooltip:"
   "Udf�rer p�fyldning af\\n"
   "farvet bl�k i patronen.",

   "EpsonUtil.Da*noAccess*messageString:"
   "Kan ikke f� fat i printeren.\\n"
   "\\n"
   "Kontroller at mtink har de forn�dne\\n"
   "filrettigheder til enhedens filer.\\n"
   "\\n"
   "Se i �vrigt dokumentationen.",

   "EpsonUtil.Da*fsb*dialogTitle:                 Mtink",

   "EpsonUtil.Da*fsb*CancelLabelString:           Afbryd",

   "EpsonUtil.Da*fsb*okLabelString:               OK",

   "EpsonUtil.Da*fsb*applyLabelString:            Filter",

   "EpsonUtil.Da*fsb*filterLabelString:           Filter",

   "EpsonUtil.Da*fsb*fileListLabelString:         Filer",

   "EpsonUtil.Da*fsb*dirListLabelString:          Biblioteker",

   "EpsonUtil.Da*browser_LB.labelString:          V�lg Browser",

   "EpsonUtil.Da*browser_PB.labelString:          \\ ...\\ ",

   "EpsonUtil.Da*browser_PB.tooltip:              Browser valg",

   "EpsonUtil.Da*tooltip_TB.labelString:          Mini Hj�lp",

   "EpsonUtil.Da*tooltip_TB.tooltip:"
   "Du kan deaktivere\\n"
   "mini hj�lpen her.",

   "EpsonUtil.Da*autodetect_TB.labelString:       Tillad automatisk genkendelse",

   "EpsonUtil.Da*autodetect_TB.tooltip:"
   "Mtink vil pr�ve at finde hvilken\\n"
   "printermodel der er tilsluttet.\\n"
   "Dette virker ikke med alle printere.",

   "EpsonUtil.Da*save_PB.labelString:             Gem",

   "EpsonUtil.Da*save_PB.tooltip:                 Godkend �ndringerne.",

   "EpsonUtil.Da*ConfigureForm.cancel_PB.tooltip:"
   "Undlad at tilf�je �ndringerne,\\n"
   "forlad konfigurationen.",

   "EpsonUtil.Da*cancel_PB.labelString:           Afbryd",

#if 0
/*
   ! Printer state

*/
#endif
   "EpsonUtil.Da*error:                        Fejl",

   "EpsonUtil.Da*printing:                     Printer",

   "EpsonUtil.Da*selfTest:                     Selv Test",

   "EpsonUtil.Da*busy:                         Optaget",

   "EpsonUtil.Da*ok:                           OK",

   "EpsonUtil.Da*cleaning:                     Renser",

   "EpsonUtil.Da*unknown:                      Ukendt",
#if 0
/*
   ! File Mtink.De, German ressources.
   ! Datei Mtink.De, Deutsche Ressourcen.
   !
   ! Notiz f�r �bersetzer. Dies ist eine X-Ressource-Datei.
   ! Nachstehende Zeichensequenzen haben eine spezielle Bedeutung:
   ! \    Am Ende einer Zeile (Leerzeichen und Tabulatoren d�rfen nicht
   !      vorhanden sein) bedeutet, dass die Ressource in der n�chste Zeile
   !      fortgesetzt werden.
   ! \n   Dies steht f�r eine Zeilenschaltung.
   ! \    (\ und Leerzeichen) Das Leerzeichen ist Teil der Zeichenkette.
   !      Leerzeichen am Anfang eine Zeichenkette werden normalerweise
   !      eliminiert.
   ! \t   Steht f�r ein Tabulatorzeichen.
   ! !    am Anfang einer Zeile (Leerzeichen d�rfen vorgesetzt sein) leitet
   !      ein Kommentar ein.
   ! 
   ! Wenn Sie eine �bersetzung vornehmen, ist die Sprachkennung innerhalb
   ! des Bezeichners einzutragen.
   ! Beispiel:
   ! Die default Ressource  EpsonUtil*unknown ist auf Deutsch zu �bersetzen
   !
   !   EpsonUtil*unknown: unknown
   !   EpsonUtil.De*unknown: Unbekannt
   ! EpsonUtil ist der Klassennamen, .De muss nach den Klassennamen
   ! eingef�gt werden. Soll von Deutsch nach Franz�sisch �bersetzt werden
   ! ist jeweils De durch Fr zu ersetzen.
   !
   ! Die Sprachkennung besteht aus zwei Zeichen, welche aus der Umgebungsvariable
   ! LANG, LC_ALL und LC_MESSAGE entnommen werden. Der erste Buchstabe wird
   ! als Kapit�lchen umgewandelt.
   !
   ! Der �bersichtlichkeit halber habe ich jede Ressource auf mehrere Zeilen
   ! umgebrochen. Dies sollte vor allem bei mehrzeiligen Texten die �bersicht
   ! bez�glich Formatierung verbessern.

*/
#endif
   "EpsonUtil.De*unknown: unbekannt",

#if 0
/*
   ! Die Version wird beim Erstellen der C-Datei ersetzt.

*/
#endif
   "EpsonUtil.De*title_LB.labelString:"
   "EPSON UTILITIES "VERSION,
   "",
   "EpsonUtil.De*legend_LB.labelString:"
   "Tintenmenge",

   "EpsonUtil.De*ok_PB.labelString:"
   "Ende",

   "EpsonUtil.De.mainWindow.ok_PB.tooltip:"
   "Status-Monitor\\n"
   "verlassen",

   "EpsonUtil.De*pref_PB.labelString:"
   "Einstellungen",

   "EpsonUtil.De*pref_PB.tooltip:"
   "- Browser,\\n"
   "- Minihilfe,\\n"
   "- Schnittstelle\\n"
   "- Druckerwahl\\n"
   "- Betriebsart",

   "EpsonUtil.De*about_PB.labelString: �ber",

   "EpsonUtil.De*about_PB.tooltip:"
   "Lizenz und\\n"
   "Mitwirkende",

   "EpsonUtil.De*help_PB.labelString:"
   "Hilfe",

   "EpsonUtil.De*help_PB.tooltip:"
   "Hilfe in einem Browser anzeigen.",

   "EpsonUtil.De*check_PB.labelString:"
   "D�sen\\n"
   "pr�fen",

   "EpsonUtil.De*check_PB.tooltip:"
   "Wenn der Ausdruck streifig ist,\\n"
   "k�nnen Sie kontrollieren ob\\n"
   "die D�sen verschmutzt sind.",

   "EpsonUtil.De*clean_PB.labelString:"
   "D�sen\\n"
   "reinigen",

   "EpsonUtil.De*clean_PB.tooltip: "
   "Wenn der Ausdruck streifig \\n"
   "und die D�sen verstopft sind,\\n"
   "k�nnen Sie hiermit eine\\n"
   "Reinigung vornehmen.",

   "EpsonUtil.De*align_PB.labelString:"
   "K�pfe\\n"
   "ausrichten",

   "EpsonUtil.De*align_PB.tooltip:"
   "Achtung!\\n"
   "Dies k�nnte den Drucker besch�digen!",

   "EpsonUtil.De*reset_PB.labelString:"
   "Drucker\\n"
   "zur�cksetzen",

   "EpsonUtil.De*reset_PB.tooltip:"
   "Das Zur�cksetzen des Druckers\\n"
   "funktioniert nicht bei allen\\n"
   "Modellen.",

   "EpsonUtil.De*cartridge_PB.labelString:"
   "Patrone\\n"
   "wechseln",

   "EpsonUtil.De*cartridge_PB.tooltip:"
   "Drucker ohne Schalter\\n"
   "ben�tigen das.",

   "EpsonUtil.De*addPrinterTxt:"
   "Andere Drucker",

   "EpsonUtil.De*printerState_LB.labelString:"
   "Status: ",

   "EpsonUtil.De*state_LB.labelString:"
   "- ",

   "EpsonUtil.De*noPrinter*messageString:"
   "Kommunikationsproblem!\\n"
   "Drucker bez�glich dieser Fehlern pr�fen:\\n"
   "\"Papierfehler\", \"Tintenstand\",\\n"
   "\"Drucker ist nicht eingeschaltet\".\\n"
   "\\n"
   "Beachten Sie, dass manche Drucker f�r\\n"
   "einige Sekunden nach dem Einschalten\\n"
   "nicht `ansprechbar' sind.",

   "EpsonUtil.De*noPrinter*dialogTitle:"
   "Fehler",

   "EpsonUtil.De*cfg1_LB.labelString:"
   "Drucker Auswahl:",

   "EpsonUtil.De*cfg2Printer_PB.labelString:"
   "Drucker Auswahl:",

   "EpsonUtil.De*cfg2Printer_PB.tooltip:"
   "Sie k�nnen in dieser Liste mit Druckern Ihr Modell ausw�hlen.",

   "EpsonUtil.De*cfg2Device_PB.labelString:"
   "Port Auswahl:",

   "EpsonUtil.De*cfg2Device_PB.tooltip:"
   "Wenn das Mtink nur\\n"
   "selbst bestimmen\\n"
   "k�nnte!",

   "EpsonUtil.De*next_PB.labelString:"
   "Weiter",

   "EpsonUtil.De*next:"
   "Weiter",

   "EpsonUtil.De*previous_PB.labelString:"
   "Zur�ck",

   "EpsonUtil.De*previous:"
   "Zur�ck",

   "EpsonUtil.De*save:"
   "Sichern",

   "EpsonUtil.De*cancel:"
   "Abbrechen",

   "EpsonUtil.De*ok:"
   "OK",

   "EpsonUtil.De*about:"
   "EPSON Utilities\\n"
   "\\n"
   "Version "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001\\n"
   "\\n"
   "Email: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "Teile des Codes stammen vom Gimp-Print Projekt\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Lizenz: GPL\\n"
   "\\nDank an:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens J�ger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter\\n"
   "  (leader Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.De*ctTxt0:"
   "Kopfausrichtung:\\n"
   "\\n"
   "Bitte ein leeres Blatt Papier in den Drucker einlegen und [Weiter] bet�tigen.",

   "EpsonUtil.De*ctTxtC0:"
   "Kopfausrichtung:\\n"
   "\\n"
   "Bitte ein leeres Blatt Papier in den Drucker einlegen, w�hlen Sie den schwarzen "
   "oder den farbigen Druckkopf\\nund dann [Weiter] um fortzufahren oder [Abbrechen].",

   "EpsonUtil.De*ctTxt1:"
   "Bitte begutachten Sie das so eben gedruckte Muster, und w�hlen Sie die besten "
   "Linienpaare aus.\\nLegen Sie das Blatt erneut in den Drucker, und bet�tigen Sie [Weiter].",

   "EpsonUtil.De*ctTxt1:"
   "Schauen Sie sich das bedruckte Blatt genau an. Finden Sie das Muster heraus, "
   "das den geradesten, senkrechten Strich ergibt. Jedem Strich ist eine Zahl von "
   "1 bis 8, beziehungsweise 15, zugeordnet. Die Zahlen stehen aus Platzgr�nden "
   "untereinander. Diese Zahl klicken Sie an.\\n"
   "\\n"
   "Legen Sie das Blatt erneut in den Drucker, und w�hlen Sie [Weiter].",


   "EpsonUtil.De*ctTxt1_1:"
   "Muster #1\\n"
   "Schauen Sie sich das bedruckte Blatt genau an. Finden Sie das Muster heraus, "
   "das den geradesten, senkrechten Strich ergibt. Jedem Strich ist eine Zahl von "
   "1 bis 8, beziehungsweise 15, zugeordnet. Die Zahlen stehen aus Platzgr�nden "
   "untereinander. Diese Zahl klicken Sie an.\\n"
   "\\n"
   "und w�hlen Sie [Weiter].",

   "EpsonUtil.De*ctTxt1_2:"
   "Muster #2\\n"
   "Schauen Sie sich das bedruckte Blatt genau an. Finden Sie das Muster heraus, "
   "das den geradesten, senkrechten Strich ergibt. Jedem Strich ist eine Zahl von "
   "1 bis 8, beziehungsweise 15, zugeordnet. Die Zahlen stehen aus Platzgr�nden "
   "untereinander. Diese Zahl klicken Sie an.\\n"
   "\\n"
   "und w�hlen Sie [Weiter].",

   "EpsonUtil.De*ctTxt1_3:"
   "Muster #3\\n"
   "Schauen Sie sich das bedruckte Blatt genau an. Finden Sie das Muster heraus, "
   "das den geradesten, senkrechten Strich ergibt. Jedem Strich ist eine Zahl von "
   "1 bis 8, beziehungsweise 15, zugeordnet. Die Zahlen stehen aus Platzgr�nden "
   "untereinander. Diese Zahl klicken Sie an.\\n"
   "\\n"
   "und w�hlen Sie [Weiter].",

   "EpsonUtil.De*ctTxtP:"
   "Warten Sie bis das Blatt ausgeworfen wird und bet�tigen Sie [Weiter].",

   "EpsonUtil.De*ctTxt5:"
   "Bitte begutachten Sie die Ergebnisse bez�glich der Ausrichtung sehr "
   "sorgf�ltig.\\n\\nSie k�nnen nun:\\n\\n  die Einstellung im Drucker [Sichern] "
   "oder\\n"
   "  [Abbrechen], ohne die Einstellungen zu �bernehmen.",

   "EpsonUtil.De*exTxt0:"
   "Patrone Wechseln:\\n"
   "\\n"
   "Bet�tigen Sie :\\n"
   "\\n"
   "  [Weiter] um fortzufahren oder\\n"
   "  [Abbruch].",

   "EpsonUtil.De*exTxt00:"
   "Patrone Wechseln:\\n"
   "\\n"
   "W�hlen Sie bitte Farbe oder Schwarz und bet�tigen Sie :\\n"
   "\\n"
   "  [Weiter] um fortzufahren oder\\n"
   "  [Abbruch].",

   "EpsonUtil.De*exTxt1:"
   "Patrone wechseln:\\n"
   "\\n"
   "Wagen wird in die Wechselstellung gefahren.",

   "EpsonUtil.De*exTxt2:"
   "Patrone Wechseln:\\n"
   "\\n"
   "Neue Patrone einsetzen und:\\n"
   "\\n"
   "  [Weiter] bet�tigen.",

   "EpsonUtil.De*exTxt3:"
   "Patrone Wechseln:\\n"
   "\\n"
   "  Tinte wird nachgef�llt.",

   "EpsonUtil.De*capabilities_LB.labelString:"
   "Drucker Konfiguration:",

   "EpsonUtil.De*capabilities_RC*id_TB.labelString:"
   "Drucker gibt Identifikation zur�ck.",

   "EpsonUtil.De*capabilities_RC*reset_TB.labelString:"
   "Software-Reset kann durchgef�hrt werden.",

   "EpsonUtil.De*capabilities_RC*state_TB.labelString:"
   "Drucker liefert Status Informationen.",

   "EpsonUtil.De*capabilities_RC*ex_TB.labelString:"
   "Tintenwechsel per Software ist notwendig.",

   "EpsonUtil.De*colors_RC*four_TB.labelString:"
   "4 Farben",

   "EpsonUtil.De*colors_RC*six_TB.labelString:"
   "6 Farben",

   "EpsonUtil.De*head_RC*col_TB.labelString:"
   "Farbe",

   "EpsonUtil.De*head_RC*bw_TB.labelString:"
   "Schwarz",

   "EpsonUtil.De*noAccess*dialogTitle:"
   "Fehler",

   "EpsonUtil.De.scrTxt_MW.head_RC.bw_TB.tooltip:"
   "Operation mit der schwarzen\\n"
   "Tintenpatrone ausf�hren.",

   "EpsonUtil.De.scrTxt_MW.head_RC.col_TB.tooltip:"
   "Operation mit der farbigen\\n"
   "Tintenpatrone ausf�hren.",

   "EpsonUtil.De*noAccess*messageString:"
   "Keine Zugriffsberechtigung auf die\\n"
   "Schnittstellendateien.\\n"
   "Stellen Sie sicher, dass mtink gen�gend Zugriffsrechte\\n"
   "erh�lt.\\n"
   "\\nMit Debian basierte Distribution wie Ubuntu\\n"
   "muss der Anwender Mitglied der Gruppe lp sein.\\n"
   "Schauen Sie daf�r in der Dokumentation nach.",

   "EpsonUtil.De*fsb*dialogTitle:"
   "Mtink",

   "EpsonUtil.De*fsb*CancelLabelString:"
   "Abbruch",

   "EpsonUtil.De*fsb*okLabelString:"
   "OK",

   "EpsonUtil.De*fsb*applyLabelString:"
   "Filter",

   "EpsonUtil.De*fsb*filterLabelString:"
   "Filter",

   "EpsonUtil.De*fsb*fileListLabelString:"
   "Dateien",

   "EpsonUtil.De*fsb*dirListLabelString:"
   "Ordner",

   "EpsonUtil.De*browser_LB.labelString:"
   "Browser w�hlen",

#if 0
/*
   ! Achtung ein ' ' folgt das '\' Zeichen.

*/
#endif
   "EpsonUtil.De*browser_PB.labelString:"
   " ...\\ ",

   "EpsonUtil.De*browser_PB.tooltip:"
   "Browser ausw�hlen.",

   "EpsonUtil.De*tooltip_TB.labelString:"
   "Minihilfe",

   "EpsonUtil.De*tooltip_TB.tooltip:"
   "Diese l�stige Minihilfe\\n"
   "k�nnen Sie hiermit\\n"
   "ein und ausschalten.",

   "EpsonUtil.De*autodetect_TB.labelString:"
   "Automatische Erkennung erlauben.",

   "EpsonUtil.De*autodetect_TB.tooltip:"
   "Mtink wird beim Start versuchen, den\\n"
   "Drucker selbst zu bestimmen.\\n"
   "Dies funktioniert nicht bei allen Druckern.",

   "EpsonUtil.De*save_PB.labelString:  Sichern",

   "EpsonUtil.De*save_PB.tooltip:  �nderungen �bernehmen.",

   "EpsonUtil.De.ConfigureForm.cancel_PB.tooltip:"
   "�nderungen nicht �bernehmen,\\n"
   "Maske verlassen.",

   "EpsonUtil.De*cancel_PB.labelString:"
   "Abbruch",

#if 0
/*
   ! Drucker Status

*/
#endif
   "EpsonUtil.De*error:"
   "Fehler",

   "EpsonUtil.De*printing:"
   "Druckt",

   "EpsonUtil.De*selfTest:"
   "Selbst Test",

   "EpsonUtil.De*busy:"
   "Besch�ftigt",

   "EpsonUtil.De*ok:"
   "OK",

   "EpsonUtil.De*cleaning:"
   "Reinigung",

   "EpsonUtil.De*unknown:"
   "Unbekannt",

#if 0
/*
   ! File Mtink.En, also default resource
   !
   ! Note for translator. This is a X-Resource file. The following
   ! character sequences have a special meaning:
   ! \    at the end of a line (no space allowed after the \):
   !      resource continue at the next line.
   ! \n   This is a linefeed.
   ! \    (\ and space) the space character is part of the resource
   !      string. Normally space at the begin and end of a resource are
   !      elinminated.
   ! \t   This is for a tabulator character.   
   !
   ! If you translate this you have to put the language designation
   ! withinh the key.
   ! Example:
   ! The resource default EpsonUtil*unknown must be translated into german
   !
   !   EpsonUtil*unknown: unknown
   !   EpsonUtil.De*unknown: Unbekannt
   ! EpsonUtil is the program class name, .De must be inserted after this
   !
   ! The language designator is composed for the first two character 
   ! which are normally stored into the environment variable LANG and/or
   ! LC_ALL, LC_MESSAGE. The first letter is to be written with upercase.

*/
#endif
   "EpsonUtil*unknown:                       unknown",

#if 0
/*
   ! The version number will be set into the c-source file !

*/
#endif
   "EpsonUtil*title_LB.labelString:          EPSON UTILITIES "VERSION,
   "",
   "EpsonUtil*legend_LB.labelString:         Ink quantity",

   "EpsonUtil*ok_PB.labelString:             Exit",

   "EpsonUtil*mainWindow.ok_PB.tooltip:      Abandon the\\nStatus Monitor.",

   "EpsonUtil*pref_PB.labelString:           Preference",

   "EpsonUtil*pref_PB.tooltip:"
   "- Browser,\\n"
   "- Mini help,\\n"
   "- Port choice\\n"
   "- Printer choice\\n"
   "- working mode",

   "EpsonUtil*about_PB.labelString:          About",

   "EpsonUtil*about_PB.tooltip:"
   "Licence and\\n"
   "cooperators",

   "EpsonUtil*help_PB.labelString:           Help",

   "EpsonUtil*help_PB.tooltip:               Display help within a browser.",

   "EpsonUtil*check_PB.labelString:          Check\\nNozzle",

   "EpsonUtil*check_PB.tooltip:"
   "Print a test pattern to check\\n"
   "for blocked nozzles.\\n"
   "Blocked nozzles cause blank\\n"
   "strips on print outs.",

   "EpsonUtil*clean_PB.labelString:          Clean\\nNozzle",

   "EpsonUtil*clean_PB.tooltip:"
   "If the nozzle are not OK,\\n"
   "you can clean the nozzle\\n"
   "herewith",

   "EpsonUtil*align_PB.labelString:          Align\\nHead",

   "EpsonUtil*align_PB.tooltip:"
   "Warning !\\n"
   "This may damage the printer.",

   "EpsonUtil*reset_PB.labelString:          Reset\\nPrinter",

   "EpsonUtil*reset_PB.tooltip:              This doesn't work for all models.",

   "EpsonUtil*cartridge_PB.labelString:      Change\\nCartridge",

   "EpsonUtil*cartridge_PB.tooltip:"
   "If your printer don't\\n"
   "offer any switch\\n"
   "You need this.",

   "EpsonUtil*addPrinterTxt:                 Other Printer",

   "EpsonUtil*printerState_LB.labelString:   State: ",

   "EpsonUtil*state_LB.labelString:   - ",

   "EpsonUtil*noPrinter*messageString:"
   "Problems with the printer communication,\\n"
   "please check the printer for errors:\\n"
   "\"Out of Paper\", \"No Ink\", \"printer not powered\"\\n"
   "\\n"
   "Note that some printer block for a few seconds after\\n"
   "powering on.",

   "EpsonUtil*noPrinter*dialogTitle:          Error",

   "EpsonUtil*cfg1_LB.labelString:            Printer Choice:",

   "EpsonUtil*cfg2Printer_PB.labelString:     Printer Choice:",

   "EpsonUtil*cfg2Printer_PB.tooltip:"
   "A list of printer will be offered.\\n"
   "You can choose your model there.",

   "EpsonUtil*cfg2Device_PB.labelString:      Port Choice:",

   "EpsonUtil*cfg2Device_PB.tooltip:"
   "It will be nice\\n"
   "if mtink where able\\nto perform this !",

   "EpsonUtil*next_PB.labelString:            Next",

   "EpsonUtil*next:                           Next",

   "EpsonUtil*previous_PB.labelString:        Previous",

   "EpsonUtil*previous:                       Previous",

   "EpsonUtil*save:                           Save",

   "EpsonUtil*cancel:                         Cancel",

   "EpsonUtil*ok:                             OK",

   "EpsonUtil*about:"
   "EPSON Utilities\\n"
   "\\n"
   "Version "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001\\n"
   "\\nEmail: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "Portions of code was taken from the gimp-print project\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Licence: GPL\\n"
   "\\n"
   "Thanks to:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens J�ger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter\\n"
   "  (leader Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil*ctTxt0:"
   "Please place a sheet of paper in your printer to begin the head alignment procedure.\\n"
   "Press [Next] to begin\\n"
   "or [Cancel] to abort.",

   "EpsonUtil*ctTxtC0:"
   "Please place a sheet of paper in your printer to begin the head alignment procedure.\\n"
   "Choose the Black or Color Head and\\n"
   "Press [Next] to begin or [Cancel] to abort.",

   "EpsonUtil*ctTxt1:"
   "Please inspect the print, and choose the best pair of lines the last printed pattern, then reinsert the page in the input tray\\nand press [Next] to continue.",

   "EpsonUtil*ctTxt1_1:"
   "Please inspect the print, and choose the best pair of lines the pattern #1,\\nthen press [Next] to continue.",

   "EpsonUtil*ctTxt1_2:"
   "Please inspect the print, and choose the best pair of lines the pattern #2,\\nthen press [Next] to continue.",

   "EpsonUtil*ctTxt1_3:"
   "Please inspect the print, and choose the best pair of lines the pattern #3,\\nthen press [Next] to continue.",

   "EpsonUtil*ctTxt1C:"
   "Inspect the alignment sheet, and determine which pattern is the smoothest.\\n"
   "This pattern will appear to have the least ``grain''.\\n"
   "If you cannot find a smooth pattern, please select the number for the\\n"
   "best pattern, and repeat the procedure.\\n"
   "Insert the sheet into the printer and press [Next] to continue.",

   "EpsonUtil*ctTxtP:"
   "Please wait until sheet is ejected from the printer and press [Next] to continue.",

   "EpsonUtil*ctTxt5:"
   "Please inspect the final output very carefully to ensure that your printer is in proper alignment.\\n"
   "\\n"
   "You may now:\\n"
   "\\n"
   "  [Save] the results in the printer or\\n"
   "  [Cancel] without saving the results",

   "EpsonUtil*exTxt0:"
   "Cartridge exchange:\\n"
   "\\n"
   "Please press :\\n"
   "\\n"
   "  [Next] \\to continue or\\n"
   "  [Cancel]",

   "EpsonUtil*exTxt00:"
   "Cartridge exchange:\\n"
   "\\n"
   "Please choose black or colour\\n"
   "Press:\\n"
   "\\n"
   "  [Next] in order to continue or\\n"
   "  [Cancel]",

   "EpsonUtil*exTxt1:"
   "Cartridge exchange:\\n"
   "\\n"
   "The head asembly will be moved to the exchange position.",

   "EpsonUtil*exTxt2:"
   "Cartridge exchange:\\n"
   "\\n"
   "Insert a new cartridge and:\\n"
   "\\n"
   "  Press [Next]",

   "EpsonUtil*exTxt3:"
   "Cartridge exchange:\\n"
   "\\n"
   "  process \"fill ink\".",

   "EpsonUtil*colors_RC*four_TB.labelString:    4 Colors",

   "EpsonUtil*colors_RC*six_TB.labelString:     6 colors",

   "EpsonUtil*head_RC*col_TB.labelString:       Color",

   "EpsonUtil*head_RC*bw_TB.labelString:        Black",

   "EpsonUtil*noAccess*dialogTitle:             Error",

   "EpsonUtil*scrTxt_MW.head_RC.bw_TB.tooltip:"
   "Apply the order\\n"
   "on the cartridge\\n"
   "with the black ink.",

   "EpsonUtil*scrTxt_MW.head_RC.col_TB.tooltip:"
   "Apply the order\\n"
   "on the cartridge\\n"
   "with the colored ink.",

   "EpsonUtil*noAccess*messageString:"
   "No access to printer device file.\\n"
   "\\n"
   "Please make sure that mtink has enough\\n"
   "right for accessing the device files.\\n"
   "\\n"
   "On Debian Based System as Ubuntu you must\\n"
   "be a member of the group lp.\\n"
   "Refer also to the documentation.",

   "EpsonUtil*fsb*dialogTitle:                 Mtink",

   "EpsonUtil*fsb*CancelLabelString:           Cancel",

   "EpsonUtil*fsb*okLabelString:               OK",

   "EpsonUtil*fsb*applyLabelString:            Filter",

   "EpsonUtil*fsb*filterLabelString: Filter",

   "EpsonUtil*fsb*fileListLabelString:         Files",

   "EpsonUtil*fsb*dirListLabelString:          Directories",

   "EpsonUtil*browser_LB.labelString:          Choose Browser",

   "EpsonUtil*browser_PB.labelString:          \\ ...\\ ",

   "EpsonUtil*browser_PB.tooltip:              Browser Selection",

   "EpsonUtil*tooltip_TB.labelString:          Mini Help",

   "EpsonUtil*tooltip_TB.tooltip:"
   "You can disable\\n"
   "the mini help\\n"
   "herewith.",

   "EpsonUtil*autodetect_TB.labelString:       Allow automatic detection",

   "EpsonUtil*autodetect_TB.tooltip:"
   "Mtink will, At Start, try to\\n"
   "find out the attached printermodel.\\n"
   "This don't work with all printers.",

   "EpsonUtil*save_PB.labelString:             Save",

   "EpsonUtil*save_PB.tooltip:                 Adopt the changes.",

   "EpsonUtil*ConfigureForm.cancel_PB.tooltip:"
   "Don't apply the changes,\\n"
   "quit the configuration.",

   "EpsonUtil*cancel_PB.labelString:           Cancel",

#if 0
/*
   ! Printer state

*/
#endif
   "EpsonUtil*error:                        Error",

   "EpsonUtil*printing:                     Printing",

   "EpsonUtil*selfTest:                     Self Test",

   "EpsonUtil*busy:                         Busy",

   "EpsonUtil*ok:                           OK",

   "EpsonUtil*cleaning:                     Cleaning",

   "EpsonUtil*unknown:                      Unknown",
#if 0
/*
   ! File Mtink.Fr, french resources.
   ! Fichier Mtink.Fr, resource fran�aises.
   !
   ! Notes pout les traducteurs. Ce fichier correspond au format des resources
   ! pour X11.
   !
   ! Les s�quences suivantes ont une signification sp�ciale:
   ! \    en fin de ligne (les espace et tabulateurs ne sont pas admis)
   !      signifie que le texte continue sur la ligne suivante.
   ! \n   correspond a un retour chariot.
   ! \    (\ et espace). L'espace fait parti du texte. 
   !      Les espace sont normalement �limin�s en d�but des textes.
   ! \t   correspond a un tabulateur.   
   ! !    en debut de ligne (les espaces en d�but de ligne sont admissibles) 
   !      d�note un commentaire.
   ! 
   ! Si vous traduis� un fichier resource vers une autre langue, il est neccessaire
   ! de modifier le mot clef en fonction de la langue cibl�e.
   ! Example:
   ! La resource EpsonUtil*unknown est a traduire en fran�ais:
   !
   !   EpsonUtil*unknown: unknown
   !   EpsonUtil.Fr*unknown: inconnus
   ! EpsonUtil est le nom de classe du programmme, .Fr doit etre inserr� a 
   ! apr�s le nom de classe. Si la traduction s'effectue du fran�ais vers
   ! l'italien, .Fr est � remplacer par .It.
   !
   ! Le code pour l'identification de la langue est extrait des variables
   ! d'environement LANG, LC_ALL und LC_MESSAGE. Le premier charact�re est
   ! transform� en lettre capitale.
   !
   ! J'ai, dans le but d'une meilleure lisibilit�, r�partit les textes
   ! sur plusieurs lignes, ce qui devrait rendre le formatage plus clair.

*/
#endif
   "EpsonUtil.Fr*unknown:"
   "inconnus",

#if 0
/*
   ! Le num�ro de version est corrig� lors de la g�n�ration des fichiers C.

*/
#endif
   "EpsonUtil.Fr*title_LB.labelString:"
   "UTILITAIRES EPSON "VERSION,
   "",
   "EpsonUtil.Fr*legend_LB.labelString:"
   "Quantit� d'encre",

   "EpsonUtil.Fr*ok_PB.labelString:"
   "Quitter",

   "EpsonUtil.Fr.mainWindow.ok_PB.tooltip:"
   "Au revoir.",

   "EpsonUtil.Fr*pref_PB.labelString"
   " Pr�f�rence",

   "EpsonUtil.Fr*pref_PB.tooltip:"
   "- Browser,\\n"
   "- Bulles d'aide,\\n"
   "- Fichier de connexion\\n"
   "- Imprimante\\n"
   "- Mode de travail",

   "EpsonUtil.Fr*about_PB.labelString:"
   "A propos",

   "EpsonUtil.Fr*about_PB.tooltip:"
   "Licence et\\n",
   "cooperateurs",

   "EpsonUtil.Fr*help_PB.labelString:"
   "Aide",

   "EpsonUtil.Fr*help_PB.tooltip:"
   "Afficher l'aide avec un browser.",

   "EpsonUtil.Fr*check_PB.labelString:"
   "Test\\n"
   "buses",

   "EpsonUtil.Fr*check_PB.tooltip:"
   "Ici, vous pouvez controller\\n"
   "l'�tat des buses.",

   "EpsonUtil.Fr*clean_PB.labelString:"
   "Nettoyage\\n"
   "buses",

   "EpsonUtil.Fr*clean_PB.tooltip:"
   "Le nettoyages des buses\\n"
   "permet l'�limination\\n"
   "des \"rayures\".",

   "EpsonUtil.Fr*align_PB.labelString:"
   "Alignement\\n",

   "EpsonUtil.Fr*align_PB.tooltip:"
   "Attention !\\n"
   "Ceci peu rendre l'imprimante inutilisable.",

   "EpsonUtil.Fr*reset_PB.labelString:"
   "RAZ\\n"
   "imprimante",

   "EpsonUtil.Fr*reset_PB.tooltip:"
   "La remise a z�ro\\n"
   "ne fonctionne pas\\n"
   "pour toutes les imprimantes.",

   "EpsonUtil.Fr*cartridge_PB.labelString:"
   "Changement\\n"
   "cartouche",

   "EpsonUtil.Fr*cartridge_PB.tooltip:"
   "Pour les imprimantes\\n"
   "sans commutateurs.",

   "EpsonUtil.Fr*addPrinterTxt:"
   "Autre Imprimante",

   "EpsonUtil.Fr*printerState_LB.labelString:"
   "Status: ",

   "EpsonUtil.Fr*state_LB.labelString:",
   "- ",

   "EpsonUtil.Fr*noPrinter*messageString:"
   "Probleme de communication\\n"
   "Controllez l'imprimante pour:\\n"
   "\"manque de papier\", \"encre epuis�e\"\\n"
   "\"imprimante hors service\"\\n"
   "\\n"
   "Notez que certaines imprimantes ne\\n"
   "sont accessibles que plusieurs secondes apr�s\\n"
   "la mise sous tension.",

   "EpsonUtil.Fr*noPrinter*dialogTitle:"
   "Erreur",

   "EpsonUtil.Fr*cfg1_LB.labelString:"
   "Choix de l'imprimante:",

   "EpsonUtil.Fr*cfg2Printer_PB.labelString:"
   "Choix de l'imprimante:",

   "EpsonUtil.Fr*cfg2Printer_PB.tooltip:"
   "Une liste d'imprimante est propos�e.",

   "EpsonUtil.Fr*cfg2Device_PB.labelString:"
   "Choix du port:",

   "EpsonUtil.Fr*cfg2Device_PB.tooltip:",
   "Malheureusement, la d�tection\\nn'est pas automatique !",

   "EpsonUtil.Fr*next_PB.labelString:"
   "Suivant",

   "EpsonUtil.Fr*next:"
   "Suivant",

   "EpsonUtil.Fr*previous_PB.labelString:"
   "pr�c�dent",

   "EpsonUtil.Fr*previous:"
   "pr�c�dent",

   "EpsonUtil.Fr*save:"
   "Enregistrer",

   "EpsonUtil.Fr*cancel:"
   "Annuler",

   "EpsonUtil.Fr*ok:"
   "OK",

   "EpsonUtil.Fr*about:"
   "EPSON Utilities\\n"
   "\\n"
   "Version "VERSION"\\n\\nCopyright: Jean-Jacques Sarton 2001\\n"
   "\\n"
   "Email: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "Des portions de code ont ete tir�es du projet gimp-print\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Licence: GPL\\n"
   "\\n"
   "Mes remerciement a:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens J�ger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter\\n"
   "  (leader Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.Fr*ctTxt0:"
   "Alignement du chariot:\\n"
   "\\n"
   "Placez une feuille vierge dans l'imprimante et activez [Suivant]pour continuer ou [Annuler]",

   "EpsonUtil.Fr*ctTxtC0:"
   "Alignement du chariot:\\n"
   "\\n"
   "Placez une feuille vierge dans l'imprimante,choisissez la t�te "
   "(noir ou couleur) et activez [Suivant] pour continuer ou [Annuler]",

   "EpsonUtil.Fr*ctTxt1:"
   "Inspectez l�impression et choisissez la meilleure paire de lignes.\\n"
   "Remettez la feuille dans l�imprimante et activez [Suivant] pour continuer.",

   "EpsonUtil.Fr*ctTxt1_1:"
   "Echantillon #1\\n"
   "Inspectez l�impression et choisissez la meilleure paire de lignes.\\n"
   "Activez [Suivant] pour continuer.",

   "EpsonUtil.Fr*ctTxt1_2:"
   "Echantillon #2\\n"
   "Inspectez l�impression et choisissez la meilleure paire de lignes.\\n"
   "Activez [Suivant] pour continuer.",

   "EpsonUtil.Fr*ctTxt1_3:"
   "Echantillon #3\\n"
   "Inspectez l�impression et choisissez la meilleure paire de lignes.\\n"
   "Activez [Suivant] pour continuer.",

   "EpsonUtil.Fr*ctTxt1C:"
   "Inspectez la feuille d'alignment, et determinez l'echantillon le plus \"lisse\".\\n"
   "Cet echantillon est celui qui semble etre le moins \"granulleux\".\\n"
   "Si vous ne trouvez pas d'echantillon \"lisse\" choisissez le meilleur echantillon\\n"
   "et r�p�tez la procedure d'ajustage.\\n"
   "Activez [Suivant] pour continuer.",

   "EpsonUtil.Fr*ctTxtP:"
   "Attendez jusqu�� la fin de l�impression et activez [Suivant] pour continuer.",

   "EpsonUtil.Fr*ctTxt5:"
   "Inspectez le resultat final afin de d�terminer si l'alignment est correct.\\n"
   "\\n"
   "Vous pouvez:\\n"
   "\\n"
   "  [Enregistrer] les r�sultats dans l'imprimante ou\\n"
   "  [Annuler] l'enregistrement dans l'imprimante",

   "EpsonUtil.Fr*exTxt0:"
   "Remplacement des cartouches:\\n"
   "\\nActivez :\\n"
   "\\n"
   "  [Suivant] pour continuer ou\\n"
   "  [Annuler]",

   "EpsonUtil.Fr*exTxt00:"
   "Remplacement des cartouches:\\n"
   "\\n"
   "Choisissez noir ou couleur\\n"
   "Activez :\\n"
   "\\n"
   "  [Suivant] pour continuer ou\\n"
   "  [Annuler]",

   "EpsonUtil.Fr*exTxt1:"
   "Remplacement des cartouches:\\n"
   "\\n"
   "Le chariot est ammene a la position de remplacement.",

   "EpsonUtil.Fr*exTxt2:"
   "Remplacement des cartouches:\\n"
   "\\n"
   "remplacez la cartouche et:\\n"
   "\\n"
   "  activez [Suivant]",

   "EpsonUtil.Fr*exTxt3:"
   "Remplacement des cartouches:\\n"
   "\\n"
   "  le remplissage est en cours.",

   "EpsonUtil.Fr*colors_RC*four_TB.labelString:"
   "4 couleurs",

   "EpsonUtil.Fr*colors_RC*six_TB.labelString:"
   "6 Couleurs",

   "EpsonUtil.Fr*head_RC*col_TB.labelString:"
   "Couleur",

   "EpsonUtil.Fr*head_RC*bw_TB.labelString:"
   "Noir",

   "EpsonUtil.Fr*noAccess*dialogTitle:"
   "Erreur",

   "EpsonUtil.Fr.scrTxt_MW.head_RC.bw_TB.tooltip:"
   "L'operation sur la\\n"
   "cartouche contenant\\n"
   "l'encre noire.",

   "EpsonUtil.Fr.scrTxt_MW.head_RC.col_TB.tooltip:"
   "L'operation sur la\\n"
   "cartouche contenant\\n"
   "les encres color�es.",

   "EpsonUtil.Fr*noAccess*messageString:"
   "Pas de droit d'acc�s aux\\n"
   "fichiers /dev/...\\n"
   "\\nPour les distribitions bas�e sur Debian\\n"
   "l'utilisateur doit �tre membre du groupe lp.\\n"
   "\\nRem�diez a ceci. Consulter la documentation si\\n"
   "necessaire.",

   "EpsonUtil.Fr*fsb*dialogTitle: Mtink",

   "EpsonUtil.Fr*fsb*CancelLabelString:"
   "Annuler",

   "EpsonUtil.Fr*fsb*okLabelString:"
   "OK",

   "EpsonUtil.Fr*fsb*applyLabelString:"
   "Filtre",

   "EpsonUtil.Fr*fsb*filterLabelString:"
   "Filtre",

   "EpsonUtil.Fr*fsb*fileListLabelString:"
   "Fichiers",

   "EpsonUtil.Fr*fsb*dirListLabelString:"
   "Repertoires",

   "EpsonUtil.Fr*browser_LB.labelString:"
   "Choisissez le Browser",

#if 0
/*
   ! Attention un espace se trouve en fin de ligne

*/
#endif
   "EpsonUtil.Fr*browser_PB.labelString:"
   " ...\\ ",

   "EpsonUtil.Fr*browser_PB.tooltip:"
   "Choix du browser.",

   "EpsonUtil.Fr*tooltip_TB.labelString:"
   "Bulles d'aide",

   "EpsonUtil.Fr*tooltip_TB.tooltip:"
   "Bulles d'aide\\n"
   "hors / en service",

   "EpsonUtil.Fr*autodetect_TB.labelString:"
   "Autoriser la detection automatique",

   "EpsonUtil.Fr*autodetect_TB.tooltip:"
   "La d�tection automatique de\\n"
   "l'imprimante ne fonctionne\\n"
   "pas sur tout les mod�les.\\n"
   "Il est n�cessaire, pour certaine\\n"
   "imprimantes de ne pas autoriser\\n"
   "cette d�tection",

   "EpsonUtil.Fr*save_PB.labelString:"
   "Enregistrer",

   "EpsonUtil.Fr*save_PB.tooltip:"
   "Prise en compte des modifications.",

   "EpsonUtil.Fr.ConfigureForm.cancel_PB.tooltip:"
   "Quiiter le mmasque sans\\n"
   "prise en compte des modifications.",

   "EpsonUtil.Fr*cancel_PB.labelString:"
   "Annuler",

#if 0
/*
   ! Printer state
*/
#endif
   "EpsonUtil.Fr*error:"
   "Erreur",

   "EpsonUtil.Fr*printing:"
   "Impression",

   "EpsonUtil.Fr*selfTest:"
   "Test",

   "EpsonUtil.Fr*busy:"
   "Occup�",

   "EpsonUtil.Fr*ok:"
   "OK",

   "EpsonUtil.Fr*cleaning:"
   "Nettoyage",

   "EpsonUtil.Fr*unknown:"
   "Inconnu",
#if 0
/*
   ! Mtink.Hu f�jl, hungarian resource
   !
   ! Megjegyz�s ford�t�knak. Ez egy X-Resource f�jl. A k�vetkez�
   ! a k�vetkez� karaktersorozatnak egy speci�lis jelent�se van:
   ! \    egy sor v�g�n (nincs sz�koz a \ jel ut�n):
   !      forr�sfolytat�s a k�vetkez� sorn�l.
   ! \n   Ez egy soremel�s.
   ! \    (\ �s sz�k�z) a sz�k�z karakter enged�lyezi a forr�s
   !      sort. Rendes k�r�lm�nyek k�z�tt a sz�k�z a forr�s elej�n �s
   !      v�g�n ki van z�rva.
   ! \t   Ez egy tabul�torkarakterhez van.
   !
   ! Ha ezt ford�tja, a megfelel� nyelvi megjel�l�st kell alkalmaznia a
   ! kulcsb�l.
   ! P�ld�ul:
   ! A forr�s alap�rtelmez�s�t EpsonUtil*unknown n�metre kell leford�tani:
   !
   !   EpsonUtil*unknown: unknown
   !   EpsonUtil.De*unknown: Unbekannt
   ! EpsonUtil a programoszt�ly neve, .De-t kell ezut�n besz�rni.
   !
   ! A nyelvi megjel�l�s els� k�t karakter�t rendes k�r�lm�nyek k�z�tt
   ! a LANG �s/vagy! LC_ALL, LC_MESSAGE k�rnyezeti v�ltoz�k tartalmazz�k.
   ! Az els� bet�t a fentiek szerint kell �rni.

*/
#endif
   "EpsonUtil.Hu*unknown:                       ismeretlen",

#if 0
/*
   ! A verzi� sz�ma a c-forr�s f�jlban van be�ll�tva!

*/
#endif
   "EpsonUtil.Hu*title_LB.labelString:          EPSON Seg�dprogramok "VERSION,
   "",
   "EpsonUtil.Hu*legend_LB.labelString:         Tintamennyis�g",

   "EpsonUtil.Hu*ok_PB.labelString:             Kil�p�s",

   "EpsonUtil.Hu*mainWindow.ok_PB.tooltip:      Az �llapotmonitor\\nelhagy�sa.",

   "EpsonUtil.Hu*pref_PB.labelString:           Be�ll�t�sok",

   "EpsonUtil.Hu*pref_PB.tooltip:"
   "- B�ng�sz�,\\n"
   "- Mini seg�ts�g,\\n"
   "- Port v�laszt�s\\n"
   "- Nyomtat� kiv�laszt�sa\\n"
   "- M�k�d�si m�d",

   "EpsonUtil.Hu*about_PB.labelString:          K�sz�t�k",

   "EpsonUtil.Hu*about_PB.tooltip:"
   "Enged�ly �s\\n"
   "k�zrem�k�d�k",

   "EpsonUtil.Hu*help_PB.labelString:           Seg�ts�g",

   "EpsonUtil.Hu*help_PB.tooltip:               Seg�ts�g megjelen�t�se egy b�ng�sz�ben.",

   "EpsonUtil.Hu*check_PB.labelString:          F�v�ka\\nellen�rz�s",

   "EpsonUtil.Hu*check_PB.tooltip:"
   "Egy tesztminta nyomtat�sa\\n"
   "f�v�kadugul�s ellen�rz�s�hez.\\n"
   "Eldugult f�v�k�k �res s�vokat\\n"
   "okoznak a nyomtat�son.",

   "EpsonUtil.Hu*clean_PB.labelString:          F�v�ka\\ntiszt�t�s",

   "EpsonUtil.Hu*clean_PB.tooltip:"
   "Ha a f�v�ka nem OK,\\n"
   "meg tudod tiszt�tani a f�v�k�t\\n"
   "ezzel",

   "EpsonUtil.Hu*align_PB.labelString:          Fej\\npozicion�l�s",

   "EpsonUtil.Hu*align_PB.tooltip:"
   "Figyelem!\\n"
   "Ez megrong�lhatja a nyomtat�t.",

   "EpsonUtil.Hu*reset_PB.labelString:          Nyomtat�\\nreset",

   "EpsonUtil.Hu*reset_PB.tooltip:              Ez nem m�k�dik minden modellel.",

   "EpsonUtil.Hu*cartridge_PB.labelString:      Tintapatron\\ncsere",

   "EpsonUtil.Hu*cartridge_PB.tooltip:"
   "Ha a nyomtat� nem\\n"
   "ad kapcsolatot,\\n"
   "erre lehet sz�ks�ged.",

   "EpsonUtil.Hu*addPrinterTxt:                 M�s nyomtat�",

   "EpsonUtil.Hu*printerState_LB.labelString:   �llapot:",

   "EpsonUtil.Hu*state_LB.labelString:   -",

   "EpsonUtil.Hu*noPrinter*messageString:"
   "Probl�ma a nyomtat�kommunik�ci�val,\\n"
   "A hib�k miatt k�rem ellen�rizze a nyomtat�t:\\n"
   "\"Kifogyott a pap�r\", \"Nincs tinta\", \"A nyomtat� nincs bekapcsolva\"\\n"
   "\\n"
   "Figyelmeztet�s, hogy n�mely nyomtat� le�ll n�h�ny m�sodpercre \\n"
   "a bekapcsol�s ut�n.",

   "EpsonUtil.Hu*noPrinter*dialogTitle:          Hiba",

   "EpsonUtil.Hu*cfg1_LB.labelString:            Nyomtat� kiv�laszt�sa:",

   "EpsonUtil.Hu*cfg2Printer_PB.labelString:     Nyomtat� kiv�laszt�sa:",

   "EpsonUtil.Hu*cfg2Printer_PB.tooltip:"
   "A nyomtat�k list�j�t aj�nlom.\\n"
   "Ott kiv�laszthatja a modellj�t.",

   "EpsonUtil.Hu*cfg2Device_PB.labelString:      Port v�laszt�s:",

   "EpsonUtil.Hu*cfg2Device_PB.tooltip:"
   "A legjobb,\\n"
   "ha az mtink\\nhajtja v�gre!",

   "EpsonUtil.Hu*next_PB.labelString:            K�vetkez�",

   "EpsonUtil.Hu*next:                           K�vetkez�",

   "EpsonUtil.Hu*previous_PB.labelString:        El�z�",

   "EpsonUtil.Hu*previous:                       El�z�",

   "EpsonUtil.Hu*save:                           Ment�s",

   "EpsonUtil.Hu*cancel:                         Visszavon",

   "EpsonUtil.Hu*ok:                             OK",

   "EpsonUtil.Hu*about:"
   "EPSON Seg�dprogramok\\n"
   "\\n"
   "Version "VERSION"\\n"
   "\\n"
   "Szerz�i jog: Jean-Jacques Sarton 2001\\n"
   "\\nEmail: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "K�dkioszt�s a gimp-print projectb�l k�sz�lt\\n"
   "Szerz�i jog 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Enged�ly: GPL\\n"
   "\\n"
   "K�sz�net:\\n"
   "Keith Amidon,\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett,\\n"
   "   gene_heskett@iolinc.net\\n"
   "Till Kamppeter\\n"
   "  (vezet� Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.Hu*ctTxt0:"
   "K�rem helyezzen egy lapot a nyomtat�ba a fejigaz�t�s folyamata el�tt.\\n"
   "Nyomjon [K�vetkez�]-t a folytat�shoz\\n"
   "vagy [Visszavon]-t a kil�p�shez.",

   "EpsonUtil*ctTxtC0:"
   "K�rem helyezzen egy lapot a nyomtat�ba a fejigaz�t�s folyamata el�tt.\\n"
   "V�lasszon a Fekete vagy Sz�nes fej k�z�tt �s\\n"
   "Nyomjon [K�vetkez�]-t a kezd�shez vagy [Visszavon]-t a kil�p�shez.",

   "EpsonUtil.Hu*ctTxt1:"
   "K�rem vizsg�lja meg a nyomtat�st, �s v�lassza az utols� nyomtat�si minta legjobb sorp�rj�t, azut�n helyezzen pap�rt az adagol�ba\\n�s nyomja meg a [K�vetkez�]-t a folytat�shoz.",

   "EpsonUtil.Hu*ctTxt1C:"
   "Vizsg�lja meg az igaz�t�si lapot, �s d�ntse el melyik minta a legegyenletesebb.\\n"
   "Ez a minta fog a legkisebb szemcs�zet�nek l�tszani.\\n"
   "Ha nen tudja kiv�lasztani az egyenletes mint�t, k�rj�k v�lassza a legjobb\\n"
   "minta sz�m�t, �s ism�telje meg az elj�r�st.\\n"
   "Helyezzen lapot a nyomtat�ba �s nyomja meg a [K�vetkez�]-t a folytat�shoz.",

   "EpsonUtil.Hu*ctTxtP:"
   "K�rem v�rja meg, am�g a nyomtat� lapot dob �s nyomja meg a [K�vekez�]-t a folytat�shoz.",

   "EpsonUtil.Hu*ctTxt5:"
   "K�rem vizsg�lja meg az utols� nyomatot nagyon figyelmesen, hogy a nyomtat� helyesen legyen be�ll�tva.\\n"
   "\\n"
   "Most lehet:\\n"
   "\\n"
   "  az eredm�nyek [Ment�s]-e a nyomtat�ba vagy\\n"
   "  [Visszavon]-�s az eredm�nyek ment�se n�lk�l",

   "EpsonUtil.Hu*exTxt0:"
   "Tintapatron csere:\\n"
   "\\n"
   "K�rem nyomjon :\\n"
   "\\n"
   "  [K�vetkez�]-t \\a folytat�shoz, vagy\\n"
   "  [Visszavon]-t",

   "EpsonUtil.Hu*exTxt00:"
   "Tintapatron csere:\\n"
   "\\n"
   "K�rem v�lasszon: fekete vagy sz�nes\\n"
   "Nyomjon:\\n"
   "\\n"
   "  [K�vetkez�]-t a folytat�shoz, vagy\\n"
   "  [Visszavon]-t",

   "EpsonUtil.Hu*exTxt1:"
   "Tintapatron csere:\\n"
   "\\n"
   "A fejilleszt� a cserepoz�ci�ba fog mozogni.",

   "EpsonUtil.Hu*exTxt2:"
   "Tintapatron csere:\\n"
   "\\n"
   "Illessze be az �j patront �s:\\n"
   "\\n"
   "  nyomjon [K�vetkez�]-t",

   "EpsonUtil.Hu*exTxt3:"
   "Tintapatron csere:\\n"
   "\\n"
   "  \"tintafelt�lt�s\" folyamat.",

   "EpsonUtil.Hu*colors_RC*four_TB.labelString:    4 sz�n�",

   "EpsonUtil.Hu*colors_RC*six_TB.labelString:     6 sz�n�",

   "EpsonUtil.Hu*head_RC*col_TB.labelString:       sz�nes",

   "EpsonUtil.Hu*head_RC*bw_TB.labelString:        fekete",

   "EpsonUtil.Hu*noAccess*dialogTitle:             Hiba",

   "EpsonUtil.Hu*scrTxt_MW.head_RC.bw_TB.tooltip:"
   "Alkalmazza a parancsot\\n"
   "a tintapatronra\\n"
   "fekete tint�val.",

   "EpsonUtil.Hu*scrTxt_MW.head_RC.col_TB.tooltip:"
   "Alkalmazza a parancsot\\n"
   "a tintapatronra\\n"
   "sz�nes tint�val.",

   "EpsonUtil.Hu*noAccess*messageString:"
   "Nincs hozz�f�r�s a nyomtat� eszk�zf�jlhoz.\\n"
   "\\n"
   "K�rem gy�z�dj�n meg arr�l, hogy az mtink elegend�\\n"
   "jogosults�ggal rendelkezik az eszk�zf�jl el�r�s�hez.\\n"
   "\\n"
   "Hivatkoz�s szint�n a dokument�ci�ra.",

   "EpsonUtil.Hu*fsb*dialogTitle:                 Mtink",

   "EpsonUtil.Hu*fsb*CancelLabelString:           Visszavon",

   "EpsonUtil.Hu*fsb*okLabelString:               OK",

   "EpsonUtil.Hu*fsb*applyLabelString:            Sz�r�",

   "EpsonUtil.Hu*fsb*filterLabelString: Sz�r�",

   "EpsonUtil.Hu*fsb*fileListLabelString:         F�jlok",

   "EpsonUtil.Hu*fsb*dirListLabelString:          K�nyvt�rak",

   "EpsonUtil.Hu*browser_LB.labelString:          V�lasszon b�ng�sz�t",

   "EpsonUtil.Hu*browser_PB.labelString:          \\ ..."
   "",
   "EpsonUtil.Hu*browser_PB.tooltip:              B�ng�sz� kiv�laszt�sa",

   "EpsonUtil.Hu*tooltip_TB.labelString:          Mini seg�ts�g",

   "EpsonUtil.Hu*tooltip_TB.tooltip:"
   "Letilthatja\\n"
   "a mini seg�ts�get\\n"
   "itt.",

   "EpsonUtil.Hu*autodetect_TB.labelString:       Automatikus felismer�s enged�lyez�se.",

   "EpsonUtil.Hu*autodetect_TB.tooltip:"
   "Mtink az ind�t�sn�l fog pr�b�lkozni\\n"
   "a csatlakoztatott nyomtat�modell megkeres�s�vel.\\n"
   "Ez nem m�k�dik minden nyomtat�modellel.",

   "EpsonUtil.Hu*save_PB.labelString:             Ment�s",

   "EpsonUtil.Hu*save_PB.tooltip:                 A v�ltoztat�sok elfogad�sa.",

   "EpsonUtil.Hu*ConfigureForm.cancel_PB.tooltip:"
   "Ne alkalmazza a v�ltoztat�sokat,\\n"
   "a kongfigur�ci� elhagy�sa.",

   "EpsonUtil.Hu*cancel_PB.labelString:           Viszavon",

#if 0
/*
   ! Printer state

*/
#endif
   "EpsonUtil.Hu*error:                        Hiba",

   "EpsonUtil.Hu*printing:                     Nyomtat�s",

   "EpsonUtil.Hu*selfTest:                     �nteszt",

   "EpsonUtil.Hu*busy:                         Foglalt",

   "EpsonUtil.Hu*ok:                           OK",

   "EpsonUtil.Hu*cleaning:                     Tiszt�t�s",

   "EpsonUtil.Hu*unknown:                      Ismeretlen",
#if 0
/*
   ! File Mtink.It, italian resources
*/
#endif
   "EpsonUtil.It*unknown: sconosciuto",
   "EpsonUtil.It*title_LB.labelString: STRUMENTI EPSON "VERSION,
   "EpsonUtil.It*legend_LB.labelString: Livello inchiostro",
   "EpsonUtil.It*ok_PB.labelString: Esci",
   "EpsonUtil.It.mainWindow.ok_PB.tooltip:  Ciao.",
   "EpsonUtil.It*pref_PB.labelString: Preferenze",
   "EpsonUtil.It*pref_PB.tooltip: - Browser,\\n- Mini aiuto ,\\n- Scelta porta\\n- Scelta stampante\\n- Modalit� operativa",
   "EpsonUtil.It*about_PB.labelString: Informazioni",
   "EpsonUtil.It*about_PB.tooltip: Licenza e\\ncooperatori",
   "EpsonUtil.It*help_PB.labelString: Aiuto",
   "EpsonUtil.It*help_PB.tooltip: Mostra l'aiuto con un browser.",
   "EpsonUtil.It*check_PB.labelString: Controllo\\nUgelli",
   "EpsonUtil.It*check_PB.tooltip: Se la stampa presenta striscie\\npuoi controllare qui\\n se gli ugelli siano sporchi.",
   "EpsonUtil.It*clean_PB.labelString: Pulizia\\nugelli",
   "EpsonUtil.It*clean_PB.tooltip: Se gli ugelli non sono a posto,\\npuoi pulirli qui.",
   "EpsonUtil.It*align_PB.labelString: Allineamento\\nTestina",
   "EpsonUtil.It*align_PB.tooltip: Attenzione !\\nQuesto pu� danneggiare la stampante.",
   "EpsonUtil.It*reset_PB.labelString: Ripristina\\nStampante",
   "EpsonUtil.It*reset_PB.tooltip: Il ripristino\\nnon funziona\\ncon tutte le stampanti.",
   "EpsonUtil.It*cartridge_PB.labelString: Cambio\\ncartuccia",
   "EpsonUtil.It*cartridge_PB.tooltip: Se la tua stampante non\\nha alcun pulsante\\nhai bisogno di questo.",
   "EpsonUtil.It*addPrinterTxt: Altra stampante",
   "EpsonUtil.It*printerState_LB.labelString: Stato: ",
   "EpsonUtil.It*state_LB.labelString: - ",
   "EpsonUtil.It*noPrinter*messageString: Problemi con la comunicazioneverso la stampante,\\ncontrolla se vi siano errori:\\n\"Carta terminata\", \"Non c'� inchiostro\", \"Stampante non accesa\"\\n\\nNota che alcune stampanti si bloccano per alcuni secondi dopo\\nesser state accese.",
   "EpsonUtil.It*noPrinter*dialogTitle: Errore",
   "EpsonUtil.It*cfg1_LB.labelString: Scelta stampante:",
   "EpsonUtil.It*cfg2Printer_PB.labelString: Scelta stampante:",
   "EpsonUtil.It*cfg2Printer_PB.tooltip: Verr� proposta una lista di stampanti.\\nPuoi scegliere qui il tuo modello.",
   "EpsonUtil.It*cfg2Device_PB.labelString: Scelta port:",
   "EpsonUtil.It*cfg2Device_PB.tooltip: Sarebbe bello\\nse mtink fosse in\\ngrado di farlo !",
   "EpsonUtil.It*next_PB.labelString: Avanti",
   "EpsonUtil.It*next: Avanti",
   "EpsonUtil.It*previous_PB.labelString: Indietro",
   "EpsonUtil.It*previous: Indietro",
   "EpsonUtil.It*save: Salva",
   "EpsonUtil.It*cancel: Annulla",
   "EpsonUtil.It*ok: OK",

   "EpsonUtil.It*about:"
   "Strumenti EPSON\\n"
   "\\n"
   "Versione "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001\\n"
   "\\n"
   "Email: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "Parti del codice sono state prese dal progetto gimp-print\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Licenza: GPL\\n"
   "Un grazie a:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens J�ger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter\\n"
   "  (leader Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.It*ctTxt0: Inserisci un foglio nella stampante per iniziare la procedura di allineamento testine.\\nPremi [Avanti] per iniziare\\no [Annulla] per terminare.",
   "EpsonUtil.It*ctTxtC0: Inserisci un foglio nella stampante per iniziare la procedura di allineamento testine.\\nScegli la cartuccia Nero o Colore e\\npremi [Avanti] per iniziare\\no [Annulla] per terminare.",
   "EpsonUtil.It*ctTxt1: Controlla la stampa, e scegli la miglior coppia di linee, poi rimetti la pagina nel vassoio della carta\\ne premi [Avanti] per continuare.",
   "EpsonUtil*ctTxt1_1: campione #1\\nAnalizza la stampa, e scegli la miglior coppia di linee dell'ultima stampa effettuata.",
   "EpsonUtil*ctTxt1_2: campione #2\\nAnalizza la stampa, e scegli la miglior coppia di linee dell'ultima stampa effettuata.",
   "EpsonUtil*ctTxt1_3: campione #3\\nAnalizza la stampa, e scegli la miglior coppia di linee dell'ultima stampa effettuata.",
   "EpsonUtil.It*ctTxt1C: Controlla l'allineamento del foglio, e vedi quale pattern sia meglio definito.\\nE' il pattern che ha grana pi� fine.\\nSe non trovassi un pattern a grana fine, scegli il numero del\\npattern migliore, e ripeti la procedura.\\nInserisci il foglio nella stampante e premi [Avanti] per continuare.",
   "EpsonUtil.It*ctTxtP: Attendi che il foglio sia uscito dalla stampante e premi [Avanti] per continuare.",
   "EpsonUtil.It*ctTxt5: Analizza il risultato finale molto attentamente, per assicurarti che la stampante sia allineata in modo corretto.\\n\\nPuoi ora :\\n\\n  [Salvare] i risultati nella stampante o\\n  [Annullare] senza salvare i risultati",
   "EpsonUtil.It*exTxt0: Cambio cartuccia:\\n\\nPremi :\\n\\n  [Avanti] per continuare o\\n  [Annulla]",
   "EpsonUtil.It*exTxt00: Cambio cartuccia:\\n\\nScegli nero o colore\\nPremi :\\n\\n  [Avanti] per continuare o\\n  [Annulla]",
   "EpsonUtil.It*exTxt1: Cambio cartuccia:\\n\\nLa testina verr� portata in posizione per la sistituzione.",
   "EpsonUtil.It*exTxt2: Cambio cartuccia:\\n\\nInserisci una nuova cartuccia e:\\n\\n  Premi [Avanti]",
   "EpsonUtil.It*exTxt3: Cambio cartuccia:\\n\\n  processo \"riempimento inchistro\".",
   "EpsonUtil.It*colors_RC*four_TB.labelString: 4 Colori",
   "EpsonUtil.It*colors_RC*six_TB.labelString: 6 colori",
   "EpsonUtil.It*head_RC*col_TB.labelString: Colore",
   "EpsonUtil.It*head_RC*bw_TB.labelString: Nero",
   "EpsonUtil.It*noAccess*dialogTitle: Errore",
   "EpsonUtil.It.scrTxt_MW.head_RC.bw_TB.tooltip:  Operazione\\nsulla cartuccia\\ncon l'inchiostro nero.",
   "EpsonUtil.It.scrTxt_MW.head_RC.col_TB.tooltip: Operazione\\nsulla cartuccia\\ncon l'inchiostro colorato.",
   "EpsonUtil.It*noAccess*messageString: Nessun accesso al file\\ndel dispositivo stampante\\n\\nAssicurati che mtink abbia i\\ndiritti per accedere ai file dispositivo.\\n\\nFai anche riferimento alla documentazione.",
   "EpsonUtil.It*fsb*dialogTitle: Mtink",
   "EpsonUtil.It*fsb*CancelLabelString: Annulla",
   "EpsonUtil.It*fsb*okLabelString: OK",
   "EpsonUtil.It*fsb*applyLabelString: Filtro",
   "EpsonUtil.It*fsb*filterLabelString: Filtro",
   "EpsonUtil.It*fsb*fileListLabelString: File",
   "EpsonUtil.It*fsb*dirListLabelString:  Cartelle",
   "EpsonUtil.It*browser_LB.labelString:  Scegli il browser",
   "EpsonUtil.It*browser_PB.labelString:  \\ ...\\ ",
   "EpsonUtil.It*browser_PB.tooltip:  Scelta del browser.",
   "EpsonUtil.It*tooltip_TB.labelString:  Mini aiuto",
   "EpsonUtil.It*tooltip_TB.tooltip:  Puoi disabilitare\\nil mini aiuto\\nqui.",
   "EpsonUtil.It*autodetect_TB.labelString: Permetti il rilevamento automatico.",
   "EpsonUtil.It*autodetect_TB.tooltip: Mtink, all'avvio, cercher�\\ndi trovare il modello della stamante collegata.\\nQuesto non funziona con tutte le stampanti.",
   "EpsonUtil.It*save_PB.labelString:  Salva",
   "EpsonUtil.It*save_PB.tooltip:  Salva i cambiamenti.",
   "EpsonUtil.It*ConfigureForm.cancel_PB.tooltip:  Non applicare i cambiamenti,\\nesci dalla configurazione.",
   "EpsonUtil.It*cancel_PB.labelString:  Annulla",

#if 0
/*
   ! Printer state
*/
#endif
   "EpsonUtil.It*error:"
   "Errore",

   "EpsonUtil.It*printing:"
   "Stampa in corso",

   "EpsonUtil.It*selfTest:"
   "Auto test",

   "EpsonUtil.It*busy:"
   "Occupato",

   "EpsonUtil.It*ok:"
   "OK",

   "EpsonUtil.It*cleaning:"
   "Pulizia in corso",

   "EpsonUtil.It*unknown:"
   "Sconosciuto",
#if 0
/*
   ! File Mtink.Pf, Pf�lzer ressources
   ! Datei Mtink.Pf, P�lzer   Ressourcen.
   !
   ! Notiz f�r �bersetzer. Dies ist eine X-Ressource-Datei.
   ! Nachstehende Zeichensequenzen haben eine spezielle Bedeutung:
   ! \    Am Ende einer Zeile (Leerzeichen und Tabulatoren d�rfen nicht
   !      vorhanden sein) bedeutet, dass die Ressource in der n�chste Zeile
   !      fortgesetzt werden.
   ! \n   Dies steht f�r eine Zeilenschaltung.
   ! \    (\ und Leerzeichen) Das Leerzeichen ist Teil der Zeichenkette.
   !      Leerzeichen am Anfang eine Zeichenkette werden normalerweise
   !      eliminiert.
   ! \t   Steht f�r ein Tabulatorzeichen.
   ! !    am Anfang einer Zeile (Leerzeichen d�rfen vorgesetzt sein) leitet
   !      ein Kommentar ein.
   ! 
   ! Wenn Sie eine �bersetzung vornehmen, ist die Sprachkennung innerhalb
   ! des Bezeichners einzutragen.
   ! Beispiel:
   ! Die default Ressource  EpsonUtil*unknown ist auf Deutsch zu �bersetzen
   !
   !   EpsonUtil*unknown: unknown
   !   EpsonUtil.De*unknown: Unbekannt
   ! EpsonUtil ist der Klassennamen, .De muss nach den Klassennamen
   ! eingef�gt werden. Soll von Deutsch nach Franz�sisch �bersetzt werden
   ! ist jeweils De durch Fr zu ersetzen.
   !
   ! Die Sprachkennung besteht aus zwei Zeichen, welche aus der Umgebungsvariable
   ! LANG, LC_ALL und LC_MESSAGE entnommen werden. Der erste Buchstabe wird
   ! als Kapit�lchen umgewandelt.
   !
   ! Der �bersichtlichkeit halber habe ich jede Ressource auf mehrere Zeilen
   ! umgebrochen. Dies sollte vor allem bei mehrzeiligen Texten die �bersicht
   ! bez�glich Formatierung verbessern.

*/
#endif
   "EpsonUtil.Pf*unknown: unbekannt",

#if 0
/*
   ! Die Version wird beim Erstellen der C-Datei ersetzt.

*/
#endif
   "EpsonUtil.Pf*title_LB.labelString:"
   "EPSON UTILITIES "VERSION,
   "",
   "EpsonUtil.Pf*legend_LB.labelString:"
   "Dindemeng  ",

   "EpsonUtil.Pf*ok_PB.labelString:"
   "Ferdich",

   "EpsonUtil.Pf.mainWindow.ok_PB.tooltip:"
   "Status-Monitor\\n"
   "verlosse ",

   "EpsonUtil.Pf*pref_PB.labelString:"
   "Eistellunge  ",

   "EpsonUtil.Pf*pref_PB.tooltip:"
   "- Agucker",
   "- Kl�nni Hilf,\\n"
   "- Druggeranschluss\\n"
   "- Drugger raussuche\\n"
   "- Betriebsart",

   "EpsonUtil.Pf*about_PB.labelString: Iwwer",

   "EpsonUtil.Pf*about_PB.tooltip:"
   "Lizenz und\\n"
   "Die h�nn mitgemacht",

   "EpsonUtil.Pf*help_PB.labelString:"
   "Hilf ",

   "EpsonUtil.Pf*help_PB.tooltip:"
   "Hilf in �m Agucker azeiche",

   "EpsonUtil.Pf*check_PB.labelString:"
   "Dise \\n"
   "prife ",

   "EpsonUtil.Pf*check_PB.tooltip:"
   "Wann de Ausdruck str�fe hot,\\n"
   "kannscht kontrolliere ob\\n"
   "die Dise dreggich sind.",

   "EpsonUtil.Pf*clean_PB.labelString:"
   "Dise \\n"
   "sauwer mache",

   "EpsonUtil.Pf*clean_PB.tooltip: "
   "Wann de Ausdruck st�fe hot, \\ n"
   "un die Dise verstobbt sinn,\\n"
   "kannschtse dodemit \\n"
   "sauwer mache.       ",

   "EpsonUtil.Pf*align_PB.labelString:"
   "Kepp \\n"
   "ausrichte ",

   "EpsonUtil.Pf*align_PB.tooltip:"
   "Bass uff ! \\n"
   "Des kennt de Drugger kabutt mache ! ",

   "EpsonUtil.Pf*reset_PB.labelString:"
   "Drugger\\n"
   "ricksetze   ",

   "EpsonUtil.Pf*reset_PB.tooltip:"
   "Des Ricksetze vum Drugger\\n"
   "geht nid bei alle Sourde \\n"
   "",

   "EpsonUtil.Pf*cartridge_PB.labelString:"
   "Badrone\\n"
   "wechsle ",

   "EpsonUtil.Pf*cartridge_PB.tooltip:"
   "Drugger uhne Schalder\\n"
   "brauchen des.",

   "EpsonUtil.Pf*addPrinterTxt:"
   "Anre Drugger  ",

   "EpsonUtil.Pf*printerState_LB.labelString:"
   "Stadus: ",

   "EpsonUtil.Pf*state_LB.labelString:"
   "- ",

   "EpsonUtil.Pf*noPrinter*messageString:"
   "Kommunikationsproblem!\\n"
   "Guggemol noch folgende Fehler:\\n"
   "Babier orrer Dinde all, orrer,\\n"
   "de Drugger esch nid eigschalde\".\\n"
   "\\n"
   "S'kann sei, dass mansche Drugger fer\\n"
   "� paar Sekunde nochem Eischalde\\n"
   "nid asprechbar sinn.        ",

   "EpsonUtil.Pf*noPrinter*dialogTitle:"
   "Fehler",

   "EpsonUtil.Pf*cfg1_LB.labelString:"
   "Drugger raussuche",

   "EpsonUtil.Pf*cfg2Printer_PB.labelString:"
   "Drugger raussuche",

   "EpsonUtil.Pf*cfg2Printer_PB.tooltip:"
   "Mit dere Lischt, kannscht der �n Drugger raussuche.          ",

   "EpsonUtil.Pf*cfg2Device_PB.labelString:"
   "Aschluss raussuche:",

   "EpsonUtil.Pf*cfg2Device_PB.tooltip:"
   "Wenn de Mtink narre  \\n"
   "selwer bstimme  \\n"
   "kinnt !",

   "EpsonUtil.Pf*next_PB.labelString:"
   "Weirer",

   "EpsonUtil.Pf*next:"
   "Weirer",

   "EpsonUtil.Pf*previous_PB.labelString:"
   "Redour",

   "EpsonUtil.Pf*previous:"
   "Redour",

   "EpsonUtil.Pf*save:"
   "Sichre ",

   "EpsonUtil.Pf*cancel:"
   "Abbreche ",

   "EpsonUtil.Pf*ok:"
   "Alles Klor",

   "EpsonUtil.Pf*about:"
   "EPSON Utilities\\n"
   "\\n"
   "Version "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001 \\n"
   "\\n"
   "Email: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "D�le vum Code stammen vum Gimp-Print Projekt\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Lizenz: GPL\\n"
   "\\nDank an:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens J�ger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter \\n"
   "  (leader Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.Pf*ctTxt0:"
   "Koppausrichte:\\n"
   "\\n"
   "Jetz emol � leeres Blatt Babier in de Drugger leche un uff [Weirer] drigge.",

   "EpsonUtil.Pf*ctTxtC0:"
   "Koppausrichte:\\n"
   "\\n"
   "Jetz emol � leeres Blatt Babier in de Drugger leche, de schwarze orrer de       "
   "farwiche Druggkopp ausw�hle\\nun dann [Weirer] fer weirermache orrer [Abbreche].",

   "EpsonUtil.Pf*ctTxt1:"
   "Gugg der mol des grad gedruggde Muschder a, un such der die Beschde  "
   "Linienpaare raus.\\nnoch � Blatt eileche und uff [Weirer] drigge.",

   "EpsonUtil.Pf*ctTxt1:"
   "Gugg der des gedruggde Blatt mol ganz genau a. Finn des Muschter raus, "
   "des de gradschde senkrechte Strich g�wwe hot. Jeder Strich hot � Zahl vun "
   "1 bis 8, orrer ach bis 15, debei. Die Zahle st�in aus Platzgrind "
   "unnernanner. Die Zahl glickschd a.\\n"
   "\\n"
   "Lech � neies Blatt in de Drugger un drigg [Weirer].",

   "EpsonUtil.Pf*ctTxtP:"
   "Ward bisses Blatt rauskummt un drigg dann [Weirer].",

   "EpsonUtil.Pf*ctTxt5:"
   "Gugg der s'Ergebnis weche de Ausrichdung arch sourchf�ldich a. "
   "\\n\\nJetz kannschd: \\n\\n  die Eistellung im Drugger [Sichre]  "
   "orrer\\n"
   "  [Abbreche], uhne die Eistellunge zu iwwernemme.   ",

   "EpsonUtil.Pf*exTxt0:"
   "Batrone Wechsle: \\n"
   "\\n"
   "Drigg jetz:    \\n"
   "\\n"
   "  [Weirer] zum weirermache orrer\\n"
   "  [Abbruch].",

   "EpsonUtil.Pf*exTxt00:"
   "Badrone Wechsle: \\n"
   "\\n"
   "Farb orrer Schwarz raussuche und drigg: \\n"
   "\\n"
   "  [Weirer] zum weirermache orrer\\n"
   "  [Abbruch].",

   "EpsonUtil.Pf*exTxt1:"
   "Badrone wechsle: \\n"
   "\\n"
   "De Wache fahrt in d' Wechselstellung.        ",

   "EpsonUtil.Pf*exTxt2:"
   "Badrone Wechsle: \\n"
   "\\n"
   "Neii Badron einsetze un:\\n"
   "\\n"
   "  [Weirer] drigge.   ",

   "EpsonUtil.Pf*exTxt3:"
   "Badrone Wechsle: \\n"
   "\\n"
   "  Dinde werd nochgfillt. ",

   "EpsonUtil.Pf*capabilities_LB.labelString:"
   "Drugger Konfiguration:",

   "EpsonUtil.Pf*capabilities_RC*id_TB.labelString:"
   "Drugger gibt sei Identifikation zurick.",

   "EpsonUtil.Pf*capabilities_RC*reset_TB.labelString:"
   "Software-Reset kammer jetzt mache.      ",

   "EpsonUtil.Pf*capabilities_RC*state_TB.labelString:"
   "Drugger lifert Stadus Informatione. ",

   "EpsonUtil.Pf*capabilities_RC*ex_TB.labelString:"
   "Dindewechsel per Software esch noudwennich.",

   "EpsonUtil.Pf*colors_RC*four_TB.labelString:"
   "4 Farwe ",

   "EpsonUtil.Pf*colors_RC*six_TB.labelString:"
   "6 Farwe ",

   "EpsonUtil.Pf*head_RC*col_TB.labelString:"
   "Farb ",

   "EpsonUtil.Pf*head_RC*bw_TB.labelString:"
   "Schwarz",

   "EpsonUtil.Pf*noAccess*dialogTitle:"
   "Fehler",

   "EpsonUtil.Pf.scrTxt_MW.head_RC.bw_TB.tooltip:"
   "Operation mit de schwarze \\n\\ ",
   "Dindebadrone ausfihre. ",

   "EpsonUtil.Pf.scrTxt_MW.head_RC.col_TB.tooltip:"
   "Operation mit de farwiche \\n"
   "Dindebadrone ausfihre. ",

   "EpsonUtil.Pf*noAccess*messageString:"
   "K�� Zugriffsberechtichung uff die \\n"
   "Schnittstelledateie.\\n"
   "Stell sicher, dass mtink genung Zugriffsrechte\\n\\  ",
   "griecht\\n"
   "Gugg do devor in de Dokumentation noch.     ",

   "EpsonUtil.Pf*fsb*dialogTitle:"
   "Mtink",

   "EpsonUtil.Pf*fsb*CancelLabelString:"
   "Abbruch",

   "EpsonUtil.Pf*fsb*okLabelString:"
   "Alles Klor",

   "EpsonUtil.Pf*fsb*applyLabelString:"
   "Filder",

   "EpsonUtil.Pf*fsb*filterLabelString:"
   "Filder",

   "EpsonUtil.Pf*fsb*fileListLabelString:"
   "Dateie ",

   "EpsonUtil.Pf*fsb*dirListLabelString:"
   "Ordner",

   "EpsonUtil.Pf*browser_LB.labelString:"
   "Agugger aussuche",

#if 0
/*
   ! Achdung � ' ' folcht des '\' Zeiche. 

*/
#endif
   "EpsonUtil.Pf*browser_PB.labelString:"
   " ...\\ ",

   "EpsonUtil.Pf*browser_PB.tooltip:"
   "Agugger aussuche",

   "EpsonUtil.Pf*tooltip_TB.labelString:"
   "Kl�nnie Hilf",

   "EpsonUtil.Pf*tooltip_TB.tooltip:"
   "Die l�schdich kl� Hilfe\\n"
   "kannschd dodemit  \\n"
   "ei und ausschalde. ",

   "EpsonUtil.Pf*autodetect_TB.labelString:"
   "Automadische Erkennung erlauwe. ",

   "EpsonUtil.Pf*autodetect_TB.tooltip:"
   "Mtink werd beim Start versuche, de\\n\\  ",
   "Drugger selwer zu bestimme. \\n"
   "Des funktioniert nid bei alle Drugger.     ",

   "EpsonUtil.Pf*save_PB.labelString:  Sichre ",

   "EpsonUtil.Pf*save_PB.tooltip:  �nrunge iwwernemme.   ",

   "EpsonUtil.Pf.ConfigureForm.cancel_PB.tooltip:"
   "�nrunge nid iwwernemme,\\n\\     ",
   "Maske verlosse. ",

   "EpsonUtil.Pf*cancel_PB.labelString:"
   "Abbruch",

#if 0
/*
   ! Drugger Stadus

*/
#endif
   "EpsonUtil.Pf*error:"
   "Fehler",

   "EpsonUtil.Pf*printing:"
   "Druggd",

   "EpsonUtil.Pf*selfTest:"
   "Selbscht Tescht",

   "EpsonUtil.Pf*busy:"
   "Besch�ftichd",

   "EpsonUtil.Pf*ok:"
   "Alles Klor",

   "EpsonUtil.Pf*cleaning:"
   "Sauwer mache",

   "EpsonUtil.Pf*unknown:"
   "Ubekannt",

#if 0
/*
   ! File Mtink.De, German ressources.
   ! ���� Mtink.Ru, ������� �������.
   !
   ! Notiz fuer Uebersetzer. ��� ist eine X-Ressource-Datei.
   ! Nachstehende Zeichensequenzen haben eine spezielle Bedeutung:
   ! \    Am Ende einer Zeile (Leerzeichen � Tabulatoren duerfen ��
   !      vorhanden sein) bedeutet, dass die Resource in der naechste Zeile
   !      fortgesetzt werden.
   ! \n   ��� steht fuer eine Zeilenschaltung.
   ! \    (\ � Leerzeichen) Das Leerzeichen ist Teil der Zeichenkette.
   !      Leerzeichen am Anfang eine Zeichenkette werden normalerweise
   !      eliminiert.
   ! \t   Steht fuer ein Tabulatorzeichen.
   ! !    am Anfang einer Zeile (Leerzeichen duerfen vorgesetzt sein) leitet
   !      ein Kommentar ein.
   ! 
   ! Wenn Sie eine Uebersetzung vornehmen, ist die Sprachkennung innerhalb
   ! des Bezeichners einzutragen.
   ! Beispiel:
   ! Die default Ressource  EpsonUtil*unknown ist auf Deutsch zu uebersetzen
   !
   !   EpsonUtil*unknown: unknown
   !   EpsonUtil.Ru*unknown: Unbekannt
   ! EpsonUtil ist der Klassennamen, .De muss nach den Klassennamen
   ! eingefuegt werden. Soll von Deutsch nach Franzoesisch uebersetzt werden
   ! ist jeweils De durch Fr zu ersetzen.
   !
   ! Die Sprachkennung besteht aus zwei Zeichen, welche aus der Umgebungsvariable
   ! LANG, LC_ALL � LC_MESSAGE entnommen werden. Der erste Buchstabe wird
   ! als Kapitaelchen umgewandelt.
   !
   ! Der uebersichtlichkeit halber habe ich jede Ressource auf mehrere Zeilen
   ! umgebrochen. ��� sollte vor allem bei mehrzeiligen Texten die uebersicht
   ! bezueglich Formatierung verbessern.

*/
#endif
   "EpsonUtil.Ru*title_LB.fontList: *-urw palladio l-bold-r-normal-*-*-*-*-*-*-*-koi8-ru",
   "EpsonUtil.Ru*fontList:          *-urw palladio l-*-r-normal-*-*-*-*-*-*-*-koi8-ru",
   "*Ru.tooltip_label.fontList:     *-urw palladio l-*-r-normal-*-*-*-*-*-*-*-koi8-ru",
   "EpsonUtil.Ru*unknown: �� ��������",

#if 0
/*
   ! Die Version wird beim Erstellen der C-Datei ersetzt.

*/
#endif
   "EpsonUtil.Ru*title_LB.labelString:"
   "EPSON UTILITIES "VERSION,
   "",
   "EpsonUtil.Ru*legend_LB.labelString:"
   "���������� ������",

   "EpsonUtil.Ru*ok_PB.labelString:"
   "�����",

   "EpsonUtil.Ru.mainWindow.ok_PB.tooltip:"
   "��������\\n"
   "������",

   "EpsonUtil.Ru*pref_PB.labelString:"
   "���������",

   "EpsonUtil.Ru*pref_PB.tooltip:"
   "- �������\\n"
   "- ���������\\n"
   "- ���������\\n"
   "- ����� ��������\\n"
   "- ����� ������",

   "EpsonUtil.Ru*about_PB.labelString:"
   "� ���������",

   "EpsonUtil.Ru*about_PB.tooltip:"
   "�������� �\\n"
   "���������",

   "EpsonUtil.Ru*help_PB.labelString:"
   "������",

   "EpsonUtil.Ru*help_PB.tooltip:"
   "�������� ������ � ����� ����.",

   "EpsonUtil.Ru*check_PB.labelString:"
   "���������\\n"
   "����\\n",

   "EpsonUtil.Ru*check_PB.tooltip:"
   "���� ���������� �������,\\n"
   "�������, ��������������� ������\\n"
   "���������� ���� ���������� �������",

   "EpsonUtil.Ru*clean_PB.labelString:"
   "����������\\n"
   "����\\n",

   "EpsonUtil.Ru*clean_PB.tooltip: "
   "���� �� �������� ���������� �������\\n"
   "��������������� �������(������),\\n"
   "�� �������� ���� ���������� �������\\n"
   "(�� ��� ����������� �������)",

   "EpsonUtil.Ru*align_PB.labelString:"
   "�������������\\n"
   "����������\\n"
   "�������",

   "EpsonUtil.Ru*align_PB.tooltip:"
   "��������!\\n"
   "��� ����� ��������� �������!",

   "EpsonUtil.Ru*reset_PB.labelString:"
   "����������\\n"
   "��������������\\n"
   "���������",

   "EpsonUtil.Ru*reset_PB.tooltip:"
   "������������ �������������� ���������\\n"
   "�� �������������� ����� ������\\n"
   "���������.",

   "EpsonUtil.Ru*cartridge_PB.labelString:"
   "�������\\n"
   "����������\\n"
   "��������",

   "EpsonUtil.Ru*cartridge_PB.tooltip:"
   "��� ��������� �� �������\\n"
   "������ �����\\n"
   "���������",

   "EpsonUtil.Ru*addPrinterTxt:"
   "������ �������",

   "EpsonUtil.Ru*printerState_LB.labelString:"
   "������",

   "EpsonUtil.Ru*state_LB.labelString:"
   "- ",

   "EpsonUtil.Ru*noPrinter*messageString:"
   "������ �����!\\n"
   "���������, ������� �� �������,\\n"
   "�� ����������� �� �������,\\n"
   "�� �������� �� ������.\\n"
   "\\n"
   "������, ��� ��������� �������� ��\\n"
   "��������� ��������� ������\\n"
   "����� ���������.",

   "EpsonUtil.Ru*noPrinter*dialogTitle:"
   "������",

   "EpsonUtil.Ru*cfg1_LB.labelString:"
   "����� ��������:",

   "EpsonUtil.Ru*cfg2Printer_PB.labelString:"
   "����� ��������:",

   "EpsonUtil.Ru*cfg2Printer_PB.tooltip:"
   "�� ������ ������� � ���� ������ ��� ��� ��������.",

   "EpsonUtil.Ru*cfg2Device_PB.labelString:"
   "����� �����:",

   "EpsonUtil.Ru*cfg2Device_PB.tooltip:"
   "���� �� Mtink ��� ��\\n"
   "���������� ��� ���!",

   "EpsonUtil.Ru*next_PB.labelString:"
   "������",

   "EpsonUtil.Ru*next:"
   "������",

   "EpsonUtil.Ru*previous_PB.labelString:"
   "�����",

   "EpsonUtil.Ru*previous:"
   "�����",

   "EpsonUtil.Ru*save:"
   "���������",

   "EpsonUtil.Ru*cancel:"
   "��������",

   "EpsonUtil.Ru*ok:"
   "OK",

   "EpsonUtil.Ru*about:"
   "EPSON Utilities\\n"
   "\\n"
   "������ "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001\\n"
   "\\n"
   "Email: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "����� ���� ������������ �� ������� Gimp-Print\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "��������: GPL\\n"
   "\\n������������� ��������� �����:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens Jaeger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter\\n"
   "  (leader Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus Wuenschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.Ru*ctTxt0:"
   "���������� �������:\\n"
   "\\n"
   "������� ������ ���� ������ � ������� � ������� [������].",

   "EpsonUtil.Ru*ctTxtC0:"
   "���������� �������:\\n"
   "\\n"
   "������� ������ ���� ������ � �������, ���������� ޣ���� "
   "��� ������� �������\\n"
   "� ������� [������] ��� [��������].",

   "EpsonUtil.Ru*ctTxt1:"
   "�������� �������� ������������� ���� �����\\n",
   "������� ������ ���� ������ � �������, � ������� [������].",

   "EpsonUtil.Ru*ctTxt1:"
   "�������� ������ � �������� ������������� �������. �������� ������� ������ ��������� ����� �� "
   "1 �� 8, ��� 15. �������� �������� ���� �� ���� �����.\\n"
   "\\n"
   "������� ���� ����� � �������, � ������� [������].",

   "EpsonUtil.Ru*ctTxtP:"
   "��������� ���� ���� ����� ����� � ������� [������].",

   "EpsonUtil.Ru*ctTxt5:"
   "����������� ��������� ����������.\\n\\n������ �� ������:\\n\\n[���������] ���������� ���������"
   "���\\n"
   "[��������], ��� ���������� ���������.",

   "EpsonUtil.Ru*exTxt0:"
   "����� ����������� ���������:\\n"
   "\\n"
   "�������:\\n"
   "\\n"
   "[������] ��� ����������� ���\\n"
   "[��������] ��� ������.",

   "EpsonUtil.Ru*exTxt00:"
   "����� ����������� ���������:\\n"
   "\\n"
   "�������� ������� ��� ������ � �������:\\n"
   "\\n"
   "[������] ��� ����������� ���\\n"
   "[��������] ��� ������.",

   "EpsonUtil.Ru*exTxt1:"
   "����� ����������� ���������:\\n"
   "\\n"
   "������� ����� ��������� � ��������� ������������.",

   "EpsonUtil.Ru*exTxt2:"
   "����� ����������� ���������:\\n"
   "\\n"
   "������� ����� ���������� �������� �:\\n"
   "\\n"
   "������� [������].",

   "EpsonUtil.Ru*exTxt3:"
   "����� ����������� ���������:\\n"
   "\\n"
   "���������� ������� ���������.",

   "EpsonUtil.Ru*capabilities_LB.labelString:"
   "������������ ��������:",

   "EpsonUtil.Ru*capabilities_RC*id_TB.labelString:"
   "������� ������ ����������������� ������.",

   "EpsonUtil.Ru*capabilities_RC*reset_TB.labelString:"
   "���������� ����� ��������.",

   "EpsonUtil.Ru*capabilities_RC*state_TB.labelString:"
   "������� ������ ������ � ��ϣ� ���������.",

   "EpsonUtil.Ru*capabilities_RC*ex_TB.labelString:"
   "���������� ����� ������ � ������� ���������.",

   "EpsonUtil.Ru*colors_RC*four_TB.labelString:"
   "4 �����",

   "EpsonUtil.Ru*colors_RC*six_TB.labelString:"
   "6 ������",

   "EpsonUtil.Ru*head_RC*col_TB.labelString:"
   "����",

   "EpsonUtil.Ru*head_RC*bw_TB.labelString:"
   "������",

   "EpsonUtil.Ru*noAccess*dialogTitle:"
   "������",

   "EpsonUtil.Ru.scrTxt_MW.head_RC.bw_TB.tooltip:"
   "��������� �������� \\n"
   "� ޣ���� ���������� ����������.",

   "EpsonUtil.Ru.scrTxt_MW.head_RC.col_TB.tooltip:"
   "��������� �������� \\n"
   "� ������� ���������� ����������.",

   "EpsonUtil.Ru*noAccess*messageString:"
   "������ � ���������� �� ��������.\\n"
   "���������, ����� �� mtink ����������\\n"
   "���� �������.\\n"
   "��������� ��� � ������������.",

   "EpsonUtil.Ru*fsb*dialogTitle:"
   "Mtink",

   "EpsonUtil.Ru*fsb*CancelLabelString:"
   "������",

   "EpsonUtil.Ru*fsb*okLabelString:"
   "OK",

   "EpsonUtil.Ru*fsb*applyLabelString:"
   "������",

   "EpsonUtil.Ru*fsb*filterLabelString:"
   "������",

   "EpsonUtil.Ru*fsb*fileListLabelString:"
   "�����",

   "EpsonUtil.Ru*fsb*dirListLabelString:"
   "����������",

   "EpsonUtil.Ru*�������_LB.labelString:"
   "������� �������",

#if 0
/*
   ! �������� ein ' ' folgt das '\' Zeichen.

*/
#endif
   "EpsonUtil.Ru*�������_PB.labelString:"
   " ...\\ ",

   "EpsonUtil.Ru*�������_PB.tooltip:"
   "������� �������.",

   "EpsonUtil.Ru*tooltip_TB.labelString:"
   "���������",

   "EpsonUtil.Ru*tooltip_TB.tooltip:"
   "� ������� ������\\n"
   "����� ��������� ���\\n"
   "���������� ���������.",

   "EpsonUtil.Ru*autodetect_TB.labelString:"
   "�������������� ����������� ���� ��������.",

   "EpsonUtil.Ru*autodetect_TB.tooltip:"
   "Mtink �������� ��� ������\\n"
   "���������� ��� �������� �������������.\\n"
   "��� ���������� ��� ���� ����� ���������.",

   "EpsonUtil.Ru*save_PB.labelString:  ���������",

   "EpsonUtil.Ru*save_PB.tooltip:  ��������� ���������.",

   "EpsonUtil.Ru.ConfigureForm.cancel_PB.tooltip:"
   "�� ��������� ���������,\\n"
   "�����.",

   "EpsonUtil.Ru*cancel_PB.labelString:"
   "������",

#if 0
/*
   ! ������� Status

*/
#endif
   "EpsonUtil.Ru*error:"
   "������",

   "EpsonUtil.Ru*printing:"
   "������",

   "EpsonUtil.Ru*selfTest:"
   "����",

   "EpsonUtil.Ru*busy:"
   "��������",

   "EpsonUtil.Ru*ok:"
   "OK",

   "EpsonUtil.Ru*cleaning:"
   "������",

   "EpsonUtil.Ru*unknown:"
   "�� ��������",

#if 0
/*
   ! File Mtink.Sv, swedish resources
   ! Translation by Daniel Tamm (daniel@tamm-tamm.de)
   !
   ! Note for translator. This is a X-Resource file. The following
   ! character sequences have a special meaning:
   ! \    at the end of a line (no space allowed after the \):
   !      resource continue at the next line.
   ! \n   This is a linefeed.
   ! \    (\ and space) the space character is part of the resource
   !      string. Normally space at the begin and end of a resource are
   !      elinminated.
   ! \t   This is for a tabulator character.   
   !
   ! If you translate this you have to put the language designation
   ! withinh the key.
   ! Example:
   ! The resource default EpsonUtil*unknown must be translated into german
   !
   !   EpsonUtil*unknown: unknown
   !   EpsonUtil.De*unknown: Unbekannt
   ! EpsonUtil is the program class name, .De must be inserted after this
   !
   ! The language designator is composed for the first two character 
   ! which are normally stored into the environment variable LANG and/or
   ! LC_ALL, LC_MESSAGE. The first letter is to be written with upercase.

*/
#endif
   "EpsonUtil.Sv*unknown:                       ok�nd",

#if 0
/*
   ! The version number will be set into the c-source file !

*/
#endif
   "EpsonUtil.Sv*title_LB.labelString:          EPSON UTILITIES "VERSION,
   "",
   "EpsonUtil.Sv*legend_LB.labelString:         bl�ckm�ngd",

   "EpsonUtil.Sv*ok_PB.labelString:             Ut",

   "EpsonUtil.Sv*mainWindow.ok_PB.tooltip:      St�nga\\nStatus Monitor.",

   "EpsonUtil.Sv*pref_PB.labelString:           Alternativ",

   "EpsonUtil.Sv*pref_PB.tooltip:"
   "- Browser,\\n"
   "- Minihj�lp,\\n"
   "- Val av anslutning\\n"
   "- Val av skrivaren\\n"
   "- funktionss�tt",

   "EpsonUtil.Sv*about_PB.labelString:          Info",

   "EpsonUtil.Sv*about_PB.tooltip:"
   "Licens och\\n"
   "medverkande",

   "EpsonUtil.Sv*help_PB.labelString:           Hj�lp",

   "EpsonUtil.Sv*help_PB.tooltip:               Visa hj�lpen i ett browser-f�nster.",

   "EpsonUtil.Sv*check_PB.labelString:          Kontrollera\\nmunstycken",

   "EpsonUtil.Sv*check_PB.tooltip:"
   "Skriv ut ett testm�nster f�r att\\n"
   "se om munstycken �r t�ppta.\\n"
   "T�ppta munstycken g�r vita\\n"
   "remsor i utskrifter.",

   "EpsonUtil.Sv*clean_PB.labelString:          Reng�r\\nmunstycken",

   "EpsonUtil.Sv*clean_PB.tooltip:"
   "Du kan reng�ra munstycken h�r\\n"
   "om dom inte �r i ordning.",

   "EpsonUtil.Sv*align_PB.labelString:          R�ta upp\\ntryckhuvud",

   "EpsonUtil.Sv*align_PB.tooltip:"
   "Varning !\\n"
   "Detta kan skada skrivaren.",

   "EpsonUtil.Sv*reset_PB.labelString:          Reset\\nskrivaren",

   "EpsonUtil.Sv*reset_PB.tooltip:              Detta funkar inte f�r alla modeller.",

   "EpsonUtil.Sv*cartridge_PB.labelString:      Byta\\npatron",

   "EpsonUtil.Sv*cartridge_PB.tooltip:"
   "Om din skrivare har\\n"
   "ingen knapp d� be�ver\\n"
   "du det h�r.",

   "EpsonUtil.Sv*addPrinterTxt:                 Andra skrivare",

   "EpsonUtil.Sv*printerState_LB.labelString:   Status: ",

   "EpsonUtil.Sv*state_LB.labelString:   - ",

   "EpsonUtil.Sv*noPrinter*messageString:"
   "Kommunikationsproblem med skrivaren!\\n"
   "V�nligen kontrollera f�ljande fel:\\n"
   "\"Pappret �r slut\", \"Bl�cket �r slut\", \"Skrivaren �r inte p�\"\\n"
   "\\n"
   "Notera att n�gra skrivare svarar inte f�r\\n"
   "n�gra sekunder efter s�tta dom p�.",

   "EpsonUtil.Sv*noPrinter*dialogTitle:          Fel",

   "EpsonUtil.Sv*cfg1_LB.labelString:            Val av skrivaren:",

   "EpsonUtil.Sv*cfg2Printer_PB.labelString:     Val av skrivaren:",

   "EpsonUtil.Sv*cfg2Printer_PB.tooltip:"
   "En lista av skrivare erbjuds.\\n"
   "D�r kan du v�lja din modell.",

   "EpsonUtil.Sv*cfg2Device_PB.labelString:      Val av porten:",

   "EpsonUtil.Sv*cfg2Device_PB.tooltip:"
   "Det vore bra\\n"
   "om mtink kunde\\n"
   "g�ra det sj�lv!",

   "EpsonUtil.Sv*next_PB.labelString:            N�sta",

   "EpsonUtil.Sv*next:                           N�sta",

   "EpsonUtil.Sv*previous_PB.labelString:        F�reg�ende",

   "EpsonUtil.Sv*previous:                       F�reg�ende",

   "EpsonUtil.Sv*save:                           Spara",

   "EpsonUtil.Sv*cancel:                         Avbryt",

   "EpsonUtil.Sv*ok:                             OK",

   "EpsonUtil.Sv*about:"
   "EPSON Utilities\\n"
   "\\n"
   "Version "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001\\n"
   "\\nEmail: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "Delar av koden har tagits fr�n gimp-print projektet\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Licens: GPL\\n"
   "\\n"
   "Tack till:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens J�ger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter\\n"
   "  (ledare Foomatic/www.linuxprinting.org projektet)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.Sv*ctTxt0:"
   "V�nligen s�tt in en ark i din skrivare f�r att b�rja med proceduren att r�ta upp huvudet.\\n"
   "Tryck p� [N�sta] f�r att b�rja\\n"
   "eller [Avbryt] f�r att avbryta.",

   "EpsonUtil.Sv*ctTxtC0:"
   "V�nligen s�tt in en ark i din skrivare f�r att b�rja med proceduren att r�ta upp huvudet.\\n"
   "V�lj det svarta eller det f�rjat huvudet och\\n"
   "tryck p� [N�sta] f�r att b�rja eller [Avbryt] f�r att avbryta.",

   "EpsonUtil.Sv*ctTxt1:"
   "Granska utskriften, och v�lj det b�sta linjeparet i m�nstret. Sedan s�tt arken i skrivaren igen "
   "och tryck p� [N�sta] f�r att forts�tta.",

   "EpsonUtil.Sv*ctTxt1C:"
   "Granska utskriften noggrant, och v�lj m�nstret med dom rakaste vertikala linjer. "
   "Om du inte kan hitta ett j�mnt m�nster, v�lj nummret av det "
   "b�sta m�nstret, och repetera proceduren.\\n"
   "\\n"
   "S�tt in arken i skrivaren igen och tryck p� [N�sta] f�r att forts�tta.",

   "EpsonUtil.Sv*ctTxtP:"
   "V�nta tills arken kastas ut ur skrivaren och sen tryck p� [N�sta] f�r att forts�tta.",

   "EpsonUtil.Sv*ctTxt5:"
   "Granska den sista utskriften mycket noggrant f�r att tillf�rs�kra att skrivaren �r korrekt r�tat upp.\\n"
   "\\n"
   "Sedan v�lj:\\n"
   "\\n"
   "  [Spara] f�r att spara resultatet i skrivaren eller\\n"
   "  [Avbryt] f�r att avbryta utan att spara resultatet.",

   "EpsonUtil.Sv*exTxt0:"
   "Utbyte av patronen:\\n"
   "\\n"
   "V�nligen tryck:\\n"
   "\\n"
   "  [N�sta] f�r att forts�tta eller\\n"
   "  [Avbryt]",

   "EpsonUtil.Sv*exTxt00:"
   "Utbyte av patronen:\\n"
   "\\n"
   "V�nligen v�lj svart eller f�rg\\n"
   "Tryck:\\n"
   "\\n"
   "  [N�sta] f�r att forts�tta eller\\n"
   "  [Avbyt]",

   "EpsonUtil.Sv*exTxt1:"
   "Utbyte av patronen:\\n"
   "\\n"
   "Skrivhuvudet flyttas till utbytespositionen.",

   "EpsonUtil.Sv*exTxt2:"
   "Utbyte av patronen:\\n"
   "\\n"
   "S�tt i en ny patron och:\\n"
   "\\n"
   "  Tryck [N�sta]",

   "EpsonUtil.Sv*exTxt3:"
   "Utbyte av patronen:\\n"
   "\\n"
   "  process \"fylla bl�cket\".",

   "EpsonUtil.Sv*colors_RC*four_TB.labelString:    4 f�rger",

   "EpsonUtil.Sv*colors_RC*six_TB.labelString:     6 f�rger",

   "EpsonUtil.Sv*head_RC*col_TB.labelString:       F�rg",

   "EpsonUtil.Sv*head_RC*bw_TB.labelString:        Svart",

   "EpsonUtil.Sv*noAccess*dialogTitle:             Fel",

   "EpsonUtil.Sv*scrTxt_MW.head_RC.bw_TB.tooltip:"
   "Utf�r operationen\\n"
   "med patronen f�r\\n"
   "svart bl�ck.",

   "EpsonUtil.Sv*scrTxt_MW.head_RC.col_TB.tooltip:"
   "Utf�r operationen\\n"
   "med patronen f�r\\n"
   "f�rgat bl�ck.",

   "EpsonUtil.Sv*noAccess*messageString:"
   "Ingen access till skrivarens apparatfil.\\n"
   "\\n"
   "V�nligen kontrollera att mtink har tillr�ckliga\\n"
   "r�tter f�r att komma �t apparafilen.\\n"
   "\\n"
   "Konsultera �ven dokumentationen f�r detta.",

   "EpsonUtil.Sv*fsb*dialogTitle:                 Mtink",

   "EpsonUtil.Sv*fsb*CancelLabelString:           Avbryt",

   "EpsonUtil.Sv*fsb*okLabelString:               OK",

   "EpsonUtil.Sv*fsb*applyLabelString:            Filter",

   "EpsonUtil.Sv*fsb*filterLabelString:           Filter",

   "EpsonUtil.Sv*fsb*fileListLabelString:         Filer",

   "EpsonUtil.Sv*fsb*dirListLabelString:          Kataloger",

   "EpsonUtil.Sv*browser_LB.labelString:          V�lj webbl�sare",

   "EpsonUtil.Sv*browser_PB.labelString:          \\ ...\\ ",

   "EpsonUtil.Sv*browser_PB.tooltip:              Val av webbl�saren",

   "EpsonUtil.Sv*tooltip_TB.labelString:          Minihj�lp",

   "EpsonUtil.Sv*tooltip_TB.tooltip:"
   "H�r kan du\\n"
   "avaktivera\\n"
   "minihj�lpen.",

   "EpsonUtil.Sv*autodetect_TB.labelString:       Till�ta automatisk uppt�ckten",

   "EpsonUtil.Sv*autodetect_TB.tooltip:"
   "Vid starten, Mtink ska f�rs�ka att\\n"
   "ta reda p� anslutna skrivaren.\\n"
   "Detta funkar inte med alla skrivare.",

   "EpsonUtil.Sv*save_PB.labelString:             Spara",

   "EpsonUtil.Sv*save_PB.tooltip:                 Anv�nd �ndringarna.",

   "EpsonUtil.Sv*ConfigureForm.cancel_PB.tooltip:"
   "Anv�nd inte �ndringarna,\\n"
   "st�nga konfigurationen.",

   "EpsonUtil.Sv*cancel_PB.labelString:           Avbryt",

#if 0
/*
   ! Printer state

*/
#endif
   "EpsonUtil.Sv*error:                        Fel",

   "EpsonUtil.Sv*printing:                     Skriver ut",

   "EpsonUtil.Sv*selfTest:                     Sj�lvtest",

   "EpsonUtil.Sv*busy:                         Upptagen",

   "EpsonUtil.Sv*ok:                           OK",

   "EpsonUtil.Sv*cleaning:                     Reng�rar",

   "EpsonUtil.Sv*unknown:                      Ok�nd",
#if 0
/*
   ! File Mink.Tr, turkisch resources
*/
#endif
   "EpsonUtil.Tr*title_LB.fontList: *-helvetica-*-r-normal--14-*-*-*-*-*-iso8859-9",
   "EpsonUtil.Tr*fontList: *-helvetica-medium-r-normal-*-12-*-*-*-*-*-iso8859-9",
   "*Tr.tooltip_label.fontList:               *-*-*-r-normal--14-*-*-*-*-*-iso8859-9",
   "EpsonUtil.Tr*title_LB.labelString: EPSON UTILITIES "VERSION,
   "EpsonUtil.Tr*legend_LB.labelString: Boya miktar�",
   "EpsonUtil.Tr*ok_PB.labelString: ��k��",
   "EpsonUtil.Tr*pref_PB.labelString: Ayarlamak",
   "EpsonUtil.Tr*about_PB.labelString: Bilgiler",
   "EpsonUtil.Tr*help_PB.labelString: Yard�m",
   "EpsonUtil.Tr*check_PB.labelString: �fle�\\nkontrol�",
   "EpsonUtil.Tr*clean_PB.labelString: �fle�\\ntemizli�i",
   "EpsonUtil.Tr*align_PB.labelString: Ba�lar�\\nAyarlamak",
   "EpsonUtil.Tr*reset_PB.labelString: Yeniden\\nyerle�tir",
   "EpsonUtil.Tr*cartridge_PB.labelString: Boya\\nde�i�ecek",
   "EpsonUtil.Tr*addPrinterTxt: Ba�ka Yaz�c�",
   "EpsonUtil.Tr*printerState_LB.labelString: Status: ",
   "EpsonUtil.Tr*state_LB.labelString: - ",
   "EpsonUtil.Tr*noPrinter*messageString: Ba�lant�problemi !\\nL�tfen Yaz�c�y� hatatalra y�nelik konr�l ediniz:\\n\"Ka��thatas�\", \"Boyadurumu\",\\n\"Yaz�c� a��k de�il\".\\n\\nDikkat baz� Yaz�c�larda\\na��ld�ktan bir ka� saniye sonra\\nba�lant� kesilebilir.",
   "EpsonUtil.Tr*noPrinter*dialogTitle: Yanl��",
   "EpsonUtil.Tr*cfg1_LB.labelString: Yaz�c� se�imi:",
   "EpsonUtil.Tr*cfg2Printer_PB.labelString: Yaz�c� se�imi:",
   "EpsonUtil.Tr*cfg2Device_PB.labelString: Port se�imi:",
   "EpsonUtil.Tr*next_PB.labelString: Devam",
   "EpsonUtil.Tr*next: Devam",
   "EpsonUtil.Tr*previous_PB.labelString: Geri",
   "EpsonUtil.Tr*previous: Geri",
   "EpsonUtil.Tr*save: Haf�zaya al",
   "EpsonUtil.Tr*cancel: Bitir",
   "EpsonUtil.Tr*ok: OK",
   "EpsonUtil.Tr*about:"
   "EPSON Utilities\\n"
   "\\nVersion "VERSION"\\n"
   "\\n"
   "Copyright: Jean-Jacques Sarton 2001\\n"
   "\\n"
   "Email: jj.sarton@t-online.de\\n"
   "\\n"
   "URL: http://xwtools.automatix.de\\n"
   "\\n"
   "Baz� kod b�l�mleri gimp-print projesinden al�nm��t�r\\n"
   "Copyright 2000 Robert Krawitz (rlk@alum.mit.edu)\\n"
   "\\n"
   "Tercuman: Hikmet Salar (Salar@gmx.de)\\n"
   "Licence: GPL\\n"
   "\\n"
   "Te�ekk�rler:\\n"
   "Keith Amidon\\n"
   "  camalot@picnicpark.org\\n"
   "Ronny Budzinske\\n"
   "Nicola Fabiano\\n\\   ivololeribar@yahoo.it\\n"
   "Tokai Ferenc\\n"
   "Karlheinz Guenster\\n"
   "Gene Heskett\\n"
   "   gene_heskett@iolinc.net\\n"
   "Mogens J�ger\\n"
   "   mogensjaeger@get2net.dk\\n"
   "Till Kamppeter\\n"
   "  (leader Foomatic/www.linuxprinting.org project)\\n"
   "  http://www.linuxprinting.org/till\\n"
   "Stefan Kraus\\n"
   "   sjk@weserbergland.de\\n"
   "   http://xwgui.automatix.de\\n"
   "Rainer Krienke\\n"
   "   krienke@uni-koblenz.de\\n"
   "Sylvain Le-Gall\\n"
   "   sylvain.le-gall@polytechnique.org\\n"
   "Steven J. Mackenzie\\n"
   "Raul Morales\\n"
   "   raul.mh@telefonica.net\\n"
   "   http://www.telefonica.net/web/ruten\\n"
   "Simon Morlat\\n"
   "Marc Riese\\n"
   "   Marc-Riese@gmx.de\\n"
   "Hikmet Salar\\n"
   "   Salar@gmx.de\\n"
   "Glen Stewart\\n"
   "Daniel Tamm\\n"
   "   daniel@tamm-tamm.de\\n"
   "   http:/www.tamm-tamm.de\\n   http:/www.expedit.org\\n"
   "Robert Wachinger\\n"
   "   nospam@robert-wachinger.de\\n"
   "Klaus W�nschel\\n"
   "   klaus.wuenschel@knittelsheim-computer.de\\n"
   "   http:/www.knilse.de\\n"
   "Alberto Zanoni.",

   "EpsonUtil.Tr*ctTxt0: Ba�l�k ayar�:\\n\\nL�tfen Yaz�c�ya bir bo� ka��t koyunuz ve [devam] tu�una bas�n�z.",
   "EpsonUtil.Tr*ctTxtC0: Ba�lik ayar�:\\n\\nL�tfen Yaz�c�ya bir bo� ka��t koyunuz, L�tfen Ba�l�k se�iniz, sisah veya renkli ve sonra [Devam] i�lemi y�r�tmek i�in veya [Bitir].",
   "EpsonUtil.Tr*ctTxt1: L�tfen, az �nce Yaz�c�dan ��kan �rne�i de�erlendiriniz ve eniyi �izgi e�lerini se�iniz.\\nKa��t� tekrar Yaz�c�ya yerle�tiriniz ve [Devam] tu�una bas�n�z.",
   "EpsonUtil.Tr*ctTxt1C: Yaz�c�dan ��kan ka��ta dikkatle bak�n�z. En uygun �rne�i belirlemek i�in �nce �rne�e bakarak a�a��ya (vertikal) y�ndeki en d�zg�n cizgiyi tesbit ediniz. Her �izginin kendine ayit bir say�s� olmak �zere 1 ile 8 veya 15 say�lar� verilmistir. Say�lar yerdarl���ndan, asa��ya do�ru s�ralanmakta. L�tfen O say�y� tu�lay�n�z.\\n\\nL�tfen, ka��t� tekrar yaz�c�ya yerle�tiriniz ve [Devanm] tu�una bas�n�z.",
   "EpsonUtil.Tr*ctTxtP: Kag�t yaz�c�dan ��kana kadar bekleyiniz ve sonra [Devam] tu�una basarak tasdikleyiniz.",
   "EpsonUtil.Tr*ctTxt5: L�tfen, da��t�m g�r�nt�s� ile ilgili sonu�lar� iyi de�erlendiriniz, �ok �zen g�stererek ve Kag�t� tekrar Yaz�c�ya yerle�tiriniz.\\n\\nVe su se�eneklerden birine karar veriniz:\\n\\n  Yaz�c�n�n ayar�n� [Haf�zaya al] veya\\n  ayar�n� haf�zaya almadan [Bitir].",
   "EpsonUtil.Tr*exTxt0: Boya de�isilecek:\\n\\ntastikleyiniz :\\n\\n  [Devam] i�lemi devam ettirmek i�in veya\\n  [Bitir]",
   "EpsonUtil.Tr*exTxt00: Boya de�isilecek:\\n\\nL�tfen siyah veya renkli se�eneklerden birine karar veriniz ve tasdikleyiniz:\\n\\n  [Devam] i�lemi y�r�tmek i�in veya\\n  [Bitir]",
   "EpsonUtil.Tr*exTxt1: Boya degisilecek:\\n\\nBoya de�i�tirme pozisyonu ayarlan�yor",
   "EpsonUtil.Tr*exTxt2: Boya de�i�ilecek:\\n\\nYeni Boya yerle�tiriniz ve\\n\\n [Devam] tu�una bas�n�z",
   "EpsonUtil.Tr*exTxt3: Boya de�i�ecek:\\n\\n  Boya doldurma i�lemi y�r�yor.",
   "EpsonUtil.Tr*colors_RC*four_TB.labelString: 4 Renkler",
   "EpsonUtil.Tr*colors_RC*six_TB.labelString: 6 Renkler",
   "EpsonUtil.Tr*head_RC*col_TB.labelString: Renkli",
   "EpsonUtil.Tr*head_RC*bw_TB.labelString:Siyah",
   "EpsonUtil.Tr*noAccess*dialogTitle: Yanl��",
   "EpsonUtil.Tr*noAccess*messageString: Giri� hakk�n�z yok\\nba�lant�bilgileri",
   "EpsonUtil.Tr*fsb*dialogTitle: Mtink",
   "EpsonUtil.Tr*fsb*CancelLabelString: Bitir",
   "EpsonUtil.Tr*fsb*okLabelString: OK",
   "EpsonUtil.Tr*fsb*applyLabelString: Filitre",
   "EpsonUtil.Tr*fsb*filterLabelString: Filitre",
   "EpsonUtil.Tr*fsb*fileListLabelString: Dosya",
   "EpsonUtil.Tr*fsb*dirListLabelString: Rehber",
   "EpsonUtil.Tr*browser_LB.labelString:  Browser se�iniz",
   "EpsonUtil.Tr*browser_PB.labelString:  \\ ...\\ ",
   "EpsonUtil.Tr*autodetect_TB.labelString: Otomatikmen bulmaya izin vermek",
   "EpsonUtil.Tr*save_PB.labelString:  Haf�zaya al",
   "EpsonUtil.Tr*cancel_PB.labelString:  Bitir",

#if 0
/*
   ! Printer state
*/
#endif
   "EpsonUtil.Tr*error:"
   "Error",

   "EpsonUtil.Tr*printing:"
   "Printing",

   "EpsonUtil.Tr*selfTest:"
   "Self Test",

   "EpsonUtil.De*busy:"
   "Busy",

   "EpsonUtil.Tr*ok:"
   "OK",

   "EpsonUtil.Tr*cleaning:"
   "Cleaning",

   "EpsonUtil.Tr*unknown:"
   "Unknown",

   (char *)0
};
