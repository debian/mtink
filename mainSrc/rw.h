/* File rw.h
 */

extern ssize_t devRead(int fd, unsigned char *buf, size_t sz, int to);
extern ssize_t devWrite(int fd, unsigned char *buf, size_t sz, int to);
