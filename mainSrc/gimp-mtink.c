/* file gimp-mtink.c
 *
 * Plug ins for gimp, call mtink
 *
 * Changes: 16-MAI-2004 adapted for gimp 2.0.x
 *
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <libgimp/gimp.h>
#include <unistd.h>
#include <dirent.h>

/* compatibility with older gimp versions:
 * use the old names and define them if necessary
 */

#ifndef GIMP_VERSION_1
#ifndef STATUS_SUCCESS
#define STATUS_SUCCESS GIMP_PDB_SUCCESS
#define GParam         GimpParam
#define GParamDef      GimpParamDef
#define GPlugInInfo    GimpPlugInInfo
#define PARAM_STATUS   GIMP_PDB_STATUS
#define PROC_EXTENSION GIMP_EXTENSION
#define PARAM_INT32    GIMP_PDB_INT32
#endif
#endif

static char defaultCommand[] = "mtink &";
static char choiceCommand[]  = "mtinkc &";

static void query  (void);
#ifdef GIMP_VERSION_2
static void run    (const gchar *name,
                    gint nparams,
                    const GimpParam *param,
                    gint *nreturn_vals,
                    GimpParam **return_vals);
#else
static void run    (char    *name,
                    int      nparams,
                    GParam  *param,
                    int     *nreturn_vals,
                    GParam **return_vals);

#endif
/* 
 * functions table
 */
 GPlugInInfo PLUG_IN_INFO[] =
{
   NULL,    /* init_proc */
   NULL,    /* quit_proc */
   query,   /* query_proc */
   run,     /* run_proc */
};

/*
 * at start gimp call load the program
 * and will be able to call the function stated above
 */

int main(int  argc, char *argv[])
{
#ifdef GIMP_VERSION_2
   return (gimp_main((const GimpPlugInInfo*)&PLUG_IN_INFO, argc, argv));
#else
   return (gimp_main(argc, argv));
#endif
}

/*
 * Tell gimp which we are and where to install us in the
 * Menus
 */ 
 
static void query ()
{
   static GParamDef args[] =
   {
      { PARAM_INT32,    "run_mode", "Interactive" },
   };

   static int nargs = sizeof (args) / sizeof (args[0]);

   gimp_install_procedure ("gimp-mtink",
                           "Epson Status Monitor",
                           "A Motif based Status Monitor for\nEPSON inkjet printers\n"
                           "Need mtink",
                           "Jean-Jacques Sarton",
                           "Jean-Jacques Sarton",
                           "15th February 2002",
                           "<Toolbox>/Xtns/Epson Status Monitor",
                           NULL,
                           PROC_EXTENSION,
                           nargs, 0,
                           args, NULL);
}

static int checkForChoices(void)
{
   char          *home = getenv("HOME");
   DIR           *dir;
   struct dirent *ent;
   int            count = 0;
           
   if ( (dir = opendir(home)) )
   {
      while ( (ent = readdir(dir)) != NULL )
      {
         if ( strncmp(ent->d_name, ".mtinkrc", 8 ) )
         {
            continue;
         }
         if ( ent->d_name[8] == '\0' )
            count++;
         else if ( ent->d_name[8] == '.' )
            count++;
         else
            continue;
      }
      closedir(dir);
   }
   
   return count > 1 ? 1 : 0;
}

/*
 * the very hard job, call simply mtink, and tell that all is OK
 */
 
#ifdef GIMP_VERSION_2
static void
run (const gchar *name,
     gint nparams,
     const GimpParam *param,
     gint *nreturn_vals,
     GimpParam **return_vals)
#else
static void
run (char    *name,
     int      nparams,
     GParam  *param,
     int     *nreturn_vals,
     GParam **return_vals)
#endif
{
   static GParam  values[1];
   char *command = defaultCommand;
   *nreturn_vals           = 1;
   *return_vals            = values;
   values[0].type          = PARAM_STATUS;
   values[0].data.d_status = STATUS_SUCCESS;
   if ( checkForChoices() )
   {
      command = choiceCommand;
   }
   system(command);
}
