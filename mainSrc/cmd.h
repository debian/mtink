/****************************************************
 * File cmd.h
 *
 * header for write and read to / from printer
 *
 *
 * Copyright (C) 2001 Jean-Jacques Sarton jj.sarton@t-online.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************/

#ifndef CMD_H

#define CMD_H

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

extern int callPrg(int command, char *name, int mode, int pass, int choice, char **buf, char *function);
extern int writeD4Data(int deviceHdl, unsigned char *data, int len, int stop);
extern int initializeD4(void);
extern int doCommand(int working);
extern void setPrinterName(char *);

typedef struct shmem_s
{
   int  command;
   char name[1024-64];
   char function[64];  /* place for a function name */
   int  printerState;
   int  mode;
   int  pass;
   int  choice;
   int  retVal;
   int  ready;
   /* these variable are for the server */
   int  fd;
   int  dataSocketId;
   int  block;
   /* data buffer for both */
   char buf[1024];
} shmem_t;

extern shmem_t *shmem;

#define MODEL_KCMy      '1'
#define MODEL_KCMycm    '2'
#define MODEL_KCMycmY   '3'
#define MODEL_KCMycmk   '4'
#define MODEL_KCMyRB    '5'
#define MODEL_KCMyRBX   '6'
#define MODEL_KCMyRBkX  '7'
#define MODEL_KCMycmGg  '8'

#if defined(__cplusplus) || defined(c_plusplus)
}
#endif

extern int devRead(int fd, unsigned char *buf, int len, int timeout); 
extern int devWrite(int fd, unsigned char *buf, int len, int timeout); 
#endif
