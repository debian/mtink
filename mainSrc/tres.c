#include "version.h"
char *fallbackResources[] = {
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Danish resource file for ttink.
   ! Dansk data for ttink.
   !
   ! This file is build like a normal X-resources files
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             is a commment character if it appears at the begin of a
   !               line.
   !
   ! .syntax1:     is ein key word.
   ! .De.syntax:   is the key for a localized text.
   !               both characters in this case De are taken from the
   !               environment variablea LANG, LC_ALL. The first letter
   !               will be set to upper case and the second remain normal.
   !               If you translate this file, please replace .En. with
   !               the correct value.
   !
   ! \             At the  end of a line (space and  tabulators are
   !               not allowed) show that the actual line will be continued.
   !
   ! \     text    allow to ident the text. This will only be required after
   !               the key word. If nor \ is present, the space are
   !               removed.
   !
   ! \n            This is a carriage return. Textes will be printed in
   !               the next line.
   !
   !               Please note that spaces at the en of a line will not
   !               be removed.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".Da.syntax1:"
   " Argumenter for ",

   ".Da.syntax2:"
   "  Obligatorisk:\\n"
   "    -d enheds_fil --device enheds_fil\\n"
   "  Valgfri:\\n"
   "    -m navn        --model navn eller nummer\\n"
   "        uden angivelse vil printeren blive autodetektet\\n"
   "  Funktion (der kan kun udf�res en funktion af gangen):\\n"
   "    -r --reset            reset printer\\n"
   "    -c --clean            rens dyser\\n"
   "    -n --nozzle-check     dyse test\\n"
   "    -s --status           (standard)\\n"
   "    -a --align-head       ret printerhovedet ind\\n"
   "    -e --exchange-ink     (ikke alle printere underst�tter dette)\\n"
   "    -i --identity         udskriv printer identitet\\n"
   "  Information:\\n"
   "    -v --version          print version\\n"
   "    -l --list-printer     list kendte printere\\n",

   ".Da.syntaxM:"
   "  Funktion (der kan kun udf�res en funktion af gangen):\\n"
   "    -r --reset            reset printer\\n"
   "    -c --clean            rens dyser\\n"
   "    -n --nozzle-check     dyse test\\n"
   "    -s --status           (standard)\\n"
   "    -a --align-head       ret printerhovedet ind\\n"
   "    -e --exchange-ink     (ikke alle printere underst�tter dette)\\n"
   "    -i --identity         udskriv printer identitet\\n"
   "  Information:\\n"
   "    -v --version          print version\\n"
   "    -l --list-printer     list kendte printere\\n",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".Da.noAccess:"
   "Ingen adgang til enhed eller ingen tilsluttet printer.",

   ".Da.noDetected:"
   "Kan ikke bestemme printer model.",

   ".Da.unknownModel:"
   "Ukendt printer.",

   ".Da.noOPen:"
   "Kan ikke �bne enheds filen.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".Da.continue:"
   "Forts�t (ja/nej) [nej] ? :",

   ".Da.yesorno: jn",
   ".Da.saveCancel: ga",

   ".Da.blackQ: sort",
   ".Da.colorQ: farve",

   ".Da.followingPrintersFound:"
   " Printere fundet:",
   ".Da.ChoosePrinter:"
   "V�lg printer ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".Da.black:    Sort",
   ".Da.cyan:     Cyan",
   ".DA.magenta:  Magenta",
   ".Da.yellow:   Gul",
   ".Da.lcyan:    Lys cyan",
   ".Da.lmagenta: Lys magenta",
   ".Da.lblack:   Lys sort",
   ".Da.lblack:   Foto/Mat sort",
   ".Da.blue:     Bl�",
   ".Da.red:      R�d",
   ".Da.dyellow:  M�rk gul",
   ".Da.gloss:    Glans optimerer",
   ".Da.grey:     Gr�",

   ".Da.printerState: printer status",
   ".Da.unknown:      unendt",
   ".Da.selfTest:     selv test",
   ".Da.busy:         optaget",
   ".Da.printing:     udskrivning",
   ".Da.cleaning:     rensning",
   ".Da.ok:           Ok",

   ".Da.error:                Fejl",
   ".Da.interfaceNotSelected: Ingen printerport valgt",
   ".Da.paperJamError:        Fejl ved papirfremf�ring",
   ".Da.inkOutError:          Der er ikke nok bl�k",
   ".Da.paperOutError:        Intet papir",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".Da.alignWarning:"
   "Pas p� !\\nDette kan skade printeren.",

   ".Da.alignBlackOrColor:"
   "Retten ind af sort eller farve hoved ?",

   "*Da.firstHeadAlign:"
   "Placer et ark papir i printeren for at begynde\\n"
   "at rette hovedet ind.",

   ".Da.nextHeadAlign:"
   "Geninds�t arket i printeren.",

   ".Da.lastHeadAlign:"
   "Inspicer det f�rdige udskrift meget omhyggeligt for at sikre\\n"
   "At printerhovedet er korrekt rettet ind.\\n\\n"
   "Du kan nu: gemme resultatet i printeren, eller\\n"
   "afbryde uden at gemme resultatet\\n(gem/afbryd) [afbryd] :",

   ".Da.choosePattern:"
   "Inspicer udskriftet omhyggeligt, og v�lg det bedste liniepar\\n"
   "fra det sidst printede m�nster.\\n"
   "Geninds�t arket i printeren.",

   ".Da.chooseCPattern:"
   "Inspicer retten ind arket, og afg�r hvilket\\n"
   "m�nster der er det glatteste.\\n"
   "Dette m�nster er det der har den mindste \"korn\"struktur.\\n"
   "Hvis du ikke kan afg�re hvilket der er det glatteste,\\n"
   "og v�lg nummeret p� et af de bedste, og\\n"
   "gentag proceduren.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".Da.functionNA:"
   "Denne funktion er ikke mulig med din printer.",

   ".Da.askDoExchange:"
   "�nsker du at skifte patronen (Ja/Nej) [nej] ? :",

   ".exchangeBlackOrColor:"
   "V�lg patron:",

   ".Da.adviseMoveCartridge:"
   "Patronen vil nu blive f�rt til udskiftnings plads.",

   ".Da.adviseExchangeCartridge:"
   "Udskift patronen nu.",

   ".Da.askExDone:"
   "Udskiftning udf�rt (Ja/Nej) [nej] ? :",

   ".Da.adviseFillCartridge:"
   "P�fyldning af bl�k starter nu.",

   ".Da.adviseExchangeDone:"
   "Udskiftning af patron udf�rt.",

   ".Da.communicationError:"
   "Fejl ved lasning/skrivning fra/til printeren !",

   ".Da.exchangeError:"
   "Printeren melder fejl p� funktionen 'udskiftning af patron' !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Danish resource file for ttink.
   ! Dansk data for ttink.
   !
   ! This file is build like a normal X-resources files
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             is a commment character if it appears at the begin of a
   !               line.
   !
   ! .syntax1:     is ein key word.
   ! .De.syntax:   is the key for a localized text.
   !               both characters in this case De are taken from the
   !               environment variablea LANG, LC_ALL. The first letter
   !               will be set to upper case and the second remain normal.
   !               If you translate this file, please replace .En. with
   !               the correct value.
   !
   ! \             At the  end of a line (space and  tabulators are
   !               not allowed) show that the actual line will be continued.
   !
   ! \     text    allow to ident the text. This will only be required after
   !               the key word. If nor \ is present, the space are
   !               removed.
   !
   ! \n            This is a carriage return. Textes will be printed in
   !               the next line.
   !
   !               Please note that spaces at the en of a line will not
   !               be removed.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".Da8.syntax1:"
   " Argumenter for ",

   ".Da8.syntax2:"
   "  Obligatorisk:\\n"
   "    -d enheds_fil --device enheds_fil\\n"
   "  Valgfri:\\n"
   "    -m navn        --model navn eller nummer\\n"
   "        uden angivelse vil printeren blive autodetektet\\n"
   "  Funktion (der kan kun udføres en funktion af gangen):\\n"
   "    -r --reset            reset printer\\n"
   "    -c --clean            rens dyser\\n"
   "    -n --nozzle-check     dyse test\\n"
   "    -s --status           (standard)\\n"
   "    -a --align-head       ret printerhovedet ind\\n"
   "    -e --exchange-ink     (ikke alle printere understøtter dette)\\n"
   "    -i --identity         udskriv printer identitet\\n"
   "  Information:\\n"
   "    -v --version          print version\\n"
   "    -l --list-printer     list kendte printere\\n",

   ".Da8.syntaxM:"
   "  Funktion (der kan kun udføres en funktion af gangen):\\n"
   "    -r --reset            reset printer\\n"
   "    -c --clean            rens dyser\\n"
   "    -n --nozzle-check     dyse test\\n"
   "    -s --status           (standard)\\n"
   "    -a --align-head       ret printerhovedet ind\\n"
   "    -e --exchange-ink     (ikke alle printere understøtter dette)\\n"
   "    -i --identity         udskriv printer identitet\\n"
   "  Information:\\n"
   "    -v --version          print version\\n"
   "    -l --list-printer     list kendte printere\\n",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".Da8.noAccess:"
   "Ingen adgang til enhed eller ingen tilsluttet printer.",

   ".Da8.noDetected:"
   "Kan ikke bestemme printer model.",

   ".Da8.unknownModel:"
   "Ukendt printer.",

   ".Da8.noOPen:"
   "Kan ikke åbne enheds filen.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".Da8.continue:"
   "Fortsæt (ja/nej) [nej] ? :",

   ".Da8.yesorno: jn",
   ".Da8.saveCancel: ga",

   ".Da8.blackQ: sort",
   ".Da8.colorQ: farve",

   ".Da8.followingPrintersFound:"
   " Printere fundet:",
   ".Da8.ChoosePrinter:"
   "Vælg printer ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".Da8.black:    Sort",
   ".Da8.cyan:     Cyan",
   ".DA8.magenta:  Magenta",
   ".Da8.yellow:   Gul",
   ".Da8.lcyan:    Lys cyan",
   ".Da8.lmagenta: Lys magenta",
   ".Da8.lblack:   Lys sort",
   ".Da8.lblack:   Foto/Mat sort",
   ".Da8.blue:     Blå",
   ".Da8.red:      Rød",
   ".Da8.dyellow:  Mørk gul",
   ".Da8.gloss:    Glans optimerer",
   ".Da8.grey:     Grå",

   ".Da8.printerState: printer status",
   ".Da8.unknown:      unendt",
   ".Da8.selfTest:     selv test",
   ".Da8.busy:         optaget",
   ".Da8.printing:     udskrivning",
   ".Da8.cleaning:     rensning",
   ".Da8.ok:           Ok",

   ".Da8.error:                Fejl",
   ".Da8.interfaceNotSelected: Ingen printerport valgt",
   ".Da8.paperJamError:        Fejl ved papirfremføring",
   ".Da8.inkOutError:          Der er ikke nok blæk",
   ".Da8.paperOutError:        Intet papir",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".Da8.alignWarning:"
   "Pas på !\\nDette kan skade printeren.",

   ".Da8.alignBlackOrColor:"
   "Retten ind af sort eller farve hoved ?",

   "*Da.firstHeadAlign:"
   "Placer et ark papir i printeren for at begynde\\n"
   "at rette hovedet ind.",

   ".Da8.nextHeadAlign:"
   "Genindsæt arket i printeren.",

   ".Da8.lastHeadAlign:"
   "Inspicer det færdige udskrift meget omhyggeligt for at sikre\\n"
   "At printerhovedet er korrekt rettet ind.\\n\\n"
   "Du kan nu: gemme resultatet i printeren, eller\\n"
   "afbryde uden at gemme resultatet\\n(gem/afbryd) [afbryd] :",

   ".Da8.choosePattern:"
   "Inspicer udskriftet omhyggeligt, og vælg det bedste liniepar\\n"
   "fra det sidst printede mønster.\\n"
   "Genindsæt arket i printeren.",

   ".Da8.chooseCPattern:"
   "Inspicer retten ind arket, og afgør hvilket\\n"
   "mønster der er det glatteste.\\n"
   "Dette mønster er det der har den mindste \"korn\"struktur.\\n"
   "Hvis du ikke kan afgøre hvilket der er det glatteste,\\n"
   "og vælg nummeret på et af de bedste, og\\n"
   "gentag proceduren.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".Da8.functionNA:"
   "Denne funktion er ikke mulig med din printer.",

   ".Da8.askDoExchange:"
   "Ønsker du at skifte patronen (Ja/Nej) [nej] ? :",

   ".exchangeBlackOrColor:"
   "Vælg patron:",

   ".Da8.adviseMoveCartridge:"
   "Patronen vil nu blive ført til udskiftnings plads.",

   ".Da8.adviseExchangeCartridge:"
   "Udskift patronen nu.",

   ".Da8.askExDone:"
   "Udskiftning udført (Ja/Nej) [nej] ? :",

   ".Da8.adviseFillCartridge:"
   "Påfyldning af blæk starter nu.",

   ".Da8.adviseExchangeDone:"
   "Udskiftning af patron udført.",

   ".Da8.communicationError:"
   "Fejl ved lasning/skrivning fra/til printeren !",

   ".Da8.exchangeError:"
   "Printeren melder fejl på funktionen 'udskiftning af patron' !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! german resource file for ttink.
   ! Deutsche Datei f�r ttink.
   !
   ! Diese Datei entspricht vom Aufbau her den �blichen
   ! X Ressourcen-Dateien.
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             ist ein Kommentarzeichen wenn es am Begin einer Zeile
   !               steht.
   !
   ! .syntax1:     ist ein Schl�ssel.
   ! .De.syntax:   ist der entspechend lokalisierte Schl�ssel.
   !               Die 2 Buschstaben, hier "De", werden von den Umgebungs-
   !               Variablen LANG und LC_ALL abgeleitet.
   !
   ! \             am Ende einer Zeile (Leerzeichen oder Tabulatoren sind
   !               nicht erlaubt) bedeutet das, das die n�chste Zeile eine
   !               Fortsetzungszeile ist.
   ! \     Text    bewirkt, dass Leerzeichen am Anfang des Textes
   !               (nach dem Schl�ssel) nicht entfernt werden.
   !
   ! \n            steht f�r eine Zeilenschaltung.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
#endif
   ".De.syntax1:"
   " Argumente f�r ",

   ".De.syntax2:"
   "  Obligatorisch:\\n"
   "    -d Ger�te_datei --device Ger�te_datei\\n"
   "  Optional:\\n"
   "    -m Name  --model Name oder Nummer\\n"
   "        Ohne diese Angabe wird der Drucker automatisch bestimmt\\n"
   "    -D --D4                D4-Protokoll immer verwenden\\n"
   "  Auftrag (Es kann nur ein Auftrag angegeben werden):\\n"
   "    -r --reset             Drucker zur�cksetzen\\n"
   "    -c --clean             D�senreinigung\\n"
   "    -n --nozzle-check      D�sentest\\n"
   "    -s --status            (Standardauftrag)\\n"
   "    -a --align-head        K�pfe ausrichten\\n"
   "    -e --exchange-ink      Patronen wechseln\\n"
   "                           (nicht alle Druckern)\\n"
   "    -i --identity          Drucker Identifizierungsstring ausgeben\\n"
   "  Informationen:\\n"
   "    -v --version           Version ausgeben\\n"
   "    -l --list-printer      Bekannte Drucker ausgeben\\n"
   "  Diverse:\\n"
   "    -L                     D4 Protokollausgabe auf stderr (debug)\\n"
   "    -u                     Text Ausgabe erfolgt mit UTF-8 Kodierung\\n",

   ".De.syntaxM:"
   "  Optional:\\n"
   "    -D --D4                D4-Protokoll immer verwenden\\n"
   "  Auftrag (Es kann nur ein Auftrag angegeben werden):\\n"
   "    -r --reset             Drucker zur�cksetzen\\n"
   "    -c --clean             D�senreinigung\\n"
   "    -n --nozzle-check      D�sentest\\n"
   "    -s --status            (Standardauftrag)\\n"
   "    -a --align-head        K�pfe ausrichten\\n"
   "    -e --exchange-ink      Patronen wechseln\\n"
   "                           (nicht alle Druckern)\\n"
   "    -i --identity          Drucker Identifizierungsstring ausgeben\\n"
   "  Informationen:\\n"
   "    -v --version           Version ausgeben\\n"
   "    -l --list-printer      Bekannte Drucker ausgeben\\n"
   "  Diverse:\\n"
   "    -L                     D4 Protokollausgabe auf stderr (debug)\\n"
   "    -u                     Text Ausgabe erfolgt mit UTF-8 Kodierung\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".De.noAccess:"
   "Kein Zugriff auf die Ger�tedatei, oder Drucker ist nicht angeschlossen.",

   ".De.noDetected:"
   "Das Druckermodell kann nicht bestimmt werden.",

   ".De.unknownModel:"
   "Unbekannter Drucker",

   ".De.noOPen:"
   "Die Ger�tedatei kann nicht ge�ffnet werden.",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".De.continue:"
   "Weiter (ja/nein) [nein]? : ",

   ".De.yesorno: jn",
   ".De.saveCancel: sa ",

   ".De.blackQ:   Schwarz",
   ".De.colorQ:   Farbe",

   ".De.followingPrintersFound:"
   "Ermittelte Drucker:",

   ".De.ChoosePrinter:"
   "Drucker w�hlen ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".De.black:                Schwarz",
   ".De.cyan:                 Zyan",
   ".De.magenta:              Magenta",
   ".De.yellow:               Gelb",
   ".De.lcyan:                Hellzyan",
   ".De.lmagenta:             Hellmagenta",
   ".De.lblack:               Photo/Matt Schwarz",
   ".De.blue:                 Blau",
   ".De.red:                  Rot",
   ".De.dyellow:              Dunkelgelb",
   ".De.gloss:                Glanzoptimierer",
   ".De.grey:                 Grau",
#if 0
/*
   ! Photo R2400 die Franz�siche Texte beinhalten korreckterweise
   ! grau und hell grau. Hellschwarz und Hellhellschwarz ist ein
   ! Unsinn !
*/
#endif
   ".De.llblack:              Grau",
   ".De.Lblack:               Hellgrau",


   ".De.printerState:         Druckertatus",
   ".De.unknown:              Unbekannt",
   ".De.selfTest:             Selbsttest",
   ".De.busy:                 Besch�ftigt",
   ".De.printing:             Druck",
   ".De.cleaning:             Reinigung",
   ".De.ok:                   Ok",

   ".De.error:                Fehler",
   ".De.interfaceNotSelected: Schnittstelle nicht angew�hlt",
   ".De.paperJamError:        Fehler im Papierweg",
   ".De.inkOutError:          Tintenmenge ist zu gering",
   ".De.paperOutError:        Papier fehlt",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".De.alignWarning:"
   "Achtung !\\nDies k�nnte den Drucker unbrauchbar machen.",

   ".De.alignBlackOrColor:"
   "Schwarz- oder Farbkopf ausrichten?",

   ".De*firstHeadAlign:"
   "Bitte ein leeres Blatt Papier in den\\nDrucker legen.",

   ".De.nextHeadAlign:"
   "Bitte das Blatt erneut in den Drucker legen.",

   ".De.lastHeadAlign:"
   "Bitte begutachten Sie die Ergebnisse bez�glich der\\n"
   "Ausrichtung sehr sorgf�ltig.\\n"
   "Sie k�nnen nun: Die Einstellungen im Drucker sichern oder abbrechen,\\n"
   "ohne die Einstellungen zu �bernehmen\\n(Sichern, Abbruch) [Abbbruch] : ",

   ".De.choosePattern:"
   "Bitte begutachten Sie das soeben gedruckte Muster,\\n"
   "und w�hlen Sie die besten Linienpaare aus.\\n"
   "Legen Sie das Blatt erneut in den Drucker.",

   ".De.sampleNo:"
   "Muster #",

   ".De.choosePattern2:"
   "Bitte begutachten Sie das soeben gedruckte Muster,\\n"
   "und w�hlen Sie die besten Linienpaare aus\\n",


   ".De.chooseCPattern:"
   "Bitte das Blatt begutachten und das Muster ausw�hlen, das\\n"
   "die geringste Kornstruktur hat.",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".De.functionNA:"
   "Diese Function ist f�r diesen Drucker nicht verf�gbar.",

   ".De.askDoExchange:"
   "M�chten Sie die Patrone wechseln (ja/nein) [nein]? :",

   ".De.exchangeBlackOrColor: Patronne w�hlen:",

   ".De.adviseMoveCartridge:"
   "Der Kopf wird in die Austauschposition bewegt.",

   ".De.adviseExchangeCartridge:"
   "Bitte die Patrone wechseln.",

   ".De.askExDone:"
   "Ist der Austausch durchgef�hrt (ja/nein) [nein]? : ",

   ".De.adviseFillCartridge:"
   "Das Auff�llen der Tinte erfolgt.",

   ".De.adviseExchangeDone:"
   "Der Patronenwechsel ist durchgef�hrt.",

   ".De.communicationError:"
   "Fehler beim Schreiben/Lesen zum/vom Drucker!",

   ".De.exchangeError:"
   "Der Drucker meldet einen Fehler auf der Anforderung \"Patrone wechseln\"!",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! german resource file for ttink.
   ! Deutsche Datei für ttink.
   !
   ! Diese Datei entspricht vom Aufbau her den üblichen
   ! X Ressourcen-Dateien.
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             ist ein Kommentarzeichen wenn es am Begin einer Zeile
   !               steht.
   !
   ! .syntax1:     ist ein Schlüssel.
   ! .De.syntax:   ist der entspechend lokalisierte Schlüssel.
   !               Die 2 Buschstaben, hier "De", werden von den Umgebungs-
   !               Variablen LANG und LC_ALL abgeleitet.
   !
   ! \             am Ende einer Zeile (Leerzeichen oder Tabulatoren sind
   !               nicht erlaubt) bedeutet das, das die nächste Zeile eine
   !               Fortsetzungszeile ist.
   ! \     Text    bewirkt, dass Leerzeichen am Anfang des Textes
   !               (nach dem Schlüssel) nicht entfernt werden.
   !
   ! \n            steht für eine Zeilenschaltung.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
#endif
   ".De8.syntax1:"
   " Argumente für ",

   ".De8.syntax2:"
   "  Obligatorisch:\\n"
   "    -d Geräte_datei --device Geräte_datei\\n"
   "  Optional:\\n"
   "    -m Name  --model Name oder Nummer\\n"
   "        Ohne diese Angabe wird der Drucker automatisch bestimmt\\n"
   "    -D --D4                D4-Protokoll immer verwenden\\n"
   "  Auftrag (Es kann nur ein Auftrag angegeben werden):\\n"
   "    -r --reset             Drucker zurücksetzen\\n"
   "    -c --clean             Düsenreinigung\\n"
   "    -n --nozzle-check      Düsentest\\n"
   "    -s --status            (Standardauftrag)\\n"
   "    -a --align-head        Köpfe ausrichten\\n"
   "    -e --exchange-ink      Patronen wechseln\\n"
   "                           (nicht alle Druckern)\\n"
   "    -i --identity          Drucker Identifizierungsstring ausgeben\\n"
   "  Informationen:\\n"
   "    -v --version           Version ausgeben\\n"
   "    -l --list-printer      Bekannte Drucker ausgeben\\n"
   "  Diverse:\\n"
   "    -L                     D4 Protokollausgabe auf stderr (debug)\\n"
   "    -u                     Text Ausgabe erfolgt mit UTF-8 Kodierung\\n",

   ".De8.syntaxM:"
   "  Optional:\\n"
   "    -D --D4                D4-Protokoll immer verwenden\\n"
   "  Auftrag (Es kann nur ein Auftrag angegeben werden):\\n"
   "    -r --reset             Drucker zurücksetzen\\n"
   "    -c --clean             Düsenreinigung\\n"
   "    -n --nozzle-check      Düsentest\\n"
   "    -s --status            (Standardauftrag)\\n"
   "    -a --align-head        Köpfe ausrichten\\n"
   "    -e --exchange-ink      Patronen wechseln\\n"
   "                           (nicht alle Druckern)\\n"
   "    -i --identity          Drucker Identifizierungsstring ausgeben\\n"
   "  Informationen:\\n"
   "    -v --version           Version ausgeben\\n"
   "    -l --list-printer      Bekannte Drucker ausgeben\\n"
   "  Diverse:\\n"
   "    -L                     D4 Protokollausgabe auf stderr (debug)\\n"
   "    -u                     Text Ausgabe erfolgt mit UTF-8 Kodierung\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".De8.noAccess:"
   "Kein Zugriff auf die Gerätedatei, oder Drucker ist nicht angeschlossen.",

   ".De8.noDetected:"
   "Das Druckermodell kann nicht bestimmt werden.",

   ".De8.unknownModel:"
   "Unbekannter Drucker",

   ".De8.noOPen:"
   "Die Gerätedatei kann nicht geöffnet werden.",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".De8.continue:"
   "Weiter (ja/nein) [nein]? : ",

   ".De8.yesorno: jn",
   ".De8.saveCancel: sa ",

   ".De8.blackQ:   Schwarz",
   ".De8.colorQ:   Farbe",

   ".De8.followingPrintersFound:"
   "Ermittelte Drucker:",

   ".De8.ChoosePrinter:"
   "Drucker wählen ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".De8.black:                Schwarz",
   ".De8.cyan:                 Zyan",
   ".De8.magenta:              Magenta",
   ".De8.yellow:               Gelb",
   ".De8.lcyan:                Hellzyan",
   ".De8.lmagenta:             Hellmagenta",
   ".De8.lblack:               Photo/Matt Schwarz",
   ".De8.blue:                 Blau",
   ".De8.red:                  Rot",
   ".De8.dyellow:              Dunkelgelb",
   ".De8.gloss:                Glanzoptimierer",
   ".De8.grey:                 Grau",
#if 0
/*
   ! Photo R2400 die Französiche Texte beinhalten korreckterweise
   ! grau und hell grau. Hellschwarz und Hellhellschwarz ist ein
   ! Unsinn !
*/
#endif
   ".De8.llblack:              Grau",
   ".De8.Lblack:               Hellgrau",


   ".De8.printerState:         Druckertatus",
   ".De8.unknown:              Unbekannt",
   ".De8.selfTest:             Selbsttest",
   ".De8.busy:                 Beschäftigt",
   ".De8.printing:             Druck",
   ".De8.cleaning:             Reinigung",
   ".De8.ok:                   Ok",

   ".De8.error:                Fehler",
   ".De8.interfaceNotSelected: Schnittstelle nicht angewählt",
   ".De8.paperJamError:        Fehler im Papierweg",
   ".De8.inkOutError:          Tintenmenge ist zu gering",
   ".De8.paperOutError:        Papier fehlt",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".De8.alignWarning:"
   "Achtung !\\nDies könnte den Drucker unbrauchbar machen.",

   ".De8.alignBlackOrColor:"
   "Schwarz- oder Farbkopf ausrichten?",

   ".De*firstHeadAlign:"
   "Bitte ein leeres Blatt Papier in den\\nDrucker legen.",

   ".De8.nextHeadAlign:"
   "Bitte das Blatt erneut in den Drucker legen.",

   ".De8.lastHeadAlign:"
   "Bitte begutachten Sie die Ergebnisse bezüglich der\\n"
   "Ausrichtung sehr sorgfältig.\\n"
   "Sie können nun: Die Einstellungen im Drucker sichern oder abbrechen,\\n"
   "ohne die Einstellungen zu übernehmen\\n(Sichern, Abbruch) [Abbbruch] : ",

   ".De8.choosePattern:"
   "Bitte begutachten Sie das soeben gedruckte Muster,\\n"
   "und wählen Sie die besten Linienpaare aus.\\n"
   "Legen Sie das Blatt erneut in den Drucker.",

   ".De8.sampleNo:"
   "Muster #",

   ".De8.choosePattern2:"
   "Bitte begutachten Sie das soeben gedruckte Muster,\\n"
   "und wählen Sie die besten Linienpaare aus\\n",


   ".De8.chooseCPattern:"
   "Bitte das Blatt begutachten und das Muster auswählen, das\\n"
   "die geringste Kornstruktur hat.",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".De8.functionNA:"
   "Diese Function ist für diesen Drucker nicht verfügbar.",

   ".De8.askDoExchange:"
   "Möchten Sie die Patrone wechseln (ja/nein) [nein]? :",

   ".De8.exchangeBlackOrColor: Patronne wählen:",

   ".De8.adviseMoveCartridge:"
   "Der Kopf wird in die Austauschposition bewegt.",

   ".De8.adviseExchangeCartridge:"
   "Bitte die Patrone wechseln.",

   ".De8.askExDone:"
   "Ist der Austausch durchgeführt (ja/nein) [nein]? : ",

   ".De8.adviseFillCartridge:"
   "Das Auffüllen der Tinte erfolgt.",

   ".De8.adviseExchangeDone:"
   "Der Patronenwechsel ist durchgeführt.",

   ".De8.communicationError:"
   "Fehler beim Schreiben/Lesen zum/vom Drucker!",

   ".De8.exchangeError:"
   "Der Drucker meldet einen Fehler auf der Anforderung \"Patrone wechseln\"!",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! English resource file for ttink.
   ! This is for translators.
   !
   ! This file is build like a normal X-resources files
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             is a commment character if it appears at the begin of a
   !               line.
   !
   ! .syntax1:     is ein key word.
   ! .De.syntax:   is the key for a localized text.
   !               both characters in this case De are taken from the
   !               environment variablea LANG, LC_ALL. The first letter
   !               will be set to upper case and the second remain normal.
   !               If you translate this file, please replace .En. with
   !               the correct value.
   !
   ! \             At the  end of a line (space and  tabulators are
   !               not allowed) show that the actual line will be continued.
   !
   ! \     text    allow to ident the text. This will only be required after
   !               the key word. If nor \ is present, the space are
   !               removed.
   !
   ! \n            This is a carriage return. Textes will be printed in
   !               the next line.
   !
   !               Please note that spaces at the en of a line will not
   !               be removed.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".syntax1:"
   " Arguments for ",

   ".syntax2:"
   "  Mandatory:\\n"
   "    -d device_file --device device_file\\n"
   "  Optional:\\n"
   "    -m name        --model name or numero\\n"
   "        without this the printer will be autodetected\\n"
   "    -D  --D4              Use always the D4 protocol\\n"
   "  Order (only one order may be given):\\n"
   "    -r --reset\\n"
   "    -c --clean\\n"
   "    -n --nozzle-check\\n"
   "    -s --status           (default)\\n"
   "    -a --align-head\\n"
   "    -e --exchange-ink     (not all printers)\\n"
   "    -i --identity         print printer identity\\n"
   "  Info:\\n"
   "    -v --version          print version\\n"
   "    -l --list-printer     list known printers\\n"
   "  Misc:\\n"
   "    -L                    debug ouput for D4 protocol on stderr\\n"
   "    -u                    printout with UTF-8 code\\n",

   ".syntaxM:"
   "  Optional:\\n"
   "    -D  --D4              Use always the D4 protocol\\n"
   "  Order (only one order may be given):\\n"
   "    -r --reset\\n"
   "    -c --clean\\n"
   "    -n --nozzle-check\\n"
   "    -s --status           (default)\\n"
   "    -a --align-head\\n"
   "    -e --exchange-ink     (not all printers)\\n"
   "    -i --identity         print printer identity\\n"
   "  Info:\\n"
   "    -v --version          print version\\n"
   "    -l --list-printer     list known printers\\n"
   "  Misc:\\n"
   "    -L                    debug ouput for D4 protocol on stderr\\n"
   "    -u                    printout with UTF-8 code\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".noAccess:"
   "No access to device file or no attached printer.",

   ".noDetected:"
   "Can't detect printer model.",

   ".unknownModel:"
   "Sorry unknown model",

   ".noOPen:"
   "Sorry can't open the device file.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".continue:"
   "Continue (yes/no) [no] ? : ",

   ".yesorno: yn",
   ".saveCancel: sc",

   ".blackQ: black",
   ".colorQ: color",

   ".followingPrintersFound:"
   "The following printers were found:",

   ".ChoosePrinter:"
   "Choose printer ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".black:                Black",
   ".cyan:                 Cyan",
   ".magenta:              Magenta",
   ".yellow:               Yellow",
   ".lcyan:                Light cyan",
   ".lmagenta:             Light magenta",
   ".lblack:               Photo/Matte black",
   ".blue:                 Blue",
   ".red:                  Red",
   ".dyellow:              Dark yellow",
   ".gloss:                Gloss optimizer",
   ".grey:                 Grey",
   ".llblack:              Light light black",
   ".Lblack:               Light black",

   ".printerState:         printer state",
   ".unknown:              unknown",
   ".selfTest:             self test",
   ".busy:                 busy",
   ".printing:             printing",
   ".cleaning:             cleaning",
   ".ok:                   Ok",

   ".error:                Error",
   ".interfaceNotSelected: Interface not selected",
   ".paperJamError:        Paper jam error",
   ".inkOutError:          Ink out error",
   ".paperOutError:        Paper out error",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".alignWarning:"
   "Warning !\\nThis may damage the printer.",

   ".alignBlackOrColor:"
   "Align black or color head ?",

   "*firstHeadAlign:"
   "Please place a sheet of paper in your printer to begin\\n"
   "the head alignment procedure.",

   ".nextHeadAlign:"
   "Please reinsert the sheet of paper in your printer to\\n"
   "continue the head alignment procedure.",

   ".lastHeadAlign:"
   "Please inspect the final output very carefully to ensure that your\\n"
   "printer is in proper alignment.\\n\\n"
   "You may now: save the results in the printer or\\n"
   "Cancel without saving the results\\n(save/cancel) [cancel] : ",

   ".choosePattern:"
   "Please inspect the print, and choose the best pair of lines\\n"
   "from the last printed pattern.\\n"
   "Reinsert the sheet into the printer.",

   "-sampleNo:"
   "pattern #",

   ".choosePattern2:"
   "Please inspect the print, and choose the best pair of lines\\n"
   "from the printed pattern\\n",

   ".chooseCPattern:"
   "Inspect the alignment sheet, and determine which\\n"
   "pattern is the smoothest.\\n"
   "This pattern will appear to have the least ``grain''.\\n"
   "If you cannot find a smooth pattern, please\\n"
   "select the number for the best pattern, and\\n"
   "repeat the procedure.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".functionNA:"
   "This function is not available for your printer.",

   ".askDoExchange:"
   "Do you really want to exchange a cartridge (Yes/No) [no] ? : ",

   ".exchangeBlackOrColor:"
   "Choose cartridge:",

   ".adviseMoveCartridge:"
   "The cartridge will now be moved to the exchange position.",

   ".adviseExchangeCartridge:"
   "Please exchange the cartridge now.",

   ".askExDone:"
   "Exchange done (yes/no) [no] ? : ",

   ".adviseFillCartridge:"
   "The ink filling will now be started.",

   ".adviseExchangeDone:"
   "Exchange cartridge done.",

   ".communicationError:"
   "Error while writing or reading from/to printer !",

   ".exchangeError:"
   "Printer return an error while requiring cartridge exchange !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! English resource file for ttink.
   ! This is for translators.
   !
   ! This file is build like a normal X-resources files
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             is a commment character if it appears at the begin of a
   !               line.
   !
   ! .syntax1:     is ein key word.
   ! .De.syntax:   is the key for a localized text.
   !               both characters in this case De are taken from the
   !               environment variablea LANG, LC_ALL. The first letter
   !               will be set to upper case and the second remain normal.
   !               If you translate this file, please replace .En. with
   !               the correct value.
   !
   ! \             At the  end of a line (space and  tabulators are
   !               not allowed) show that the actual line will be continued.
   !
   ! \     text    allow to ident the text. This will only be required after
   !               the key word. If nor \ is present, the space are
   !               removed.
   !
   ! \n            This is a carriage return. Textes will be printed in
   !               the next line.
   !
   !               Please note that spaces at the en of a line will not
   !               be removed.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".syntax1:"
   " Arguments for ",

   ".syntax2:"
   "  Mandatory:\\n"
   "    -d device_file --device device_file\\n"
   "  Optional:\\n"
   "    -m name        --model name or numero\\n"
   "        without this the printer will be autodetected\\n"
   "    -D  --D4              Use always the D4 protocol\\n"
   "  Order (only one order may be given):\\n"
   "    -r --reset\\n"
   "    -c --clean\\n"
   "    -n --nozzle-check\\n"
   "    -s --status           (default)\\n"
   "    -a --align-head\\n"
   "    -e --exchange-ink     (not all printers)\\n"
   "    -i --identity         print printer identity\\n"
   "  Info:\\n"
   "    -v --version          print version\\n"
   "    -l --list-printer     list known printers\\n"
   "  Misc:\\n"
   "    -L                    debug ouput for D4 protocol on stderr\\n"
   "    -u                    printout with UTF-8 code\\n",

   ".syntaxM:"
   "  Optional:\\n"
   "    -D  --D4              Use always the D4 protocol\\n"
   "  Order (only one order may be given):\\n"
   "    -r --reset\\n"
   "    -c --clean\\n"
   "    -n --nozzle-check\\n"
   "    -s --status           (default)\\n"
   "    -a --align-head\\n"
   "    -e --exchange-ink     (not all printers)\\n"
   "    -i --identity         print printer identity\\n"
   "  Info:\\n"
   "    -v --version          print version\\n"
   "    -l --list-printer     list known printers\\n"
   "  Misc:\\n"
   "    -L                    debug ouput for D4 protocol on stderr\\n"
   "    -u                    printout with UTF-8 code\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".noAccess:"
   "No access to device file or no attached printer.",

   ".noDetected:"
   "Can't detect printer model.",

   ".unknownModel:"
   "Sorry unknown model",

   ".noOPen:"
   "Sorry can't open the device file.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".continue:"
   "Continue (yes/no) [no] ? : ",

   ".yesorno: yn",
   ".saveCancel: sc",

   ".blackQ: black",
   ".colorQ: color",

   ".followingPrintersFound:"
   "The following printers were found:",

   ".ChoosePrinter:"
   "Choose printer ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".black:                Black",
   ".cyan:                 Cyan",
   ".magenta:              Magenta",
   ".yellow:               Yellow",
   ".lcyan:                Light cyan",
   ".lmagenta:             Light magenta",
   ".lblack:               Photo/Matte black",
   ".blue:                 Blue",
   ".red:                  Red",
   ".dyellow:              Dark yellow",
   ".gloss:                Gloss optimizer",
   ".grey:                 Grey",
   ".llblack:              Light light black",
   ".Lblack:               Light black",

   ".printerState:         printer state",
   ".unknown:              unknown",
   ".selfTest:             self test",
   ".busy:                 busy",
   ".printing:             printing",
   ".cleaning:             cleaning",
   ".ok:                   Ok",

   ".error:                Error",
   ".interfaceNotSelected: Interface not selected",
   ".paperJamError:        Paper jam error",
   ".inkOutError:          Ink out error",
   ".paperOutError:        Paper out error",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".alignWarning:"
   "Warning !\\nThis may damage the printer.",

   ".alignBlackOrColor:"
   "Align black or color head ?",

   "*firstHeadAlign:"
   "Please place a sheet of paper in your printer to begin\\n"
   "the head alignment procedure.",

   ".nextHeadAlign:"
   "Please reinsert the sheet of paper in your printer to\\n"
   "continue the head alignment procedure.",

   ".lastHeadAlign:"
   "Please inspect the final output very carefully to ensure that your\\n"
   "printer is in proper alignment.\\n\\n"
   "You may now: save the results in the printer or\\n"
   "Cancel without saving the results\\n(save/cancel) [cancel] : ",

   ".choosePattern:"
   "Please inspect the print, and choose the best pair of lines\\n"
   "from the last printed pattern.\\n"
   "Reinsert the sheet into the printer.",

   "-sampleNo:"
   "pattern #",

   ".choosePattern2:"
   "Please inspect the print, and choose the best pair of lines\\n"
   "from the printed pattern\\n",

   ".chooseCPattern:"
   "Inspect the alignment sheet, and determine which\\n"
   "pattern is the smoothest.\\n"
   "This pattern will appear to have the least ``grain''.\\n"
   "If you cannot find a smooth pattern, please\\n"
   "select the number for the best pattern, and\\n"
   "repeat the procedure.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".functionNA:"
   "This function is not available for your printer.",

   ".askDoExchange:"
   "Do you really want to exchange a cartridge (Yes/No) [no] ? : ",

   ".exchangeBlackOrColor:"
   "Choose cartridge:",

   ".adviseMoveCartridge:"
   "The cartridge will now be moved to the exchange position.",

   ".adviseExchangeCartridge:"
   "Please exchange the cartridge now.",

   ".askExDone:"
   "Exchange done (yes/no) [no] ? : ",

   ".adviseFillCartridge:"
   "The ink filling will now be started.",

   ".adviseExchangeDone:"
   "Exchange cartridge done.",

   ".communicationError:"
   "Error while writing or reading from/to printer !",

   ".exchangeError:"
   "Printer return an error while requiring cartridge exchange !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! French resource file for ttink.
   ! Ce fichier est destine aux traducteurs.
   !
   ! Ce fichier a le meme format qu'un fichier X-resources.
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             d�bute un commmentaire s'il apparait au d�but d'une
   !               ligne.
   !
   ! .syntax1:     est un mot clef.
   ! .Fr.syntax:   est le mot clef pour la version francaise.
   !               Les lettres F et r sont g�n�r�es a partir des variables
   !               d'environement LANG, LC_ALL et LC_MESSAGE. La premiere
   !               lettre est transform�e en majuscule, la seconde n'est pas
   !               modifi�e.
   !               Si vous traduisez ce texte,remplacer .Fr. par 
   !               la chaine de charactere correspondant � votre langue.
   !
   ! \             En fin de ligne (les espaces et tabulateurs ne sont pas
   !               admis) \ signifie que le texte continue sur la ligne
   !               suivante.
   !
   ! \     texte   permet d'inserer des espaces en d�but du texte.
   !
   ! \n            Ceci signifie retour chariot, le texte est continu�
   !               sur la ligne suivante.
   !
   ! \t            repr�sente un tabulateur.
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".Fr.syntax1:"
   " Arguments pour ",

   ".Fr.syntax2:"
   "  Obligatoire:\\n"
   "    -d device_file --device fichier_interface\\n"
   "  En option:\\n"
   "    -m name--model nom ou num�ro\\n"
   "                          L'imprimante est autod�tect�e si cette\\n"
   "                          option n'est pas pr�sente\\n"
   "    -D --D4               force l'utilisation exclusive du protocolle D4\\n"
   "  Commandes (une seule commande est admise):\\n"
   "    -r --reset            remise a z�ro\\n"
   "    -c --clean            nettoyage des buses\\n"
   "    -n --nozzle-check     test des buses\\n"
   "    -s --status           (commande par defaut)\\n"
   "    -a --align-head       alignement des t�tes\\n"
   "    -e --exchange-ink     remplacemrnt des cartouches (seulement\\n"
   "                          quelques imprimantes)\\n"
   "    -i --identity         retourne l'identit� de l'imprimante\\n"
   "  Informations:\\n"
   "    -v --version          affiche la version\\n"
   "    -l --list-printer     liste des imprimantes connues\\n"
   "  Divers:\\n"
   "    -L                    Impression du protocol D4 sur stderr\\n"
   "    -u                    Impression des textes avec UTF-8\\n",

   ".Fr.syntaxM:"
   "  En option:\\n"
   "    -D --D4               force l'utilisation exclusive du protocolle D4\\n"
   "  Commandes (une seule commande est admise):\\n"
   "    -r --reset            remise a z�ro\\n"
   "    -c --clean            nettoyage des buses\\n"
   "    -n --nozzle-check     test des buses\\n"
   "    -s --status           (commande par defaut)\\n"
   "    -a --align-head       alignement des t�tes\\n"
   "    -e --exchange-ink     remplacemrnt des cartouches (seulement\\n"
   "                          quelques imprimantes)\\n"
   "    -i --identity         retourne l'identit� de l'imprimante\\n"
   "  Informations:\\n"
   "    -v --version          affiche la version\\n"
   "    -l --list-printer     liste des imprimantes connues\\n"
   "  Divers:\\n"
   "    -L                    Impresson du protocol D4 sur stderr\\n"
   "    -u                    Impression des textes avec UTF-8\\n",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".Fr.noAccess:"
   "Pas d'acc�s au fichier d'interface ou imprimante �teinte.",

   ".Fr.noDetected:"
   "L'imprimante n'a pas �t� d�tect�e..",

   ".Fr.unknownModel:"
   "modele inconnus",

   ".Fr.noOPen:"
   "Erreur � l'ouverture du fichier d'interface.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".Fr.continue:"
   "Continuer (oui/non) [non] ? : ",

   ".Fr.yesorno: on",
   ".Fr.saveCancel: ea",

   ".Fr.blackQ: noir",
   ".Fr.colorQ: couleur",

   ".Fr.followingPrintersFound:"
   "Imprimantes detect�es:",

   ".Fr.ChoosePrinter:"
   "Choississez l'imprimante ",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".Fr.black:                noir",
   ".Fr.cyan:                 cyan",
   ".Fr.magenta:              magenta",
   ".Fr.yellow:               jaune",
   ".Fr.lcyan:                cyan clair",
   ".Fr.lmagenta:             magenta clair",
   ".Fr.lblack:               noir photo/matte",
   ".Fr.blue:                 bleu",
   ".Fr.red:                  rouge",
   ".Fr.dyellow:              jaune fonc�",
   ".Fr.gloss:                optimisation du gla�age",
   ".Fr.grey:                 gris",
   ".Fr.llblack:              gris",
   ".Fr.Lblack:               gris clair",

   ".Fr.printerState:         status imprimante",
   ".Fr.unknown:              inconnu",
   ".Fr.selfTest:             Test",
   ".Fr.busy:                 occupe",
   ".Fr.printing:             impression",
   ".Fr.cleaning:             nettoyage",
   ".Fr.ok:                   Ok",

   ".Fr.error:                Erreur",
   ".Fr.interfaceNotSelected: Interface non s�l�ctionn�e",
   ".Fr.paperJamError:        Erreur papier",
   ".Fr.inkOutError:          Encre �puis�e",
   ".Fr.paperOutError:        Manque de papier",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".Fr.alignWarning:"
   "Attention !\\nCeci peut endommager l'imprimante.",

   ".Fr.alignBlackOrColor:"
   "Alignement t�te noir ou couleur ?",

   "*firstHeadAlign:"
   "Inserez une feuille vierge dans l'imprimante, avant",
   "de commencer l'alignement des t�tes.",

   ".Fr.nextHeadAlign:"
   "Reinserez la feuille de papier\\n"
   "pour continuer la proc�dure d'alignement.",

   ".Fr.lastHeadAlign:"
   "Inspectez le r�sultat soigneusement afin d'�tre sur\\n"
   "que les t�tes de l'imprimante sont align�es correctement.\\n\\n"
   "Vous pouvez maintenant: enregistrer les r�sultats dans l'imprimante\\n"
   "ou annuler les modifications\\n(engregistrer/annuler) [annuler] : ",

   ".Fr.choosePattern:"
   "Inpectez les lignes imprim�es et choisissez la meilleure paire\\n"
   "de lignes.\\n"
   "Reinserez la feuille dans l'imprimante..",

   ".Fr.sampleNo:"
   "�chantillon #",

   ".Fr.choosePattern2:"
   "Inpectez les lignes imprim�es et choisissez la meilleure paire\\n"
   "de lignes\\n",

   ".Fr.chooseCPattern:"
   "Inspectez la feuille d'alignement et determinez l'�chantillon\\n"
   "le plus \"lisse\".\\n"
   "Cet �chantillon est celui qui semble �tre le moins \"granuleux\".\\n"
   "Si vous ne trouvez pas d'�chantillon \"lisse\" choisissez le meilleur\\n"
   "�chantillon et r�p�tez la proc�dure d'ajustage.\\n"
   "",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".Fr.functionNA:"
   "Cette fonction n'est pas disponible pour votre imprimante.",

   ".Fr.askDoExchange:"
   "Voulez vous vraiment remplacer une cartouche (oui/non) [non] ? : ",

   ".exchangeBlackOrColor: Choisissez la cartouche:",

   ".Fr.adviseMoveCartridge:"
   "La cartouche est amen�e en position de remplacement.",

   ".Fr.adviseExchangeCartridge:"
   "Remplacez la cartouche maintenant.",

   ".Fr.askExDone:"
   "Echange effectu� (oui/non) [non] ? : ",

   ".Fr.adviseFillCartridge:"
   "Le remplissage est enclench�.",

   ".Fr.adviseExchangeDone:"
   "Remplacement cartouche termin�.",

   ".Fr.communicationError:"
   "Erreur pour lecture ou �criture sur l'imprimante !",

   ".Fr.exchangeError:"
   "L'imprimante a retourn� une erreur pour la fonction de remplacement !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! French resource file for ttink.
   ! Ce fichier est destine aux traducteurs.
   !
   ! Ce fichier a le meme format qu'un fichier X-resources.
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             débute un commmentaire s'il apparait au début d'une
   !               ligne.
   !
   ! .syntax1:     est un mot clef.
   ! .Fr.syntax:   est le mot clef pour la version francaise.
   !               Les lettres F et r sont générées a partir des variables
   !               d'environement LANG, LC_ALL et LC_MESSAGE. La premiere
   !               lettre est transformée en majuscule, la seconde n'est pas
   !               modifiée.
   !               Si vous traduisez ce texte,remplacer .Fr. par 
   !               la chaine de charactere correspondant à votre langue.
   !
   ! \             En fin de ligne (les espaces et tabulateurs ne sont pas
   !               admis) \ signifie que le texte continue sur la ligne
   !               suivante.
   !
   ! \     texte   permet d'inserer des espaces en début du texte.
   !
   ! \n            Ceci signifie retour chariot, le texte est continué
   !               sur la ligne suivante.
   !
   ! \t            représente un tabulateur.
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".Fr8.syntax1:"
   " Arguments pour ",

   ".Fr8.syntax2:"
   "  Obligatoire:\\n"
   "    -d device_file --device fichier_interface\\n"
   "  En option:\\n"
   "    -m name--model nom ou numéro\\n"
   "                          L'imprimante est autodétectée si cette\\n"
   "                          option n'est pas présente\\n"
   "    -D --D4               force l'utilisation exclusive du protocolle D4\\n"
   "  Commandes (une seule commande est admise):\\n"
   "    -r --reset            remise a zéro\\n"
   "    -c --clean            nettoyage des buses\\n"
   "    -n --nozzle-check     test des buses\\n"
   "    -s --status           (commande par defaut)\\n"
   "    -a --align-head       alignement des têtes\\n"
   "    -e --exchange-ink     remplacemrnt des cartouches (seulement\\n"
   "                          quelques imprimantes)\\n"
   "    -i --identity         retourne l'identité de l'imprimante\\n"
   "  Informations:\\n"
   "    -v --version          affiche la version\\n"
   "    -l --list-printer     liste des imprimantes connues\\n"
   "  Divers:\\n"
   "    -L                    Impression du protocol D4 sur stderr\\n"
   "    -u                    Impression des textes avec UTF-8\\n",

   ".Fr8.syntaxM:"
   "  En option:\\n"
   "    -D --D4               force l'utilisation exclusive du protocolle D4\\n"
   "  Commandes (une seule commande est admise):\\n"
   "    -r --reset            remise a zéro\\n"
   "    -c --clean            nettoyage des buses\\n"
   "    -n --nozzle-check     test des buses\\n"
   "    -s --status           (commande par defaut)\\n"
   "    -a --align-head       alignement des têtes\\n"
   "    -e --exchange-ink     remplacemrnt des cartouches (seulement\\n"
   "                          quelques imprimantes)\\n"
   "    -i --identity         retourne l'identité de l'imprimante\\n"
   "  Informations:\\n"
   "    -v --version          affiche la version\\n"
   "    -l --list-printer     liste des imprimantes connues\\n"
   "  Divers:\\n"
   "    -L                    Impresson du protocol D4 sur stderr\\n"
   "    -u                    Impression des textes avec UTF-8\\n",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".Fr8.noAccess:"
   "Pas d'accés au fichier d'interface ou imprimante éteinte.",

   ".Fr8.noDetected:"
   "L'imprimante n'a pas été détectée..",

   ".Fr8.unknownModel:"
   "modele inconnus",

   ".Fr8.noOPen:"
   "Erreur à l'ouverture du fichier d'interface.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".Fr8.continue:"
   "Continuer (oui/non) [non] ? : ",

   ".Fr8.yesorno: on",
   ".Fr8.saveCancel: ea",

   ".Fr8.blackQ: noir",
   ".Fr8.colorQ: couleur",

   ".Fr8.followingPrintersFound:"
   "Imprimantes detectées:",

   ".Fr8.ChoosePrinter:"
   "Choississez l'imprimante ",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".Fr8.black:                noir",
   ".Fr8.cyan:                 cyan",
   ".Fr8.magenta:              magenta",
   ".Fr8.yellow:               jaune",
   ".Fr8.lcyan:                cyan clair",
   ".Fr8.lmagenta:             magenta clair",
   ".Fr8.lblack:               noir photo/matte",
   ".Fr8.blue:                 bleu",
   ".Fr8.red:                  rouge",
   ".Fr8.dyellow:              jaune foncé",
   ".Fr8.gloss:                optimisation du glaçage",
   ".Fr8.grey:                 gris",
   ".Fr8.llblack:              gris",
   ".Fr8.Lblack:               gris clair",

   ".Fr8.printerState:         status imprimante",
   ".Fr8.unknown:              inconnu",
   ".Fr8.selfTest:             Test",
   ".Fr8.busy:                 occupe",
   ".Fr8.printing:             impression",
   ".Fr8.cleaning:             nettoyage",
   ".Fr8.ok:                   Ok",

   ".Fr8.error:                Erreur",
   ".Fr8.interfaceNotSelected: Interface non séléctionnée",
   ".Fr8.paperJamError:        Erreur papier",
   ".Fr8.inkOutError:          Encre épuisée",
   ".Fr8.paperOutError:        Manque de papier",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".Fr8.alignWarning:"
   "Attention !\\nCeci peut endommager l'imprimante.",

   ".Fr8.alignBlackOrColor:"
   "Alignement tête noir ou couleur ?",

   "*firstHeadAlign:"
   "Inserez une feuille vierge dans l'imprimante, avant",
   "de commencer l'alignement des têtes.",

   ".Fr8.nextHeadAlign:"
   "Reinserez la feuille de papier\\n"
   "pour continuer la procédure d'alignement.",

   ".Fr8.lastHeadAlign:"
   "Inspectez le résultat soigneusement afin d'être sur\\n"
   "que les têtes de l'imprimante sont alignées correctement.\\n\\n"
   "Vous pouvez maintenant: enregistrer les résultats dans l'imprimante\\n"
   "ou annuler les modifications\\n(engregistrer/annuler) [annuler] : ",

   ".Fr8.choosePattern:"
   "Inpectez les lignes imprimées et choisissez la meilleure paire\\n"
   "de lignes.\\n"
   "Reinserez la feuille dans l'imprimante..",

   ".Fr8.sampleNo:"
   "échantillon #",

   ".Fr8.choosePattern2:"
   "Inpectez les lignes imprimées et choisissez la meilleure paire\\n"
   "de lignes\\n",

   ".Fr8.chooseCPattern:"
   "Inspectez la feuille d'alignement et determinez l'échantillon\\n"
   "le plus \"lisse\".\\n"
   "Cet échantillon est celui qui semble être le moins \"granuleux\".\\n"
   "Si vous ne trouvez pas d'échantillon \"lisse\" choisissez le meilleur\\n"
   "échantillon et répétez la procédure d'ajustage.\\n"
   "",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".Fr8.functionNA:"
   "Cette fonction n'est pas disponible pour votre imprimante.",

   ".Fr8.askDoExchange:"
   "Voulez vous vraiment remplacer une cartouche (oui/non) [non] ? : ",

   ".exchangeBlackOrColor: Choisissez la cartouche:",

   ".Fr8.adviseMoveCartridge:"
   "La cartouche est amenée en position de remplacement.",

   ".Fr8.adviseExchangeCartridge:"
   "Remplacez la cartouche maintenant.",

   ".Fr8.askExDone:"
   "Echange effectué (oui/non) [non] ? : ",

   ".Fr8.adviseFillCartridge:"
   "Le remplissage est enclenché.",

   ".Fr8.adviseExchangeDone:"
   "Remplacement cartouche terminé.",

   ".Fr8.communicationError:"
   "Erreur pour lecture ou écriture sur l'imprimante !",

   ".Fr8.exchangeError:"
   "L'imprimante a retourné une erreur pour la fonction de remplacement !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Magyar forr�s f�jl a ttink-hez.
   ! Ez a ford�t�nak sz�l.
   !
   ! Ez a f�jl norm�l X-forr�s f�jlk�nt van fel�p�tve
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             ez egy megjegyz�s karakter ha egy sor elej�n tal�lhat�.
   !
   !
   ! .syntax1:     ez egy kulcssz�.
   ! .De.syntax:   kulcs a sz�veg hely�nek meghat�roz�s�hoz.
   !               mindk�t karaktere - ebben az esetben a De - a LANG, LC_ALL
   !               k�rnyezeti v�ltoz�b�l kaphat�. Az els� bet�je
   !               nagybet�, a m�sodik kisbet�s marad.
   !               Ha ford�tja ezt a f�jlt, k�rem helyettes�tse az .En.-t a
   !               helyes �rt�kre (jelen esetben .Hu.).
   !
   ! \             egy sor v�g�n�l (space �s tabul�tor nincs
   !               enged�lyezve) mutatja, hogy az aktu�lis sor befejez�d�tt.
   !
   ! \     text    sz�veg azonos�t�s�hoz. Ez csak a kulcsz� ut�n
   !               k�vetelm�ny. Ha csak a \ karakter van jelen, a sz�k�z
   !               el van t�vol�tva.
   !
   ! \n            Ez egy sor elej�re �ll�t�s. Az ut�na k�vetkez� sz�veg
   !               ki�r�dik a k�vetkez� sorban.
   !
   !               K�rem figyeljen, hogy a sz�k�z�ket �s a sorv�geket ne
   !               t�vol�tsa el.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".Hu.syntax1:"
   " Arguments for ",

   ".Hu.syntax2:"
   "  K�telez�:\\n"
   "    -d eszk�z_f�jl --device eszk�z_f�jl\\n"
   "  Tetsz�leges:\\n"
   "    -m name        --modell neve vagy sz�ma\\n"
   "        en�lk�l a nyomtat� automatikusan detekt�l�dik\\n"
   "  Parancsok (csak egy parancsot lehet kiadni):\\n"
   "    -r --megszak�t�s\\n"
   "    -c --t�rl�s\\n"
   "    -n --f�v�ka ellen�rz�s\\n"
   "    -s --�llapot           (alapeset)\\n"
   "    -a --fejigaz�t�s\\n"
   "    -e --tinta csere     (nem minden nyomtat�)\\n"
   "    -i --azonos�t�s         nyomtat� azonos�t�s ki�r�sa\\n"
   "  Info:\\n"
   "    -v --verzi�          nyomtat�s verzi�\\n"
   "    -l --nyomtat� lista     ismert nyomtat�k list�ja\\n",

   ".Hu.syntaxM:"
   "  Parancsok (csak egy parancsot lehet kiadni):\\n"
   "    -r --megszak�t�s\\n"
   "    -c --t�rl�s\\n"
   "    -n --f�v�ka ellen�rz�s\\n"
   "    -s --�llapot           (alapeset)\\n"
   "    -a --fejigaz�t�s\\n"
   "    -e --tinta csere     (nem minden nyomtat�)\\n"
   "    -i --azonos�t�s         nyomtat� azonos�t�s ki�r�sa\\n"
   "  Info:\\n"
   "    -v --verzi�          nyomtat�s verzi�\\n"
   "    -l --nyomtat� lista     ismert nyomtat�k list�ja\\n",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".Hu.noAccess:"
   "Nincs hozz�f�r�s az eszk�zf�jlhoz, vagy nincs csatlakoztatott nyomtat�.",

   ".Hu.noDetected:"
   "Nem tudom detekt�lni a nyomtat� modellt.",

   ".Hu.unknownModel:"
   "Sajn�lom, ismeretlen modell.",

   ".Hu.noOPen:"
   "Sajn�lom, nem tudom megnyitni az eszk�zf�jlt.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".Hu.continue:"
   "Folytat�s (igen/nem) [nem] ? :",

   ".Hu.yesorno: in",
   ".Hu.saveCancel: mv",

   ".Hu.blackQ: fekete",
   ".Hu.colorQ: sz�nes",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".Hu.black:    Fekete",
   ".Hu.cyan:     Ci�n",
   ".Hu.magenta:  B�bor",
   ".Hu.yellow:   S�rga",
   ".Hu.lcyan:    F�nyl� ci�n",
   ".Hu.lmagenta: F�nyl� b�bor",
   ".Hu.lblack:   F�nyl� fekete",

   ".Hu.printerState: nyomtat� �llapot",
   ".Hu.unknown:      ismeretlen",
   ".Hu.selfTest:     �nteszt",
   ".Hu.busy:         foglalt",
   ".Hu.printing:     nyomtat�s",
   ".Hu.cleaning:     tiszt�t�s",
   ".ok:           Ok",

   ".Hu.error:                Hiba",
   ".Hu.interfaceNotSelected: Interf�sz nincs kiv�lasztva",
   ".Hu.paperJamError:        Pap�r akad�s hiba",
   ".Hu.inkOutError:          Tintakifogy�s hiba",
   ".Hu.paperOutError:        Nincs pap�r hiba",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".Hu.alignWarning:"
   "Figyelem !\\nEz megrong�lhatja a nyomtat�t.",

   ".Hu.alignBlackOrColor:"
   "Fekete vagy sz�nes fej igaz�t�sa?",

   ".Hu*firstHeadAlign:"
   "K�rem helyezzen egy lapot a nyomtat�ba a\\n"
   "fejigaz�t�s m�velete el�tt.",

   ".Hu.nextHeadAlign:"
   "K�rem ism�t helyezzen egy lapot a nyomtat�ba\\n"
   "a fejigaz�t�s folytat�s�hoz.",

   ".Hu.lastHeadAlign:"
   "K�rem vizsg�lja meg az utols� mint�t nagyon figyelmesen �s gondoskodjon\\n"
   "nyomtat�ja helyes be�ll�t�s�r�l.\\n\\n"
   "Most lehet: Ment-eni az eredm�nyeket a nyomtat�ba, vagy\\n"
   "Visszavon-ni az eredm�nyek ment�se n�lk�l\\n(ment/visszavon) [visszavon] :",

   ".Hu.choosePattern:"
   "K�rem vizsg�lja meg a nyomtat�st, �s v�lassza a legjobb sorp�rt\\n"
   "az utolj�ra nyomtatott mint�kb�l.\\n"
   "Helyezzen lapot a nyomtat�ba.",

   ".Hu.chooseCPattern:"
   "Vizsg�lja meg az igaz�t�si lapot, �s hat�rozza meg,\\n"
   "melyik minta a legegyenletesebb.\\n"
   "Ez a minta fog a legkev�sb� szemcs�zettnek l�tszani.\\n"
   "Ha tud egyenletes mint�t tal�lni, k�rem\\n"
   "v�lassza a legjobb minta sz�m�t, �s\\n"
   "ism�telje meg a folyamatot.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".Hu.functionNA:"
   "Ez a funkci� nem el�rhet� a nyomtat�j�hoz.",

   ".Hu.askDoExchange:"
   "Val�ban tintapatront akar cser�lni (Igen/Nem) [Nem] ? :",

   ".Hu.adviseMoveCartridge:"
   "A tintapatron most cserepoz�ci�ba fog mozogni.",

   ".Hu.adviseExchangeCartridge:"
   "K�rem cser�lje ki a tintapatront most.",

   ".Hu.askExDone:"
   "Csere rendben (igen/nem) [nem] ? :",

   ".Hu.adviseFillCartridge:"
   "A tintafelt�lt�s most indul.",

   ".Hu.adviseExchangeDone:"
   "Tintapatron csere rendben.",

   ".Hu.communicationError:"
   "�r�si vagy olvas�si hiba a nyomtat�b�l/nyomtat�ba !",

   ".Hu.exchangeError:"
   "A nyomtat� egy hib�t ad vissza, amikor a tintapatron cser�je k�telez� !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Magyar forrás fájl a ttink-hez.
   ! Ez a fordítónak szól.
   !
   ! Ez a fájl normál X-forrás fájlként van felépítve
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             ez egy megjegyzés karakter ha egy sor elején található.
   !
   !
   ! .syntax1:     ez egy kulcsszó.
   ! .De.syntax:   kulcs a szöveg helyének meghatározásához.
   !               mindkét karaktere - ebben az esetben a De - a LANG, LC_ALL
   !               környezeti változóból kapható. Az elsõ betûje
   !               nagybetû, a második kisbetûs marad.
   !               Ha fordítja ezt a fájlt, kérem helyettesítse az .En.-t a
   !               helyes értékre (jelen esetben .Hu.).
   !
   ! \             egy sor végénél (space és tabulátor nincs
   !               engedélyezve) mutatja, hogy az aktuális sor befejezõdött.
   !
   ! \     text    szöveg azonosításához. Ez csak a kulcszó után
   !               követelmény. Ha csak a \ karakter van jelen, a szóköz
   !               el van távolítva.
   !
   ! \n            Ez egy sor elejére állítás. Az utána következõ szöveg
   !               kiíródik a következõ sorban.
   !
   !               Kérem figyeljen, hogy a szóközöket és a sorvégeket ne
   !               távolítsa el.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".Hu8.syntax1:"
   " Arguments for ",

   ".Hu8.syntax2:"
   "  Kötelezõ:\\n"
   "    -d eszköz_fájl --device eszköz_fájl\\n"
   "  Tetszõleges:\\n"
   "    -m name        --modell neve vagy száma\\n"
   "        enélkül a nyomtató automatikusan detektálódik\\n"
   "  Parancsok (csak egy parancsot lehet kiadni):\\n"
   "    -r --megszakítás\\n"
   "    -c --törlés\\n"
   "    -n --fúvóka ellenõrzés\\n"
   "    -s --állapot           (alapeset)\\n"
   "    -a --fejigazítás\\n"
   "    -e --tinta csere     (nem minden nyomtató)\\n"
   "    -i --azonosítás         nyomtató azonosítás kiírása\\n"
   "  Info:\\n"
   "    -v --verzió          nyomtatás verzió\\n"
   "    -l --nyomtató lista     ismert nyomtatók listája\\n",

   ".Hu8.syntaxM:"
   "  Parancsok (csak egy parancsot lehet kiadni):\\n"
   "    -r --megszakítás\\n"
   "    -c --törlés\\n"
   "    -n --fúvóka ellenõrzés\\n"
   "    -s --állapot           (alapeset)\\n"
   "    -a --fejigazítás\\n"
   "    -e --tinta csere     (nem minden nyomtató)\\n"
   "    -i --azonosítás         nyomtató azonosítás kiírása\\n"
   "  Info:\\n"
   "    -v --verzió          nyomtatás verzió\\n"
   "    -l --nyomtató lista     ismert nyomtatók listája\\n",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".Hu8.noAccess:"
   "Nincs hozzáférés az eszközfájlhoz, vagy nincs csatlakoztatott nyomtató.",

   ".Hu8.noDetected:"
   "Nem tudom detektálni a nyomtató modellt.",

   ".Hu8.unknownModel:"
   "Sajnálom, ismeretlen modell.",

   ".Hu8.noOPen:"
   "Sajnálom, nem tudom megnyitni az eszközfájlt.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".Hu8.continue:"
   "Folytatás (igen/nem) [nem] ? :",

   ".Hu8.yesorno: in",
   ".Hu8.saveCancel: mv",

   ".Hu8.blackQ: fekete",
   ".Hu8.colorQ: színes",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".Hu8.black:    Fekete",
   ".Hu8.cyan:     Cián",
   ".Hu8.magenta:  Bíbor",
   ".Hu8.yellow:   Sárga",
   ".Hu8.lcyan:    Fénylõ cián",
   ".Hu8.lmagenta: Fénylõ bíbor",
   ".Hu8.lblack:   Fénylõ fekete",

   ".Hu8.printerState: nyomtató állapot",
   ".Hu8.unknown:      ismeretlen",
   ".Hu8.selfTest:     önteszt",
   ".Hu8.busy:         foglalt",
   ".Hu8.printing:     nyomtatás",
   ".Hu8.cleaning:     tisztítás",
   ".ok:           Ok",

   ".Hu8.error:                Hiba",
   ".Hu8.interfaceNotSelected: Interfész nincs kiválasztva",
   ".Hu8.paperJamError:        Papír akadás hiba",
   ".Hu8.inkOutError:          Tintakifogyás hiba",
   ".Hu8.paperOutError:        Nincs papír hiba",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".Hu8.alignWarning:"
   "Figyelem !\\nEz megrongálhatja a nyomtatót.",

   ".Hu8.alignBlackOrColor:"
   "Fekete vagy színes fej igazítása?",

   ".Hu*firstHeadAlign:"
   "Kérem helyezzen egy lapot a nyomtatóba a\\n"
   "fejigazítás mûvelete elõtt.",

   ".Hu8.nextHeadAlign:"
   "Kérem ismét helyezzen egy lapot a nyomtatóba\\n"
   "a fejigazítás folytatásához.",

   ".Hu8.lastHeadAlign:"
   "Kérem vizsgálja meg az utolsó mintát nagyon figyelmesen és gondoskodjon\\n"
   "nyomtatója helyes beállításáról.\\n\\n"
   "Most lehet: Ment-eni az eredményeket a nyomtatóba, vagy\\n"
   "Visszavon-ni az eredmények mentése nélkül\\n(ment/visszavon) [visszavon] :",

   ".Hu8.choosePattern:"
   "Kérem vizsgálja meg a nyomtatást, és válassza a legjobb sorpárt\\n"
   "az utoljára nyomtatott mintákból.\\n"
   "Helyezzen lapot a nyomtatóba.",

   ".Hu8.chooseCPattern:"
   "Vizsgálja meg az igazítási lapot, és határozza meg,\\n"
   "melyik minta a legegyenletesebb.\\n"
   "Ez a minta fog a legkevésbé szemcsézettnek látszani.\\n"
   "Ha tud egyenletes mintát találni, kérem\\n"
   "válassza a legjobb minta számát, és\\n"
   "ismételje meg a folyamatot.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".Hu8.functionNA:"
   "Ez a funkció nem elérhetõ a nyomtatójához.",

   ".Hu8.askDoExchange:"
   "Valóban tintapatront akar cserélni (Igen/Nem) [Nem] ? :",

   ".Hu8.adviseMoveCartridge:"
   "A tintapatron most cserepozícióba fog mozogni.",

   ".Hu8.adviseExchangeCartridge:"
   "Kérem cserélje ki a tintapatront most.",

   ".Hu8.askExDone:"
   "Csere rendben (igen/nem) [nem] ? :",

   ".Hu8.adviseFillCartridge:"
   "A tintafeltöltés most indul.",

   ".Hu8.adviseExchangeDone:"
   "Tintapatron csere rendben.",

   ".Hu8.communicationError:"
   "Írási vagy olvasási hiba a nyomtatóból/nyomtatóba !",

   ".Hu8.exchangeError:"
   "A nyomtató egy hibát ad vissza, amikor a tintapatron cseréje kötelezõ !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! File Italiano per ttink.
   ! Qanto segue � per i traduttori.
   !
   ! Questo file � prodotto come un semplice file X-resource
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             � un carattere di commmento se appare all'inizio di una
   !               linea.
   !
   ! .syntax1:     � una parola chiave.
   ! .It.syntax:   � la chiave per un testo particolare.
   !               entrambi i caratteri (in questo caso It) sono presi dalle
   !               variabili d'ambiente LANG, LC_ALL. La prima lettera
   !               viene posta maiuscola e la seconda rimane normale.
   !               Qualora tu traduca questo file, sostituisci .En. con
   !               il valore corretto.
   !
   ! \             Alla fine di una linea (spazi e tabulatori non sono
   !               ammessi) indica che la linea continua nella successiva.
   !
   ! \     text    permette di indentare text. E' richiesto solo dopo 
   !               la parola chiave. Se neppure \ � presente, lo spazio viene
   !               rimosso.
   !
   ! \n            Segno di "a capo". Il seguito verr� scritto sulla
   !               linea successiva.
   !
   !               Nota che gli spazi alla fine di una linea non vengono
   !               rimossi.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".It.syntax1:"
   " Argomenti per ",

   ".It.syntax2:"
   "  Obbligatorio:\\n"
   "    -d device_file --device device_file\\n"
   "  Opzionali:\\n"
   "    -m name        --model nome o numero\\n"
   "        senza questo la stampante viene rilevata sutomaticamente\\n"
   "  Ordine (solo un ordine pu� essere impartito):\\n"
   "    -r --reset\\n"
   "    -c --clean\\n"
   "    -n --nozzle-check\\n"
   "    -s --status           (default)\\n"
   "    -a --align-head\\n"
   "    -e --exchange-ink     (non per tutte le stampanti)\\n"
   "    -i --identity         stampa il tipo di stampante\\n\\ ",
   "Informazioni:\\n"
   "    -v --version          stampa la versione\\n"
   "    -l --list-printer     elenca le stampanti presenti\\n",

   ".It.syntaxM:"
   "  Ordine (solo un ordine pu� essere impartito):\\n"
   "    -r --reset\\n"
   "    -c --clean\\n"
   "    -n --nozzle-check\\n"
   "    -s --status           (default)\\n"
   "    -a --align-head\\n"
   "    -e --exchange-ink     (non per tutte le stampanti)\\n"
   "    -i --identity         stampa il tipo di stampante\\n\\ ",
   "Info:\\n"
   "    -v --version          stampa la versione\\n"
   "    -l --list-printer     elenca le stampanti presenti\\n",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Messaggi d'errore e di avvertimento

*/
#endif
   ".It.noAccess:"
   "Accesso al file dispositivo impossibile o stampante non connessa.",

   ".It.noDetected:"
   "Modello di stampante non rilevabile.",

   ".It.unknownModel:"
   "Spiacente, modello sconosciuto.",

   ".It.noOPen:"
   "Spiacente, nessun accesso al file dispositivo.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Testi per richieste ed altro

*/
#endif
   ".It.continue:"
   "Continua (s�/no) [no] ? : ",

   ".It.yesorno: sn",
   ".It.saveCancel: se",

   ".It.blackQ: nero",
   ".It.colorQ: colore",

   ".It.Hcontinue:"
   "@QContinua ?@Bs�@Bno ",

   ".It.Wyesorno:"
   " Save=YES,Save=NO",

   ".It.WsaveCancel: se",

   ".It.followingPrintersFound:"
   "Sono state rilevate le seguenti stampanti:",

   ".It.ChoosePrinter:"
   "Scegli la stampante ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Stato

*/
#endif
   ".It.black:    Nero",
   ".It.cyan:     Ciano",
   ".It.magenta:  Magenta",
   ".It.yellow:   Giallo",
   ".It.lcyan:    Ciano chiaro",
   ".It.lmagenta: Magenta chiaro",
   ".It.lblack:   Grigio",
   ".It.lblack:   Foto / nero matte ",
   ".It.blue:     Blu",
   ".It.red:      Rosso",
   ".It.dyellow:  Giallo scuro",
   ".It.gloss:    Ottimizzatore lucentezza",
   ".It.grey:     Grigio",

   ".It.printerState: stato stampante",
   ".It.unknown:      sconosciuto",
   ".It.selfTest:     auto test",
   ".It.busy:         occupata",
   ".It.printing:     stampa in corso",
   ".It.cleaning:     pulizia",
   ".It.ok:           Ok",

   ".It.error:                Errore",
   ".It.interfaceNotSelected: Interfaccia non selezionata",
   ".It.paperJamError:        Errore: carta inceppata",
   ".It.inkOutError:          Errore: inchiostro terminato",
   ".It.paperOutError:        Errore: carta terminata",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Allineamento testine

*/
#endif
   ".It.alignWarning:"
   "Attenzione !\\nQuesto pu� danneggiare la stampante.",

   ".It.alignBlackOrColor:"
   "Allineo la testina del nero o del colore ?",

   "*firstHeadAlign:"
   "Inserisci un foglio nella stampante per iniziare\\n"
   "la procedura di allineamento testina.",

   ".It.nextHeadAlign:"
   "Inserisci di nuovo il foglio nella stampante per\\n"
   "continuare la procedura di allineamento testina.",

   ".It.lastHeadAlign:"
   "Controlla attentamente il risultato finale per assicurarti che la tua\\n"
   "stampante sia allineata correttamente.\\n\\n"
   "Puoi ora: salvare i risultati nella stampante o\\n"
   "uscire senza salvare i risultati\\n(salva/esci) [esci] : ",

   ".It.choosePattern:"
   "Analizza la stampa, e scegli la miglior coppia di linee\\n"
   "dell'ultima stampa effettuata.\\n"
   "Inserisci di nuovo il foglio nella stampante.",

   ".It.sampleNo:"
   "campione #",

   ".It.choosePattern2:"
   "Analizza le linee stampate e scegli la miglior coppia\\n",

   ".It.chooseCPattern:"
   "Analizza l'allineamento del foglio, e trova quale\\n"
   "schema sia il pi� fine.\\n"
   "Questo schema � quello con grana pi� piccola.\\n"
   "Se non riesci a trovarne uno fine,\\n"
   "seleziona il numero di quello migliore, e\\n"
   "ripeti la procedura.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Cambio cartuccia

*/
#endif
   ".It.functionNA:"
   "Questa funzione non � disponibile per la stampante.",

   ".It.askDoExchange:"
   "Vuoi davvero sostituire la cartuccia (s�/no) [no] ? : ",

   ".It.exchangeBlackOrColor:"
   "Scegli la cartuccia:",

   ".It.adviseMoveCartridge:"
   "La cartuccia viene ora spostata nella posizione di sostituzione.",

   ".It.adviseExchangeCartridge:"
   "Sostituisci ora la cartuccia.",

   ".It.askExDone:"
   "Sostituzione effettuata (s�/no) [no] ? : ",

   ".It.adviseFillCartridge:"
   "Inizio riempimento inchiostro.",

   ".It.adviseExchangeDone:"
   "Sostituzione cartuccia effettuata.",

   ".It.communicationError:"
   "Errore durante la lettura o scrittura dalla/alla stampante !",

   ".It.exchangeError:"
   "La stampante ha dato errore durante la richiesta di sostituzione cartuccia !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! File Italiano per ttink.
   ! Qanto segue è per i traduttori.
   !
   ! Questo file è prodotto come un semplice file X-resource
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             è un carattere di commmento se appare all'inizio di una
   !               linea.
   !
   ! .syntax1:     è una parola chiave.
   ! .It.syntax:   è la chiave per un testo particolare.
   !               entrambi i caratteri (in questo caso It) sono presi dalle
   !               variabili d'ambiente LANG, LC_ALL. La prima lettera
   !               viene posta maiuscola e la seconda rimane normale.
   !               Qualora tu traduca questo file, sostituisci .En. con
   !               il valore corretto.
   !
   ! \             Alla fine di una linea (spazi e tabulatori non sono
   !               ammessi) indica che la linea continua nella successiva.
   !
   ! \     text    permette di indentare text. E' richiesto solo dopo 
   !               la parola chiave. Se neppure \ è presente, lo spazio viene
   !               rimosso.
   !
   ! \n            Segno di "a capo". Il seguito verrà  scritto sulla
   !               linea successiva.
   !
   !               Nota che gli spazi alla fine di una linea non vengono
   !               rimossi.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".It8.syntax1:"
   " Argomenti per ",

   ".It8.syntax2:"
   "  Obbligatorio:\\n"
   "    -d device_file --device device_file\\n"
   "  Opzionali:\\n"
   "    -m name        --model nome o numero\\n"
   "        senza questo la stampante viene rilevata sutomaticamente\\n"
   "  Ordine (solo un ordine può essere impartito):\\n"
   "    -r --reset\\n"
   "    -c --clean\\n"
   "    -n --nozzle-check\\n"
   "    -s --status           (default)\\n"
   "    -a --align-head\\n"
   "    -e --exchange-ink     (non per tutte le stampanti)\\n"
   "    -i --identity         stampa il tipo di stampante\\n\\ ",
   "Informazioni:\\n"
   "    -v --version          stampa la versione\\n"
   "    -l --list-printer     elenca le stampanti presenti\\n",

   ".It8.syntaxM:"
   "  Ordine (solo un ordine può essere impartito):\\n"
   "    -r --reset\\n"
   "    -c --clean\\n"
   "    -n --nozzle-check\\n"
   "    -s --status           (default)\\n"
   "    -a --align-head\\n"
   "    -e --exchange-ink     (non per tutte le stampanti)\\n"
   "    -i --identity         stampa il tipo di stampante\\n\\ ",
   "Info:\\n"
   "    -v --version          stampa la versione\\n"
   "    -l --list-printer     elenca le stampanti presenti\\n",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Messaggi d'errore e di avvertimento

*/
#endif
   ".It8.noAccess:"
   "Accesso al file dispositivo impossibile o stampante non connessa.",

   ".It8.noDetected:"
   "Modello di stampante non rilevabile.",

   ".It8.unknownModel:"
   "Spiacente, modello sconosciuto.",

   ".It8.noOPen:"
   "Spiacente, nessun accesso al file dispositivo.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Testi per richieste ed altro

*/
#endif
   ".It8.continue:"
   "Continua (sì/no) [no] ? : ",

   ".It8.yesorno: sn",
   ".It8.saveCancel: se",

   ".It8.blackQ: nero",
   ".It8.colorQ: colore",

   ".It8.Hcontinue:"
   "@QContinua ?@Bsì@Bno ",

   ".It8.Wyesorno:"
   " Save=YES,Save=NO",

   ".It8.WsaveCancel: se",

   ".It8.followingPrintersFound:"
   "Sono state rilevate le seguenti stampanti:",

   ".It8.ChoosePrinter:"
   "Scegli la stampante ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Stato

*/
#endif
   ".It8.black:    Nero",
   ".It8.cyan:     Ciano",
   ".It8.magenta:  Magenta",
   ".It8.yellow:   Giallo",
   ".It8.lcyan:    Ciano chiaro",
   ".It8.lmagenta: Magenta chiaro",
   ".It8.lblack:   Grigio",
   ".It8.lblack:   Foto / nero matte ",
   ".It8.blue:     Blu",
   ".It8.red:      Rosso",
   ".It8.dyellow:  Giallo scuro",
   ".It8.gloss:    Ottimizzatore lucentezza",
   ".It8.grey:     Grigio",

   ".It8.printerState: stato stampante",
   ".It8.unknown:      sconosciuto",
   ".It8.selfTest:     auto test",
   ".It8.busy:         occupata",
   ".It8.printing:     stampa in corso",
   ".It8.cleaning:     pulizia",
   ".It8.ok:           Ok",

   ".It8.error:                Errore",
   ".It8.interfaceNotSelected: Interfaccia non selezionata",
   ".It8.paperJamError:        Errore: carta inceppata",
   ".It8.inkOutError:          Errore: inchiostro terminato",
   ".It8.paperOutError:        Errore: carta terminata",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Allineamento testine

*/
#endif
   ".It8.alignWarning:"
   "Attenzione !\\nQuesto può danneggiare la stampante.",

   ".It8.alignBlackOrColor:"
   "Allineo la testina del nero o del colore ?",

   "*firstHeadAlign:"
   "Inserisci un foglio nella stampante per iniziare\\n"
   "la procedura di allineamento testina.",

   ".It8.nextHeadAlign:"
   "Inserisci di nuovo il foglio nella stampante per\\n"
   "continuare la procedura di allineamento testina.",

   ".It8.lastHeadAlign:"
   "Controlla attentamente il risultato finale per assicurarti che la tua\\n"
   "stampante sia allineata correttamente.\\n\\n"
   "Puoi ora: salvare i risultati nella stampante o\\n"
   "uscire senza salvare i risultati\\n(salva/esci) [esci] : ",

   ".It8.choosePattern:"
   "Analizza la stampa, e scegli la miglior coppia di linee\\n"
   "dell'ultima stampa effettuata.\\n"
   "Inserisci di nuovo il foglio nella stampante.",

   ".It8.sampleNo:"
   "campione #",

   ".It8.choosePattern2:"
   "Analizza le linee stampate e scegli la miglior coppia\\n",

   ".It8.chooseCPattern:"
   "Analizza l'allineamento del foglio, e trova quale\\n"
   "schema sia il più fine.\\n"
   "Questo schema è quello con grana più piccola.\\n"
   "Se non riesci a trovarne uno fine,\\n"
   "seleziona il numero di quello migliore, e\\n"
   "ripeti la procedura.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Cambio cartuccia

*/
#endif
   ".It8.functionNA:"
   "Questa funzione non è disponibile per la stampante.",

   ".It8.askDoExchange:"
   "Vuoi davvero sostituire la cartuccia (sì/no) [no] ? : ",

   ".It8.exchangeBlackOrColor:"
   "Scegli la cartuccia:",

   ".It8.adviseMoveCartridge:"
   "La cartuccia viene ora spostata nella posizione di sostituzione.",

   ".It8.adviseExchangeCartridge:"
   "Sostituisci ora la cartuccia.",

   ".It8.askExDone:"
   "Sostituzione effettuata (sì/no) [no] ? : ",

   ".It8.adviseFillCartridge:"
   "Inizio riempimento inchiostro.",

   ".It8.adviseExchangeDone:"
   "Sostituzione cartuccia effettuata.",

   ".It8.communicationError:"
   "Errore durante la lettura o scrittura dalla/alla stampante !",

   ".It8.exchangeError:"
   "La stampante ha dato errore durante la richiesta di sostituzione cartuccia !",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Svensk resursfil f�r ttink.
   ! Swedish recourse file for ttink.
   ! This is for translators.
   !
   ! This file is built like a normal X-resources file
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             is a commment character if it appears at the begin of a
   !               line.
   !
   ! .syntax1:     is ein key word.
   ! .En.syntax1:  is the key for a localized text.
   !               both characters (in this case En) are taken from the
   !               environment variablea LANG, LC_ALL. The first letter
   !               will be set to upper case and the second remain normal.
   !               If you translate this file, please replace .En. with
   !               the correct value.
   !
   ! \             At the  end of a line (following spaces and  tabulators are
   !               not allowed) show that the actual line will be continued.
   !
   ! \     text    allow to indent the text. This will only be required after
   !               the key word. If no \ is present, spaces are removed.
   !
   ! \n            This is a carriage return. Following text will be
   !               printed in the next line.
   !
   !               Please note that spaces at the end of a line will not
   !               be removed.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".Sv.syntax1:"
   " Argumenter f�r ",

   ".Sv.syntax2:"
   "  Obligatorisk:\\n"
   "    -d apparatfil --device apparatfil\\n"
   "  Optional:\\n"
   "    -m namn        --model namn eller nummer\\n"
   "        utan detta uppt�ckas skrivaren automatiskt\\n"
   "    -D  --D4              Anv�nd alltid D4-protokollet\\n"
   "  Uppdrag (bara ett uppdrag kan ges):\\n"
   "    -r --reset            S�tt tillbaka skrivaren\\n"
   "    -c --clean            Reng�r munstycken\\n"
   "    -n --nozzle-check     Testa munstycken\\n"
   "    -s --status           (standarduppdrag)\\n"
   "    -a --align-head       r�t upp huvudet\\n"
   "    -e --exchange-ink     utbyt patronen (inte alla skrivaren)\\n"
   "    -i --identity         skriv ut skrivarens identitet\\n"
   "  Information:\\n"
   "    -v --version          skriv ut versionsnummer\\n"
   "    -l --list-printer     skriv ut lista av k�nda skrivaren\\n"
   "  Andra:\\n"
   "    -L                    debug ouput f�r D4-protokollet p� stderr\\n"
   "    -u                    utskrift med UTF-8-kod\\n",

   ".Sv.syntaxM:"
   "  Optional:\\n"
   "    -D  --D4              Anv�nd alltid D4-protokollet\\n"
   "  Uppdrag (bara ett uppdrag kan ges):\\n"
   "    -r --reset            S�tt tillbaka skrivaren\\n"
   "    -c --clean            Reng�r munstycken\\n"
   "    -n --nozzle-check     Testa munstycken\\n"
   "    -s --status           (standarduppdrag)\\n"
   "    -a --align-head       r�t upp huvudet\\n"
   "    -e --exchange-ink     utbyt patronen (inte alla skrivaren)\\n"
   "    -i --identity         skriv ut skrivarens identitet\\n"
   "  Information:\\n"
   "    -v --version          skriv ut versionsnummer\\n"
   "    -l --list-printer     skriv ut lista av k�nda skrivaren\\n"
   "  Andra:\\n"
   "    -L                    debug ouput f�r D4-protokollet p� stderr\\n"
   "    -u                    utskrift med UTF-8-kod\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".Sv.noAccess:"
   "Ingen tillg�ng till apparatfilen eller ingen skrivare �r ansluten.",

   ".Sv.noDetected:"
   "Kan inte best�mma skrivarens modell.",

   ".Sv.unknownModel:"
   "Ok�nd skrivare",

   ".Sv.noOPen:"
   "Kan inte �ppna apparatfilen.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".Sv.continue:"
   "Forts�tta (ja/nej) [nej] ? : ",

   ".Sv.yesorno: jn",
   ".Sv.saveCancel: sa",

   ".Sv.blackQ: svart",
   ".Sv.colorQ: f�rg",

   ".Sv.followingPrintersFound:"
   "Hittade f�ljande skrivare:",

   ".Sv.ChoosePrinter:"
   "V�lj skrivaren ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".Sv.black:    Svart",
   ".Sv.cyan:     Cyan",
   ".Sv.magenta:  Magenta",
   ".Sv.yellow:   Gul",
   ".Sv.lcyan:    Ljuscyan",
   ".Sv.lmagenta: Ljusmagenta",
   ".Sv.lblack:   Foto/matt svart",
   ".Sv.blue:     Bl�",
   ".Sv.red:      R�d",
   ".Sv.dyellow:  M�rkgul",
   ".Sv.gloss:    Glansoptimerare",

   ".Sv.printerState: skrivarens status",
   ".Sv.unknown:      ok�nd",
   ".Sv.selfTest:     sj�lvtest",
   ".Sv.busy:         upptagen",
   ".Sv.printing:     skriver ut",
   ".Sv.cleaning:     reningen p�g�r",
   ".Sv.ok:           Ok",

   ".Sv.error:                Fel",
   ".Sv.interfaceNotSelected: Gr�nssnittet �r inte valt",
   ".Sv.paperJamError:        Pappersstockningsfel",
   ".Sv.inkOutError:          Bl�cket �r slut",
   ".Sv.paperOutError:        Pappret �r slut",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".Sv.alignWarning:"
   "Varning !\\nDetta kunde f�rst�ra skrivaren.",

   ".Sv.alignBlackOrColor:"
   "R�ta upp det svarta eller det f�rgade huvudet?",

   ".Sv*firstHeadAlign:"
   "V�nligen s�tt in en pappersark i din skrivare.",

   ".Sv.nextHeadAlign:"
   "V�nligen s�tt in igen arken i skrivaren f�r att fors�tta processen.",

   ".Sv.lastHeadAlign:"
   "V�nligen granska den sista utskriften mycket noggrant f�r\\n"
   "att tillf�rs�kra dig att skrivaren �r inst�lld bra.\\n\\n"
   "Du kan nu: spara resultatet i skrivaren eller\\n"
   "avbryta utan att spara resultatet\\n(spara/avbryta) [avbryta] : ",

   ".Sv.choosePattern:"
   "V�nligen granska utskriften och v�lj paret med\\n"
   "dom rakaste linjer fr�n det sista m�nstret.\\n"
   "S�tt arken i skrivaren igen.",

   ".Sv.chooseCPattern:"
   "Granska arken med m�nstren och fastst�ll\\n"
   "m�nstret som �r j�mnast.\\n"
   "Om du inte kan hitta ett j�mnt m�nster\\n"
   "v�lj nummret av det b�sta m�nstret,\\n"
   "och repetera processen.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".Sv.functionNA:"
   "Denna funktion �r inte tillg�nglig f�r din skrivare.",

   ".Sv.askDoExchange:"
   "Vill du utbyta en patron (ja/nej) [nej] ? : ",

   ".Sv.exchangeBlackOrColor: V�lj patronen:",

   ".Sv.adviseMoveCartridge:"
   "Skrivhuvudet flyttas nu till utbytespositionen.",

   ".Sv.adviseExchangeCartridge:"
   "Du kan nu byta patronen.",

   ".Sv.askExDone:"
   "Utbyte klart (ja/nej) [nej] ? : ",

   ".Sv.adviseFillCartridge:"
   "P�fyllning av bl�cket startas nu.",

   ".Sv.adviseExchangeDone:"
   "Utbyte av patronen �r klar.",

   ".Sv.communicationError:"
   "L�s-/skrivfel p� skrivaren!",

   ".Sv.exchangeError:"
   "Skrivaren svarar med fel vid beg�ran av patronbyte!",
#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Svensk resursfil för ttink.
   ! Swedish recourse file for ttink.
   ! This is for translators.
   !
   ! This file is built like a normal X-resources file
   !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   ! !             is a commment character if it appears at the begin of a
   !               line.
   !
   ! .syntax1:     is ein key word.
   ! .En.syntax1:  is the key for a localized text.
   !               both characters (in this case En) are taken from the
   !               environment variablea LANG, LC_ALL. The first letter
   !               will be set to upper case and the second remain normal.
   !               If you translate this file, please replace .En. with
   !               the correct value.
   !
   ! \             At the  end of a line (following spaces and  tabulators are
   !               not allowed) show that the actual line will be continued.
   !
   ! \     text    allow to indent the text. This will only be required after
   !               the key word. If no \ is present, spaces are removed.
   !
   ! \n            This is a carriage return. Following text will be
   !               printed in the next line.
   !
   !               Please note that spaces at the end of a line will not
   !               be removed.

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

*/
#endif
   ".Sv8.syntax1:"
   " Argumenter för ",

   ".Sv8.syntax2:"
   "  Obligatorisk:\\n"
   "    -d apparatfil --device apparatfil\\n"
   "  Optional:\\n"
   "    -m namn        --model namn eller nummer\\n"
   "        utan detta upptäckas skrivaren automatiskt\\n"
   "    -D  --D4              Använd alltid D4-protokollet\\n"
   "  Uppdrag (bara ett uppdrag kan ges):\\n"
   "    -r --reset            Sätt tillbaka skrivaren\\n"
   "    -c --clean            Rengör munstycken\\n"
   "    -n --nozzle-check     Testa munstycken\\n"
   "    -s --status           (standarduppdrag)\\n"
   "    -a --align-head       rät upp huvudet\\n"
   "    -e --exchange-ink     utbyt patronen (inte alla skrivaren)\\n"
   "    -i --identity         skriv ut skrivarens identitet\\n"
   "  Information:\\n"
   "    -v --version          skriv ut versionsnummer\\n"
   "    -l --list-printer     skriv ut lista av kända skrivaren\\n"
   "  Andra:\\n"
   "    -L                    debug ouput för D4-protokollet på stderr\\n"
   "    -u                    utskrift med UTF-8-kod\\n",

   ".Sv8.syntaxM:"
   "  Optional:\\n"
   "    -D  --D4              Använd alltid D4-protokollet\\n"
   "  Uppdrag (bara ett uppdrag kan ges):\\n"
   "    -r --reset            Sätt tillbaka skrivaren\\n"
   "    -c --clean            Rengör munstycken\\n"
   "    -n --nozzle-check     Testa munstycken\\n"
   "    -s --status           (standarduppdrag)\\n"
   "    -a --align-head       rät upp huvudet\\n"
   "    -e --exchange-ink     utbyt patronen (inte alla skrivaren)\\n"
   "    -i --identity         skriv ut skrivarens identitet\\n"
   "  Information:\\n"
   "    -v --version          skriv ut versionsnummer\\n"
   "    -l --list-printer     skriv ut lista av kända skrivaren\\n"
   "  Andra:\\n"
   "    -L                    debug ouput för D4-protokollet på stderr\\n"
   "    -u                    utskrift med UTF-8-kod\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Error and warning messages

*/
#endif
   ".Sv8.noAccess:"
   "Ingen tillgång till apparatfilen eller ingen skrivare är ansluten.",

   ".Sv8.noDetected:"
   "Kan inte bestämma skrivarens modell.",

   ".Sv8.unknownModel:"
   "Okänd skrivare",

   ".Sv8.noOPen:"
   "Kan inte öppna apparatfilen.",


#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Strings for queries and so on

*/
#endif
   ".Sv8.continue:"
   "Fortsätta (ja/nej) [nej] ? : ",

   ".Sv8.yesorno: jn",
   ".Sv8.saveCancel: sa",

   ".Sv8.blackQ: svart",
   ".Sv8.colorQ: färg",

   ".Sv8.followingPrintersFound:"
   "Hittade följande skrivare:",

   ".Sv8.ChoosePrinter:"
   "Välj skrivaren ",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Status

*/
#endif
   ".Sv8.black:    Svart",
   ".Sv8.cyan:     Cyan",
   ".Sv8.magenta:  Magenta",
   ".Sv8.yellow:   Gul",
   ".Sv8.lcyan:    Ljuscyan",
   ".Sv8.lmagenta: Ljusmagenta",
   ".Sv8.lblack:   Foto/matt svart",
   ".Sv8.blue:     Blå",
   ".Sv8.red:      Röd",
   ".Sv8.dyellow:  Mörkgul",
   ".Sv8.gloss:    Glansoptimerare",

   ".Sv8.printerState: skrivarens status",
   ".Sv8.unknown:      okänd",
   ".Sv8.selfTest:     självtest",
   ".Sv8.busy:         upptagen",
   ".Sv8.printing:     skriver ut",
   ".Sv8.cleaning:     reningen pågår",
   ".Sv8.ok:           Ok",

   ".Sv8.error:                Fel",
   ".Sv8.interfaceNotSelected: Gränssnittet är inte valt",
   ".Sv8.paperJamError:        Pappersstockningsfel",
   ".Sv8.inkOutError:          Bläcket är slut",
   ".Sv8.paperOutError:        Pappret är slut",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Head aligment

*/
#endif
   ".Sv8.alignWarning:"
   "Varning !\\nDetta kunde förstöra skrivaren.",

   ".Sv8.alignBlackOrColor:"
   "Räta upp det svarta eller det färgade huvudet?",

   ".Sv*firstHeadAlign:"
   "Vänligen sätt in en pappersark i din skrivare.",

   ".Sv8.nextHeadAlign:"
   "Vänligen sätt in igen arken i skrivaren för att forsätta processen.",

   ".Sv8.lastHeadAlign:"
   "Vänligen granska den sista utskriften mycket noggrant för\\n"
   "att tillförsäkra dig att skrivaren är inställd bra.\\n\\n"
   "Du kan nu: spara resultatet i skrivaren eller\\n"
   "avbryta utan att spara resultatet\\n(spara/avbryta) [avbryta] : ",

   ".Sv8.choosePattern:"
   "Vänligen granska utskriften och välj paret med\\n"
   "dom rakaste linjer från det sista mönstret.\\n"
   "Sätt arken i skrivaren igen.",

   ".Sv8.chooseCPattern:"
   "Granska arken med mönstren och fastställ\\n"
   "mönstret som är jämnast.\\n"
   "Om du inte kan hitta ett jämnt mönster\\n"
   "välj nummret av det bästa mönstret,\\n"
   "och repetera processen.\\n",

#if 0
/*
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exchange of cartridge

*/
#endif
   ".Sv8.functionNA:"
   "Denna funktion är inte tillgänglig för din skrivare.",

   ".Sv8.askDoExchange:"
   "Vill du utbyta en patron (ja/nej) [nej] ? : ",

   ".Sv8.exchangeBlackOrColor: Välj patronen:",

   ".Sv8.adviseMoveCartridge:"
   "Skrivhuvudet flyttas nu till utbytespositionen.",

   ".Sv8.adviseExchangeCartridge:"
   "Du kan nu byta patronen.",

   ".Sv8.askExDone:"
   "Utbyte klart (ja/nej) [nej] ? : ",

   ".Sv8.adviseFillCartridge:"
   "Påfyllning av bläcket startas nu.",

   ".Sv8.adviseExchangeDone:"
   "Utbyte av patronen är klar.",

   ".Sv8.communicationError:"
   "Läs-/skrivfel på skrivaren!",

   ".Sv8.exchangeError:"
   "Skrivaren svarar med fel vid begäran av patronbyte!",

   (char *)0
};
