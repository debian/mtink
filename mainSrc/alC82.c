/****************************************************
 * File alC82.c
 *
 * write and read to / from printer
 *
 *
 * Copyright (C) 2001 Jean-Jacques Sarton jj.sarton@t-online.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 ****************************************************/


/* commands for head alignment pattern Stylus Photo C82, derived from 820 code */

/* pattern for ESC i command :1 dot set , 63 unset */
#ifndef p1
#define P1 0x00, 0xc0, 0xf2, 0x00,
#endif
                              
#define P1_60  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  \
               P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  \
               P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  \
               P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  \
               P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1  P1
                              
#ifdef PATTERN
#undef PATTERN
#endif
#define PATTERN P1_60

static uc topPosC82[] = {
    0x1b, '(',  'v', 0x02, 0x00, 0xa0, 0x00                   /* vert. pos. abs. 160 * 1/60 inch */
};

static uc remoteInitC82[] =
{
    0x1b, '(',  'R', 0x08, 0x00, 0x00, 'R', 'E', 'M', 'O', 'T', 'E', '1',
    'L', 'C', 0x02, 0x00, 0x00, 0x01,
    'S', 'N', 0x03, 0x00, 0x00, 0x00, 0x00,
    0x1b, 0x00, 0x00, 0x00,                                    /* end remote 1 */
};

static uc remoteEndC82[] =
{

    0x1b, '(', 'R',   0x08, 0x00, 0x00, 'R', 'E', 'M', 'O', 'T', 'E', '1',
    'L', 'C', 0x02,   0x00, 0x00, 0x00,
    0x1b, 0x00, 0x00, 0x00,
    0x1b, '@',
    0x1b, '@',
    0x1b, '(', 'R',   0x08, 0x00, 0x00, 'R', 'E', 'M', 'O', 'T', 'E', '1',
    'J',  'E',  0x01, 0x00, 0x00, 
    0x1b, 0x00, 0x00, 0x00
};

#define PASS_N0_IDX 7

#define SIZE_N0_IDX 37

static uc passDataC82[] =
{
    0x1b, 'U', 0x01, 
    0x1b, 'l', 0x19, '#', '1', 0x0d, 0x0a,                     /* left margin on column 25, print "#1" and go to next line */
    0x1b, '@',                                                 /* reset */
    0x1b, '(',  'G', 0x01, 0x00, 0x01,                         /* select graphic mode on*/
    0x1b, '+', 0x00,                                           /* set line spacing = 0 !!! */
    0x1b, '(', 'U',  0x05, 0x00, 0x04, 0x04, 0x01, 0xa0, 0x05, /* set unit extended p=4, v=4, h=1 1/1440 dpi */
    0x1b, '(', 'e',  0x02, 0x00, 0x00, 0x12,                   /* select dot size 0x12 */
    0x1b, '(', 'K',  0x02, 0x00, 0x00, 0x01,                   /* select monochrome print mode */
    0x1b, 'U', 0x01,                                           /* set print mode char: auto, graphic: unidirectional */
    0x1b, '(', 'D',  0x04, 0x00, 0xa0, 0x05, 0x08, 0x04,       /* set res. for raster image v= 8/1440=1/180, h=4/1440=1/360 */
    0x1b, '(', '$',  0x04, 0x00, 0x80, 0x0f, 0x00, 0x00,       /* And Hor. pos: 3968 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0xf0, 0x00, 0x3C, 0x00,       PATTERN PATTERN PATTERN PATTERN PATTERN  /* 240 bytes / line, 60 lines */
                                                               PATTERN PATTERN PATTERN PATTERN PATTERN
                                                               PATTERN PATTERN PATTERN PATTERN PATTERN
    0x0d, 
    0x1b, '(', 'v', 0x02, 0x00, 0x31, 0x00,                    /* set vert. rel. position 49 units = 0,136 inch = 0,345 cm */
    0x1b, 0x06,                                                /* !!!!! */ 
    0x1b, 'U', 0x03,                                           /* set print mode char:?, graphic: ? */ 
    0x1b, '(', '$',  0x04, 0x00, 0x87, 0x1d, 0x00, 0x00,       /* And Hor. pos: 7559 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN                                       /* 16 bytes / line, 60 lines */
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x86, 0x1c, 0x00, 0x00,       /* And Hor. pos: 7302  (-257) */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x85, 0x1b, 0x00, 0x00,       /* And Hor. pos: 7045 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x84, 0x1a, 0x00, 0x00,       /* And Hor. pos: 6788 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x83, 0x19, 0x00, 0x00,       /* And Hor. pos: 6531 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x82, 0x18, 0x00, 0x00,       /* And Hor. pos: 6274 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x81, 0x17, 0x00, 0x00,       /* And Hor. pos: 6017 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x80, 0x16, 0x00, 0x00,       /* And Hor. pos: 5760 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x7f, 0x15, 0x00, 0x00,       /* And Hor. pos: 5503 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x7e, 0x14, 0x00, 0x00,       /* And Hor. pos: 5246 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x7d, 0x13, 0x00, 0x00,       /* And Hor. pos: 4989 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x7c, 0x12, 0x00, 0x00,       /* And Hor. pos: 4732 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x7b, 0x11, 0x00, 0x00,       /* And Hor. pos: 4475 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x7a, 0x10, 0x00, 0x00,       /* And Hor. pos: 4218 */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', '$',  0x04, 0x00, 0x79, 0x0f, 0x00, 0x00,       /* And Hor. pos: 3961  Delta = 257/1440 dpi  (0.45332) */
    0x1b, 'i', 0x00, 0x01, 0x02, 0x10, 0x00, 0x3c, 0x00, PATTERN
    0x0d,
    0x1b, '(', 'v', 0x02, 0x00, 0x90, 0x00,
    0x1b, '@',
    0x1b, 'U', 0x01,
    0x1b, '$', 0xa0, 0x00, '1',   /* 160 */
    0x1b, '$', 0xab, 0x00, '2',   /* 171  (+11/60 inch = +0.46567 inch) */
    0x1b, '$', 0xb6, 0x00, '3',
    0x1b, '$', 0xc1, 0x00, '4',
    0x1b, '$', 0xcc, 0x00, '5',
    0x1b, '$', 0xd7, 0x00, '6',
    0x1b, '$', 0xe2, 0x00, '7',
    0x1b, '$', 0xed, 0x00, '8',
    0x1b, '$', 0xf8, 0x00, '9',
    0x1b, '$', 0x03, 0x01, '1',
    0x1b, '$', 0x0e, 0x01, '1',
    0x1b, '$', 0x19, 0x01, '1',
    0x1b, '$', 0x24, 0x01, '1',
    0x1b, '$', 0x2f, 0x01, '1',
    0x1b, '$', 0x3a, 0x01, '1',
    0x0d, 0x0a,
    0x1b, '$', 0x03, 0x01, '0',
    0x1b, '$', 0x0e, 0x01, '1',
    0x1b, '$', 0x19, 0x01, '2',
    0x1b, '$', 0x24, 0x01, '3',
    0x1b, '$', 0x2f, 0x01, '4',
    0x1b, '$', 0x3a, 0x01, '5',
    0x0d, 0x0a, 0x0d, 0x0a
};


/******************************************************
 * Function: doAlignPatternC82()
 *
 * build the align command for the printer according to the
 * given parameters.
 *
 * input: int pass  pass number 1,2,3 or 0 if conmtrol pass
 *
 * return: size of data
 *
 ******************************************************/

int doAlignPatternC82(int pass, int bufpos, unsigned char *printer_cmd)
{
   int i;

   if ( pass == 1 )
   {
      memcpy(printer_cmd + bufpos, remoteInitC82, sizeof(remoteInitC82));
      bufpos += sizeof(remoteInitC82);
   }
 
   if ( pass > 0 )
   {
      memcpy(printer_cmd + bufpos, topPosC82, sizeof(topPosC82));
      bufpos += sizeof(topPosC82);

      bufpos = add_newlines((pass - 1) * 7, bufpos, printer_cmd );

      passDataC82[PASS_N0_IDX] = '0' + pass;
      passDataC82[SIZE_N0_IDX] = 0x13 - pass;
      memcpy(printer_cmd + bufpos, passDataC82, sizeof(passDataC82));
      bufpos += sizeof(passDataC82);
   
      bufpos = add_formfeed(bufpos, printer_cmd);

      if ( pass == 3 )
      {
         memcpy(printer_cmd + bufpos, remoteEndC82, sizeof(remoteEndC82));
         bufpos += sizeof(remoteEndC82);
      }

   }
   else /* pass == 0, final print */
   {
      memcpy(printer_cmd + bufpos, remoteInitC82, sizeof(remoteInitC82));
      bufpos += sizeof(remoteInitC82);
      memcpy(printer_cmd + bufpos, topPosC82, sizeof(topPosC82));
      bufpos += sizeof(topPosC82);
      bufpos = add_newlines(21, bufpos, printer_cmd);
      for ( i = 1; i < 4; i++ )
      {
         passDataC82[PASS_N0_IDX] = '0' + i;
         passDataC82[SIZE_N0_IDX] = 0x13 - i;
         memcpy(printer_cmd + bufpos, passDataC82, sizeof(passDataC82));
         bufpos += sizeof(passDataC82);
      }
      bufpos = add_formfeed(bufpos, printer_cmd);
      memcpy(printer_cmd + bufpos, remoteEndC82, sizeof(remoteEndC82));
      bufpos += sizeof(remoteEndC82);
   }
   return bufpos;
}

