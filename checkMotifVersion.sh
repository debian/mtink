#!/bin/sh
# try to check for required library and check if header file comply
# set a few variable so we can build the Makefile

# Changes:
#  01-JAN-2004 Added the /usr/lib path to the line
#            for l in `cat /etc/ld.so.conf`
#  according to code proposed by Raul Morales
#

# initialize a few variables

MotifLib=
found=0
XmHFound=0
XmLibFound=0

if uname | grep Linux > /dev/null 2>&1
then
   # declare the possible pathes for include files
   InclPathes='/usr/X11R6/include
/usr/X11/include
/usr/local/X11R6/include
/usr/local/X11/include
/usr/openwin
/usr/X11R6/LessTif/Motif1.2/include
/usr/include
/usr/local/include'

   # declare standard path which will not be into ld.so.conf
   LibPathes='/usr/lib'

   # for each library path found in the file /etc/ld.so.conf
   if grep include /etc/ld.so.conf >/dev/null
   then
      nb=`ls /etc/ld.so.conf.d/*.conf 2>/dev/null | wc -l`
      if [ $nb -eq 0 ]
      then
         CMD='cat /etc/ld.so.conf'
      else
         CMD='cat /etc/ld.so.conf.d/*.conf /etc/ld.so.conf'
      fi
   else
      CMD='cat /etc/ld.so.conf'
   fi

   for l in `$CMD | grep -v include` $LibPathes
   do
      # and for each of the possible location for the motif headers
      for XmHPath in $InclPathes
      do
         Xm_h=$XmHPath/Xm/Xm.h
         if [ -f $Xm_h ]
         then
            XmHFound=1
         else
            continue
         fi
         # for each motif library file
         for f in $l/libXm.so $l/libXm.so.[123]
         do
            if [ -f $f ]
            then
               XmLibFound=1
               Ver=`strings $f | grep ' Version'`
               if grep "$Ver" $Xm_h > /dev/null
               then
                  if [ `basename $f` = libXm.so ]
                  then
                     echo no special requirement for `echo $Ver | sed 's/^....//'`
                     found=1
                     break
                  else
                     echo must link $f for `echo $Ver | sed 's/^....//'`
                     MotifLib=$f
                     found=1
                     break
                  fi
               fi
            fi
         done
         if [ $found -eq 1 ]
         then
            break
         fi
      done
      if [ $found -eq 1 ]
      then
         break
      fi
   done >/dev/tty
   # final check
   if [ $found = 0 ]
   then
      # tell was is not OK
      if [ $XmLibFound -eq 0 ]
      then
         echo Motif / Lesstif library not found, please install the openmotif or lesstif package
      fi
      if [ $XmHFound -eq 0 ]
      then
         echo Motif / Lesstif devel not found, please install the openmotif or lesstif devel package
      fi
      # if the vesrion for devel and run time package differs
      if [ $XmLibFound -ne $XmHFound ]
      then
         echo runtime and devel package for openmotif / lesstif mismatch
         if [ "$XmHPath" != "" ]
         then
            echo Header file is for `sed -n 's/.*XmVERSION_STRING.*@(#)\(.*\)"/\1/p' $XmHPath/Xm/Xm.h`
         fi
         if [ "$Ver" != "" ]
         then
            echo Library file is `echo $Ver | sed 's/^....//'`
         fi
      fi
      exit 1
   else
     # set the variable building the Makefile
     if [ "$MotifLib" = "" ]
     then
        MotifLib=-lXm
     else
        #check again for xpm
        if [ -f $XmHPath/Xm/XmXpm.h ]
        then
            mxpmheader="-DMOTIF_XPM":
            mxpmlib=""
        else
            mxpmheader="-DXPM_XPM"
            mxpmlib="-lXpm"
        fi
     fi
     # be a little bit verbose
     echo Motif library set to $MotifLib
     echo Motif include Path is $XmHPath
     if [ "$mxpmlib" != "" ]
     then
        echo Xpm library is $mxpmlib
     fi
     echo
   fi > /dev/tty

else 
   if uname | grep Darwin >/dev/null
   then
      Pathes='/usr/OpenMotif
/sw
/usr/X11R6'
       for path in $Pathes
       do
          if [ -f $path/include/Xm/Xm.h ]
          then
             Xm_h=$path/include/Xm/Xm.h
             headVer=`sed -n 's/.*VERSION_STRING.*\(@(#).*\)"/\1/p' $Xm_h`
             if [ -f $path/lib/libXm.dylib ]
             then
                Xm_l=$path/lib/libXm.dylib
                libVer=`strings -a  $Xm_l | grep "$headVer"`
                if [ ! -z "$libVer" ]
                then
                   found=1
                   MotifLib="-L$path/lib -lXm"
                   XmHPath=$path/include
                   # be a little bit verbose
                   echo Motif library set to $MotifLib
                   echo Motif include Path is $XmHPath
                   break;
                fi
             fi
         fi
      done
   fi
   if [ $found -eq 0 ]
   then
      echo Motif / Lesstif devel not found, please install the openmotif or lesstif package
   fi > /dev/tty
fi

