#!/bin/sh
#set -x

if id | grep root > /dev/null
then
   :
else
   echo you must be root
   exit 1
fi

if [ `uname` != Linux ]
then
   echo sorry this script is only for Linux
   exit 1
fi

# check for wanted directories
RC_PATH=/etc
if [ -d /etc/init.d -a -d /etc/rc1.d ]
then
   : # this is OK
else
   # possibly a SuSE $#!@....
   if  [ -d /etc/init.d -a -d /etc/init.d/rc1.d ]
   then
      RC_PATH=/etc/init.d
   else
      echo LSB must be supported, Distribution too old !
      exit
   fi
fi

uninstall=0
if [ $# -eq 1 ]
then
   if [ $1 = "uninstall" ]
   then
      uninstall=1
   fi
fi

myDir=`dirname $0`
if [ "$myDir" = "" -o "$myDir" = . ]
then
   :
else
   cd $myDir
fi

PATH=$PATH:/usr/sbin:/usr/local/sbin:`pwd`../detect:

if [ $uninstall = 0 ]
then
   START_LEVEL=S14mtink
   STOP_LEVEL=K61mtink

   if [ -f /etc/init.d/mtink ]
   then
      : # ignore copying
   else
      cp mtink /etc/init.d
   fi
   chmod 744 /etc/init.d/mtink
   for d in 2 3 4 5
   do
    (
    cd $RC_PATH/rc$d.d
    if [ -h ${START_LEVEL} ]
    then
       rm ${START_LEVEL}
    fi
    if [ $RC_PATH = /etc ]
    then
       ln -s ../init.d/mtink ${START_LEVEL}
    else
       ln -s ../mtink ${START_LEVEL}
    fi
    )
   done
   for d in 0 1 6
   do
    (
    cd $RC_PATH/rc$d.d
    if [ -h ${STOP_LEVEL} ]
    then
       rm ${STOP_LEVEL}
    fi
    if [ $RC_PATH = /etc ]
    then
       ln -s ../init.d/mtink ${STOP_LEVEL}
    else
       ln -s ../mtink ${START_LEVEL}
    fi
    )
   done
   
   # detect all EPSON D4 able printers
   modprobe printer >/dev/null 2>&1
   modprobe ppdev >/dev/null 2>&1
   sleep 1;
   for f in /dev/usb/lp* /dev/lp*
   do
      name=`askPrinter $f | grep '[:,]D4[,;]' | sed -n '/Stylus/s/.*:EPSON Stylus \(.*\);/\1/pg' | tr ' ' '_'`
      if [ "$name" != "" ]
      then
 	 if echo $f | grep usb > /dev/null
 	 then
 	    echo -name $name -usbbase /dev/usb/lp
 	 else
 	    echo -name $name -dev $f
 	 fi
      fi
   done > /etc/mtinkd.conf

else
   rm /etc/init.d/mtink*
   rm $RC_PATH/rc?.d/*mtink*
   rm /etc/mtinkd.conf
fi
